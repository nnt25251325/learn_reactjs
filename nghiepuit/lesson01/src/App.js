import React, { Component } from 'react';
import './App.css';
import TaskForm from './components/TaskForm';
import TaskControl from './components/TaskControl';
import TaskList from './components/TaskList';
import {findIndex} from 'lodash';

class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
			tasks: [], // id: unique, name, status
			isDisplayForm: false,
			itemEditing: null,
			filterName: '',
			filterStatus: -1,
			keywork: '',
			sortBy: 'name',
			sortValue: 1
		};
	}

	componentWillMount() {
		if(localStorage && localStorage.getItem('tasks')) {
			var tasks_arr = JSON.parse( localStorage.getItem('tasks') );
			this.setState({
				tasks: tasks_arr
			});
		}
	}

	s4() {
		return Math.floor((1+Math.random()) * 0x10000).toString(16).substring(1);
	}

	genarateID() {
		return this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4();
	}

	onToggleForm = () => { // nhấn vào nút thêm công việc
		if(this.state.isDisplayForm && this.state.itemEditing !== null) { // khi form đang mở và đang có task sửa
			this.setState({
				isDisplayForm: true,
				itemEditing: null
			});
		} else {
			this.setState({
				isDisplayForm: !this.state.isDisplayForm,
				itemEditing: null
			});
		}
	}

	onCloseForm = () => { // nhấn vào nút đóng form
		this.setState({
			isDisplayForm: false
		});
	}

	onShowForm = () => {
		this.setState({
			isDisplayForm: true
		});
	}

	onSubmit = (data) => { // nhấn vào nút Lưu lại trên form, nhận đối tượng data bên TaskForm gửi sang
		var {tasks} = this.state;
		if(data.id === '') { // ko có id => đang tạo mới
			data.id = this.genarateID();
			tasks.push(data);
		} else { // id đã có => đang sửa, nạp dữ liệu từ đối tượng data cho đối tượng task đang sửa (tasks[index])
			var index = findIndex(tasks, (task) => { return task.id === data.id; }); // đang sử dụng findIndex của thư viện lodash
			tasks[index] = data;
		}
		this.setState({
			tasks: tasks,
			itemEditing: null
		});
		localStorage.setItem('tasks', JSON.stringify(tasks));
	}

	onUpdateStatus = (id) => {
		var {tasks} = this.state;
		var index = findIndex(tasks, (task) => { return task.id === id; });
		if(index !== -1) {
			tasks[index].status = !tasks[index].status;
			this.setState({
				tasks: tasks
			});
			localStorage.setItem('tasks', JSON.stringify(tasks));
		}
	}

	onDelete = (id) => {
		var {tasks} = this.state;
		var index = findIndex(tasks, (task) => { return task.id === id; });
		if(index !== -1) {
			tasks.splice(index, 1);
			this.setState({
				tasks: tasks
			});
			localStorage.setItem('tasks', JSON.stringify(tasks));
		}
		this.onCloseForm();
	}

	onUpdate = (id) => { // nhấn vào nút Sửa
		var {tasks} = this.state;
		var index = findIndex(tasks, (task) => { return task.id === id; }); // tìm được task đang sửa
		this.setState({
			itemEditing: tasks[index] //nạp dữ liệu vào itemEditing
		});
		this.onShowForm();
	}

	onFilter = (filterName, filterStatus) => { // Lọc Dữ Liệu Trên Table
		this.setState({
			filterName: filterName.toLowerCase(), // từ tìm kiếm sẽ chuyển hết thành chữ thường
			filterStatus: parseInt(filterStatus) // ép kiểu thành dạng số nguyên
		});
	}

	onSearch = (keywork) => { // Tìm kiếm
		this.setState({
			keywork: keywork.toLowerCase()
		});
	}

	onSort = (sortBy, sortValue) => { // Sắp xếp
		this.setState({
			sortBy: sortBy,
			sortValue: sortValue
		});
	}

	render() {
		var {tasks, isDisplayForm, itemEditing, filterName, filterStatus, keywork, sortBy, sortValue} = this.state; // var tasks = this.state.tasks; var isDisplayForm = this.state.isDisplayForm; ...

		// Lọc Dữ Liệu Trên Table
		if(filterName) { // Lọc theo tên
			tasks = tasks.filter((task) => {
				return task.name.toLowerCase().indexOf(filterName) !== -1
			});
		}

		tasks = tasks.filter((task) => { // Lọc theo trạng thái
			if(filterStatus === -1) { // -1 tương ứng Tất cả
				return tasks;
			} else { // 0 hoặc 1 tương ứng với Ẩn hoặc Kích hoạt
				return task.status === (filterStatus === 1 ? true : false);
			}
		});

		// Tìm kiếm
		if(keywork) {
			tasks = tasks.filter((task) => {
				return task.name.toLowerCase().indexOf(keywork) !== -1
			});
		}

		// Sắp xếp
		if(sortBy === 'name') {
			tasks.sort((a, b) => {
				if(a.name > b.name) return sortValue;
				else if(a.name < b.name) return -sortValue;
				else return 0;
			});
		} else {
			tasks.sort((a, b) => {
				if(a.status > b.status) return -sortValue;
				else if(a.status < b.status) return sortValue;
				else return 0;
			});
		}

		var elmTaskForm = isDisplayForm ? <TaskForm
																				onCloseFormP={this.onCloseForm}
																				onSubmitP={this.onSubmit}
																				itemEditingP={itemEditing}
																			/> : '';
		return (
			<div className="container">
				<div className="text-center">
					<h1>Quản Lý Công Việc</h1><hr />
				</div>
				<div className="row">
					<div className={isDisplayForm ? "col-xs-4" : ""}>
						{elmTaskForm}
					</div>
					<div className={isDisplayForm ? "col-xs-8" : "col-xs-12"}>
						<div className="form-group">
							<button type="button" className="btn btn-primary"
								onClick={this.onToggleForm}
							>
								<i className="fa fa-plus mr-5"></i>Thêm Công Việc
							</button>
						</div>
						<TaskControl
							onSearchP2={this.onSearch}
							onSortP2={this.onSort}
							sortBy={sortBy}
							sortValue={sortValue}
						/>
						<TaskList
							tasks={tasks}
							onUpdateStatusP2={this.onUpdateStatus} // nhấn vào giá trị status trên danh sách
							onDeleteP2={this.onDelete} // nhấn vào nút Xóa
							onUpdateP2={this.onUpdate} // nhấn vào nút Sửa
							onFilterP={this.onFilter}
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default App;
