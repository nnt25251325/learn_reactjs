import React, { Component } from 'react';

class TaskForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			id: '',
			name: '',
			status: false
		}
	}

	componentWillMount() { // Thực hiện một số tác vụ, hàm này chỉ thực hiện 1 lần duy nhất, nếu TaskForm được gọi lên thì thực thi lệnh bên dưới
		if(this.props.itemEditingP) { // đưa dữ liệu của task đang sửa lên form
			this.setState({
				id: this.props.itemEditingP.id,
				name: this.props.itemEditingP.name,
				status: this.props.itemEditingP.status
			});
		}
	}

	componentWillReceiveProps(nextProps) { // Hàm này thực hiện liên tục mỗi khi props thay đổi
		if(nextProps && nextProps.itemEditingP) { // đưa dữ liệu của task đang sửa lên form
			this.setState({
				id: nextProps.itemEditingP.id,
				name: nextProps.itemEditingP.name,
				status: nextProps.itemEditingP.status
			});
		} else if(!nextProps.itemEditingP) {
			this.setState({
				id: '',
				name: '',
				status: false
			});
		}
	}

	onChange = (event) => {
		var target = event.target;
		var name = target.name;
		var value = target.value;
		if(name === 'status') {
			value = target.value === 'true' ? true : false;
		}
		this.setState({
			[name]: value
		});
	}

	onSubmit = (event) => {
		event.preventDefault();
		this.props.onSubmitP(this.state);

		// Cancel & Close Form
		this.onClear();
		this.onCloseForm();
	}

	onCloseForm = () => {
		this.props.onCloseFormP();
	}

	onClear = () => {
		this.setState({
			name: '',
			status: false
		});
	}

	render() {
		var { id } = this.state;
		return (
			<div className="panel panel-warning">
				<div className="panel-heading">
					<h3 className="panel-title">
						{/*nếu form được gọi đến nhờ hàm componentWillMount, kiểm tra id đã tồn tại hay rỗng*/}
						{id !== '' ? 'Cập Nhật Công Việc' : 'Thêm Công Việc'}
						<i className="fa fa-times-circle text-right btn-close" onClick={this.onCloseForm}></i>
					</h3>
				</div>
				<div className="panel-body">
					<form onSubmit={this.onSubmit}>
						<div className="form-group">
							<label>Tên :</label>
							<input type="text" className="form-control" name="name" value={this.state.name} onChange={this.onChange}/>
						</div>
						<div className="form-group">
							<label>Trạng Thái :</label>
							<select className="form-control" name="status" value={this.state.status} onChange={this.onChange}>
								<option value={true}>Kích Hoạt</option>
								<option value={false}>Ẩn</option>
							</select>
						</div>
						<div className="form-group text-center">
							<button type="submit" className="btn btn-warning">
								<i className="fa fa-plus mr-5"></i>Lưu Lại
							</button>&nbsp;
							<button type="button" className="btn btn-danger" onClick={this.onClear}>
								<i className="fa fa-close mr-5"></i>Hủy Bỏ
							</button>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default TaskForm;
