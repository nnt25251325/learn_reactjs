import React, { Component } from 'react';
import TaskSearchControl from './TaskSearchControl';
import TaskSortControl from './TaskSortControl';

class TaskControl extends Component {

	render() {
		return (
			<div className="form-group">
				<div className="row">
					<TaskSearchControl
						onSearchP={this.props.onSearchP2}
					/>
					<TaskSortControl
						onSortP={this.props.onSortP2}
						sortBy={this.props.sortBy}
						sortValue={this.props.sortValue}
					/>
				</div>
			</div>
		);
	}
}

export default TaskControl;
