import React, { Component } from 'react';

class TaskItem extends Component {

	onUpdateStatus = () => {
		this.props.onUpdateStatusP(this.props.task.id);
	}

	onDelete = () => {
		this.props.onDeleteP(this.props.task.id);
	}

	onUpdate = () => {
		this.props.onUpdateP(this.props.task.id);
	}

	render() {
		var {task, index} = this.props;
		return (
			<tr>
				<td>{index+1}</td>
				<td>{task.name}</td>
				<td className="text-center">
					<span
						className={task.status === true ? "label label-status label-danger" : "label label-status label-success"}
						onClick={this.onUpdateStatus}
					>
						{task.status === true ? "Kích hoạt" : "Ẩn"}
					</span>
				</td>
				<td className="text-center">
					<button type="button" className="btn btn-warning" onClick={this.onUpdate}>
						<i className="fa fa-pencil mr-5"></i>Sửa
					</button>&nbsp;
					<button type="button" className="btn btn-danger" onClick={this.onDelete}>
						<i className="fa fa-trash mr-5"></i>Xóa
					</button>
				</td>
			</tr>
		);
	}
}

export default TaskItem;
