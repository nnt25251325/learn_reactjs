import React, { Component } from 'react';
import { Route, Link} from 'react-router-dom';

const menus = [
	{
		name: 'Trang chủ',
		to: '/',
		exact: true
	},
	{
		name: 'Giới thiệu',
		to: '/about',
		exact: false
	},
	{
		name: 'Liên hệ',
		to: '/contact',
		exact: false
	},
	{
		name: 'Sản phẩm',
		to: '/products',
		exact: false
	},
	{
		name: 'Đăng nhập',
		to: '/login',
		exact: false
	}
]

const Menulink = ({ label, to, activeOnlyWhenExact }) => {
	return (
		<Route
			path={to}
			exact={activeOnlyWhenExact}
			children={
				({ match }) => {
					var active = match ? 'active abc' : '';
					return (
						<li className={`my-li ${active}`}> {/*Không thêm nhiều class thì viết: <li className={active}>*/}
							<Link to={to} className="my-link">{label}</Link>
						</li>
					);
				}
			}
		/>
	)
}

class Menu extends Component {
	render() {
		return (
			<nav className="navbar navbar-default">
				<div className="container">
					<ul className="nav navbar-nav">
						{ this.showMenu(menus) }
					</ul>
				</div>
			</nav>
		);
	}

	showMenu = (menus) => {
		var result = null;
		if(menus.length > 0) {
			result = menus.map((menu, index) => {
				return (
							<Menulink
								key={index}
								label={menu.name}
								to={menu.to}
								activeOnlyWhenExact={menu.exact}
							/>
				);
			});
		}
		return result;
	}
}

export default Menu;
