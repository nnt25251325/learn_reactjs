import React, { Component } from 'react';
import { NavLink, Route } from 'react-router-dom';
import Product from './Product';

class Products extends Component {
	render() {
		var products = [
			{
				id: 1,
				slug: 'iphone',
				name: 'Iphone X',
				price: 35000000
			},
			{
				id: 2,
				slug: 'samsung',
				name: 'Samsung Galaxy 7',
				price: 22000000
			},
			{
				id: 3,
				slug: 'oppo',
				name: 'Oppo F1S',
				price: 7000000
			}
		];

		var { match, location } = this.props;
		var url = match.url;

		console.log(location);

		var result = products.map((product, index)=> {
			return 	(
						<li key={index} className="list-group-item">
							<NavLink to={`${url}/${product.slug}`}>
								{ product.id } - { product.name } - { product.price }
							</NavLink>
						</li>
			);
		});
		return (
			<div>
				<div className="container">
					<h1>Danh sách sản phẩm</h1>
					<div className="section">
						<ul className="list-group">
							{ result }
						</ul>
					</div>
					<div className="section">
						<Route path="/products/:name" component={Product}/>
					</div>
				</div>
			</div>
		);
	}
}

export default Products;
