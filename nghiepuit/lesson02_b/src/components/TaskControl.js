import React, { Component } from 'react';
import TaskSearchControl from './TaskSearchControl';
import TaskSortControl from './TaskSortControl';

class TaskControl extends Component {

	render() {
		return (
			<div className="form-group">
				<div className="row">
					<TaskSearchControl />
					<TaskSortControl />
				</div>
			</div>
		);
	}
}

export default TaskControl;
