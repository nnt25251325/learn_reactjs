import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/index';

class TaskForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			id: '',
			name: '',
			status: false
		}
	}

	componentWillMount() { // Thực hiện một số tác vụ, hàm này chỉ thực hiện 1 lần duy nhất, nếu TaskForm được gọi lên thì thực thi lệnh bên dưới
		if(this.props.itemEditing && this.props.itemEditing.id !== null) { // đưa dữ liệu của task đang sửa lên form
			this.setState({
				id: this.props.itemEditing.id,
				name: this.props.itemEditing.name,
				status: this.props.itemEditing.status
			});
		} else {
			this.onClear();
		}
	}

	componentWillReceiveProps(nextProps) { // Hàm này thực hiện liên tục mỗi khi props thay đổi
		if(nextProps && nextProps.itemEditing) { // đưa dữ liệu của task đang sửa lên form
			this.setState({
				id: nextProps.itemEditing.id,
				name: nextProps.itemEditing.name,
				status: nextProps.itemEditing.status
			});
		} else if(!nextProps.itemEditing) {
			this.onClear();
		}
	}

	onHandleChange = (event) => {
		var target = event.target;
		var name = target.name;
		var value = target.value === 'checkbox' ? target.checked : target.value;
		this.setState({
			[name]: value
		});

	}

	onSave = (event) => {
		event.preventDefault();
		this.props.onSaveTask(this.state);
		this.onClear();
		this.onExitForm();
	}

	onClear = () => {
		this.setState({
			name: '',
			status: false
		});
	}

	onExitForm = () => {
		this.props.onCloseForm();
	}

	render() {
		if(!this.props.isDisplayForm) return null;
		return (
			<div className="panel panel-warning">
				<div className="panel-heading">
					<h3 className="panel-title">
						{/*nếu form được gọi đến nhờ hàm componentWillMount, kiểm tra id đã tồn tại hay rỗng*/}
						{!this.state.id ? 'Thêm Công Việc' : 'Cập Nhật Công Việc'}
						<i className="fa fa-times-circle text-right btn-close" onClick={this.onExitForm}></i>
					</h3>
				</div>
				<div className="panel-body">
					<form onSubmit={this.onSave}>
						<div className="form-group">
							<label>Tên :</label>
							<input type="text" className="form-control" name="name" value={this.state.name} onChange={this.onHandleChange}/>
						</div>
						<div className="form-group">
							<label>Trạng Thái :</label>
							<select className="form-control" name="status" value={this.state.status} onChange={this.onHandleChange}>
								<option value={true}>Kích Hoạt</option>
								<option value={false}>Ẩn</option>
							</select>
						</div>
						<div className="form-group text-center">
							<button type="submit" className="btn btn-warning">
								<i className="fa fa-plus mr-5"></i>Lưu Lại
							</button>&nbsp;
							<button type="button" className="btn btn-danger" onClick={this.onClear}>
								<i className="fa fa-close mr-5"></i>Hủy Bỏ
							</button>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => { //vì chỉ có 1 tham số, tương đương cách viết: const mapStateToProps = (state) => {
	return {
		isDisplayForm: state.isDisplayForm,
		itemEditing: state.itemEditing,
		filterTable: state.filterTable
	};
};

//mapDispatchToProps: chuyển dispatch thành props
//->onSaveTask(task): tạo 1 props tên là onSaveTask, chứa tham số đầu vào là task (chính là cái task mới cần thêm)
//	+Khi nhấn submit, đoạn code this.props.onSaveTask(this.state) được gọi (đoạn code đó trong hàm này onSave)
//	+qua hàm dispatch, task được đưa vào hàm action actions.saveTask(task)
//	+hàm action saveTask sẽ truyền tham số task đến reducer tasks trong file reducers/tasks.js
const mapDispatchToProps = (dispatch, props) => {
	return {
		onSaveTask: (task) => {
			dispatch( actions.saveTask(task) );
		},
		onCloseForm: () => {
			dispatch( actions.closeForm() );
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);
