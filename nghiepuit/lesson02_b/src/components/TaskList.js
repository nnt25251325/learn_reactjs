import React, { Component } from 'react';
import TaskItem from './TaskItem';
import { connect } from 'react-redux';
import * as actions from '../actions/index';

class TaskList extends Component {

	constructor(props) {
		super(props);
		this.state = {
			filterName: '',
			filterStatus: -1
		}
	}

	onChange = (event) => {
		var target = event.target;
		var name = target.name;
		var value = target.value === 'checkbox' ? target.checked : target.value;
		var filter = {
			name: name === 'filterName' ? value : this.state.filterName,
			status: name === 'filterStatus' ? value : this.state.filterStatus
		};
		this.props.onFilterTable(filter);
		this.setState({
			[name]: value
		});
	}

	render() {
		var { tasks, filterTable, keywork, sort } = this.props; // tasks = this.props.tasks //props tasks lúc này được lấy ra từ hàm mapStateToProps bên dưới.

		// Lọc Dữ Liệu Trên Table
		if(filterTable.name) { // Lọc theo tên
			tasks = tasks.filter((task) => {
				return task.name.toLowerCase().indexOf(filterTable.name) !== -1
			});
		}

		tasks = tasks.filter((task) => { // Lọc theo trạng thái
			if(filterTable.status === -1) { // -1 tương ứng Tất cả
				return tasks;
			} else { // 0 hoặc 1 tương ứng với Ẩn hoặc Kích hoạt
				return task.status === (filterTable.status === 1 ? true : false);
			}
		});

		// Tìm kiếm
		if(keywork) {
			tasks = tasks.filter((task) => {
				return task.name.toLowerCase().indexOf(keywork) !== -1
			});
		}

		// Sắp xếp
		if(sort.by === 'name') {
			tasks.sort((a, b) => {
				if(a.name > b.name) return sort.value;
				else if(a.name < b.name) return -sort.value;
				else return 0;
			});
		} else {
			tasks.sort((a, b) => {
				if(a.status > b.status) return -sort.value;
				else if(a.status < b.status) return sort.value;
				else return 0;
			});
		}

		var elmTasks = tasks.map((task, index) => {
			return 	<TaskItem
								key={task.id}
								task={task}
								index={index + 1}
							/>
		});
		return (
			<div className="form-group">
				<table className="table table-bordered table-hover">
					<thead>
						<tr>
							<th className="text-center">STT</th>
							<th className="text-center">Tên</th>
							<th className="text-center">Trạng Thái</th>
							<th className="text-center">Hành Động</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td>
								<input
									type="text"
									className="form-control"
									name="filterName"
									onChange={this.onChange}
									value={this.state.filterName}
								/>
							</td>
							<td>
								<select
									className="form-control"
									name="filterStatus"
									onChange={this.onChange}
									value={this.state.filterStatus}
								>
									<option value={-1}>Tất Cả</option>
									<option value={0}>Ẩn</option>
									<option value={1}>Kích Hoạt</option>
								</select>
							</td>
							<td></td>
						</tr>
						{elmTasks}
					</tbody>
				</table>
			</div>
		);
	}
}

const mapStateToProps = (state) => { //hàm này chuyển các state trong store sang props
	return {
		tasks: state.tasks, //tasks trước dấu hai chấm là props, tasks sau dấu hai chấm là state, state tasks này được lấy từ reducers/index.js
		filterTable: state.filterTable,
		keywork: state.search,
		sort: state.sort
	};
};

const mapDispatchToProps = (dispatch, props) => {
	return {
		onFilterTable: (filter) => {
			dispatch( actions.filterTask(filter) );
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
