import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/index';

class TaskSearchControl extends Component {

	constructor(props) {
		super(props);
		this.state = {
			keyword: ''
		}
	}

	onHandleChange = (event) => {
		var target = event.target;
		var name = target.name;
		var value = target.value;
		this.setState({
			[name]: value
		});
	}

	onSearch = () => {
		this.props.onSearch(this.state.keyword);
	}

	render() {
		return (
			<div className="col-xs-6">
				<div className="input-group">
					<input
						type="text"
						name="keyword"
						className="form-control"
						placeholder="Nhập từ khóa..."
						value={this.state.keyword}
						onChange={this.onHandleChange}
					/>
					<span className="input-group-btn">
						<button className="btn btn-primary" type="button" onClick={this.onSearch}><i className="fa fa-search mr-5"></i>Tìm</button>
					</span>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = (dispatch, props) => {
	return {
		onSearch: (keyword) => {
			dispatch( actions.searchTask(keyword) );
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskSearchControl);
