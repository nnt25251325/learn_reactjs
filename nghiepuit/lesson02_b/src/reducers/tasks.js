import * as types from  './../constants/ActionTypes';

var s4 = () => {
	return Math.floor((1+Math.random()) * 0x10000).toString(16).substring(1);
}

var randomID = () => {
	return s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4();
}

var findIndex = (tasks, id) => {
	var result = -1;
	tasks.forEach((task, index) => {
		if(task.id === id) {
			result = index;
		}
	});
	return result;
}

var data = JSON.parse(localStorage.getItem('tasks'));
var initialState = data ? data : [];

var myReducers = (state = initialState, action) => {
	var id = '',
			index = -1;
	switch(action.type) {
		//Hiển thị danh sách công việc
		case types.LIST_ALL:
			return state;

		//Thêm và sửa công việc
		case types.SAVE_TASK:
			var task = {
				id: action.task.id,
				name: action.task.name,
				status: (action.task.status === true || action.task.status === 'true') ? true : false
			};
			if(!task.id) { //Chưa tồn tại id là thêm
				task.id = randomID();
				state.push(task);
			} else { //Tồn tại id rồi là Sửa
				index = findIndex(state, task.id);
				state[index] = task;
			}
			localStorage.setItem('tasks', JSON.stringify(state));
			return [...state];

		//Cập nhật trạng thái công việc
		case types.UPDATE_STATUS_TASK:
			id = action.id;
			index = findIndex(state, id);
			state[index] = {
				...state[index],
				status: !state[index].status
			};//Đây là cú pháp mới. Cách viết cũ như sau: // var cloneTask = {...state[index]}; cloneTask.status = !cloneTask.status; state[index] = cloneTask;
			localStorage.setItem('tasks', JSON.stringify(state));
			return [...state];

		//Xóa công việc
		case types.DELETE_TASK:
			id = action.id;
			index = findIndex(state, id);
			state.splice(index, 1);
			localStorage.setItem('tasks', JSON.stringify(state));
			return [...state];

		default: return state;
	}
};

export default myReducers;
