export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT'; //Lưu sản phẩm đang sửa lại khi nhấn Lưu lại trên form
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const EDIT_PRODUCT = 'EDIT_PRODUCT'; //Lấy sản phẩm đang sửa rồi đưa vào form
