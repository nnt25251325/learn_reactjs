import React, { Component } from 'react';

class NotFoundPage extends Component {
	render() {
		return (
			<div className="container">
				<div className="alert alert-warning">
					<button type="button" className="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h1>404 - Không tìm thấy trang</h1>
				</div>
			</div>
		);
	}
}

export default NotFoundPage;
