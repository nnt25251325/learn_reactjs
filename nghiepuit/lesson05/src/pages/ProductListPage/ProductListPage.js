import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ProductList from '../../components/ProductList/ProductList';
import ProductItem from '../../components/ProductItem/ProductItem';
import { actFetchProductsRequest, actDeleteProductRequest } from '../../actions/index';

class ProductListPage extends Component {
	//method này được thực thi khi 1 component được render trên client side
	//sau khi ProductListPage render thì componentDidMount được gọi
	componentDidMount() {
		this.props.fetchAllProducts();
	}

	onDelete = (id) => {
		this.props.onDeleteProduct(id);
	}

	render() {
		var { products } = this.props;
		// console.log(products);
		return (
			<div className="section">
				<div className="container">
					<Link to='/product/add' className="btn btn-info mb-10">Thêm sản phẩm</Link>
					<ProductList>
						{ this.showProducts(products) }
					</ProductList>
				</div>
			</div>
		);
	}

	showProducts = (products) => {
		var result = null;
		if(products.length > 0) {
			result = products.map((product, index) => {
				return (
					<ProductItem
						key={index}
						product={product}
						index={index} //từng dòng có thứ tự nên phải truyền thêm index
						onDelete={this.onDelete}
					/>
				);
			});
		}
		return result;
	}
}

ProductListPage.propTypes = {
	products: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.number.isRequired,
			name: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired,
			status: PropTypes.bool.isRequired
		})
	).isRequired,
	fetchAllProducts: PropTypes.func.isRequired,
	onDeleteProduct: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
	return {
		products: state.products
	};
};

const mapDispatchToProps = (dispatch, props) => {
	return {
		fetchAllProducts: () => {
			dispatch(actFetchProductsRequest());
		},
		onDeleteProduct: (id) => {
			dispatch(actDeleteProductRequest(id));
		}
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductListPage);
