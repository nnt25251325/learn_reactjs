import * as Types from '../constants/ActionTypes';

var initialState = [];

var findIndex = (products, id) => {
	var result = -1;
	products.forEach((product, index) => {
		if(product.id === id) {
			result = index;
		}
	});
	return result;
}

const products = (state = initialState, action) => {
	var index = -1;
	var { id, product } = action;
	switch(action.type) {
		//Hiện thị sản phẩm
		case Types.FETCH_PRODUCTS:
			state = action.products;
			return [...state];

		//Xoá sản phẩm
		case Types.DELETE_PRODUCT:
			index = findIndex(state, id);
			state.splice(index, 1);
			return [...state];

		//Thêm sản phẩm
		case Types.ADD_PRODUCT:
			state.push(product);
			return [...state];

		//Cập nhật sản phẩm
		case Types.UPDATE_PRODUCT:
			index = findIndex(state, product.id);
			state[index] = product;
			return [...state];

		default:
			return [...state];
	}
};

export default products;
