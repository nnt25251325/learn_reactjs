import * as Types from '../constants/ActionTypes';
import callApi from '../utils/apiCaller';

export const actFetchProductsRequest = () => {
	return (dispatch) => {
		return callApi('products_lesson05', 'GET', null).then((res) => {
			dispatch(actFetchProducts(res.data));
		});
	};
}

export const actFetchProducts = (products) => {
	return {
		type: Types.FETCH_PRODUCTS,
		products
	}
}

export const actDeleteProductRequest = (id) => {
	return (dispatch) => {
		return callApi(`products_lesson05/${id}`, 'DELETE', null).then((res) => {
			dispatch(actDeleteProduct(id));
		});
	};
}

export const actDeleteProduct = (id) => {
	return {
		type: Types.DELETE_PRODUCT,
		id
	}
}

export const actAddProductRequest = (product) => {
	return (dispatch) => {
		return callApi('products_lesson05', 'POST', product).then((res) => {
			dispatch(actAddProduct(res.data));
		});
	};
}

export const actAddProduct = (product) => {
	return {
		type: Types.ADD_PRODUCT,
		product
	}
}

//Khi nhấn vào nút Sửa, sẽ đi đến trang sửa, là trang ProductActionPage.
//Dữ liệu sẽ được nạp vào trang này nhờ 2 hàm: actGetProductRequest và actGetProduct
//Nạp như sau:
//	+Gọi lên server dựa vào id lấy trên url (match.params.id)
//	+Sau đó server trả về response, res.data chính là product có id tương ứng
//	+Sau đó dispatch action actGetProduct để lưu vào store
export const actGetProductRequest = (id) => {
	return (dispatch) => {
		return callApi(`products_lesson05/${id}`, 'GET', null).then((res) => {
			dispatch(actGetProduct(res.data));
		});
	};
}

//Hàm này lấy product trên store (ko phải lấy trong server)
export const actGetProduct = (product) => {
	return {
		type: Types.EDIT_PRODUCT,
		product
	}
}

//Khi nhấn nút Lưu lại ở trang ProductActionPage
//Hai hàm actUpdateProductRequest và actUpdateProduct sẽ gửi giá trị đã sửa trong form lên server
//Đồng thời sẽ đưa giá trị đó vào lại store
export const actUpdateProductRequest = (product) => {
	return (dispatch) => {
		return callApi(`products_lesson05/${product.id}`, 'PUT', product).then((res) => {
			dispatch(actUpdateProduct(res.data));
		});
	};
}

export const actUpdateProduct = (product) => {
	return {
		type: Types.UPDATE_PRODUCT,
		product
	}
}
