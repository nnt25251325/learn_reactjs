import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { actAddProductRequest, actGetProductRequest, actUpdateProductRequest } from '../../actions/index';

class ProductActionPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id: '',
			txtName: '',
			txtPrice: '',
			chkbStatus: false
		};
	}

	//method này được thực thi khi 1 component được render trên client side
	//sau khi ProductActionPage render thì componentDidMount được gọi
	componentDidMount() {
		var { match } = this.props;
		if(match) {
			var id = match.params.id;
			this.props.onEditProduct(id);
		}
	}

	//method này được thực thi ngay khi thuộc tính props được update và trước khi component được render lại.
	componentWillReceiveProps(nextProps) {
		if(nextProps && nextProps.itemEditing) {
			var { itemEditing } = nextProps;
			this.setState({
				id: itemEditing.id,
				txtName: itemEditing.name,
				txtPrice: itemEditing.price,
				chkbStatus: itemEditing.status
			});
		}
	}

	onChange = (e) => {
		var target = e.target;
		var name = target.name;
		var value = target.type === 'checkbox' ? target.checked : target.value;
		this.setState({
			[name]: value
		});
	}

	onSave = (e) => {
		e.preventDefault();
		var { id, txtName, txtPrice, chkbStatus } = this.state;
		var { history } = this.props;
		var product = {
			id: id,
			name: txtName,
			price: parseInt(txtPrice),
			status: chkbStatus
		};
		//console.log(product);
		if(id) { //update
			this.props.onUpdateProduct(product);
		} else { //add
			this.props.onAddProduct(product);
		}
		history.goBack(); //Hoặc có thể dùng: history.push('/product-list');
	}

	render() {
		var { txtName, txtPrice, chkbStatus } = this.state;
		// console.log(this.props.itemEditing);
		return (
			<div className="section mb-10">
				<div className="container">
					<div className="row">
						<div className="col-xs-6 col-xs-push-3">
							<form onSubmit={this.onSave}>
								<div className="form-group">
									<label>Tên sản phẩm</label>
									<input
										type="text"
										className="form-control"
										name="txtName"
										value={txtName}
										onChange={this.onChange}
									/>
								</div>
								<div className="form-group">
									<label>Giá sản phẩm</label>
									<input
										type="number"
										className="form-control"
										name="txtPrice"
										value={txtPrice}
										onChange={this.onChange}
									/>
								</div>
								<div className="checkbox">
									<label>
										<input
											type="checkbox"
											name="chkbStatus"
											value={chkbStatus}
											checked={chkbStatus}
											onChange={this.onChange}
										/>
										Còn hàng
									</label>
								</div>
								<button type="submit" className="btn btn-primary">Lưu lại</button>&nbsp;
								<Link to='/product-list' className="btn btn-default">Trở lại</Link>
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

ProductActionPage.propTypes = {
	// itemEditing: PropTypes.shape({
	// 	id: PropTypes.number.isRequired,
	// 	name: PropTypes.string.isRequired,
	// 	price: PropTypes.number.isRequired,
	// 	status: PropTypes.bool.isRequired
	// }).isRequired,
	onAddProduct: PropTypes.func.isRequired,
	onEditProduct: PropTypes.func.isRequired,
	onUpdateProduct: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
	return {
		itemEditing: state.itemEditing
	};
};

const mapDispatchToProps= (dispatch, props) => {
	return {
		onAddProduct: (product) => {
			dispatch(actAddProductRequest(product));
		},
		onEditProduct: (id) => {
			dispatch(actGetProductRequest(id));
		},
		onUpdateProduct: (product) => {
			dispatch(actUpdateProductRequest(product));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductActionPage);
