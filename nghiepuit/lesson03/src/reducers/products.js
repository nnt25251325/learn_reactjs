var initialState = [
	{
		id: 1,
		name: 'Iphone 7 Plus',
		image: 'img/img_iphone_6s_plus.jpg',
		description: 'Sản phẩm do Apple sản xuất',
		price: 500,
		inventory: 10,
		rating: 4
	},
	{
		id: 2,
		name: 'Samsung Galaxy S6 Edge',
		image: 'img/img_galaxy_s6_edge.jpg',
		description: 'Sản phẩm do Samsung sản xuất',
		price: 400,
		inventory: 15,
		rating: 3
	},
	{
		id: 3,
		name: 'Oppo F1S',
		image: 'img/img_oppo_f1s.jpg',
		description: 'Sản phẩm do Oppo sản xuất',
		price: 450,
		inventory: 5,
		rating: 5
	}
];

var pruducts = (state = initialState, action) => {
	switch(action.type) {
		default: return [...state];
	}
};

export default pruducts;
