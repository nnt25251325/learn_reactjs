import React from 'react';
import { useParams } from 'react-router-dom';

export default () => {
  const urlParams = useParams();

  console.log('id', urlParams.id);

  return (
    <>
      <div className="p-user-detail">
        <h1>User Detail</h1>
        <p>id = {urlParams.id}</p>
      </div>
    </>
  );
};
