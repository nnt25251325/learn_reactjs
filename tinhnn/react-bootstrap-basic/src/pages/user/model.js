import { action, thunk } from 'easy-peasy';

import { ENDPOINTS } from '../../configs';
import { Http } from '../../common';

export const model = {
  data: [],
  total: 0,
  loadingList: false,

  setUserList: action((state, payload) => {
    if (state?.data === undefined || state?.total === undefined) return;

    if (!(Array.isArray(payload?.data) && payload.data.length)) {
      state.data = [];
      state.total = 0;
      return;
    }

    state.data = payload.data;
    state.total = payload.total;
  }),

  setLoadingList: action((state, payload) => {
    if (state?.loadingList === undefined) return;

    state.loadingList = payload;
  }),

  getUserList: thunk(async (action, payload) => {
    try {
      action.setLoadingList(true);

      const url = ENDPOINTS.USER;
      const res = await Http.get(url).then(res => res.data);
      // const res = {
      //   status: 200,
      //   message: 'Success',
      //   data: {
      //     total: 100,
      //     data: [
      //       {
      //         id: '1834',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '3450',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '1451',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '5152',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '3940',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '1513',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '1234',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '3340',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '1631',
      //         name: 'Lorem ipsum dolor sit amet'
      //       },
      //       {
      //         id: '1515',
      //         name: 'Lorem ipsum dolor sit amet'
      //       }
      //     ]
      //   }
      // };

      if (!res?.data) {
        action.setUserList([]);
        throw res;
      }

      action.setUserList(res.data);
    } catch (err) {
      console.error(err);
    } finally {
      action.setLoadingList(false);
    }
  })
};
