import React, { useEffect } from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';

export default () => {
  const getUserList = useStoreActions(action => action.user.getUserList);
  const setUserList = useStoreActions(action => action.user.setUserList);
  const data = useStoreState(state => state.user.data);
  // const total = useStoreState(state => state.user.total);
  // const loadingList = useStoreState(state => state.user.loadingList);

  useEffect(() => {
    getUserList();
  }, [getUserList]);

  useEffect(() => {
    console.log('User', data);
  }, [data]);

  /**
   * Unmounted
   */
  useEffect(() => {
    return () => {
      setUserList([]);
    };
  }, [setUserList]);

  return (
    <>
      <div className="p-user-list">User</div>
    </>
  );
};
