import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';

import PageList from './page-list';
import PageDetail from './page-detail';

import './style.scss';

export default () => {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={`${match.path}/:id`} component={PageDetail} />
      <Route path="/user*" component={PageList} />
    </Switch>
  );
};
