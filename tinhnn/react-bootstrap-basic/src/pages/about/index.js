import React from 'react';
import { Route, Switch } from 'react-router';

import PageAbout from './page-about';

import './style.scss';

export default () => {
  return (
    <Switch>
      <Route path="/about*" component={PageAbout} />
    </Switch>
  );
};
