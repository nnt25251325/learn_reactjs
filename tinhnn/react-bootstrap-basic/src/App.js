import React, { Suspense } from 'react';
import { StoreProvider } from 'easy-peasy';

import './assets/scss/style.scss';

import store from './common/store';
import Layout from './layout';
import Pages from './pages';
import { RouterLoading } from './components';

function App() {
  return (
    <div className="App">
      <StoreProvider store={store}>
        <Suspense fallback={<RouterLoading />}>
          <Layout>
            <Pages />
          </Layout>
        </Suspense>
      </StoreProvider>
    </div>
  );
}

export default App;
