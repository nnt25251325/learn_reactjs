import React, { Suspense } from 'react';

import { RouterLoading, MainHeader, MainFooter } from '../components';

import store from '../common/store';
import { model as modelUser } from '../pages/user/model';

store.addModel('user', modelUser);

export default ({ children }) => {
  return (
    <Suspense fallback={<RouterLoading />}>
      <div className="l-default">
        <MainHeader />

        <div className="main-body">{children}</div>

        <MainFooter />
      </div>
    </Suspense>
  );
};
