import React, { Suspense } from 'react';
import { StoreProvider } from 'easy-peasy';

import 'antd/dist/antd.css';
import './assets/scss/styles.scss';

import store from './core/create-store';
import { RouterLoading } from './components/RouterLoading';
import Layout from './layouts';
import Pages from './pages';

const App = () => {
  return (
    <div className="App">
      <StoreProvider store={store}>
        <Suspense fallback={<RouterLoading />}>
          <Layout>
            <Pages />
          </Layout>
        </Suspense>
      </StoreProvider>
    </div>
  );
};

export default App;
