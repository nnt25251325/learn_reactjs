import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Breadcrumb } from 'antd';

import { BREADCRUMB_ROUTES_CONFIG } from '../../constants';
import { buildRoutesForBreadcrumb } from '../../helpers';

import './style.scss';

export const BasicBreadcrumb = ({
  className = '',
  addedRoutes = [],
  ...rest
}) => {
  const location = useLocation();

  /**
   * Item render for breadcrumb
   */
  const itemRenderForBreadcrumb = (route, params, routes, paths) => {
    const last = routes.indexOf(route) === routes.length - 1;
    const RouteText = () => (
      <>
        {route.path === '/' ? route.icon : ''} {route.breadcrumbName}
      </>
    );
    return route.noLink || last ? (
      <RouteText />
    ) : (
      <Link to={route.path || '/'}>
        <RouteText />
      </Link>
    );
  };

  return (
    <Breadcrumb
      itemRender={itemRenderForBreadcrumb}
      routes={[
        ...buildRoutesForBreadcrumb(
          location.pathname,
          BREADCRUMB_ROUTES_CONFIG
        ),
        ...addedRoutes
      ]}
      className={`c-basic-breadcrumb ${className}`}
      {...rest}
    />
  );
};
