import React from 'react';
import { Row, Col } from 'antd';

import './style.scss';

export const MainFooter = () => {
  return (
    <>
      <footer className="c-main-footer">
        <Row gutter={30}>
          <Col xs={12}>&copy; Copyright 2021</Col>

          <Col xs={12} className="text-right">
            Powered by Pro
          </Col>
        </Row>
      </footer>
    </>
  );
};
