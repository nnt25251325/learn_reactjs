/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu, Dropdown } from 'antd';
import { CaretDownOutlined } from '@ant-design/icons';

import { UserCircle } from '../../assets/svg-icons';

import './style.scss';

const { Header } = Layout;

export const MainHeader = () => {
  /**
   * Account menu
   */
  const AccountMenu = () => {
    return (
      <Menu>
        <Menu.Item>
          <Link to="/profile">Manage your account</Link>
        </Menu.Item>

        <Menu.Divider />

        <Menu.Item>
          <a
            onClick={() => {
              window.location.href = '/login';
            }}
          >
            Sign in
          </a>
        </Menu.Item>
      </Menu>
    );
  };

  return (
    <>
      <Header className="c-main-header">
        <div
          className="logo text-center"
          style={{
            minWidth: '190px'
          }}
        >
          <Link
            to="/"
            style={{
              minWidth: '220px',
              fontSize: '30px',
              fontWeight: 'bold',
              color: '#fff'
            }}
          >
            LOGO
          </Link>
        </div>

        <div className="box-right">
          <Dropdown
            overlay={AccountMenu}
            trigger={['click']}
            placement="bottomCenter"
          >
            <div className="account-menu">
              <div className="ant-dropdown-link cursor-pointer">
                <UserCircle className="ic-account" />
                <span>Hello, Guest!</span>
                <CaretDownOutlined className="ic-arrow" />
              </div>
            </div>
          </Dropdown>
        </div>
      </Header>
    </>
  );
};
