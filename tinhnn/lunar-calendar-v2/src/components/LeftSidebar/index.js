import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Layout, Menu } from 'antd';

import { BREADCRUMB_ROUTES_CONFIG } from '../../constants';
import {
  convertPermissionsToMenus,
  buildPathNameList
} from '../../helpers/utils';

const { SubMenu } = Menu;
const { Sider } = Layout;

export const LeftSidebar = () => {
  const location = useLocation();
  const [menus, setMenus] = useState([]);
  const [collapsed, setCollapsed] = useState(false);

  /**
   * Set collapsed
   */
  useEffect(() => {
    const permissions = [{ moduleId: 1 }, { moduleId: 2 }];
    const newMenus = convertPermissionsToMenus(
      permissions,
      BREADCRUMB_ROUTES_CONFIG
    );

    setMenus(newMenus);
  }, []);

  /**
   * Set collapsed
   */
  useEffect(() => {
    const viewportWidth = Math.max(
      document.documentElement.clientWidth,
      window.innerWidth || 0
    );

    setCollapsed(viewportWidth < 992);
  }, []);

  return (
    <Sider
      collapsible
      collapsed={collapsed}
      width={220}
      className="c-left-sidebar"
      onCollapse={() => setCollapsed(!collapsed)}
    >
      <Menu
        theme="dark"
        mode="inline"
        selectedKeys={buildPathNameList(location.pathname)}
        defaultOpenKeys={buildPathNameList(location.pathname)}
      >
        {Array.isArray(menus) &&
          menus.length > 0 &&
          menus.map(item => {
            if (Array.isArray(item.child) && item.child.length) {
              return (
                <SubMenu
                  key={item.path}
                  icon={item.icon}
                  title={item.breadcrumbName}
                >
                  {item.child.map(sub => (
                    <Menu.Item key={sub.path} icon={sub.icon}>
                      <Link to={sub.path}>{sub.breadcrumbName}</Link>
                    </Menu.Item>
                  ))}
                </SubMenu>
              );
            }

            return (
              <Menu.Item key={item.path} icon={item.icon}>
                <Link to={item.path}>{item.breadcrumbName}</Link>
              </Menu.Item>
            );
          })}
      </Menu>
    </Sider>
  );
};
