import React, { Suspense } from 'react';
import { Layout } from 'antd';

import {
  RouterLoading,
  LeftSidebar,
  MainHeader,
  MainFooter
} from '../components';

const { Content } = Layout;

export default ({ children }) => {
  return (
    <Suspense fallback={<RouterLoading />}>
      <Layout className="l-default">
        <MainHeader />

        <Layout className="main-body">
          <LeftSidebar />

          <Layout>
            <Content className="main-content">{children}</Content>
            <MainFooter />
          </Layout>
        </Layout>
      </Layout>
    </Suspense>
  );
};
