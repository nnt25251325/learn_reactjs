import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Card, Input, Form } from 'antd';
import {
  MailOutlined,
  SendOutlined,
  ArrowLeftOutlined
} from '@ant-design/icons';

import store from '../../core/create-store';
import { model } from './model';

store.addModel('forgotpassword', model);

export default () => {
  const [form] = Form.useForm();
  let history = useHistory();

  /**
   * On login
   */
  const onSend = values => {
    console.log('values', values);
  };

  return (
    <Card className="card-login max-w-500">
      <Form
        form={form}
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        className="pt-2 pb-2"
        onFinish={onSend}
      >
        <div className="form-wrapper">
          <div className="mb-3">
            Forgot your account’s password or having trouble logging into your
            Team? Enter your email address and we’ll send you a recovery link.
          </div>

          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please fill in this field'
              },
              {
                type: 'email',
                message: 'This field is invalid format'
              }
            ]}
          >
            <Input
              prefix={<MailOutlined />}
              placeholder="Enter Email"
              className="only-bd-bottom"
            />
          </Form.Item>

          <Form.Item className="text-center mb-0">
            <Button
              shape="round"
              size="large"
              icon={<ArrowLeftOutlined />}
              className="min-w-120 mx-1"
              onClick={() => history.push('/login')}
            >
              Back
            </Button>

            <Button
              type="primary"
              htmlType="submit"
              shape="round"
              size="large"
              icon={<SendOutlined />}
              className="min-w-120 mx-1"
            >
              Send
            </Button>
          </Form.Item>
        </div>
      </Form>
    </Card>
  );
};
