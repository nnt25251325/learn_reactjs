import { notification } from 'antd';
import { action, thunk } from 'easy-peasy';

import { ENDPOINTS } from '../../constants';
import { Http } from '../../core/http';
import { buildQuery, buildQueryForSaveToStore } from '../../helpers';

export const model = {
  /**
   * State
   */
  data: [],
  total: 0,
  editingUser: {},
  query: {},
  loadingList: false,
  loadingItem: false,

  /**
   * Action: Set user list
   */
  setUserList: action((state, payload) => {
    if (!(state && state.data !== undefined && state.total !== undefined)) {
      return;
    }

    if (!(payload && Array.isArray(payload.rows) && payload.rows.length)) {
      state.data = [];
      state.total = 0;

      return;
    }

    state.data = payload.rows;
    state.total = payload.count;
  }),

  /**
   * Action: Set editing user
   */
  setEditingUser: action((state, payload) => {
    if (!(state && state.editingUser !== undefined)) return;

    state.editingUser = payload;
  }),

  /**
   * Action: Set query
   * Used to save state and combine different types of queries: offset, limit, where, order, ...
   */
  setQuery: action((state, payload) => {
    if (!(state && state.query !== undefined)) return;

    state.query = buildQueryForSaveToStore(payload);
  }),

  /**
   * Action: Set loading list
   */
  setLoadingList: action((state, payload) => {
    if (!(state && state.loadingList !== undefined)) return;

    state.loadingList = payload;
  }),

  /**
   * Action: Set loading get/update item
   */
  setLoadingItem: action((state, payload) => {
    if (!(state && state.loadingItem !== undefined)) return;

    state.loadingItem = payload;
  }),

  /**
   * Action: Call api to get user list
   */
  getUserList: thunk(async (action, payload) => {
    const clonePayload = { ...payload };

    try {
      action.setLoadingList(true);
      action.setQuery(clonePayload);

      // const res = await Http.get(
      //   `${ENDPOINTS.USER}?q=${buildQuery(payload)}`
      // ).then(res => res.data);

      const res = JSON.parse(
        `{"data":{"count":120,"rows":[{"id":"1","name":"Test 1"},{"id":"2","name":"Name 2"},{"id":"3","name":"Name 3"},{"id":"4","name":"Name 4"},{"id":"5","name":"Name 5"},{"id":"6","name":"Name 6"},{"id":"7","name":"Name 7"},{"id":"8","name":"Name 8"},{"id":"9","name":"Name 9"},{"id":"10","name":"Name 10"}]}}`
      );

      if (!(res && res.data)) {
        action.setUserList(null);
        throw res;
      }

      action.setUserList(res.data);
    } catch (err) {
      console.error(err);

      if (clonePayload.actionType !== 'SEARCH') {
        notification.error({
          message: 'Get user list',
          description: 'Failed to get!'
        });
      }
    } finally {
      action.setLoadingList(false);
    }
  }),

  /**
   * Action: Call api to get user by Id
   */
  getUserById: thunk(async (action, payload) => {
    try {
      action.setLoadingItem(true);

      const res = await Http.get(`${ENDPOINTS.USER}/${payload}`).then(
        res => res.data
      );

      if (!res) throw res;

      action.setEditingUser(res.data);
    } catch (err) {
      console.error(err);

      notification.error({
        message: 'Get user item',
        description: 'Failed to get!'
      });
    } finally {
      action.setLoadingItem(false);
    }
  }),

  /**
   * Action: Call api to create user
   */
  createUser: thunk(async (action, payload, helpers) => {
    try {
      action.setLoadingItem(true);

      const res = await Http.post(`${ENDPOINTS.USER}`, payload).then(
        res => res.data
      );

      if (!res) throw res;

      await action.getUserList(helpers.getState()['query']);

      notification.success({
        message: 'Creating user',
        description: 'Created successfully!'
      });

      return res;
    } catch (err) {
      console.error(err);

      notification.error({
        message: 'Creating user',
        description: 'Failed to create!'
      });
    } finally {
      action.setLoadingItem(false);
    }
  }),

  /**
   * Action: Call api to update user
   */
  updateUser: thunk(async (action, payload, helpers) => {
    const id = payload.id ? payload.id : '';

    try {
      action.setLoadingItem(true);

      const res = await Http.put(`${ENDPOINTS.USER}/${id}`, payload).then(
        res => res.data
      );

      if (!res) throw res;

      notification.success({
        message: 'Updating user',
        description: 'Updated successfully!'
      });

      action.getUserList(helpers.getState()['query']);
    } catch (err) {
      console.error(err);

      notification.error({
        message: 'Updating user',
        description: 'Failed to update!'
      });
    } finally {
      action.setLoadingItem(false);
    }
  }),

  /**
   * Action: Call api to delete user
   */
  deleteUser: thunk(async (action, payload, helpers) => {
    try {
      const res = await Http.delete(`${ENDPOINTS.USER}/${payload}`).then(
        res => res.data
      );

      if (!res) throw res;

      notification.success({
        message: 'Deleting user',
        description: 'Deleted successfully!'
      });

      action.getUserList(helpers.getState()['query']);
    } catch (err) {
      console.error(err);

      notification.error({
        message: 'Deleting user',
        description: 'Failed to delete!'
      });
    }
  })
};
