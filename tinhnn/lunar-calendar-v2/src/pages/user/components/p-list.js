import React, { useEffect, useState } from 'react';
import { Card, Button, Modal, Tooltip } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useStoreState, useStoreActions } from 'easy-peasy';

import { BasicBreadcrumb, BasicTable } from '../../../components';
import { UserSearchForm } from './user-search-form';
import { UserModal } from './user-modal';

export default () => {
  // For user action
  const getUserList = useStoreActions(action => action.user.getUserList);
  const createUser = useStoreActions(action => action.user.createUser);
  const updateUser = useStoreActions(action => action.user.updateUser);
  const deleteUser = useStoreActions(action => action.user.deleteUser);
  const data = useStoreState(state => state.user.data);
  const total = useStoreState(state => state.user.total);
  const loadingList = useStoreState(state => state.user.loadingList);
  const loadingItem = useStoreState(state => state.user.loadingItem);

  // For query condition
  const setQuery = useStoreActions(action => action.user.setQuery);
  const query = useStoreState(state => state.user.query);

  // Other state
  const [sortedInfo, setSortedInfo] = useState({});
  const [currentUserId, setCurrentUserId] = useState('');
  const [visibleUserModal, setVisibleUserModal] = useState(false);

  /**
   * Columns for table
   */
  const columns = [
    {
      dataIndex: 'id',
      title: 'ID',
      width: 80,
      sorter: () => {},
      sortOrder: sortedInfo.field === 'id' && sortedInfo.order
    },
    {
      dataIndex: 'name',
      title: 'Name',
      width: '25%',
      sorter: () => {},
      sortOrder: sortedInfo.field === 'name' && sortedInfo.order,
      render: (text, item) => (
        <div
          className="text-primary cursor-pointer over"
          onClick={() => onShowDetail(item.id)}
        >
          {text}
        </div>
      )
    },
    {
      dataIndex: 'action',
      title: 'Action',
      width: 100,
      fixed: 'right',
      render: (text, item) => (
        <div className="text-nowrap text-center">
          <Tooltip placement="top" title="Edit" destroyTooltipOnHide={true}>
            <Button
              type="link"
              icon={<EditOutlined />}
              size="small"
              className="border-transparent mr-2"
              onClick={() => onShowDetail(item.id)}
            ></Button>
          </Tooltip>

          <Tooltip placement="top" title="Delete" destroyTooltipOnHide={true}>
            <Button
              type="link"
              danger
              icon={<DeleteOutlined />}
              size="small"
              onClick={() =>
                Modal.confirm({
                  title: 'Warning',
                  content: (
                    <>
                      Are you sure you want to delete{' '}
                      <strong>{item.name}</strong> ?
                    </>
                  ),
                  autoFocusButton: null,
                  maskClosable: true,
                  okText: 'Delete',
                  okButtonProps: {
                    danger: true,
                    type: 'primary',
                    icon: <DeleteOutlined />
                  },
                  cancelText: 'Cancel',
                  onOk: () => deleteUser(item.id)
                })
              }
            ></Button>
          </Tooltip>
        </div>
      )
    }
  ];

  /**
   * Get user list
   */
  useEffect(getUserList, [getUserList]);

  /**
   * On show detail
   */
  const onShowDetail = userId => {
    setCurrentUserId(userId);
    setVisibleUserModal(true);
  };

  /**
   * Close user modal
   */
  const closeUserModal = () => {
    setCurrentUserId('');
    setVisibleUserModal(false);
  };

  /**
   * On create/update user
   */
  const onSaveUser = async formData => {
    if (loadingItem || loadingList || !formData) {
      return;
    }

    if (formData.id) {
      updateUser(formData);
    } else {
      const res = await createUser(formData);

      if (res && res.data && res.data.id) {
        closeUserModal();
      }
    }
  };

  /**
   * Unmount
   */
  useEffect(() => {
    return () => {
      setQuery({});
    };
  }, [setQuery]);

  return (
    <>
      <div className="p-user-list">
        <BasicBreadcrumb />

        <h3 className="ant-typography mb-4">User</h3>

        <Card>
          <UserSearchForm
            loadingList={loadingList}
            onShowDetail={() => onShowDetail('')}
            onSearch={params => getUserList({ ...query, ...params })}
            onReload={params => {
              getUserList({ ...query, ...params });
              setSortedInfo({});
            }}
          />

          <BasicTable
            columns={columns}
            dataSource={data}
            total={total}
            currentPage={query.page}
            currentLimit={query.limit}
            loading={loadingList}
            className="main-table"
            onSortChange={setSortedInfo}
            onTableChange={params => getUserList({ ...query, ...params })}
          />
        </Card>
      </div>

      {visibleUserModal && (
        <UserModal
          visible={visibleUserModal}
          id={currentUserId}
          loadingList={loadingList}
          onSaveUser={onSaveUser}
          onCancel={closeUserModal}
        />
      )}
    </>
  );
};
