import React, { useEffect } from 'react';
import {
  Form,
  Input,
  DatePicker,
  Button,
  Modal,
  Spin,
  Tooltip,
  Select
} from 'antd';
import {
  Loading3QuartersOutlined,
  EditOutlined,
  SaveOutlined,
  ReloadOutlined
} from '@ant-design/icons';
import { useStoreState, useStoreActions } from 'easy-peasy';
import moment from 'moment';

import { SHORT_DATE_FORMAT } from '../../../constants';
import { PlusCircle } from '../../../assets/svg-icons';

const { RangePicker } = DatePicker;

// Fake
const userTypes = [
  {
    id: 1,
    label: 'Type 1',
    value: '1'
  },
  {
    id: 2,
    label: 'Type 2',
    value: '2'
  },
  {
    id: 3,
    label: 'Type 3',
    value: '3'
  }
];

export const UserModal = ({
  visible,
  id,
  loadingList = false,
  onSaveUser,
  onCancel,
  ...rest
}) => {
  const [form] = Form.useForm();
  const getUserById = useStoreActions(action => action.user.getUserById);
  const setEditingUser = useStoreActions(action => action.user.setEditingUser);
  const editingUser = useStoreState(state => state.user.editingUser);
  const loadingItem = useStoreState(state => state.user.loadingItem);

  /**
   * Get user by id and set editing user data to Store
   */
  useEffect(() => {
    if (!id) return;

    getUserById(id);
  }, [id, getUserById]);

  /**
   * Set editing user to form
   */
  useEffect(() => {
    if (!editingUser) return;

    form.setFieldsValue({
      name: editingUser.name,
      userType: editingUser.userType,
      version: editingUser.version,
      description: editingUser.description
    });

    const startDate = moment(editingUser.startDate, 'YYYY-MM-DD');
    const endDate = moment(editingUser.endDate, 'YYYY-MM-DD');

    if (startDate.isValid() && endDate.isValid()) {
      form.setFieldsValue({ rangeDate: [startDate, endDate] });
    }
  }, [editingUser, form]);

  /**
   * On submit
   */
  const onSubmit = values => {
    if (loadingItem || loadingList) return;

    const formData = {};

    if (id) {
      formData.id = id;
    }

    if (values.name) {
      formData.name = values.name;
    }

    if (values.userType) {
      formData.userType = values.userType;
    }

    if (values.version) {
      formData.version = values.version;
    }

    if (values.description) {
      formData.description = values.description;
    }

    if (Array.isArray(values.rangeDate) && values.rangeDate.length) {
      formData.startDate = moment(values.rangeDate[0]).format('YYYY-MM-DD');
      formData.endDate = moment(values.rangeDate[1]).format('YYYY-MM-DD');
    }

    onSaveUser(formData);
  };

  /**
   * Title modal
   *
   * @return {object} - Element
   */
  const TitleModal = () => {
    return (
      <div className="title-modal">
        {id ? (
          <>
            <EditOutlined /> Edit user
          </>
        ) : (
          <>
            <PlusCircle /> Add user
          </>
        )}

        <Tooltip placement="top" title="Reload" destroyTooltipOnHide={true}>
          <Button
            type="link"
            icon={<ReloadOutlined />}
            className="btn-reload"
            onClick={() => {
              setEditingUser({});
              !id && form.resetFields();
              id && getUserById(id);
            }}
          ></Button>
        </Tooltip>
      </div>
    );
  };

  /**
   * Unmount
   */
  useEffect(() => {
    return () => {
      setEditingUser({});
      form.resetFields();
    };
  }, [setEditingUser, form]);

  return (
    <Modal
      title={<TitleModal />}
      visible={visible}
      width={800}
      maskClosable={false}
      keyboard={false}
      footer={null}
      forceRender // For fix: Instance created by `useForm` is not connected to any Form element.
      onCancel={onCancel}
      className="c-user-modal p-0-modal-body footer-btns-style"
      {...rest}
    >
      <Form
        form={form}
        labelCol={{ sm: 6, md: 6 }}
        wrapperCol={{ sm: 17, md: 16 }}
        onFinish={onSubmit}
      >
        <div className="form-wrapper p-4">
          <Spin
            indicator={<Loading3QuartersOutlined spin />}
            spinning={loadingItem}
          >
            <Form.Item
              label="Name"
              name="name"
              rules={[{ required: true, message: 'Please fill in this field' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item label="Type" name="userType">
              <Select
                options={Array.isArray(userTypes) ? userTypes : []}
                optionFilterProp="label"
                showSearch
                allowClear
                placeholder="Select A Type"
              />
            </Form.Item>

            <Form.Item label="Version" name="version">
              <Input />
            </Form.Item>

            <Form.Item label="Description" name="description">
              <Input.TextArea rows="3" />
            </Form.Item>

            <Form.Item
              label="Start and End Date"
              name="rangeDate"
              rules={[{ required: true, message: 'Please fill in this field' }]}
            >
              <RangePicker format={SHORT_DATE_FORMAT} className="w-100" />
            </Form.Item>
          </Spin>
        </div>

        <div className="ant-modal-footer">
          <Button htmlType="submit" type="primary" icon={<SaveOutlined />}>
            Save
          </Button>

          <Button onClick={onCancel}>Cancel</Button>
        </div>
      </Form>
    </Modal>
  );
};
