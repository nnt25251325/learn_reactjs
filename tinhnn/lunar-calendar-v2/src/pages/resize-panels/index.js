import React from 'react';

import { BasicBreadcrumb } from '../../components';
import { ResizePanels } from './components';

const ResizePanelsDemo = () => {
  return (
    <div>
      <BasicBreadcrumb />

      <h3 className="ant-typography mb-4">Resize panels</h3>

      <ResizePanels
        className="row-wrapper w-100"
        leftProps={{ className: 'col-tree' }}
        rightProps={{ className: 'col-content' }}
      >
        <ResizePanels.Left>
          Left <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
        </ResizePanels.Left>

        <ResizePanels.Right>
          Right <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
        </ResizePanels.Right>
      </ResizePanels>
    </div>
  );
};

export default ResizePanelsDemo;
