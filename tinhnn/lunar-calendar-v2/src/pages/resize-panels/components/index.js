import { Col, Row } from 'antd';
import React, { Children, useRef, useState, useEffect } from 'react';

import './style.scss';

const MIN_WIDTH = 100;
let LeftSite = null;
let RightSite = null;

const ResizePanels = ({
  leftProps,
  rightProps,
  className = '',
  children,
  ...rest
}) => {
  const wrapperRef = useRef();
  const leftRef = useRef();
  const rightRef = useRef();

  const [leftWidth, setLeftWidth] = useState(0);
  const [rightWidth, setRightWidth] = useState(0);
  const [initialPosition, setInitialPosition] = useState(0);
  const [isDragging, setIsDragging] = useState(false);

  /**
   * Set children
   */
  Children.forEach(children, child => {
    if (child.type === ResizePanelsLeft) LeftSite = child;
    if (child.type === ResizePanelsRight) RightSite = child;
  });

  /**
   * On mouse down
   * Start drag
   */
  const onMouseDown = e => {
    setInitialPosition(e.clientX);
    setIsDragging(true);
  };

  /**
   * On touch start
   * Start drag
   */
  const onTouchStart = e => {
    setInitialPosition(e.touches[0].clientX);
    setIsDragging(true);
  };

  /**
   * On mouse move
   * Dragging
   */
  const onMouseMove = e => {
    e.preventDefault();
    handleDrag(e.clientX);
  };

  /**
   * On touch move
   * Dragging
   */
  const onTouchMove = e => {
    handleDrag(e.touches[0].clientX);
  };

  /**
   * End drag
   */
  const onEndDrag = () => setIsDragging(false);

  /**
   * Handle move
   */
  const handleDrag = clientX => {
    if (isDragging && leftWidth && initialPosition && wrapperRef.current) {
      const wrapperWidth = wrapperRef.current.clientWidth;
      const newLeftWidth = leftWidth + clientX - initialPosition;
      const newRightWidth = wrapperWidth - newLeftWidth;

      setInitialPosition(clientX);

      if (newLeftWidth < MIN_WIDTH) {
        setLeftWidth(MIN_WIDTH);
        setRightWidth(wrapperWidth - MIN_WIDTH);
        return;
      }

      if (newLeftWidth > wrapperWidth - MIN_WIDTH) {
        setLeftWidth(wrapperWidth - MIN_WIDTH);
        setRightWidth(MIN_WIDTH);
        return;
      }

      setLeftWidth(newLeftWidth);
      setRightWidth(newRightWidth);
    }
  };

  /**
   * Set width
   */
  useEffect(() => {
    if (!wrapperRef.current || !leftRef.current || !rightRef.current) return;

    if (!leftWidth && !rightWidth) {
      const wrapperWidth = wrapperRef.current.clientWidth;
      const newLeftWidth = leftRef.current.clientWidth;

      setLeftWidth(newLeftWidth);
      setRightWidth(wrapperWidth - newLeftWidth);
      return;
    }

    leftRef.current.style.maxWidth = `${leftWidth}px`;
    leftRef.current.style.flex = `0 0 ${leftWidth}px`;

    rightRef.current.style.maxWidth = `${rightWidth}px`;
    rightRef.current.style.flex = `0 0 ${rightWidth}px`;
  }, [wrapperRef, leftRef, rightRef, leftWidth, rightWidth]);

  /**
   * Event Listener
   */
  React.useEffect(() => {
    window.addEventListener('mousemove', onMouseMove);
    window.addEventListener('touchmove', onTouchMove);
    window.addEventListener('mouseup', onEndDrag);

    return () => {
      window.removeEventListener('mousemove', onMouseMove);
      window.removeEventListener('touchmove', onTouchMove);
      window.removeEventListener('mouseup', onEndDrag);
    };
  });

  return (
    <Row ref={wrapperRef} className={`c-resize-panels ${className}`} {...rest}>
      <Col
        ref={leftRef}
        {...leftProps}
        className={`col-left pr-3 ${leftProps.className || ''}`}
      >
        {LeftSite}

        <div
          className="resize-control"
          onMouseDown={onMouseDown}
          onTouchStart={onTouchStart}
          onTouchEnd={onEndDrag}
        >
          <div className="divider" />
        </div>
      </Col>

      <Col
        ref={rightRef}
        {...rightProps}
        className={`col-right pl-3 ${rightProps.className || ''}`}
      >
        {RightSite}
      </Col>
    </Row>
  );
};

const ResizePanelsLeft = ({ children }) => <>{children}</>;
const ResizePanelsRight = ({ children }) => <>{children}</>;

ResizePanels.Left = ResizePanelsLeft;
ResizePanels.Right = ResizePanelsRight;

export { ResizePanels };
