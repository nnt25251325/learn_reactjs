import React from 'react';
import { Route, Switch } from 'react-router';
import { useRouteMatch } from 'react-router-dom';

import store from '../../core/create-store';
import { model } from './model';
import DragTable from './components/p-drag-table';
import MultiDrag from './components/p-multi-drag';
import MultiTableDrag from './components/p-multi-table-drag';
import List from './components/p-list';

import './style.scss';

store.addModel('dragAndDrop', model);

export default () => {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={`${match.path}/drag-and-drop`} component={List} />
      <Route path="/drag-and-drop/drag-table" component={DragTable} />
      <Route path="/drag-and-drop/multi-drag" component={MultiDrag} />
      <Route
        path="/drag-and-drop/multi-table-drag"
        component={MultiTableDrag}
      />
    </Switch>
  );
};
