import React, { useState } from 'react';
import { Button, Col, Row, Table } from 'antd';
import { RightOutlined, LeftOutlined } from '@ant-design/icons';

import './style.scss';
import { BasicBreadcrumb } from '../../components';

const SOURCE_ID = 'source';
const TARGET_ID = 'target';

const initialData = {
  columnIds: [SOURCE_ID, TARGET_ID],
  columns: {
    [SOURCE_ID]: {
      id: SOURCE_ID,
      title: 'Source',
      total: 120,
      list: [],
      selectedRows: []
    },
    [TARGET_ID]: {
      id: TARGET_ID,
      title: 'Target',
      list: [],
      selectedRows: []
    }
  }
};

for (let i = 0; i < 120; i++) {
  initialData.columns.source.list.push({
    id: i + 1,
    name: `Item ${i + 1}`
  });
}

const Transfer = () => {
  const [data, setdata] = useState(initialData);

  const tableColumns = [
    {
      dataIndex: 'id',
      title: 'ID'
    },
    {
      dataIndex: 'name',
      title: 'Name'
    }
  ];

  /**
   * On change row selection
   */
  const onChangeRowSelection = (columnId, selectedRows) => {
    const newData = JSON.parse(JSON.stringify(data));
    newData.columns[columnId].selectedRows = selectedRows;
    setdata(newData);
  };

  /**
   * On click row
   */
  const onClickRow = (record, columnId) => {
    if (!record) return;

    const newData = JSON.parse(JSON.stringify(data));
    let selectedRows = [...newData.columns[columnId].selectedRows];

    if (selectedRows.some(item => item.id === record.id)) {
      selectedRows = [...newData.columns[columnId].selectedRows].filter(
        item => item.id !== record.id
      );
    } else {
      selectedRows = [...newData.columns[columnId].selectedRows, record];
    }

    newData.columns[columnId].selectedRows = selectedRows;
    setdata(newData);
  };

  /**
   * On table change
   */
  const onTableChange = (id, pagination, filters, sorter) => {
    if (id !== SOURCE_ID) return;
    console.log(pagination.current, pagination.pageSize);
  };

  /**
   * On transfer
   */
  const onTransfer = columnId => {
    const newData = JSON.parse(JSON.stringify(data));
    const destinationId = columnId === SOURCE_ID ? TARGET_ID : SOURCE_ID;
    const currentSelectedRows = [...newData.columns[columnId].selectedRows];
    const newCurrentList = [...newData.columns[columnId].list].filter(item => {
      return !currentSelectedRows.some(row => row.id === item.id);
    });
    const newDestinationList = [
      ...currentSelectedRows,
      ...newData.columns[destinationId].list
    ];

    newData.columns[columnId].list = newCurrentList;
    newData.columns[destinationId].list = newDestinationList;

    const sourceTotal =
      newData.columns[SOURCE_ID].total +
      (columnId === SOURCE_ID
        ? -currentSelectedRows.length
        : currentSelectedRows.length);

    // Reset
    newData.columns[columnId].selectedRows = [];
    newData.columns[destinationId].selectedRows = [];
    newData.columns[SOURCE_ID].total = sourceTotal;

    // Set new value
    setdata(newData);
  };

  return (
    <div className="p-transfer">
      <BasicBreadcrumb />

      <h3 className="ant-typography mb-4">Transfer</h3>

      <div className="control-btns">
        <Button
          type="primary"
          icon={<RightOutlined />}
          size="small"
          disabled={!data.columns[SOURCE_ID].selectedRows.length}
          onClick={() => onTransfer(SOURCE_ID)}
        />
        <br />
        <Button
          type="primary"
          icon={<LeftOutlined />}
          size="small"
          disabled={!data.columns[TARGET_ID].selectedRows.length}
          onClick={() => onTransfer(TARGET_ID)}
        />
      </div>

      <Row gutter="40">
        {data.columnIds.map(id => (
          <Col key={id} xs={12}>
            <p>
              selectedRows:{' '}
              {JSON.stringify(data.columns[id].selectedRows.map(i => i.id))}
            </p>

            <Table
              columns={tableColumns}
              dataSource={data.columns[id].list}
              rowKey="id"
              pagination={{
                total:
                  id === SOURCE_ID
                    ? data.columns[id].total
                    : data.columns[id].list.length,
                defaultPageSize: 10,
                size: 'small',
                showTotal: val => `Total: ${val} record(s)`,
                showSizeChanger: true
              }}
              rowSelection={{
                selectedRowKeys: data.columns[id].selectedRows.map(
                  row => row.id
                ),
                onChange: (selectedRowKeys, selectedRows) =>
                  onChangeRowSelection(id, selectedRows)
              }}
              onRow={(record, index) => ({
                onClick: () => onClickRow(record, id)
              })}
              onChange={(pagination, filters, sorter) =>
                onTableChange(id, pagination, filters, sorter)
              }
            />
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default Transfer;
