import React, { Suspense } from 'react';
import { Layout } from 'antd';

import {
  RouterLoading,
  LeftSidebar,
  MainHeader,
  MainFooter
} from '../components';

import store from '../common/store';
import { model as modelUser } from '../pages/user/model';

store.addModel('user', modelUser);

const { Content } = Layout;

export default ({ children }) => {
  return (
    <Suspense fallback={<RouterLoading />}>
      <Layout className="l-default">
        <MainHeader />

        <Layout className="main-body">
          <LeftSidebar />

          <Layout className="main-content-wrap">
            <Content className="main-content">{children}</Content>
            <MainFooter />
          </Layout>
        </Layout>
      </Layout>
    </Suspense>
  );
};
