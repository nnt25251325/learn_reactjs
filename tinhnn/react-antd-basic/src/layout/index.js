import React from 'react';
import { Redirect, useLocation } from 'react-router-dom';

import Default from './default';
import Blank from './blank';

import './style.scss';

export default ({ children }) => {
  const location = useLocation();

  // const token =
  //   reactLocalStorage.get(LS_SESSION_TOKEN) ||
  //   reactSessionStorage.get(LS_SESSION_TOKEN);

  const token = 'token';

  if (location.pathname === '/login' && token) {
    return <Redirect to="/" />;
  }

  if (
    location.pathname === '/login' ||
    location.pathname === '/403' ||
    location.pathname === '/404'
  ) {
    return <Blank>{children}</Blank>;
  }

  return <Default>{children}</Default>;
};
