import React from 'react';
import { Route, Switch } from 'react-router';

import PageIndex from './page-index';

import './style.scss';

export default () => {
  return (
    <Switch>
      <Route path="/about*" component={PageIndex} />
    </Switch>
  );
};
