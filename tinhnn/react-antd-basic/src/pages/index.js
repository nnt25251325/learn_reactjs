import React, { lazy } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import { PrivateRoutes } from '../components/private-route';

const Login = lazy(() => import('./login'));
const Home = lazy(() => import('./home'));
const About = lazy(() => import('./about'));
const User = lazy(() => import('./user'));
const Error403 = lazy(() => import('./error-403'));
const Error404 = lazy(() => import('./error-404'));

export default () => {
  return (
    <Switch>
      <PrivateRoutes exact path="/" component={Home} />
      <PrivateRoutes path="/user" component={User} />
      <PrivateRoutes path="/about" component={About} />
      <Route path="/login" component={Login} />
      <Route path="/403" component={Error403} />
      <Route path="/404" component={Error404} />
      <Redirect to="/404" />
    </Switch>
  );
};
