import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

export default () => {
  const history = useHistory();

  useEffect(() => {
    history.push('./user');
  }, [history]);

  return <div>Home</div>;
};
