import React from 'react';
import { Link } from 'react-router-dom';
import { Form, Input, Button, Checkbox } from 'antd';

export default () => {
  const [form] = Form.useForm();

  return (
    <div className="card-login">
      <Form form={form} layout="vertical" onFinish={() => {}}>
        <div className="form-wrapper">
          <div className="text-center mb-4">
            <img src="logo192.png" alt="Logo" height="68px" />
          </div>

          <Form.Item
            name="username"
            rules={[{ required: true, message: 'This field is required.' }]}
          >
            <Input placeholder="User Name" />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{ required: true, message: 'This field is required.' }]}
          >
            <Input.Password placeholder="Password" />
          </Form.Item>

          <div className="ant-row ant-row-space-between ant-row-middle mb-4">
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Remember Me</Checkbox>
            </Form.Item>

            <Link to="/forgot-password">Forgot Password</Link>
          </div>

          <Form.Item className="text-center mb-0">
            <Button
              type="primary"
              htmlType="submit"
              block
              loading={false}
              className="min-w-150"
            >
              Log In
            </Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
};
