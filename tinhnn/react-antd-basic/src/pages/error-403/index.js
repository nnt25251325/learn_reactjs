import React from 'react';
import { Button, Result } from 'antd';
import { HomeOutlined, LogoutOutlined } from '@ant-design/icons';

import { LS_SESSION_TOKEN } from '../../configs';

import './style.scss';

export default () => {
  const resetBeforeLogout = () => {
    sessionStorage.removeItem(LS_SESSION_TOKEN);
    localStorage.removeItem(LS_SESSION_TOKEN);
    window.location.href = '/login';
  };

  return (
    <Result
      status="403"
      title="403"
      subTitle="Don't Have Permission"
      className="p-403"
      extra={
        <>
          <Button
            className="ant-btn ant-btn-primary"
            onClick={() => (window.location.href = '/')}
          >
            <HomeOutlined /> Back Home
          </Button>

          <Button
            className="ant-btn ant-btn-primary"
            onClick={() => resetBeforeLogout()}
          >
            <LogoutOutlined /> Log Out
          </Button>
        </>
      }
    />
  );
};
