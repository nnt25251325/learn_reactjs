import React, { useEffect, useState } from 'react';
import { Card, Button, Modal } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useStoreState, useStoreActions } from 'easy-peasy';

import { PAGE_SIZE, DEFAULT_SORT } from '../../constants';
import { BaseBreadcrumb, BaseTable } from '../../components';

export default () => {
  const getReleaseList = useStoreActions(action => action.user.getReleaseList);
  const setReleaseList = useStoreActions(action => action.user.setReleaseList);
  const data = useStoreState(state => state.user.data);
  const total = useStoreState(state => state.user.total);
  const loadingList = useStoreState(state => state.user.loadingList);
  const setQuery = useStoreActions(action => action.user.setQuery);
  const query = useStoreState(state => state.user.query);

  const [sortedInfo, setSortedInfo] = useState({});

  const columns = [
    {
      dataIndex: 'id',
      title: 'ID',
      width: 80,
      sorter: () => {},
      sortOrder: sortedInfo.field === 'id' && sortedInfo.order
    },
    {
      dataIndex: 'name',
      title: 'Name',
      sorter: () => {},
      sortOrder: sortedInfo.field === 'name' && sortedInfo.order
    },
    {
      dataIndex: 'action',
      title: 'Action',
      fixed: 'right',
      width: 120,
      className: 'text-nowrap text-center',
      render: (value, item) => renderAction(item)
    }
  ];

  const renderAction = item => {
    if (!item?.id) return null;

    return (
      <>
        <Button
          title="Edit"
          type="link"
          icon={<EditOutlined />}
          size="small"
          className="border-transparent mr-2"
        ></Button>

        <Button
          title="Delete"
          type="link"
          danger
          icon={<DeleteOutlined />}
          size="small"
          onClick={() => onDeleteRelease(item)}
        ></Button>
      </>
    );
  };

  useEffect(() => {
    getReleaseList({
      page: 1,
      limit: PAGE_SIZE,
      order: [DEFAULT_SORT]
    });
  }, [getReleaseList]);

  const onDeleteRelease = item => {
    if (loadingList || !item?.id) return;

    Modal.confirm({
      title: 'Warning',
      content: 'Are you sure you want to delete it?',
      autoFocusButton: null,
      maskClosable: true,
      okText: 'Delete',
      okButtonProps: { danger: true, type: 'primary' },
      cancelText: 'Cancel',
      onOk: () => {}
    });
  };

  useEffect(() => {
    return () => {
      setQuery({});
      setReleaseList([]);
    };
  }, [setQuery, setReleaseList]);

  return (
    <>
      <div className="p-user-list">
        <BaseBreadcrumb />

        <Card>
          <h4 className="ant-typography mb-3">User</h4>

          <BaseTable
            columns={columns}
            dataSource={data}
            total={total}
            currentPage={query?.page}
            currentLimit={query?.limit}
            loading={loadingList}
            className="main-table"
            onSortChange={setSortedInfo}
            onTableChange={params => getReleaseList({ ...query, ...params })}
          />
        </Card>
      </div>
    </>
  );
};
