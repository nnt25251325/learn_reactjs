import React from 'react';
import { Route, Switch } from 'react-router';

import PageList from './page-list';

import './style.scss';

export default () => {
  return (
    <Switch>
      <Route path="/user*" component={PageList} />
    </Switch>
  );
};
