import { action, thunk } from 'easy-peasy';

import { ENDPOINTS } from '../../configs';
import { Http, handleError } from '../../common';

export const model = {
  data: [],
  total: 0,
  editingRelease: {},
  query: {},
  loadingList: false,
  loadingItem: false,

  setReleaseList: action((state, payload) => {
    if (state?.data === undefined || state?.total === undefined) return;

    if (!(Array.isArray(payload?.rows) && payload.rows.length)) {
      state.data = [];
      state.total = 0;
      return;
    }

    state.data = payload.rows;
    state.total = payload.count;
  }),

  setEditingRelease: action((state, payload) => {
    if (state?.editingRelease === undefined) return;

    state.editingRelease = payload;
  }),

  setQuery: action((state, payload) => {
    if (state?.query === undefined) return;

    state.query = payload;
  }),

  setLoadingList: action((state, payload) => {
    if (state?.loadingList === undefined) return;

    state.loadingList = payload;
  }),

  setLoadingItem: action((state, payload) => {
    if (state?.loadingItem === undefined) return;

    state.loadingItem = payload;
  }),

  getReleaseList: thunk(async (action, payload) => {
    try {
      if (!(payload !== null && typeof payload === 'object')) {
        throw new Error('Invalid Payload');
      }

      action.setLoadingList(true);
      action.setQuery(payload);

      // const url = `${ENDPOINTS.USER}?q=${buildQuery(payload)}`;
      // const res = await Http.get(url).then(res => res.data);
      const res = {
        status: 200,
        message: 'Success',
        data: {
          count: 100,
          rows: [
            {
              id: '1834',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '3450',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '1451',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '5152',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '3940',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '1513',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '1234',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '3340',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '1631',
              name: 'Lorem ipsum dolor sit amet'
            },
            {
              id: '1515',
              name: 'Lorem ipsum dolor sit amet'
            }
          ]
        }
      };

      if (!res?.data) {
        action.setReleaseList([]);
        throw res;
      }

      action.setReleaseList(res.data);
    } catch (err) {
      handleError(err);
    } finally {
      action.setLoadingList(false);
    }
  }),

  getReleaseById: thunk(async (action, payload) => {
    try {
      if (!payload) throw new Error('Invalid Payload');

      action.setLoadingItem(true);

      const url = `${ENDPOINTS.USER}/${payload}`;
      const res = await Http.get(url).then(res => res.data);

      if (!res?.data) throw res;

      action.setEditingRelease(res.data);
    } catch (err) {
      handleError(err);
    } finally {
      action.setLoadingItem(false);
    }
  }),

  createRelease: thunk(async (action, payload, helpers) => {
    try {
      if (!(payload !== null && typeof payload === 'object')) {
        throw new Error('Invalid Payload');
      }

      action.setLoadingItem(true);

      // ==========> Create release
      const url = ENDPOINTS.USER;
      const res = await Http.post(url, payload).then(res => res.data);

      // notification.success({
      //   message: i18next.t('akaat:release.messageCreatingRelease'),
      //   description: i18next.t('akaat:message.createdSuccessfully')
      // });

      return res;
    } catch (err) {
      handleError(err);
    } finally {
      action.setLoadingItem(false);
    }
  }),

  updateRelease: thunk(async (action, payload, helpers) => {
    try {
      if (!payload?.id) throw new Error('Invalid Payload');

      action.setLoadingItem(true);

      // ==========> Update release
      const url = `${ENDPOINTS.USER}/${payload.id}`;
      const res = await Http.put(url, payload).then(res => res.data);

      // notification.success({
      //   message: i18next.t('akaat:release.messageUpdatingRelease'),
      //   description: i18next.t('akaat:message.updatedSuccessfully')
      // });

      return res;
    } catch (err) {
      handleError(err);
    } finally {
      action.setLoadingItem(false);
    }
  }),

  deleteRelease: thunk(async (action, payload, helpers) => {
    try {
      if (!payload) throw new Error('Invalid Payload');

      const url = `${ENDPOINTS.USER}/${payload}`;
      await Http.delete(url).then(res => res.data);

      // notification.success({
      //   message: i18next.t('akaat:release.messageDeletingRelease'),
      //   description: i18next.t('akaat:message.deletedSuccessfully')
      // });
    } catch (err) {
      handleError(err);
    }
  })
};
