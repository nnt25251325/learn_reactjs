import React from 'react';
import { Button, Result } from 'antd';
import { HomeOutlined } from '@ant-design/icons';

import './style.scss';

export default () => {
  return (
    <Result
      status="404"
      title="404"
      subTitle="Page Not Found"
      className="p-404"
      extra={
        <Button
          className="ant-btn ant-btn-primary"
          onClick={() => (window.location.href = '/')}
        >
          <HomeOutlined /> Back Home
        </Button>
      }
    />
  );
};
