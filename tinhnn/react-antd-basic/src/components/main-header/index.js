/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu, Dropdown, Row } from 'antd';
import {
  LoginOutlined,
  LogoutOutlined,
  CaretDownOutlined
} from '@ant-design/icons';

import { LS_SESSION_TOKEN } from '../../configs';
import { reactLocalStorage, reactSessionStorage } from '../../common';
import { UserCircle } from '../../assets/svg-icons';

import './style.scss';

const { Header } = Layout;

export const MainHeader = ({ className = '', ...rest }) => {
  const token =
    reactLocalStorage.get(LS_SESSION_TOKEN) ||
    reactSessionStorage.get(LS_SESSION_TOKEN);

  const renderMenuOnAccountDropdown = () => {
    return (
      <Menu>
        <Menu.Item key="changePassword">
          <a>
            <UserCircle /> Change Password
          </a>
        </Menu.Item>

        <Menu.Item key="/login">
          <Link to="/login" onClick={() => resetBeforeLogout()}>
            {token ? (
              <>
                <LogoutOutlined /> Log Out
              </>
            ) : (
              <>
                <LoginOutlined /> Log In
              </>
            )}
          </Link>
        </Menu.Item>
      </Menu>
    );
  };

  const resetBeforeLogout = () => {
    sessionStorage.removeItem(LS_SESSION_TOKEN);
    localStorage.removeItem(LS_SESSION_TOKEN);
    window.location.href = '/login';
  };

  return (
    <Header className={`c-main-header ${className}`} {...rest}>
      <Row className="box-left">
        <div className="logo mr-3">
          <a href="/">
            <img
              src="https://www.google.com.vn/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"
              alt="Logo"
              height="40px"
            />
          </a>
        </div>
      </Row>

      <div className="box-right">
        <Dropdown
          overlay={renderMenuOnAccountDropdown()}
          trigger={['click']}
          placement="bottomRight"
          className="dropdown-account ml-3"
        >
          <div className="ant-dropdown-link cursor-pointer">
            <span>Peter</span>
            <CaretDownOutlined className="ic-arrow ml-1" />
          </div>
        </Dropdown>
      </div>
    </Header>
  );
};
