import React, { useState, useEffect } from 'react';
import { Table, Spin } from 'antd';
import { Loading3QuartersOutlined } from '@ant-design/icons';

import { PAGE_SIZES, DEFAULT_SORT } from '../../constants';

import './style.scss';

export const BaseTable = ({
  columns = [],
  data = [],
  total = 0,
  currentPage,
  currentLimit,
  rowKey = 'id',
  showTotal = true,
  defaultSortBy = DEFAULT_SORT,
  loading = false,
  isResponsive = true,
  className = '',
  restPagination,
  onSortChange,
  onTableChange,
  ...rest
}) => {
  const [page, setPage] = useState(null);
  const [limit, setLimit] = useState(null);

  useEffect(() => {
    if (!currentPage) return;

    setPage(currentPage);
  }, [currentPage]);

  useEffect(() => {
    if (!currentLimit) return;

    setLimit(currentLimit);
  }, [currentLimit]);

  const handleTableChange = (pagination, filters, sorter) => {
    const page = pagination.current;
    const limit = pagination.pageSize;
    const params = {
      page,
      limit,
      order: [defaultSortBy]
    };

    setPage(page);
    setLimit(limit);

    if (sorter.order) {
      params.order = [
        [sorter.field, sorter.order === 'ascend' ? 'ASC' : 'DESC']
      ];
    }

    if (typeof onSortChange === 'function') {
      onSortChange(sorter);
    }

    onTableChange(params);
  };

  return (
    <div className={isResponsive ? 'table-responsive' : ''}>
      {Array.isArray(data) && (
        <Table
          dataSource={data}
          columns={columns}
          rowKey={rowKey}
          loading={{
            spinning: loading,
            indicator: <Spin indicator={<Loading3QuartersOutlined spin />} />
          }}
          pagination={
            page &&
            limit && {
              total,
              current: page,
              pageSize: limit,
              pageSizeOptions: PAGE_SIZES,
              showSizeChanger: true,
              showQuickJumper: true,
              showTotal: (totalnumber, range) =>
                `${range[0]} - ${range[1]} of ${totalnumber} record(s)`,
              ...restPagination
            }
          }
          className={`c-base-table ${className}`}
          onChange={handleTableChange}
          {...rest}
        />
      )}
    </div>
  );
};
