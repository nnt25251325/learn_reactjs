import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Breadcrumb } from 'antd';

import { BREADCRUMBS } from '../../configs';
import { buildRoutesForBreadcrumb } from '../../common';

import './style.scss';

export const BaseBreadcrumb = ({
  className = '',
  addedRoutes = [],
  ...rest
}) => {
  const location = useLocation();

  const itemRenderForBreadcrumb = (route, params, routes, paths) => {
    const last = routes.indexOf(route) === routes.length - 1;
    const RouteText = () => (
      <>
        {route.path === '/' ? route.icon : ''} {route.breadcrumbName}
      </>
    );

    return route.noLink || last ? (
      <RouteText />
    ) : route.path === '/' ? (
      <a href="/">
        <RouteText />
      </a>
    ) : (
      <Link to={route.path || '/'}>
        <RouteText />
      </Link>
    );
  };

  return (
    <Breadcrumb
      itemRender={itemRenderForBreadcrumb}
      routes={[
        ...buildRoutesForBreadcrumb(location.pathname, BREADCRUMBS),
        ...addedRoutes
      ]}
      separator=">"
      className={`c-base-breadcrumb ${className}`}
      {...rest}
    />
  );
};
