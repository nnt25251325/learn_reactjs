import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { Scrollbars } from 'react-custom-scrollbars';

import { BREADCRUMBS } from '../../configs';
import { buildPathNameList } from '../../common';

import './style.scss';

const { SubMenu } = Menu;
const { Sider } = Layout;

export const LeftSidebar = ({ className = '', ...rest }) => {
  const location = useLocation();
  const [menus, setMenus] = useState([]);
  const [collapsed, setCollapsed] = useState(false);

  useEffect(() => {
    const newMenus = BREADCRUMBS;

    setMenus(newMenus);
  }, []);

  return (
    <Sider
      collapsible
      collapsed={collapsed}
      width={220}
      className={`c-left-sidebar ${className}`}
      onCollapse={() => setCollapsed(!collapsed)}
      {...rest}
    >
      <Scrollbars
        autoHide
        renderThumbVertical={props => (
          <div {...props} className="thumb-vertical" />
        )}
        className="scrollbars-left-sidebar"
      >
        {Array.isArray(menus) && menus.length > 0 && (
          <Menu
            theme="dark"
            mode="inline"
            selectedKeys={buildPathNameList(location.pathname)}
          >
            {menus.map(item => {
              if (Array.isArray(item.children) && item.children.length) {
                return (
                  <SubMenu
                    // key={item.path}
                    key={Math.random()}
                    icon={item.icon}
                    title={item.breadcrumbName}
                  >
                    {item.children.map(sub => (
                      <Menu.Item key={sub.path} icon={sub.icon}>
                        <Link to={sub.path} title={item.breadcrumbName}>
                          {item.breadcrumbName}
                        </Link>
                      </Menu.Item>
                    ))}
                  </SubMenu>
                );
              }

              return (
                <Menu.Item
                  //  key={item.path}
                  key={Math.random()}
                  icon={item.icon}
                >
                  <Link to={item.path} title={item.breadcrumbName}>
                    {item.breadcrumbName}
                  </Link>
                </Menu.Item>
              );
            })}
          </Menu>
        )}
      </Scrollbars>
    </Sider>
  );
};
