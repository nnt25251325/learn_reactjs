import { PAGE_SIZE, DEFAULT_SORT } from '../constants';

export const buildQuery = (params = {}) => {
  const query = {
    offset: 0, // Same page = 1
    limit: PAGE_SIZE // If limit is null => Don't send limit => Get all
  };

  // Add offset
  if (+params.page >= 1) {
    const page = params.page < 0 ? 1 : params.page;
    query.offset = (page - 1) * (params.limit || query.limit);
  }

  // Add limit
  if (+params.limit >= 0) {
    query.limit = params.limit < 0 ? PAGE_SIZE : params.limit;
  }

  // Add where
  if (
    params.where !== null &&
    typeof params.where === 'object' &&
    Object.keys(params.where).length
  ) {
    query.where = params.where;
  }

  // Add order (sort)
  if (!params.order) {
    query.order = [DEFAULT_SORT]; // Default is order by updatedAt: DESC
  } else if (Array.isArray(params.order) && params.order.length) {
    query.order = params.order;
  }

  // Add attributes
  if (
    (Array.isArray(params.attributes) && params.attributes.length) ||
    (typeof params.attributes === 'object' &&
      Object.keys(params.attributes).length)
  ) {
    query.attributes = params.attributes;
  }

  // Don't add when value is null
  Object.keys(params).forEach(key => {
    if (params[key] === null) delete query[key];
  });

  return encodeURI(JSON.stringify({ ...query }));
};

export const buildPathNameList = pathName => {
  if (!pathName) return [];

  const pathList = [];
  const pathArray = pathName.substr(pathName.indexOf('/') + 1).split('/'); // Example: ['parent', 'child-1', 'child-2']

  pathArray.forEach((item, index) => {
    if (!pathList[index - 1]) {
      pathList.push('/' + item);
      return;
    }

    pathList.push(pathList[index - 1] + '/' + item);
  });

  // For Defect Tracking
  if (pathName === '/defect-tracking' || pathName === '/jira-defect-tracking') {
    pathList.push('/defect-tracking-index');
  }

  return pathList;
};

export const buildRoutesForBreadcrumb = (pathName, breadcrumbConfig) => {
  if (!pathName) {
    return [];
  }

  if (pathName === '/') {
    // Only home page
    return breadcrumbConfig.filter(item => item.path === '/');
  }

  const breadcrumbs = [];
  const pathList = [];
  const pathArray = pathName.split('/'); // Example: ['', 'parent', 'child-1', 'child-2']

  pathArray.forEach((item, index) => {
    if (!pathList[index - 1]) {
      const curentPath = '/' + item;
      pathList.push(curentPath);

      const found = breadcrumbConfig.find(breadcrumb => {
        return breadcrumb.path === curentPath && !breadcrumb.isHideOnBreadcrumb;
      });

      if (found) {
        breadcrumbs.push(found);
      }

      return;
    }

    const separator = pathList[index - 1] === '/' ? '' : '/';
    const curentPath = pathList[index - 1] + separator + item;
    pathList.push(curentPath);

    const found = breadcrumbConfig.find(breadcrumb => {
      return breadcrumb.path === curentPath && !breadcrumb.isHideOnBreadcrumb;
    });

    if (found) {
      breadcrumbs.push(found);
    }
  });

  return breadcrumbs;
};

export const getObjectByValue = (val, list) => {
  if (!(Array.isArray(list) && list.length)) return {};

  const found = list.find(item => item.value === val);

  if (!found) return {};

  return found;
};

export const debounce = (func, wait, immediate) => {
  let timeout;

  return function () {
    const context = this;
    const args = arguments;

    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) func.apply(context, args);
  };
};

export const jsonParse = data => {
  try {
    return JSON.parse(data);
  } catch (err) {
    console.error(err);
    return null;
  }
};
