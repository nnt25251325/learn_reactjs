import Axios from 'axios';
import { Modal } from 'antd';

import { LS_SESSION_TOKEN } from '../configs';

const token =
  localStorage.getItem(LS_SESSION_TOKEN) ||
  sessionStorage.getItem(LS_SESSION_TOKEN);

const Http = Axios.create({
  baseURL: process.env.REACT_APP_API_ENDPOINT,
  headers: { Authorization: `Bearer ${token}` }
});

Http.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response?.status === 401) {
      Modal.error({
        title: 'Error',
        content: 'Login',
        autoFocusButton: 'ok',
        zIndex: 9999,
        onOk: () => {
          localStorage.removeItem(LS_SESSION_TOKEN);
          sessionStorage.removeItem(LS_SESSION_TOKEN);
          window.location.href = `/login?nextUrl=${window.location.href}`;
        }
      });
    }

    return Promise.reject(error.response ? error.response : error);
  }
);

export { Http };
