import { notification } from 'antd';

export const handleError = err => {
  if (!err) return;

  notification.error({
    message: `Error ${err.statusText}`,
    description: err.data.message
  });
};
