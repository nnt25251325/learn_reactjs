import queryString from 'query-string';

import { DEFAULT_SORT, PAGE_SIZE } from '../constants';

/**
 * Build query for params url
 *
 * Example: ?limit=10&name=abc&offset=0&sort=createdAt%7CDESC
 */
export const buildQueryForParamsUrl = (params = {}) => {
  const query = {
    _start: 0,
    _end: PAGE_SIZE
  }; // If page and limit is null => Don't send _start and  _end => Get all

  // Add _start
  if (+params.page >= 1) {
    query._start = (params.page - 1) * params.limit;
  }

  // Add _end
  if (+params.limit >= 0 && +params.page >= 1) {
    query._end = params.page * params.limit;
  }

  // Add sort
  const paths =
    typeof params.sort === 'string'
      ? params.sort.split('|')
      : DEFAULT_SORT.split('|');
  query._sort = paths[0];
  query._order = paths[1];

  // Add filter
  if (
    params.filters !== null &&
    typeof params.filters === 'object' &&
    Object.keys(params.filters).length
  ) {
    Object.keys(params.filters).forEach(key => {
      query[key] = params.filters[key];
    });
  }

  // Don't add when value is null
  Object.keys(params).forEach(key => {
    if (
      (key === 'page' && params[key] === null) ||
      (key === 'limit' && params[key] === null)
    ) {
      delete query._start;
      delete query._end;
    } else if (key === 'sort' && params[key] === null) {
      delete query._sort;
      delete query._order;
    } else if (params[key] === null) {
      delete query[key];
    }
  });

  return queryString.stringify({ ...query });
};

/**
 * Build path name list, for active nav link
 *
 * Example - Before: /parent/child-1/child-2
 * Example - After: ['/parent', '/parent/child-1', '/parent/child-1/child-2']
 */
export const buildPathNameList = pathName => {
  if (!pathName) return [];

  const pathList = [];
  const pathArray = pathName.substr(pathName.indexOf('/') + 1).split('/'); // Example: ['parent', 'child-1', 'child-2']

  pathArray.forEach((item, index) => {
    if (!pathList[index - 1]) {
      pathList.push('/' + item);
      return;
    }

    pathList.push(pathList[index - 1] + '/' + item);
  });

  return pathList;
};

/**
 * Get object in array by value
 */
export const getObjectByValue = (val, list) => {
  if (!(Array.isArray(list) && list.length)) return;

  return list.find(item => item.value === val);
};

/**
 * Debounce function
 */
export const debounce = (func, wait, immediate) => {
  let timeout;

  return function () {
    const context = this;
    const args = arguments;

    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) func.apply(context, args);
  };
};

/**
 * Json parse
 * Use try catch to avoid errors
 */
export const jsonParse = data => {
  try {
    return JSON.parse(data);
  } catch (err) {
    console.error(err);
    return null;
  }
};

/**
 * Scroll into hash
 */
export const scrollToLocationHash = locationHash => {
  if (!locationHash) return;

  const elementId = locationHash.split('#')[1];
  const element = document.getElementById(elementId);

  if (!element) return;

  element.scrollIntoView();
};

/**
 * Scroll to first class name
 */
export const scrollToFirstClassName = className => {
  if (!className) return;

  setTimeout(() => {
    const items = document.body.getElementsByClassName(className);
    const visibleItems = [...items].filter(el => {
      return el.offsetWidth || el.offsetHeight || el.getClientRects().length;
    });

    if (visibleItems.length > 0) {
      items[0].scrollIntoView();
    }
  }, 500);
};
