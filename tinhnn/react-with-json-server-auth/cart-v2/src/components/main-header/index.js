import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Layout, Menu, Dropdown, Row } from 'antd';
import {
  UserOutlined,
  LogoutOutlined,
  TrademarkOutlined,
  CaretDownOutlined
} from '@ant-design/icons';
import { useStoreState } from 'easy-peasy';

import { LS_CURRENT_USER, LS_TOKEN } from '../../constants';
import { reactLocalStorage } from '../../common';
import { UserCircle } from '../../assets/svg-icons';
import { LanguageDropdown } from '../../components';

import './style.scss';

const { Header } = Layout;

export const MainHeader = () => {
  const location = useLocation();

  const token = reactLocalStorage.get(LS_TOKEN);

  // For language
  const [t] = useTranslation('cartLanguage');

  // For global user info action
  const globalUserInfo = useStoreState(state => state.global.globalUserInfo);

  /**
   * Render menu on account dropdown
   *
   * @return {Object} - Element
   */
  const renderMenuOnAccountDropdown = currentUserInfo => {
    return (
      <Menu>
        {token && currentUserInfo?.id && (
          <>
            <Menu.Item key="/user">
              <Link to="/user">
                <UserOutlined /> {t('header.userManagement')}
              </Link>
            </Menu.Item>

            <Menu.Item key="/trademark">
              <Link to="/trademark">
                <TrademarkOutlined /> {t('header.trademarkManagement')}
              </Link>
            </Menu.Item>
          </>
        )}

        <Menu.Item key="login" onClick={() => resetBeforeLogout()}>
          {token ? (
            <>
              <LogoutOutlined /> {t('header.logout')}
            </>
          ) : (
            <>
              <LogoutOutlined /> {t('header.login')}
            </>
          )}
        </Menu.Item>
      </Menu>
    );
  };

  /**
   * Reset before logout
   */
  const resetBeforeLogout = () => {
    reactLocalStorage.remove(LS_CURRENT_USER);
    reactLocalStorage.remove(LS_TOKEN);

    window.location.href = '/login';
  };

  return (
    <>
      <Header className="c-main-header">
        <Row className="box-left">
          <div className="logo mr-3">
            <a href="/">LOGO</a>
          </div>
        </Row>

        <div className="box-right">
          <LanguageDropdown
            restDropdown={{ placement: 'bottom' }}
            className="ml-3"
          />

          <Dropdown
            overlay={renderMenuOnAccountDropdown(globalUserInfo, location)}
            trigger={['click']}
            destroyPopupOnHide={true}
            placement="bottomRight"
            className="btn-dropdown-account ml-3"
          >
            <div className="ant-dropdown-link cursor-pointer">
              {globalUserInfo?.id && (
                <span>
                  {globalUserInfo?.id
                    ? globalUserInfo.fullName
                    : t('header.guest')}
                  !
                </span>
              )}

              <div className="box-avatar">
                {globalUserInfo?.picture ? (
                  <div
                    className="img-avatar"
                    style={{
                      backgroundImage: 'url(' + globalUserInfo.picture + ')'
                    }}
                  ></div>
                ) : (
                  <UserCircle className="ic-user-circle" />
                )}
              </div>

              <CaretDownOutlined className="ic-arrow" />
            </div>
          </Dropdown>
        </div>
      </Header>
    </>
  );
};
