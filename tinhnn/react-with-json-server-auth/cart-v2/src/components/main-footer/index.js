import React from 'react';
import { useTranslation } from 'react-i18next';
import { Row, Col } from 'antd';

import './style.scss';

export const MainFooter = ({ className = '', ...rest }) => {
  // For language
  const [t] = useTranslation('cartLanguage');

  return (
    <div className={`c-main-footer ${className}`} {...rest}>
      <Row justify="space-between">
        <Col>
          <a
            href="https://www.google.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            My Website
          </a>{' '}
          {t('footer.copyright')}
        </Col>

        <Col>
          {t('footer.poweredBy')} <span className="powered">Me</span>
        </Col>
      </Row>
    </div>
  );
};
