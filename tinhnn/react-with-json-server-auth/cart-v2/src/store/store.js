import { action, createStore, thunk } from 'easy-peasy';
import { handleError, Http } from '../api';

import { ENDPOINTS } from '../constants';

export default createStore({
  global: {
    /**
     * State
     */
    globalLanguage: '',
    globalUserInfo: {},
    loadingGlobalUserInfo: true,

    /**
     * Action: Set global language
     */
    setGlobalLanguage: action((state, payload) => {
      if (!(state?.globalLanguage !== undefined)) return;

      state.globalLanguage = payload;
    }),

    /**
     * Action: Set global user info
     */
    setGlobalUserInfo: action((state, payload) => {
      if (!(state?.globalUserInfo !== undefined)) return;

      if (payload) {
        state.globalUserInfo = payload;
      }
    }),

    /**
     * Action: Set loading global user info
     */
    setLoadingGlobalUserInfo: action((state, payload) => {
      if (!(state?.loadingGlobalUserInfo !== undefined)) return;

      state.loadingGlobalUserInfo = payload;
    }),

    /**
     * Action: Call api to get global user info
     */
    getGlobalUserInfo: thunk(async (action, payload) => {
      try {
        if (!payload) throw new Error('There are no user ID');

        action.setLoadingGlobalUserInfo(true);

        const url1 = `${ENDPOINTS.USERS}/${payload}`;
        const res = await Http.get(url1);

        action.setGlobalUserInfo(res?.data);
      } catch (err) {
        handleError(err);
      } finally {
        action.setLoadingGlobalUserInfo(false);
      }
    })
  }
});
