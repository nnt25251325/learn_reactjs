/**
 * Endpoint declaration
 */
export const ENDPOINTS = {
  REGISTER: '/register',
  LOGIN: '/login',
  USERS: '/660/users',
  PRODUCTS: '/660/products',
  CARTS: '/660/carts',
  TRADEMARKS: '/660/trademarks'
};
