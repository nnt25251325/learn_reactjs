import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Result } from 'antd';
import { HomeOutlined, LogoutOutlined } from '@ant-design/icons';

import { LS_CURRENT_USER, LS_TOKEN } from '../../constants';
import { reactLocalStorage } from '../../common';

import './style.scss';

export default function Error403() {
  // For language
  const [t] = useTranslation('cartLanguage');

  /**
   * Reset before logout
   */
  const resetBeforeLogout = () => {
    reactLocalStorage.remove(LS_CURRENT_USER);
    reactLocalStorage.remove(LS_TOKEN);

    window.location.href = '/login';
  };

  return (
    <Result
      status="403"
      title="403"
      subTitle={t('message.youDoNotHavePermissionToAccess')}
      className="p-403"
      extra={
        <>
          <Button type="primary" href="/">
            <HomeOutlined /> {t('common.backHome')}
          </Button>

          <Button
            className="ant-btn ant-btn-primary"
            onClick={() => resetBeforeLogout()}
          >
            <LogoutOutlined /> {t('header.logout')}
          </Button>
        </>
      }
    />
  );
}
