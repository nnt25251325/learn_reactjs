import React, { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Form, Input, Button, Row, Alert } from 'antd';
import { LoginOutlined } from '@ant-design/icons';
import { useStoreActions, useStoreState } from 'easy-peasy';

import { LS_CURRENT_USER, LS_TOKEN } from '../../constants';
import { reactLocalStorage } from '../../common';
import { LanguageDropdown } from '../../components';

import './style.scss';

let timeout = 0;

export default () => {
  const [form] = Form.useForm();
  const refEmail = useRef(null);

  // For language
  const [t] = useTranslation('cartLanguage');

  // For login action
  const login = useStoreActions(action => action.auth.login);
  const loadingLogin = useStoreState(state => state.auth.loadingLogin);

  // Other state
  const [error, setError] = useState('');

  /**
   * Focus in name input
   */
  useEffect(() => {
    refEmail?.current && refEmail.current.focus();
  }, []);

  /**
   * On login
   */
  const onLogin = async values => {
    if (loadingLogin) return;

    // Reset
    clearTimeout(timeout);
    setError('');

    const res = await login({
      email: values.email,
      password: values.password
    });

    if (res) {
      reactLocalStorage.set(LS_TOKEN, res.data?.accessToken);
      reactLocalStorage.set(LS_CURRENT_USER, res.data?.user?.id);

      window.location.href = '/';
    }
  };

  return (
    <div className="p-login">
      <div className="card-login">
        <Row align="middle" justify="end" className="top-action">
          {/* <span className="mr-2">{t('login.doNotHaveAnAccount')}</span>
          <Link
            to="/register"
            className="ant-btn ant-btn-round ant-btn-lg min-w-100 mr-4 min-w-100 mr-4"
          >
            {t('login.signUp')}
          </Link> */}

          <LanguageDropdown />
        </Row>

        <Form form={form} layout="vertical" onFinish={onLogin}>
          <div className="form-wrapper">
            <h1 className="text-center mb-4">
              <Link to="/login">LOGO</Link>
            </h1>

            {error && (
              <Alert
                message={error}
                type="error"
                showIcon
                closable
                className="mb-4"
              />
            )}

            <Form.Item
              name="email"
              rules={[{ required: true, message: t('message.required') }]}
            >
              <Input
                ref={refEmail}
                size="large"
                title={t('login.email')}
                placeholder={t('login.email')}
                className="input-gray"
                onChange={() => setError('')}
              />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[{ required: true, message: t('message.required') }]}
            >
              <Input.Password
                size="large"
                title={t('login.password')}
                placeholder={t('login.password')}
                className="input-gray"
                onChange={() => setError('')}
              />
            </Form.Item>

            <Form.Item className="text-center mb-0">
              <Button
                type="primary"
                htmlType="submit"
                shape="round"
                size="large"
                icon={<LoginOutlined />}
                loading={loadingLogin}
                className="min-w-150"
              >
                {t('login.login')}
              </Button>
            </Form.Item>
          </div>
        </Form>
      </div>
    </div>
  );
};
