import React from 'react';
import { Route, Switch } from 'react-router';

import Index from './p-index';

import './style.scss';

export default function Home() {
  return (
    <Switch>
      <Route path="/" component={Index} />
    </Switch>
  );
}
