import React from 'react';
import { useTranslation } from 'react-i18next';
import { Modal } from 'antd';

import { PlusCircle } from '../../../assets/svg-icons';
import { TrademarkDetail } from './trademark-detail';

export const TrademarkModal = ({
  visible,
  className = '',
  onCreateTrademark,
  onCancel,
  ...rest
}) => {
  // For language
  const [t] = useTranslation('cartLanguage');

  /**
   * Title modal
   *
   * @return {object} - Element
   */
  const TitleModal = () => {
    return (
      <div className="title-modal">
        <PlusCircle /> {t('trademark.addTrademark')}
      </div>
    );
  };

  return (
    <Modal
      title={<TitleModal />}
      visible={visible}
      width={900}
      maskClosable={false}
      keyboard={false}
      footer={null}
      forceRender // For fix: Instance created by `useForm` is not connected to any Form element.
      centered // For "modal-fixed-header"
      wrapClassName="modal-fixed-header" // Enable "centered" mode, wrap content by class "modal-body-with-scroll"
      className={`c-trademark-modal p-0-modal-body modal-footer-common ${className}`}
      onCancel={onCancel}
      {...rest}
    >
      <TrademarkDetail
        onSaveTrademark={onCreateTrademark}
        onCancel={onCancel}
      />
    </Modal>
  );
};
