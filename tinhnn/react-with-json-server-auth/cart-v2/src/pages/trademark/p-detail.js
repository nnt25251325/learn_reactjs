import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { ArrowLeftOutlined } from '@ant-design/icons';

import { TrademarkDetail } from './components/trademark-detail';

export default function DetailPage() {
  const urlParams = useParams();

  // For language
  const [t] = useTranslation('cartLanguage');

  // For trademark action
  const getTrademarkById = useStoreActions(
    action => action.trademark.getTrademarkById
  );
  const updateTrademark = useStoreActions(
    action => action.trademark.updateTrademark
  );
  const editingTrademark = useStoreState(
    state => state.trademark.editingTrademark
  );
  const loadingItem = useStoreState(state => state.trademark.loadingItem);

  /**
   * On update trademark
   */
  const onUpdateTrademark = async formData => {
    if (loadingItem || !formData) return;

    const res = await updateTrademark(formData);

    if (res) {
      getTrademarkById(urlParams?.id);
    }
  };

  /**
   * Render title page
   */
  const renderTitlePage = () => {
    return (
      <h4 className="ant-typography mb-3" style={{ minHeight: 28 }}>
        <Link
          to={{
            pathname: '/trademark'
          }}
          title={t('common.back')}
          className="text-gray text-hover-primary mr-3"
          style={{ fontSize: '0.9em' }}
        >
          <ArrowLeftOutlined />
        </Link>

        {editingTrademark?.name}
      </h4>
    );
  };

  return (
    <>
      <div className="p-trademark-detail">
        {renderTitlePage()}

        <TrademarkDetail
          id={urlParams?.id}
          onSaveTrademark={onUpdateTrademark}
        />

        <div className="text-center mt-3">
          <Link
            to={{
              pathname: '/trademark'
            }}
            className="ant-btn min-w-120"
            style={{ fontSize: '0.9em' }}
          >
            <ArrowLeftOutlined /> {t('common.back')}
          </Link>
        </div>
      </div>
    </>
  );
}
