import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';

import List from './p-list';
import Detail from './p-detail';

import './style.scss';

export default function Trademark() {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={`${match.path}/:id`} component={Detail} />
      <Route path="/trademark*" component={List} />
    </Switch>
  );
}
