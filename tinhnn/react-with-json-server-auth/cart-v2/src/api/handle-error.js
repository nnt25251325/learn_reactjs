import i18next from 'i18next';
import { notification } from 'antd';

/**
 * Handle error
 */
export const handleError = err => {
  if (!err) return;

  // For throw new Error(your_message_string) on model.js
  if (typeof err === 'object' && err.message) {
    notification.error({
      message: i18next.t('cartLanguage:common.error'),
      description: err.message
    });
  }
};
