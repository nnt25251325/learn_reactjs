import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useStoreActions } from 'easy-peasy';

import Default from './default';
import Admin from './admin';
import Login from './login';
import Blank from './blank';

import store from '../store/store';
import { modeLogin } from '../pages/login/models/model';
import { model as modelTrademark } from '../pages/trademark/models/model';

import { LS_CURRENT_USER, LS_TOKEN } from '../constants';
import { reactLocalStorage } from '../common';
import { RouterLoading } from '../components';

import './style.scss';

// Add models
store.addModel('auth', modeLogin);
store.addModel('trademark', modelTrademark);

export default function Layout({ children }) {
  const location = useLocation();

  const token = reactLocalStorage.get(LS_TOKEN);
  const localCurrentUser = reactLocalStorage.get(LS_CURRENT_USER);

  // For global user info action
  const getGlobalUserInfo = useStoreActions(
    action => action.global.getGlobalUserInfo
  );

  /**
   * Get user info, set to localStorage
   */
  useEffect(() => {
    if (localCurrentUser) {
      getGlobalUserInfo(localCurrentUser);
    }
  }, [getGlobalUserInfo]);

  /**
   * Return
   */
  if (
    location.pathname.split('/')[1] === 'login' ||
    location.pathname.split('/')[1] === 'register'
  ) {
    if (token && localCurrentUser) {
      window.location.href = '/';
      return;
    }

    return <Login>{children}</Login>;
  }

  if (!token || !localCurrentUser) {
    window.location.href = '/login';

    return <RouterLoading />;
  }

  if (
    location.pathname.split('/')[1] === '403' ||
    location.pathname.split('/')[1] === '404'
  ) {
    return <Blank>{children}</Blank>;
  }

  if (
    location.pathname.split('/')[1] === 'user' ||
    location.pathname.split('/')[1] === 'trademark'
  ) {
    return <Admin>{children}</Admin>;
  }

  return <Default>{children}</Default>;
}
