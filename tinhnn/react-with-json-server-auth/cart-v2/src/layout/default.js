import React, { Suspense } from 'react';
import { Layout } from 'antd';

import { RouterLoading, MainHeader, MainFooter } from '../components';

const { Content } = Layout;

export default function DefaultLayout({ children }) {
  return (
    <Suspense fallback={<RouterLoading />}>
      <Layout className="l-default">
        <MainHeader />

        <Layout className="main-body">
          <Content className="main-content">{children}</Content>
        </Layout>

        <MainFooter />
      </Layout>
    </Suspense>
  );
}
