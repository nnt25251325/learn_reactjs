import React, { Suspense } from 'react';
import { Layout } from 'antd';

import {
  RouterLoading,
  LeftSidebar,
  MainHeader,
  MainFooter
} from '../components';

const { Content } = Layout;

export default function DefaultLayout({ children }) {
  return (
    <Suspense fallback={<RouterLoading />}>
      <Layout className="l-admin">
        <MainHeader />

        <Layout className="main-body">
          <LeftSidebar />

          <Layout>
            <Content className="main-content">{children}</Content>
            <MainFooter />
          </Layout>
        </Layout>
      </Layout>
    </Suspense>
  );
}
