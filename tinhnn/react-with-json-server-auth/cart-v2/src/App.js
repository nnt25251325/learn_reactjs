import React, { useEffect } from 'react';
import { StoreProvider } from 'easy-peasy';
import i18next from 'i18next';
import { I18nextProvider } from 'react-i18next';

import en from './translations/en.json';
import vi from './translations/vi.json';

import 'antd/dist/antd.min.css';
// import 'antd/dist/antd.dark.css';
import './assets/scss/style.scss';

import { LS_LANGUAGE } from './constants';
import { reactLocalStorage } from './common';
import store from './store/store';
import Layout from './layout';
import Pages from './pages';

// Init i18next - Language
i18next.init({
  interpolation: { escapeValue: false }, // React already does escaping
  lng: 'en', // language to use
  resources: {
    en: { cartLanguage: en },
    vi: { cartLanguage: vi }
  }
});

export default function App() {
  const localLanguage = reactLocalStorage.get(LS_LANGUAGE);

  /**
   * Set language to i18next
   */
  useEffect(() => {
    let lang = 'en';

    if (localLanguage) {
      lang = localLanguage;
    } else {
      reactLocalStorage.set(LS_LANGUAGE, lang);
    }

    i18next.changeLanguage(lang);
  }, []);

  return (
    <div className="App">
      <StoreProvider store={store}>
        <I18nextProvider i18n={i18next}>
          <Layout>
            <Pages />
          </Layout>
        </I18nextProvider>
      </StoreProvider>
    </div>
  );
}
