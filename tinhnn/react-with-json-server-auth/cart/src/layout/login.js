import React from 'react';

import './style.scss';

export default ({ children }) => {
  return <div className="l-login">{children}</div>;
};
