import React from 'react';
import { EnFlag, VnFlag } from '../assets/svg-icons';

export * from './endpoints';

/**
 * Config localStorage, sessionStorage variables
 */
export const LS_TOKEN = 'token';
export const LS_CURRENT_USER = 'currentUser';
export const LS_LANGUAGE = 'lang';

/**
 * Language list
 */
export const LANGUAGES = [
  { label: 'common.english', value: 'en', flag: <EnFlag /> },
  { label: 'common.vietnamese', value: 'vi', flag: <VnFlag /> }
];

/**
 * Max length (for validatiton)
 */
export const MAX_LENGTH_TITLE = 128;

/**
 * List of number record in one page
 */
export const PAGE_SIZES = [10, 20, 50, 100];

/**
 * Number record in one page
 */
export const PAGE_SIZE = PAGE_SIZES[0];
export const PAGE_SIZE_5 = 5;

/**
 * Default sort value
 */
export const DEFAULT_SORT = 'id|desc';
/**
 * Patterns
 */
export const EMAIL_PATTERN =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/gim;
export const URL_PATTERN =
  /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\\-\\-.]{1}[a-z0-9]+)*\.[a-z]{2,}(:[0-9]{1,5})?(\/.*)?$/;
export const ESTIMATED_TIME_PATTERN = /^(([0-9])*(\.)*([0-9])+([wdhm])(\s)*)+$/; // Example: 1w 1.2h 5d 2.5h 32m 8h
export const NUMBER_PATTERN = /^-?[0-9]\d*(\.\d+)?$/; // Integer, Float
export const PASSWORD_PATTERN = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;

/**
 * Date format
 */
export const SHORT_DATE_FORMAT = 'MM/DD/YYYY';
