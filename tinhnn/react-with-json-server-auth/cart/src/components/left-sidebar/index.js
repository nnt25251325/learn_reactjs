import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Layout, Menu } from 'antd';
import { UserOutlined, TrademarkOutlined } from '@ant-design/icons';

import { buildPathNameList } from '../../common';

import './style.scss';

const { Sider } = Layout;

export const LeftSidebar = () => {
  const location = useLocation();

  // For language
  const [t] = useTranslation('myCart');

  // // For global user info action
  // const globalUserInfo = useStoreState(state => state.global.globalUserInfo);
  // const loadingGlobalUserInfo = useStoreState(state => state.global.loadingGlobalUserInfo);

  // Other state
  const [menus, setMenus] = useState([]);
  const [collapsed, setCollapsed] = useState(false);
  const [openKeys, setOpenKeys] = useState([]);

  /**
   * Watching change of globalRoleInfo
   */
  useEffect(() => {
    const newMenus = [
      {
        path: '/user',
        name: 'menu.userManagement',
        icon: <UserOutlined />
      },
      {
        path: '/trademark',
        name: 'menu.trademarkManagement',
        icon: <TrademarkOutlined />
      }
    ];

    setMenus(newMenus);
  }, [t]);

  return (
    <Sider
      collapsible
      collapsed={collapsed}
      theme="light"
      width={220}
      className="c-left-sidebar"
      onCollapse={() => setCollapsed(!collapsed)}
    >
      {Array.isArray(menus) && menus.length > 0 && (
        <Menu
          selectedKeys={buildPathNameList(location.pathname)}
          openKeys={openKeys}
          mode="inline"
          theme="light"
          onOpenChange={setOpenKeys}
        >
          {menus.map(item => {
            return (
              <Menu.Item key={item.path} icon={item.icon}>
                <Link to={item.path} title={t(`${item.name}`)}>
                  {t(`${item.name}`)}
                </Link>
              </Menu.Item>
            );
          })}
        </Menu>
      )}
    </Sider>
  );
};
