import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { LS_TOKEN } from '../../constants';
import { reactLocalStorage } from '../../common';

export const PrivateRoute = ({ component: Component, children: Children, allowedRoles = [], forPage, ...rest }) => {
  const token = reactLocalStorage.get(LS_TOKEN);

  return (
    <Route
      render={routeProps => {
        if (!token) {
          return <Redirect to="/403" />;
        }

        // Has token
        if (token && Component) {
          return <Component {...routeProps} />;
        }

        // Has token
        if (token && Children) {
          return <Children {...routeProps} />;
        }
      }}
      {...rest}
    />
  );
};
