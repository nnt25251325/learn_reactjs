import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Result } from 'antd';
import { HomeOutlined } from '@ant-design/icons';

import './style.scss';

export default function Error404() {
  // For language
  const [t] = useTranslation('myCart');

  return (
    <Result
      status="404"
      title="404"
      subTitle={t('message.pageNotFound')}
      className="p-404"
      extra={
        <Button type="primary" href="/">
          <HomeOutlined /> {t('common.backHome')}
        </Button>
      }
    />
  );
}
