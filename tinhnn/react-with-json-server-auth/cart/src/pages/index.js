import React, { lazy, useEffect, useState } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { ConfigProvider } from 'antd';

// Language
import i18next from 'i18next';
import enUS from 'antd/lib/locale/en_US';
import viVN from 'antd/lib/locale/vi_VN';
import moment from 'moment';
import 'moment/locale/en-gb';
import 'moment/locale/fr';
import 'moment/locale/ja';
import 'moment/locale/vi';

import { LS_LANGUAGE } from '../constants';
import { reactLocalStorage } from '../common';
import { PrivateRoute } from '../components';

const Login = lazy(() => import('./login'));
const Home = lazy(() => import('./home'));
const Trademark = lazy(() => import('./trademark'));
const Error403 = lazy(() => import('./error-403'));
const Error404 = lazy(() => import('./error-404'));

export default function Page() {
  const localLanguage = reactLocalStorage.get(LS_LANGUAGE);

  // For global language action
  const setGlobalLanguage = useStoreActions(action => action.global.setGlobalLanguage);
  const globalLanguage = useStoreState(state => state.global.globalLanguage);

  // Other state
  const [locale, setLocale] = useState(null);

  /**
   * Set global language by "lang" in localStorage
   */
  useEffect(() => {
    setGlobalLanguage(localLanguage);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /**
   * Watching change of globalLanguage in STORE
   * Set language to localStorage
   * Language for i18next, for ant design locate, for moment
   */
  useEffect(() => {
    if (!globalLanguage) return;

    if (globalLanguage === 'en') {
      setLocale(enUS);
      moment.locale('en');
    } else if (globalLanguage === 'vi') {
      setLocale(viVN);
      moment.locale('vi');
    }

    i18next.changeLanguage(globalLanguage);
    reactLocalStorage.set(LS_LANGUAGE, globalLanguage);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalLanguage]);

  return (
    <ConfigProvider locale={locale}>
      <Switch>
        <PrivateRoute exact path="/" component={Home} />

        <PrivateRoute path="/trademark" component={Trademark} />

        <Route path="/login" component={Login} />

        <Route path="/403" component={Error403} />

        <Route path="/404" component={Error404} />

        <Redirect to="/404" />
      </Switch>
    </ConfigProvider>
  );
}
