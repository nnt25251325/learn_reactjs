import { action, thunk } from 'easy-peasy';
import i18next from 'i18next';
import { notification } from 'antd';

import { ENDPOINTS } from '../../../constants';
import { Http, handleError } from '../../../api';
import { buildQueryForParamsUrl } from '../../../common';

export const model = {
  /**
   * State
   */
  data: [],
  total: 0,
  editingTrademark: {},
  query: {},
  loadingList: false,
  loadingItem: false,

  /**
   * Action: Set trademark list
   */
  setTrademarkList: action((state, payload) => {
    if (state?.data === undefined || state?.total === undefined) return;

    if (!(Array.isArray(payload?.data) && payload.data.length)) {
      state.data = [];
      state.total = 0;
      return;
    }

    state.data = payload.data;
    state.total = payload.headers['x-total-count'];
  }),

  /**
   * Action: Set editing trademark
   */
  setEditingTrademark: action((state, payload) => {
    if (state?.editingTrademark === undefined) return;

    if (!payload?.data) {
      console.log(111);
      state.editingTrademark = {};
      return;
    }

    state.editingTrademark = payload.data;
  }),

  /**
   * Action: Set query
   */
  setQuery: action((state, payload) => {
    if (state?.query === undefined) return;

    state.query = payload;
  }),

  /**
   * Action: Set loading list
   */
  setLoadingList: action((state, payload) => {
    if (state?.loadingList === undefined) return;

    state.loadingList = payload;
  }),

  /**
   * Action: Set loading get/update item
   */
  setLoadingItem: action((state, payload) => {
    if (state?.loadingItem === undefined) return;

    state.loadingItem = payload;
  }),

  /**
   * Action: Call api to get trademark list
   */
  getTrademarkList: thunk(async (action, payload) => {
    try {
      action.setLoadingList(true);
      action.setQuery(payload);

      const url = `${ENDPOINTS.TRADEMARKS}?${buildQueryForParamsUrl(payload)}`;
      const res = await Http.get(url);

      action.setTrademarkList(res);
    } catch (err) {
      handleError(err);
    } finally {
      action.setLoadingList(false);
    }
  }),

  /**
   * Action: Call api to get trademark by Id
   */
  getTrademarkById: thunk(async (action, payload) => {
    try {
      if (!payload) throw new Error('There are no trademark ID');

      action.setLoadingItem(true);

      const url = `${ENDPOINTS.TRADEMARKS}/${payload}`;
      const res = await Http.get(url);

      console.log('111', res);

      action.setEditingTrademark(res);
    } catch (err) {
      handleError(err);
    } finally {
      action.setLoadingItem(false);
    }
  }),

  /**
   * Action: Call api to create trademark
   */
  createTrademark: thunk(async (action, payload) => {
    try {
      if (!(payload !== null && typeof payload === 'object')) {
        throw new Error('Invalid Payload');
      }

      action.setLoadingItem(true);

      const url = ENDPOINTS.TRADEMARKS;
      const res = await Http.post(url, payload);

      notification.success({
        message: i18next.t('myCart:trademark.messageCreatingTrademark'),
        description: i18next.t('myCart:message.createdSuccessfully')
      });

      return res;
    } catch (err) {
      handleError(err);
    } finally {
      action.setLoadingItem(false);
    }
  }),

  /**
   * Action: Call api to update trademark
   */
  updateTrademark: thunk(async (action, payload) => {
    try {
      if (!payload?.id) throw new Error('There are no trademark ID');

      action.setLoadingItem(true);

      const url = `${ENDPOINTS.TRADEMARKS}/${payload.id}`;
      const res = await Http.put(url, payload);

      notification.success({
        message: i18next.t('myCart:trademark.messageUpdatingTrademark'),
        description: i18next.t('myCart:message.updatedSuccessfully')
      });

      return res;
    } catch (err) {
      handleError(err);
    } finally {
      action.setLoadingItem(false);
    }
  }),

  /**
   * Action: Call api to delete trademark
   */
  deleteTrademark: thunk(async (action, payload) => {
    try {
      if (!payload) throw new Error('There are no trademark ID');

      const url = `${ENDPOINTS.TRADEMARKS}/${payload}`;
      await Http.delete(url);

      notification.success({
        message: i18next.t('myCart:trademark.messageDeletingTrademark'),
        description: i18next.t('myCart:message.deletedSuccessfully')
      });

      return true;
    } catch (err) {
      handleError(err);
    }
  })
};
