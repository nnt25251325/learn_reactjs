import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Card, Button, Tooltip, Row, Col, Table, Spin, Modal } from 'antd';
import {
  ReloadOutlined,
  DeleteOutlined,
  Loading3QuartersOutlined
} from '@ant-design/icons';

import { DEFAULT_SORT, PAGE_SIZE, PAGE_SIZES } from '../../constants';
import { TrademarkModal } from './components/trademark-modal';
import { TrademarkSearchForm } from './components/trademark-search-form';

export default function ListPage() {
  // For language
  const [t] = useTranslation('myCart');

  // For trademark action
  const getTrademarkList = useStoreActions(
    action => action.trademark.getTrademarkList
  );
  const createTrademark = useStoreActions(
    action => action.trademark.createTrademark
  );
  const deleteTrademark = useStoreActions(
    action => action.trademark.deleteTrademark
  );
  const setTrademarkList = useStoreActions(
    action => action.trademark.setTrademarkList
  );
  const data = useStoreState(state => state.trademark.data);
  const total = useStoreState(state => state.trademark.total);
  const loadingList = useStoreState(state => state.trademark.loadingList);
  const loadingItem = useStoreState(state => state.trademark.loadingItem);

  // For query condition
  const setQuery = useStoreActions(action => action.trademark.setQuery);
  const query = useStoreState(state => state.trademark.query);

  // Other state
  const [columns, setColumns] = useState([]);
  const [sortedInfo, setSortedInfo] = useState({});
  const [visibleTrademarkModal, setVisibleTrademarkModal] = useState(false);

  /**
   * All of columns for table
   */
  useEffect(() => {
    const allColumns = [
      {
        dataIndex: 'id',
        title: t('trademark.id'),
        width: 70,
        sorter: () => {},
        sortOrder: sortedInfo.field === 'id' && sortedInfo.order
      },
      {
        dataIndex: 'name',
        title: t('trademark.name'),
        width: 330,
        sorter: () => {},
        sortOrder: sortedInfo.field === 'name' && sortedInfo.order,
        render: (value, item) => (
          <Link
            to={{ pathname: `/trademark/${item.id}` }}
            className="d-block text-primary cursor-pointer over min-height-22"
          >
            {value}
          </Link>
        )
      },
      {
        dataIndex: 'description',
        title: t('trademark.description'),
        ellipsis: true,
        width: 230,
        sorter: () => {},
        sortOrder: sortedInfo.field === 'description' && sortedInfo.order
      },
      {
        dataIndex: 'action',
        title: t('trademark.action'),
        fixed: 'right',
        width: 75,
        className: 'text-nowrap text-center',
        render: (value, item) => renderAction(item)
      }
    ];

    setColumns(allColumns);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadingList, loadingItem]);

  /**
   * Render action
   *
   * @return {Object} - Element
   */
  const renderAction = item => {
    if (!item?.id) return null;

    return (
      <>
        <Tooltip
          placement="top"
          title={t('common.delete')}
          destroyTooltipOnHide={true}
        >
          <Button
            type="link"
            danger
            icon={<DeleteOutlined />}
            size="small"
            onClick={() => onDeleteTrademark(item)}
          ></Button>
        </Tooltip>
      </>
    );
  };

  /**
   * On search
   */
  const onSearch = params => {
    if (loadingList) return;

    getTrademarkList(params);
  };

  /**
   * Get list when mounted. F5 browser
   */
  useEffect(() => {
    const newQuery = {
      page: 1,
      limit: PAGE_SIZE,
      order: DEFAULT_SORT
    };

    onSearch(newQuery);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query]);

  /**
   * On table change
   */
  const onTableChange = (pagination, filters, sorter) => {
    const newQuery = {
      page: pagination?.current,
      limit: pagination?.pageSize,
      sort: sorter.order
        ? `${sorter.field}|${sorter.order === 'ascend' ? 'asc' : 'desc'}`
        : DEFAULT_SORT
    };

    setSortedInfo(sorter);
    onSearch({ ...query, ...newQuery });
  };

  /**
   * On create trademark
   */
  const onCreateTrademark = async formData => {
    if (loadingItem || loadingList || !formData) return;

    const res = await createTrademark(formData);

    // On close detail
    if (res) {
      setVisibleTrademarkModal(false);
      onSearch(query);
    }
  };

  /**
   * On delete trademark
   */
  const onDeleteTrademark = async item => {
    if (!item) return;

    Modal.confirm({
      title: t('common.warning'),
      content: t('message.areYouSureDelete', { name: item.name }),
      autoFocusButton: null,
      maskClosable: true,
      okText: t('common.delete'),
      okButtonProps: {
        danger: true,
        type: 'primary',
        icon: <DeleteOutlined />
      },
      cancelText: t('common.cancel'),
      onOk: async () => {
        const res = await deleteTrademark(item.id);

        if (res) onSearch(query);
      }
    });
  };

  /**
   * Unmount
   */
  useEffect(() => {
    return () => {
      setQuery({});
      setTrademarkList([]);
    };
  }, [setQuery, setTrademarkList]);

  return (
    <>
      <div className="p-trademark-list">
        <h4 className="ant-typography mb-3">{t('menu.trademarkManagement')}</h4>

        <Card>
          <div className="position-relative">
            <TrademarkSearchForm
              onAdd={() => setVisibleTrademarkModal(true)}
              onSearch={params => onSearch({ ...query, ...params })}
            />

            <Row
              align="middle"
              justify="end"
              className="table-title-style px-3 py-1"
            >
              <Col>
                <Tooltip
                  placement="top"
                  title={t('common.reload')}
                  destroyTooltipOnHide={true}
                >
                  <Button
                    type="link"
                    icon={<ReloadOutlined />}
                    className="btn-reload-on-top-table"
                    onClick={() => {
                      onSearch(query);
                    }}
                  ></Button>
                </Tooltip>
              </Col>
            </Row>
          </div>

          <div className="c-basic-table">
            <Table
              columns={columns}
              dataSource={data}
              rowKey="id"
              pagination={{
                total,
                current: query?.page,
                pageSize: query?.limit,
                pageSizeOptions: PAGE_SIZES,
                showSizeChanger: true,
                showTotal: (totalnumber, range) => {
                  return t('common.showTotalOfTable', {
                    start: range[0],
                    end: range[1],
                    total: totalnumber
                  });
                }
              }}
              loading={{
                spinning: loadingList,
                indicator: (
                  <Spin indicator={<Loading3QuartersOutlined spin />} />
                )
              }}
              onChange={onTableChange}
            />
          </div>
        </Card>
      </div>

      {/* This modal only for create trademark */}
      {visibleTrademarkModal && (
        <TrademarkModal
          visible={visibleTrademarkModal}
          onCreateTrademark={onCreateTrademark}
          onCancel={() => setVisibleTrademarkModal(false)}
        />
      )}
    </>
  );
}
