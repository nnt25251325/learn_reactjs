import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Form, Spin, Input, Button, Tooltip } from 'antd';
import {
  SaveOutlined,
  ReloadOutlined,
  Loading3QuartersOutlined
} from '@ant-design/icons';

import { MAX_LENGTH_TITLE } from '../../../constants';
import { scrollToFirstClassName } from '../../../common';

export const TrademarkDetail = ({
  id,
  onSaveTrademark,
  onCancel,
  className = '',
  ...rest
}) => {
  const [form] = Form.useForm();
  const refName = useRef(null);

  // For language
  const [t] = useTranslation('myCart');

  // For trademark action
  const getTrademarkById = useStoreActions(
    action => action.trademark.getTrademarkById
  );
  const setEditingTrademark = useStoreActions(
    action => action.trademark.setEditingTrademark
  );
  const editingTrademark = useStoreState(
    state => state.trademark.editingTrademark
  );
  const loadingItem = useStoreState(state => state.trademark.loadingItem);
  const loadingList = useStoreState(state => state.trademark.loadingList);

  /**
   * Focus in name input
   */
  useEffect(() => {
    if (id) return;

    refName?.current && refName.current.focus();
  }, [id]);

  /**
   * Get trademark by id and set editing trademark data to Store
   */
  useEffect(() => {
    if (!id) return;

    getTrademarkById(id);
  }, [id, getTrademarkById]);

  /**
   * Set editing trademark to form
   */
  useEffect(() => {
    if (!editingTrademark) return;

    if (editingTrademark.id) {
      form.setFieldsValue({
        name: editingTrademark.name,
        description: editingTrademark.description
      });
    }
  }, [form, editingTrademark]);

  /**
   * On reload
   */
  const onReload = () => {
    if (!id) {
      form.resetFields();
    }

    if (id) {
      setEditingTrademark({});
      getTrademarkById(id);
    }
  };

  /**
   * On submit
   */
  const onSubmit = values => {
    if (loadingItem || loadingList) return;

    const formData = {};

    if (id) {
      formData.id = id;
    }

    if (values.name) {
      formData.name = values.name;
    }

    if (values.description) {
      formData.description = values.description;
    }

    onSaveTrademark(formData);
  };

  /**
   * Unmount
   */
  useEffect(() => {
    return () => {
      form.resetFields();
      setEditingTrademark({});
    };
  }, [form, setEditingTrademark]);

  return (
    <div
      className={`c-trademark-detail position-relative ${className}`}
      {...rest}
    >
      <Tooltip
        placement="top"
        title={t('common.reload')}
        destroyTooltipOnHide={true}
      >
        <Button
          type="link"
          icon={<ReloadOutlined />}
          className="btn-reload-on-modal btn-reload-on-detail-page"
          onClick={onReload}
        ></Button>
      </Tooltip>

      <div className={id ? 'ant-card ant-card-bordered' : ''}>
        <div className={id ? 'ant-card-body mx-auto max-w-900 pt-5' : ''}>
          <Form
            form={form}
            labelCol={{ sm: 7 }}
            wrapperCol={{ sm: 17 }}
            className="label-on-left"
            onFinish={onSubmit}
          >
            <div className="modal-body-with-scroll">
              <div className={`form-wrapper ${!id ? 'p-4' : ''}`}>
                <Spin
                  indicator={<Loading3QuartersOutlined spin />}
                  spinning={loadingItem}
                >
                  <Form.Item
                    label={t('trademark.name')}
                    name="name"
                    rules={[
                      {
                        required: true,
                        whitespace: true,
                        message: t('message.required')
                      },
                      {
                        max: MAX_LENGTH_TITLE,
                        message: t('message.maxLength', {
                          max: MAX_LENGTH_TITLE
                        })
                      }
                    ]}
                  >
                    <Input
                      ref={refName}
                      placeholder={t('trademark.enterName')}
                    />
                  </Form.Item>

                  <Form.Item
                    label={t('trademark.description')}
                    name="description"
                  >
                    <Input.TextArea
                      rows="6"
                      placeholder={t('trademark.enterDescription')}
                    />
                  </Form.Item>
                </Spin>
              </div>
            </div>

            {/* ------------------------------ FOR ADD ------------------------------ */}

            <div className="ant-modal-footer text-center">
              <Button
                htmlType="submit"
                type="primary"
                icon={<SaveOutlined />}
                loading={loadingItem}
                onClick={() =>
                  scrollToFirstClassName('ant-form-item-has-error')
                }
              >
                {t('common.save')}
              </Button>

              {!id && (
                <Button
                  onClick={() => {
                    if (typeof onCancel === 'function') onCancel();
                  }}
                >
                  {t('common.cancel')}
                </Button>
              )}
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
};
