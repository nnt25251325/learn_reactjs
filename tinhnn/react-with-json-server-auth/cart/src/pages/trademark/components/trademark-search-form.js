import React from 'react';
import { useTranslation } from 'react-i18next';
import { Row, Col, Form, Input, Button } from 'antd';
import { PlusOutlined, SearchOutlined, ClearOutlined } from '@ant-design/icons';
import { useStoreState } from 'easy-peasy';

export const TrademarkSearchForm = ({ onAdd, onSearch }) => {
  const [form] = Form.useForm();

  // For language
  const [t] = useTranslation('myCart');

  // For trademark action
  const loadingList = useStoreState(state => state.trademark.loadingList);

  /**
   * On submit and search
   */
  const onSubmit = values => {
    if (loadingList || !values) return;

    const filters = {};

    if (values.id) {
      filters.id = values.id;
    }

    if (values.name) {
      filters.name_like = values.name;
    }

    if (values.description) {
      filters.description_like = values.description;
    }

    onSearch({
      page: 1,
      filters
    });
  };

  return (
    <div className="c-trademark-search-form mb-2">
      <Form form={form} layout="vertical" onFinish={onSubmit}>
        <Row gutter={30}>
          <Col xs={24} sm={12} md={8} xl={6}>
            <Form.Item
              label={t('common.id')}
              name="id"
              rules={[
                {
                  pattern: /^[0-9]*$/,
                  message: t('message.fieldMustBeAnInteger')
                }
              ]}
            >
              <Input placeholder={t('common.id')} />
            </Form.Item>
          </Col>

          <Col xs={24} sm={12} md={8} xl={6}>
            <Form.Item label={t('trademark.name')} name="name">
              <Input placeholder={t('trademark.name')} />
            </Form.Item>
          </Col>

          <Col xs={24} sm={12} md={8} xl={6}>
            <Form.Item label={t('trademark.description')} name="description">
              <Input placeholder={t('trademark.description')} />
            </Form.Item>
          </Col>

          <Col xs={24} sm={12} md={8} xl={6}>
            <Form.Item label="Invisible Label" className="invisible-label">
              <Button
                htmlType="submit"
                type="primary"
                icon={<SearchOutlined />}
                className="min-w-100 mr-2"
              >
                {t('common.search')}
              </Button>

              <Button
                icon={<ClearOutlined />}
                className="min-w-100"
                onClick={() => {
                  form.resetFields();
                  setTimeout(() => form.submit(), 200);
                }}
              >
                {t('common.reset')}
              </Button>
            </Form.Item>
          </Col>
        </Row>

        <Row justify="end" className="top-table-btn">
          <Button
            type="primary"
            icon={<PlusOutlined />}
            className="btn-add mt-1 mb-1 ml-2"
            onClick={onAdd}
          >
            {t('trademark.createTrademark')}
          </Button>
        </Row>
      </Form>
    </div>
  );
};
