import Axios from 'axios';
import i18next from 'i18next';
import { Modal, notification } from 'antd';

import { LS_CURRENT_USER, LS_TOKEN } from '../constants';

const token = localStorage.getItem(LS_TOKEN);

/**
 * Axios create
 */
const Http = Axios.create({
  baseURL: process.env.REACT_APP_API_ENDPOINT,
  headers: { Authorization: `Bearer ${token}` }
});

/**
 * Response intorceptor
 */
Http.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    const status = error?.response?.status;

    if (status === 401) {
      Modal.error({
        title: error?.response?.statusText ? error?.response?.statusText : i18next.t('myCart:common.error'),
        content: i18next.t('myCart:common.sessionTimeout'),
        autoFocusButton: 'ok',
        zIndex: 9999,
        okText: i18next.t('myCart:common.ok'),
        onOk: () => goToLogin()
      });
    } else {
      console.log(11111, error.response.statusText);
      notification.error({
        message: error?.response?.statusText ? error.response.statusText : i18next.t('myCart:common.error'),
        description: typeof error?.response?.data === 'string' ? error.response.data : ''
      });
    }

    return Promise.reject(error.response ? error.response : error);
  }
);

/**
 * Go to login
 */
const goToLogin = () => {
  localStorage.removeItem(LS_TOKEN);
  localStorage.removeItem(LS_CURRENT_USER);

  window.location.href = '/login';
};

export { Http };
