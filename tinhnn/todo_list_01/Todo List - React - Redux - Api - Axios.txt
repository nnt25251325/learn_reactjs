﻿Todo List - React - Redux - Api - Axios
https://github.com/nnt25251325/todo_list_react_redux_v1/
http://nnt25251325.freevnn.com/todo_list_react_redux_v1/
http://nnt25251325.0fees.us/todo_list_react_redux_v1/

//api tasks_todo_list_01
[
  {
    "id": "2",
    "name": "Ông Mặt Trời",
    "status": true
  },
  {
    "id": "3",
    "name": "Evolable",
    "status": false
  },
  {
    "id": "5",
    "name": "Người đến từ Thiên Đàng",
    "status": true
  },
  {
    "id": "7",
    "name": "Chú voi con",
    "status": false
  },
  {
    "id": "8",
    "name": "Học Viện Hàng Không",
    "status": true
  },
  {
    "id": "9",
    "name": "Đại học Quốc Gia Hà Nội",
    "status": false
  },
  {
    "id": "10",
    "name": "Ngữ Văn",
    "status": false
  },
  {
    "id": "11",
    "name": "Nguyễn Như Khôi",
    "status": false
  },
  {
    "id": "12",
    "name": "Kiều Phong",
    "status": false
  },
  {
    "id": "13",
    "name": "Đại Học Kiến Trúc",
    "status": false
  },
  {
    "id": "14",
    "name": "Đại Học Thủy Lợi",
    "status": true
  },
  {
    "id": "24",
    "name": "Tia nắng khẽ qua khe cửa",
    "status": true
  },
  {
    "id": "25",
    "name": "Hai con thằn lằn con\t",
    "status": false
  },
  {
    "id": "27",
    "name": "Học lập trình ReactJS\t",
    "status": false
  },
  {
    "id": "28",
    "name": "Lỗ Trí Thâm",
    "status": true
  },
  {
    "id": "29",
    "name": "Iron Man (2008)",
    "status": true
  },
  {
    "id": "30",
    "name": "The Incredible Hulk (2008)",
    "status": true
  },
  {
    "id": "31",
    "name": "Iron Man 2 (2010)",
    "status": true
  },
  {
    "id": "32",
    "name": "Thor (2011)",
    "status": true
  },
  {
    "id": "33",
    "name": "Captain America: The First Avenger (2011)",
    "status": true
  },
  {
    "id": "34",
    "name": "Marvel's The Avengers (2012)",
    "status": true
  },
  {
    "id": "35",
    "name": "Iron Man 3 (2013)",
    "status": true
  },
  {
    "id": "36",
    "name": "Thor: The Dark World (2013)",
    "status": true
  },
  {
    "id": "37",
    "name": "Captain America: The Winter Soldier (2014)",
    "status": true
  },
  {
    "id": "38",
    "name": "Guardians of the Galaxy (2014)",
    "status": true
  },
  {
    "id": "39",
    "name": "Avengers: Age of Ultron (2015)",
    "status": true
  },
  {
    "id": "40",
    "name": "Ant-Man (2015)",
    "status": true
  },
  {
    "id": "41",
    "name": "Captain America: Civil War (2016)",
    "status": true
  },
  {
    "id": "42",
    "name": "Doctor Strange (2016)",
    "status": true
  },
  {
    "id": "43",
    "name": "Guardians of the Galaxy Vol. 2 (2017)",
    "status": true
  },
  {
    "id": "44",
    "name": "Spider-Man: Homecoming (2017)",
    "status": true
  },
  {
    "id": "45",
    "name": "Thor: Ragnarok (2017)",
    "status": true
  },
  {
    "id": "46",
    "name": "Black Panther (2018)",
    "status": true
  },
  {
    "id": "47",
    "name": "Avengers: Infinity War (2018)",
    "status": true
  },
  {
    "id": "48",
    "name": "Ant-Man and the Wasp (2018)",
    "status": true
  },
  {
    "id": "49",
    "name": "Captain Marvel (2019)",
    "status": true
  },
  {
    "id": "50",
    "name": "Avengers: Endgame (2019)",
    "status": true
  },
  {
    "id": "51",
    "name": "Spider-Man: Far From Home (2019)",
    "status": true
  }
]
