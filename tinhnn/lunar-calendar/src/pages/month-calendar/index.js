/**
 * Chepvang Studio
 */

import React from 'react'
import moment from 'moment'
import {
  Row,
  Col,
  Button,
  Card,
  Checkbox
} from 'antd'

import {
  DoubleLeftOutlined,
  DoubleRightOutlined,
  LeftOutlined,
  RightOutlined,
  DownOutlined
} from '@ant-design/icons'

import {
  DATE_FORMAT,
  FIRST_DAY,
  LAST_DAY,
  DAYS_IN_WEEK,
  TRUC_LIST,

  // Tốt
  NGAY_BAT_TUONG,
  NGAY_SAO_SAT_CONG,
  NGAY_SAO_TRUC_TINH,
  NGAY_SAO_NHAN_DUYEN,
  NGAY_SAO_THIEN_AN,
  NGAY_SAO_THIEN_THUY,
  NGAY_SAO_NGU_HOP,
  NGAY_SAO_THIEN_DUC,
  NGAY_SAO_THIEN_DUC_HOP,
  NGAY_SAO_NGUYET_DUC,
  NGAY_SAO_NGUYET_DUC_HOP,
  NGAY_SAO_THIEN_HY,
  NGAY_SAO_THIEN_PHU,
  NGAY_SAO_THIEN_QUY,
  NGAY_SAO_THIEN_XA,
  NGAY_SAO_SINH_KHI,
  NGAY_SAO_THIEN_PHUC,
  NGAY_SAO_THIEN_THANH,
  NGAY_SAO_THIEN_QUAN,
  NGAY_SAO_THIEN_MA,
  NGAY_SAO_THIEN_TAI,
  NGAY_SAO_DIA_TAI,
  NGAY_SAO_NGUYET_TAI,
  NGAY_SAO_NGUYET_AN,
  NGAY_SAO_NGUYET_KHONG,
  NGAY_SAO_MINH_TINH,
  NGAY_SAO_THANH_TAM,
  NGAY_SAO_NGU_PHU,
  NGAY_SAO_LOC_KHO,
  NGAY_SAO_PHUC_SINH,
  NGAY_SAO_CAT_KHANH,
  NGAY_SAO_AM_DUC,
  NGAY_SAO_U_VI_TINH,
  NGAY_SAO_MAN_DUC_TINH,
  NGAY_SAO_KINH_TAM,
  NGAY_SAO_TUE_HOP,
  NGAY_SAO_NGUYET_GIAI,
  NGAY_SAO_QUAN_NHAT,
  NGAY_SAO_HOAT_DIEU,
  NGAY_SAO_GIAI_THAN,
  NGAY_SAO_PHO_BO,
  NGAY_SAO_ICH_HAU,
  NGAY_SAO_TUC_THE,
  NGAY_SAO_YEU_YEN,
  NGAY_SAO_DICH_MA,
  NGAY_SAO_TAM_HOP,
  NGAY_SAO_LUC_HOP,
  NGAY_SAO_MAU_THUONG,
  NGAY_SAO_PHUC_HAU,
  NGAY_SAO_DAI_HONG_SA,
  NGAY_SAO_DAN_NHAT_THOI_DUC,
  NGAY_SAO_HOANG_AN,
  NGAY_SAO_THANH_LONG,
  NGAY_SAO_MINH_DUONG,
  NGAY_SAO_KIM_DUONG,
  NGAY_SAO_NGOC_DUONG,

  // Xấu
  NGAY_SAT_CHU,
  NGAY_THANG_SAT_CHU,
  NGAY_SAT_CHU_TRONG_4_MUA,
  NGAY_BON_MUA_SAT_CHU,
  NGAY_THO_TU,
  NGAY_LY_SAO,
  NGAY_VANG_VONG,
  NGAY_XICH_TONG_TU,
  NGAY_KHONG_VONG,
  NGAY_HUNG_BAI,
  NGAY_XICH_KHAU,
  NGAY_HOANG_OC_4_MUA,
  NGAY_GIA_OC_4_MUA,
  NGAY_HOA_TINH,
  NGAY_THIEN_HOA,
  NGAY_DIA_HOA,
  NGAY_DOC_HOA,
  NGAY_THIEN_TAI_DAI_HOA,
  NGAY_SAO_HOA,
  NGAY_LOI_GIANG,
  NGAY_LOI_DINH_CHINH_SAT,
  NGAY_LOI_DINH_SAT_CHU,
  NGAY_SAT_SU,
  NGAY_THANG_SAT_SU,
  NGAY_NGUYET_KY,
  NGAY_THIEN_MA_TAM_CUONG,
  NGAY_TAM_NUONG_SAT,
  NGAY_BAT_TUONG_XAU,
  NGAY_NGUYET_TAN,
  NGAY_NGUU_LANG_CHUC_NU_4_MUA,
  NGAY_KHONG_SANG4_MUA,
  NGAY_KHONG_PHONG4_MUA,
  NGAY_TU_LA_DOAT_GIA,
  NGAY_THAP_AC_DAI_BAI,
  NGAY_NHAP_MO4_MUA,
  NGAY_THAN_HAO,
  NGAY_QUY_KHOC,
  NGAY_THIEN_MON_BE_TAC,
  NGAY_TU_LY,
  NGAY_TU_TUYET,
  NGAY_SAO_CUU_THO_QUY,
  NGAY_SAO_THIEN_CUONG,
  NGAY_SAO_THIEN_LAI,
  NGAY_SAO_TIEU_HONG_SA,
  NGAY_SAO_DAI_HAO,
  NGAY_SAO_TIEU_HAO,
  NGAY_SAO_NGUYET_PHA,
  NGAY_SAO_KIEP_SAT,
  NGAY_SAO_DIA_PHA,
  NGAY_SAO_THO_PHU,
  NGAY_SAO_THO_ON,
  NGAY_SAO_THIEN_ON,
  NGAY_SAO_THU_TU,
  NGAY_SAO_HOANG_VU,
  NGAY_SAO_THIEN_TAC,
  NGAY_SAO_DIA_TAC,
  NGAY_SAO_HOA_TAI,
  NGAY_SAO_NGUYET_YEM,
  NGAY_SAO_NGUYET_HU,
  NGAY_SAO_HOANG_SA,
  NGAY_SAO_LUC_BAT_THANH,
  NGAY_SAO_NHAN_CACH,
  NGAY_SAO_THAN_CACH,
  NGAY_SAO_PHI_MA_SAT,
  NGAY_SAO_NGU_QUY,
  NGAY_SAO_BANG_TIEU_NGOA_HAM,
  NGAY_SAO_HA_KHOI_CAU_GIAO,
  NGAY_SAO_CUU_KHONG,
  NGAY_SAO_TRUNG_TANG,
  NGAY_SAO_TRUNG_PHUC,
  NGAY_SAO_CHU_TUOC_HAC_DAO,
  NGAY_SAO_BACH_HO,
  NGAY_SAO_HUYEN_VU,
  NGAY_SAO_CAU_TRAN,
  NGAY_SAO_LOI_CONG,
  NGAY_SAO_CO_THAN,
  NGAY_SAO_QUA_TU,
  NGAY_SAO_NGUYET_HINH,
  NGAY_SAO_TOI_CHI,
  NGAY_SAO_NGUYET_KIEN_CHUYEN_SAT,
  NGAY_SAO_THIEN_DIA_CHINH_CHUYEN,
  NGAY_SAO_THIEN_DIA_CHUYEN_SAT,
  NGAY_SAO_LO_BAN_SAT,
  NGAY_SAO_PHU_DAU_SAT,
  NGAY_SAO_TAM_TANG,
  NGAY_SAO_NGU_HU,
  NGAY_SAO_TU_THOI_DAI_MO,
  NGAY_SAO_THO_CAM,
  NGAY_SAO_LY_SANG,
  NGAY_SAO_TU_THOI_CO_QUA,
  NGAY_SAO_AM_THAC,
  NGAY_SAO_DUONG_THAC,
  NGAY_SAO_QUY_KHOC
} from '../../constants'

import {
  FILTERED_DATE_LIST
} from '../../constants/filtered-dates'

import {
  getLunarDate,
  check30DaysInMonth,
  getCanChiDMY,
  getTietKhi,
  getTietKhiDatesAndTrucKienDates,
  getSeasonStartDate,
  getCurrentTruc,
  getCanHour0,
  getSao,
  getLucThapHoaGiap
} from '../../utils/lunar'

import AllFilter from '../../utils/filters'

import DateDetailModal from '../../components/DateDetailModal'

export default class MonthCalendar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      today: moment(moment().format(DATE_FORMAT), DATE_FORMAT),
      currentDate: moment(moment().format(DATE_FORMAT), DATE_FORMAT),
      currentLunarDate: getLunarDate(
        +moment().format('D'),
        +moment().format('M'),
        +moment().format('YYYY')
      ),
      tietKhiDatesAndTrucKienDates: getTietKhiDatesAndTrucKienDates(
        moment().format('YYYY')
      ),
      selectedDate: null,
      is30DaysInMonth: null,
      canChiDMY: null,
      canHour0: null,
      currentTietKhi: null,
      currentSeasonStartDate: null,
      currentLucThapHoaGiap: null,
      currentTruc: null,
      currentSao: null,
      isVisibleDateTypeList: false
    }

    this.refInputMonth = React.createRef()
    this.refInputYear = React.createRef()
    this.refDateDetailModal = React.createRef()
  }

  componentDidMount() {
    const checkboxValueDateTypeList = {}
    FILTERED_DATE_LIST.forEach(item => {
      checkboxValueDateTypeList[item.checkboxValue] = false
    })

    this.setState({
      ...checkboxValueDateTypeList
    })

    this.setCommonStates()
  }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  // }

  setCommonStates = () => {
    const { currentLunarDate } = this.state
    const {
      tietKhiDateList,
      trucKienDateList,
      seasonStartDateList
    } = this.state.tietKhiDatesAndTrucKienDates
    const currentDate = moment(moment(this.state.currentDate).format(DATE_FORMAT), DATE_FORMAT)
    const canChiDMY = getCanChiDMY(currentLunarDate)

    // console.log(555, currentDate)

    this.setState({
      is30DaysInMonth: check30DaysInMonth(currentLunarDate),
      canChiDMY: getCanChiDMY(currentLunarDate),
      canHour0: getCanHour0(currentLunarDate.jd),
      currentTietKhi: getTietKhi(currentLunarDate.jd),
      currentSeasonStartDate: getSeasonStartDate(currentDate, seasonStartDateList),
      currentLucThapHoaGiap: getLucThapHoaGiap(canChiDMY.day.can.id, canChiDMY.day.chi.id),
      currentTruc: getCurrentTruc(currentDate, trucKienDateList, tietKhiDateList, TRUC_LIST),
      currentSao: getSao(currentDate)
    })
  }

  daysInMonth = () => {
    return +this.state.currentDate.daysInMonth()
  }

  daysInPrevMonth = () => {
    let currentDate = Object.assign({}, this.state.currentDate)
    currentDate = moment(currentDate).subtract(1, 'month')

    return +currentDate.daysInMonth()
  }

  firstDayOfMonth = () => {
    // Thứ tự của ngày trong tuần: 0 1 2 3 4 5 6, tương ứng CN, T2, T3, T4, T5, T6, T7
    const startOf = +moment(this.state.currentDate).startOf('month').format('d')

    // Chuyển đổi CN ra sau thứ 7
    // Thứ tự của ngày trong tuần: 0 1 2 3 4 5 6, tương ứng T2, T3, T4, T5, T6, T7, CN
    return startOf === 0 ? 6 : startOf - 1
  }

  changeMonth = (actionType, month) => {
    if (
      +month > 1 &&
      +month > 12 &&
      +month === +moment(this.state.currentDate).format('M')
    ) {
      return
    }

    let currentDate = Object.assign({}, this.state.currentDate)

    if (actionType === -1) {
      // Tiến một tháng
      currentDate = moment(currentDate).subtract(1, 'month')
    } else if (actionType === 1) {
      // Lùi một tháng
      currentDate = moment(currentDate).add(1, 'month')
    } else if (actionType === 0) {
      // Đi đến một tháng chỉ định
      currentDate = moment(currentDate).set('month', +month - 1)
    } else {
      return
    }

    if (
      !currentDate.isValid() ||
      currentDate.diff(moment(FIRST_DAY, DATE_FORMAT), 'days') < 0 ||
      currentDate.diff(moment(LAST_DAY, DATE_FORMAT), 'days') > 0
    ) {
      return
    }

    const currentYear = +moment(this.state.currentDate).format('YYYY')
    const tietKhiDatesAndTrucKienDates = +moment(currentDate).format('YYYY') !== currentYear
      ? getTietKhiDatesAndTrucKienDates(+moment(currentDate).format('YYYY'))
      : this.state.tietKhiDatesAndTrucKienDates
    const currentLunarDate = getLunarDate(
      +moment(currentDate).format('D'),
      +moment(currentDate).format('M'),
      +moment(currentDate).format('YYYY')
    )

    this.setState({
      currentDate,
      currentLunarDate,
      tietKhiDatesAndTrucKienDates
    }, () => {
      console.log('prevMonth', this.state.currentDate.format('M'))
      this.setCommonStates()
    })
  }

  changeYear = (actionType, year) => {
    if (
      +year < +FIRST_DAY.split('/')[2] ||
      +year > +LAST_DAY.split('/')[2] ||
      +year === +moment(this.state.currentDate).format('YYYY')
    ) {
      return
    }

    let currentDate = Object.assign({}, this.state.currentDate)

    if (actionType === -1) {
      // Lùi một năm
      currentDate = moment(currentDate).subtract(1, 'year')
    } else if (actionType === 1) {
      // Tiến một năm
      currentDate = moment(currentDate).add(1, 'year')
    } else if (actionType === 0) {
      // Đi đến một tháng chỉ định
      currentDate = moment(currentDate).set('year', +year)
    } else {
      return
    }

    if (
      !currentDate.isValid() ||
      currentDate.diff(moment(FIRST_DAY, DATE_FORMAT), 'days') < 0 ||
      currentDate.diff(moment(LAST_DAY, DATE_FORMAT), 'days') > 0
    ) {
      return
    }

    const currentLunarDate = getLunarDate(
      +moment(currentDate).format('D'),
      +moment(currentDate).format('M'),
      +moment(currentDate).format('YYYY')
    )
    const tietKhiDatesAndTrucKienDates = getTietKhiDatesAndTrucKienDates(
      +moment(currentDate).format('YYYY')
    )

    this.setState({
      currentDate,
      currentLunarDate,
      tietKhiDatesAndTrucKienDates
    }, () => {
      console.log('changeYear', this.state.currentDate.format('YYYY'))
      this.setCommonStates()
    })
  }

  goToday = () => {
    const today = moment(moment().format(DATE_FORMAT), DATE_FORMAT)
    const currentDate = moment(moment().format(DATE_FORMAT), DATE_FORMAT)
    const currentLunarDate = getLunarDate(
      +moment(moment()).format('D'),
      +moment(moment()).format('M'),
      +moment(moment()).format('YYYY')
    )
    const tietKhiDatesAndTrucKienDates = getTietKhiDatesAndTrucKienDates(
      +moment().format('YYYY')
    )

    this.setState({
      today,
      currentDate,
      currentLunarDate,
      tietKhiDatesAndTrucKienDates
    }, () => {
      console.log('goToday', this.state.today)
      this.setCommonStates()
    })
  }

  onDayClick = (dateInLoop) => {
    console.log('click', dateInLoop)

    this.setState({
      selectedDate: dateInLoop
    }, () => {
      this.refDateDetailModal.current.onOpen()
    })
  }

  onChangeCheckDateType = (e) => {
    const target = e.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name

    this.setState({
      [name]: value
    })
  }

  onChangeCheckAllDateType = (isCheckedAll = true) => {
    FILTERED_DATE_LIST.forEach(item => {
      this.setState({
        [item.checkboxValue]: isCheckedAll
      })
    })
  }

  getDatesOfMonth = (startDay, endDay, monthPosition) => {
    let dateList = []
    const currentMonth = +this.state.currentDate.format('M')
    const currentYear = +this.state.currentDate.format('YYYY')

    for (let d = startDay; d <= endDay; d++) {
      let dateInLoop = null

      // Tìm ngày tháng năm tại vị trí hiện tại trong vòng lặp
      if (monthPosition === -1 && currentMonth === 1) {
        dateInLoop = moment(`${d}/12/${currentYear - 1}`,DATE_FORMAT)
      } else if (monthPosition === 1 && currentMonth === 12) {
        dateInLoop = moment(`${d}/1/${currentYear + 1}`,DATE_FORMAT)
      } else {
        dateInLoop = moment(`${d}/${currentMonth + monthPosition}/${currentYear}`,DATE_FORMAT)
      }

      dateList.push(dateInLoop)
    }

    return dateList
  }

  getCellList = () => {
    let cellList = []

    // Ngày, tháng, năm của hôm nay
    const todayDay = +this.state.today.format('D')
    const todayMonth = +this.state.today.format('M')
    const todayYear = +this.state.today.format('YYYY')
    const tietKhiDatesAndTrucKienDates = this.state.tietKhiDatesAndTrucKienDates
    const {
      lapDongNamTruocDate,
      lapXuanDate,
      lapHaDate,
      lapThuDate,
      lapDongDate,
      lapXuanNamSauDate,
      tietKhiDateList,
      trucKienDateList
    } = tietKhiDatesAndTrucKienDates

    // Nếu mùng 1 của tháng hiện tại không ở vị trí hàng 1, cột 1,
    // Thì lấp đầy chỗ trống bằng các ngày của tháng trước
    const prevMonthDays = this.getDatesOfMonth(
      this.daysInPrevMonth() - this.firstDayOfMonth() + 1, this.daysInPrevMonth(), -1
    )

    // Đẩy các ngày của tháng hiện tại vào mảng
    const currentMonthDays = this.getDatesOfMonth(1, this.daysInMonth(), 0)

    // Nếu chưa đủ 7 hàng, thì lấp đầy chỗ trống bằng các ngày của tháng sau
    // 42 bên dưới tương ứng với 6 hàng và 7 cột
    const nextMonthDays = this.getDatesOfMonth(
      1, 42 - prevMonthDays.length - currentMonthDays.length, 1
    )

    // Nối 3 mảng lại, thành mảng chứa 42 phần tử, tương ứng 42 ngày trên bảng
    const dateList = [...prevMonthDays, ...currentMonthDays, ...nextMonthDays]

    if (!Array.isArray(dateList)) {
      return
    }

    dateList.forEach(dateInLoop => {
      if (
        dateInLoop.diff(moment(FIRST_DAY, DATE_FORMAT), 'days') < 0 ||
        dateInLoop.diff(moment(LAST_DAY, DATE_FORMAT), 'days') > 0
      ) {
        cellList.push(
          <td key={ Math.random() } className="text-transparent">
            <div style={{ fontSize: 20 }}>
              <strong>Empty</strong>
              <sub><strong>Empty</strong></sub>
            </div>
            <div><strong>Empty</strong></div>
            <div><small>Empty</small></div>
          </td>
        )

        return
      }

      // Ngày, tháng, năm của một ô trong bảng (chính là một trong 42 phần tử trong vòng lặp)
      const dayInLoop = +moment(dateInLoop).format('D')
      const monthInLoop = +moment(dateInLoop).format('M')
      const yearInLoop = +moment(dateInLoop).format('YYYY')
      const lunarDateInLoop = getLunarDate(dayInLoop, monthInLoop, yearInLoop)
      const isToday = dayInLoop === todayDay && monthInLoop === todayMonth && yearInLoop === todayYear
      const canChiDMY = getCanChiDMY(lunarDateInLoop)
      const canOfDayId = canChiDMY.day.can.id
      const chiOfDayId = canChiDMY.day.chi.id
      const canOfYearId = canChiDMY.year.can.id
      const currentTruc = getCurrentTruc(dateInLoop, trucKienDateList, tietKhiDateList, TRUC_LIST)
      const currentTietKhi = getTietKhi(lunarDateInLoop.jd)
      const isMuaXuan = dateInLoop >= lapXuanDate && dateInLoop < lapHaDate
      const isMuaHa = dateInLoop >= lapHaDate && dateInLoop < lapThuDate
      const isMuaThu = dateInLoop >= lapThuDate && dateInLoop < lapDongDate
      const isMuaDong = (dateInLoop >= lapDongDate && dateInLoop < lapXuanNamSauDate) ||
        (dateInLoop >= lapDongNamTruocDate && dateInLoop < lapXuanDate)
      const is30DaysInMonth = check30DaysInMonth(lunarDateInLoop)

      const dayStatusList = this.getDayStatusList(
        dateInLoop,
        lunarDateInLoop,
        canOfDayId,
        chiOfDayId,
        canOfYearId,
        currentTruc,
        tietKhiDatesAndTrucKienDates,
        isMuaXuan,
        isMuaHa,
        isMuaThu,
        isMuaDong,
        is30DaysInMonth
      )

      const selectedDateClass = dateInLoop.isSame(this.state.selectedDate) ? 'selected-date ' : ''
      const todayClass = isToday ? 'today ' : ''
      const otherMonthClass = (monthInLoop !== +this.state.currentDate.format('M')) ? 'other-month ' : ''
      const firstDayLunarClass = (lunarDateInLoop.day === 1) ? 'first-day-in-month ' : ''
      const dayClassList = 'day ' +
        todayClass +
        selectedDateClass +
        otherMonthClass +
        firstDayLunarClass

      const elementDayLabel = FILTERED_DATE_LIST.map((item, index) => {
        // dayStatusList[item.code] chính là dayStatusList[NGAY_BAT_TUONG], dayStatusList[NGAY_SAO_SAT_CONG], ...
        if (!this.state[item.checkboxValue] || !dayStatusList[item.code]) {
          return null
        }

        return (
          <li
            key={ index }
            className={ `${item.labelClass} ${item.goodDayStatus ? 'good-day' : 'bad-day'}` }
          >
            { item.labelName }
          </li>
        )
      })

      cellList.push(
        <td
          key={ `${dayInLoop}${monthInLoop}${yearInLoop}` }
          className={ dayClassList }
          onClick={() => { this.onDayClick(dateInLoop) }}
        >
          <div style={{ fontSize: 20 }}>
            <strong>{ dayInLoop }</strong>
            <sub>
              <strong>{ lunarDateInLoop.day }</strong>
              <strong>{ lunarDateInLoop.day === 1 ? '/' + lunarDateInLoop.month : '' }</strong>
              { lunarDateInLoop.day === 1 ? is30DaysInMonth ? '(Đ)' : '(T)' : null }
              { lunarDateInLoop.day === 1 && lunarDateInLoop.leap === 1 ? '(N)' : '' }
            </sub>
          </div>
          <div><strong>Ngày { canChiDMY.day.can.name } { canChiDMY.day.chi.name }</strong></div>
          <div>
            <small>
              { currentTietKhi ? 'Tiết '+ currentTietKhi.name : '' }
              { currentTruc ? ', Trực '+ currentTruc.name : '' }
            </small>
          </div>

          <ul className="day-labels">
            { elementDayLabel }
          </ul>
        </td>
      )
    })

    return cellList
  }

  getDayStatusList = (
    dateInLoop,
    lunarDateInLoop,
    canOfDayId,
    chiOfDayId,
    canOfYearId,
    currentTruc,
    tietKhiDatesAndTrucKienDates,
    isMuaXuan,
    isMuaHa,
    isMuaThu,
    isMuaDong,
    is30DaysInMonth
  ) => {
    if (typeof is30DaysInMonth !== 'boolean') {
      return
    }

    const lunarDay = lunarDateInLoop.day
    const lunarMonth = lunarDateInLoop.month
    const {
      dongChiNamTruocDate,
      lapXuanDate,
      xuanPhanDate,
      lapHaDate,
      haChiDate,
      lapThuDate,
      thuPhanDate,
      lapDongDate,
      dongChiDate
    } = tietKhiDatesAndTrucKienDates

    const dayStatusList = {}

    // Tốt
    dayStatusList[NGAY_BAT_TUONG] = AllFilter.getNgayBatTuong(
      this.state.isCheckedNgayBatTuong, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_SAT_CONG] = AllFilter.getNgaySaoSatCong(
      this.state.isCheckedNgaySaoSatCong, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_TRUC_TINH] = AllFilter.getNgaySaoTrucTinh(
      this.state.isCheckedNgaySaoTrucTinh, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NHAN_DUYEN] = AllFilter.getNgaySaoNhanDuyen(
      this.state.isCheckedNgaySaoNhanDuyen, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_AN] = AllFilter.getNgaySaoThienAn(
      this.state.isCheckedNgaySaoThienAn, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_THUY] = AllFilter.getNgaySaoThienThuy(
      this.state.isCheckedNgaySaoThienThuy, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGU_HOP] = AllFilter.getNgaySaoNguHop(
      this.state.isCheckedNgaySaoNguHop, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_DUC] = AllFilter.getNgaySaoThienDuc(
      this.state.isCheckedNgaySaoThienDuc, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_DUC_HOP] = AllFilter.getNgaySaoThienDucHop(
      this.state.isCheckedNgaySaoThienDucHop, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_DUC] = AllFilter.getNgaySaoNguyetDuc(
      this.state.isCheckedNgaySaoNguyetDuc, lunarMonth, canOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_DUC_HOP] = AllFilter.getNgaySaoNguyetDucHop(
      this.state.isCheckedNgaySaoNguyetDucHop, lunarMonth, canOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_HY] = AllFilter.getNgaySaoThienHy(
      this.state.isCheckedNgaySaoThienHy, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_PHU] = AllFilter.getNgaySaoThienPhu(
      this.state.isCheckedNgaySaoThienQuy, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_QUY] = AllFilter.getNgaySaoThienQuy(
      this.state.isCheckedNgaySaoThienQuy,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      canOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_XA] = AllFilter.getNgaySaoThienXa(
      this.state.isCheckedNgaySaoThienXa, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_SINH_KHI] = AllFilter.getNgaySaoSinhKhi(
      this.state.isCheckedNgaySaoSinhKhi, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_PHUC] = AllFilter.getNgaySaoThienPhuc(
      this.state.isCheckedNgaySaoThienPhuc, lunarMonth, canOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_THANH] = AllFilter.getNgaySaoThienThanh(
      this.state.isCheckedNgaySaoThienThanh, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_QUAN] = AllFilter.getNgaySaoThienQuan(
      this.state.isCheckedNgaySaoThienQuan, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_MA] = AllFilter.getNgaySaoThienMa(
      this.state.isCheckedNgaySaoThienMa, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_TAI] = AllFilter.getNgaySaoThienTai(
      this.state.isCheckedNgaySaoThienTai, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_DIA_TAI] = AllFilter.getNgaySaoDiaTai(
      this.state.isCheckedNgaySaoDiaTai, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_TAI] = AllFilter.getNgaySaoNguyetTai(
      this.state.isCheckedNgaySaoNguyetTai, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_AN] = AllFilter.getNgaySaoNguyetAn(
      this.state.isCheckedNgaySaoNguyetAn, lunarMonth, canOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_KHONG] = AllFilter.getNgaySaoNguyetKhong(
      this.state.isCheckedNgaySaoNguyetKhong, lunarMonth, canOfDayId
    )

    dayStatusList[NGAY_SAO_MINH_TINH] = AllFilter.getNgaySaoMinhTinh(
      this.state.isCheckedNgaySaoMinhTinh, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THANH_TAM] = AllFilter.getNgaySaoThanhTam(
      this.state.isCheckedNgaySaoThanhTam, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGU_PHU] = AllFilter.getNgaySaoNguPhu(
      this.state.isCheckedNgaySaoNguPhu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_LOC_KHO] = AllFilter.getNgaySaoLocKho(
      this.state.isCheckedNgaySaoLocKho, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_PHUC_SINH] = AllFilter.getNgaySaoPhucSinh(
      this.state.isCheckedNgaySaoPhucSinh, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_CAT_KHANH] = AllFilter.getNgaySaoCatKhanh(
      this.state.isCheckedNgaySaoCatKhanh, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_AM_DUC] = AllFilter.getNgaySaoAmDuc(
      this.state.isCheckedNgaySaoAmDuc, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_U_VI_TINH] = AllFilter.getNgaySaoUViTinh(
      this.state.isCheckedNgaySaoUViTinh, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_MAN_DUC_TINH] = AllFilter.getNgaySaoManDucTinh(
      this.state.isCheckedNgaySaoManDucTinh, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_KINH_TAM] = AllFilter.getNgaySaoKinhTam(
      this.state.isCheckedNgaySaoKinhTam, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_TUE_HOP] = AllFilter.getNgaySaoTueHop(
      this.state.isCheckedNgaySaoTueHop, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_GIAI] = AllFilter.getNgaySaoNguyetGiai(
      this.state.isCheckedNgaySaoNguyetGiai, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_QUAN_NHAT] = AllFilter.getNgaySaoQuanNhat(
      this.state.isCheckedNgaySaoQuanNhat, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_HOAT_DIEU] = AllFilter.getNgaySaoHoatDieu(
      this.state.isCheckedNgaySaoHoatDieu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_GIAI_THAN] = AllFilter.getNgaySaoGiaiThan(
      this.state.isCheckedNgaySaoGiaiThan, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_PHO_BO] = AllFilter.getNgaySaoPhoBo(
      this.state.isCheckedNgaySaoPhoBo, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_ICH_HAU] = AllFilter.getNgaySaoIchHau(
      this.state.isCheckedNgaySaoIchHau, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_TUC_THE] = AllFilter.getNgaySaoTucThe(
      this.state.isCheckedNgaySaoTucThe, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_YEU_YEN] = AllFilter.getNgaySaoYeuYen(
      this.state.isCheckedNgaySaoYeuYen, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_DICH_MA] = AllFilter.getNgaySaoDichMa(
      this.state.isCheckedNgaySaoDichMa, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_TAM_HOP] = AllFilter.getNgaySaoTamHop(
      this.state.isCheckedNgaySaoTamHop, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_LUC_HOP] = AllFilter.getNgaySaoLucHop(
      this.state.isCheckedNgaySaoLucHop, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_MAU_THUONG] = AllFilter.getNgaySaoMauThuong(
      this.state.isCheckedNgaySaoMauThuong,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_PHUC_HAU] = AllFilter.getNgaySaoPhucHau(
      this.state.isCheckedNgaySaoPhucHau,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_DAI_HONG_SA] = AllFilter.getNgaySaoDaiHongSa(
      this.state.isCheckedNgaySaoDaiHongSa,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_DAN_NHAT_THOI_DUC] = AllFilter.getNgaySaoDanNhatThoiDuc(
      this.state.isCheckedNgaySaoDanNhatThoiDuc,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_HOANG_AN] = AllFilter.getNgaySaoHoangAn(
      this.state.isCheckedNgaySaoHoangAn, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THANH_LONG] = AllFilter.getNgaySaoThanhLong(
      this.state.isCheckedNgaySaoThanhLong, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_MINH_DUONG] = AllFilter.getNgaySaoMinhDuong(
      this.state.isCheckedNgaySaoMinhDuong, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_KIM_DUONG] = AllFilter.getNgaySaoKimDuong(
      this.state.isCheckedNgaySaoKimDuong, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGOC_DUONG] = AllFilter.getNgaySaoNgocDuong(
      this.state.isCheckedNgaySaoNgocDuong, lunarMonth, chiOfDayId
    )

    // Xấu
    dayStatusList[NGAY_SAT_CHU] = AllFilter.getNgaySatChu(
      this.state.isCheckedNgaySatChu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_THANG_SAT_CHU] = AllFilter.getNgayThangSatChu(
      this.state.isCheckedNgayThangSatChu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAT_CHU_TRONG_4_MUA] = AllFilter.getNgaySatChuTrong4Mua(
      this.state.isCheckedNgaySatChuTrong4Mua,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_BON_MUA_SAT_CHU] = AllFilter.getNgayBonMuaSatChu(
      this.state.isCheckedNgayBonMuaSatChu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_THO_TU] = AllFilter.getNgayThoTu(
      this.state.isCheckedNgayThoTu, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_LY_SAO] = AllFilter.getNgayLySao(
      this.state.isCheckedNgayLySao, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_VANG_VONG] = AllFilter.getNgayVangVong(
      this.state.isCheckedNgayVangVong, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_XICH_TONG_TU] = AllFilter.getNgayXichTongTu(
      this.state.isCheckedNgayXichTongTu, lunarDay, lunarMonth
    )

    dayStatusList[NGAY_KHONG_VONG] = AllFilter.getNgayKhongVong(
      this.state.isCheckedNgayKhongVong, lunarDay, lunarMonth
    )

    dayStatusList[NGAY_HUNG_BAI] = AllFilter.getNgayHungBai(
      this.state.isCheckedNgayHungBai, lunarDay, lunarMonth
    )

    dayStatusList[NGAY_XICH_KHAU] = AllFilter.getNgayXichKhau(
      this.state.isCheckedNgayXichKhau, lunarDay, lunarMonth
    )

    dayStatusList[NGAY_HOANG_OC_4_MUA] = AllFilter.getNgayHoangOc4Mua(
      this.state.isCheckedNgayHoangOc4Mua,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_GIA_OC_4_MUA] = AllFilter.getNgayGiaOc4Mua(
      this.state.isCheckedNgayGiaOc4Mua,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_HOA_TINH] = AllFilter.getNgayHoaTinh(
      this.state.isCheckedNgayHoaTinh, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_THIEN_HOA] = AllFilter.getNgayThienHoa(
      this.state.isCheckedNgayThienHoa, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_DIA_HOA] = AllFilter.getNgayDiaHoa(
      this.state.isCheckedNgayDiaHoa, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_DOC_HOA] = AllFilter.getNgayDocHoa(
      this.state.isCheckedNgayDocHoa, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_THIEN_TAI_DAI_HOA] = AllFilter.getNgayThienTaiDaiHoa(
      this.state.isCheckedNgayThienTaiDaiHoa, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_HOA] = AllFilter.getNgaySaoHoa(
      this.state.isCheckedNgaySaoHoa, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_LOI_GIANG] = AllFilter.getNgayLoiGiang(
      this.state.isCheckedNgayLoiGiang, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_LOI_DINH_CHINH_SAT] = AllFilter.getNgayLoiDinhChinhSat(
      this.state.isCheckedNgayLoiDinhChinhSat, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_LOI_DINH_SAT_CHU] = AllFilter.getNgayLoiDinhSatChu(
      this.state.isCheckedNgayLoiDinhSatChu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAT_SU] = AllFilter.getNgaySatSu(
      this.state.isCheckedNgaySatSu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_THANG_SAT_SU] = AllFilter.getNgayThangSatSu(
      this.state.isCheckedNgayThangSatSu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_NGUYET_KY] = AllFilter.getNgayNguyetKy(
      this.state.isCheckedNgayNguyetKy, lunarDay
    )

    dayStatusList[NGAY_THIEN_MA_TAM_CUONG] = AllFilter.getNgayThienMaTamCuong(
      this.state.isCheckedNgayThienMaTamCuong, lunarDay
    )

    dayStatusList[NGAY_TAM_NUONG_SAT] = AllFilter.getNgayTamNuongSat(
      this.state.isCheckedNgayTamNuongSat, lunarDay
    )

    dayStatusList[NGAY_BAT_TUONG_XAU] = AllFilter.getNgayBatTuongXau(
      this.state.isCheckedNgayBatTuongXau, lunarDay
    )

    dayStatusList[NGAY_NGUYET_TAN] = AllFilter.getNgayNguyetTan(
      this.state.isCheckedNgayNguyetTan, lunarDay, is30DaysInMonth
    )

    dayStatusList[NGAY_NGUU_LANG_CHUC_NU_4_MUA] = AllFilter.getNgayNguuLangChucNu4Mua(
      this.state.isCheckedNgayNguuLangChucNu4Mua,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_KHONG_SANG4_MUA] = AllFilter.getNgayKhongSang4Mua(
      this.state.isCheckedNgayKhongSang4Mua,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_KHONG_PHONG4_MUA] = AllFilter.getNgayKhongPhong4Mua(
      this.state.isCheckedNgayKhongPhong4Mua,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_TU_LA_DOAT_GIA] = AllFilter.getNgayTuLaDoatGia(
      this.state.isCheckedNgayTuLaDoatGia, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_THAP_AC_DAI_BAI] = AllFilter.getNgayThapAcDaiBai(
      this.state.isCheckedNgayThapAcDaiBai,
      lunarMonth,
      canOfDayId,
      chiOfDayId,
      canOfYearId
    )

    dayStatusList[NGAY_NHAP_MO4_MUA] = AllFilter.getNgayNhapMo4Mua(
      this.state.isCheckedNgayNhapMo4Mua,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_THAN_HAO] = AllFilter.getNgayThanHao(
      this.state.isCheckedNgayThanHao, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_QUY_KHOC] = AllFilter.getNgayQuyKhoc(
      this.state.isCheckedNgayQuyKhoc, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_THIEN_MON_BE_TAC] = AllFilter.getNgayThienMonBeTac(
      this.state.isCheckedNgayThienMonBeTac, canOfDayId, currentTruc
    )

    dayStatusList[NGAY_TU_LY] = AllFilter.getNgayTuLyOrTuTuyet(
      this.state.isCheckedNgayTuLy,
      dateInLoop,
      [dongChiNamTruocDate, xuanPhanDate, haChiDate, thuPhanDate, dongChiDate]
    )

    dayStatusList[NGAY_TU_TUYET] = AllFilter.getNgayTuLyOrTuTuyet(
      this.state.isCheckedNgayTuTuyet,
      dateInLoop,
      [lapXuanDate, lapHaDate, lapThuDate, lapDongDate]
    )

    dayStatusList[NGAY_SAO_THIEN_CUONG] = AllFilter.getNgaySaoThienCuong(
      this.state.isCheckedNgaySaoThienCuong, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_CUU_THO_QUY] = AllFilter.getNgaySaoCuuThoQuy(
      this.state.isCheckedNgaySaoCuuThoQuy, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_LAI] = AllFilter.getNgaySaoThienLai(
      this.state.isCheckedNgaySaoThienLai, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_TIEU_HONG_SA] = AllFilter.getNgaySaoTieuHongSa(
      this.state.isCheckedNgaySaoTieuHongSa, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_DAI_HAO] = AllFilter.getNgaySaoDaiHao(
      this.state.isCheckedNgaySaoDaiHao, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_TIEU_HAO] = AllFilter.getNgaySaoTieuHao(
      this.state.isCheckedNgaySaoTieuHao, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_PHA] = AllFilter.getNgaySaoNguyetPha(
      this.state.isCheckedNgaySaoNguyetPha, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_KIEP_SAT] = AllFilter.getNgaySaoKiepSat(
      this.state.isCheckedNgaySaoKiepSat, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_DIA_PHA] = AllFilter.getNgaySaoDiaPha(
      this.state.isCheckedNgaySaoDiaPha, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THO_PHU] = AllFilter.getNgaySaoThoPhu(
      this.state.isCheckedNgaySaoThoPhu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THO_ON] = AllFilter.getNgaySaoThoOn(
      this.state.isCheckedNgaySaoThoOn, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_ON] = AllFilter.getNgaySaoThienOn(
      this.state.isCheckedNgaySaoThienOn, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THU_TU] = AllFilter.getNgaySaoThuTu(
      this.state.isCheckedNgaySaoThuTu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_HOANG_VU] = AllFilter.getNgaySaoHoangVu(
      this.state.isCheckedNgaySaoHoangVu,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_TAC] = AllFilter.getNgaySaoThienTac(
      this.state.isCheckedNgaySaoThienTac, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_DIA_TAC] = AllFilter.getNgaySaoDiaTac(
      this.state.isCheckedNgaySaoDiaTac, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_HOA_TAI] = AllFilter.getNgaySaoHoaTai(
      this.state.isCheckedNgaySaoHoaTai, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_YEM] = AllFilter.getNgaySaoNguyetYem(
      this.state.isCheckedNgaySaoNguyetYem, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_HU] = AllFilter.getNgaySaoNguyetHu(
      this.state.isCheckedNgaySaoNguyetHu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_HOANG_SA] = AllFilter.getNgaySaoHoangSa(
      this.state.isCheckedNgaySaoHoangSa, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_LUC_BAT_THANH] = AllFilter.getNgaySaoLucBatThanh(
      this.state.isCheckedNgaySaoLucBatThanh, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NHAN_CACH] = AllFilter.getNgaySaoNhanCach(
      this.state.isCheckedNgaySaoNhanCach, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_THAN_CACH] = AllFilter.getNgaySaoThanCach(
      this.state.isCheckedNgaySaoThanCach, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_PHI_MA_SAT] = AllFilter.getNgaySaoPhiMaSat(
      this.state.isCheckedNgaySaoPhiMaSat, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGU_QUY] = AllFilter.getNgaySaoNguQuy(
      this.state.isCheckedNgaySaoNguQuy, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_BANG_TIEU_NGOA_HAM] = AllFilter.getNgaySaoBangTieuNgoaHam(
      this.state.isCheckedNgaySaoBangTieuNgoaHam, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_HA_KHOI_CAU_GIAO] = AllFilter.getNgaySaoHaKhoiCauGiao(
      this.state.isCheckedNgaySaoHaKhoiCauGiao, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_CUU_KHONG] = AllFilter.getNgaySaoCuuKhong(
      this.state.isCheckedNgaySaoCuuKhong, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_TRUNG_TANG] = AllFilter.getNgaySaoTrungTang(
      this.state.isCheckedNgaySaoTrungTang, lunarMonth, canOfDayId
    )

    dayStatusList[NGAY_SAO_TRUNG_PHUC] = AllFilter.getNgaySaoTrungPhuc(
      this.state.isCheckedNgaySaoTrungPhuc, lunarMonth, canOfDayId
    )

    dayStatusList[NGAY_SAO_CHU_TUOC_HAC_DAO] = AllFilter.getNgaySaoChuTuocHacDao(
      this.state.isCheckedNgaySaoChuTuocHacDao, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_BACH_HO] = AllFilter.getNgaySaoBachHo(
      this.state.isCheckedNgaySaoBachHo, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_HUYEN_VU] = AllFilter.getNgaySaoHuyenVu(
      this.state.isCheckedNgaySaoHuyenVu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_CAU_TRAN] = AllFilter.getNgaySaoCauTran(
      this.state.isCheckedNgaySaoCauTran, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_LOI_CONG] = AllFilter.getNgaySaoLoiCong(
      this.state.isCheckedNgaySaoLoiCong, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_CO_THAN] = AllFilter.getNgaySaoCoThan(
      this.state.isCheckedNgaySaoCoThan, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_QUA_TU] = AllFilter.getNgaySaoQuaTu(
      this.state.isCheckedNgaySaoQuaTu, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_HINH] = AllFilter.getNgaySaoNguyetHinh(
      this.state.isCheckedNgaySaoNguyetHinh, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_TOI_CHI] = AllFilter.getNgaySaoToiChi(
      this.state.isCheckedNgaySaoToiChi, lunarMonth, chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGUYET_KIEN_CHUYEN_SAT] = AllFilter.getNgaySaoNguyetKienChuyenSat(
      this.state.isCheckedNgaySaoNguyetKienChuyenSat,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    // xxx

    dayStatusList[NGAY_SAO_THIEN_DIA_CHINH_CHUYEN] = AllFilter.getNgaySaoThienDiaChinhChuyen(
      this.state.isCheckedNgaySaoThienDiaChinhChuyen,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      canOfDayId,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_THIEN_DIA_CHUYEN_SAT] = AllFilter.getNgaySaoThienDiaChuyenSat(
      this.state.isCheckedNgaySaoThienDiaChuyenSat,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      canOfDayId,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_LO_BAN_SAT] = AllFilter.getNgaySaoLoBanSat(
      this.state.isCheckedNgaySaoLoBanSat,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_PHU_DAU_SAT] = AllFilter.getNgaySaoPhuDauSat(
      this.state.isCheckedNgaySaoPhuDauSat,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_TAM_TANG] = AllFilter.getNgaySaoTamTang(
      this.state.isCheckedNgaySaoTamTang,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_NGU_HU] = AllFilter.getNgaySaoNguHu(
      this.state.isCheckedNgaySaoNguHu,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_TU_THOI_DAI_MO] = AllFilter.getNgaySaoTuThoiDaiMo(
      this.state.isCheckedNgaySaoTuThoiDaiMo,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      canOfDayId,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_THO_CAM] = AllFilter.getNgaySaoThoCam(
      this.state.isCheckedNgaySaoThoCam,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_LY_SANG] = AllFilter.getNgaySaoLySang(
      this.state.isCheckedNgaySaoLySang,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_TU_THOI_CO_QUA] = AllFilter.getNgaySaoTuThoiCoQua(
      this.state.isCheckedNgaySaoTuThoiCoQua,
      isMuaXuan,
      isMuaHa,
      isMuaThu,
      isMuaDong,
      chiOfDayId
    )

    dayStatusList[NGAY_SAO_AM_THAC] = AllFilter.getNgaySaoAmThac(
      this.state.isCheckedNgaySaoAmThac, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_DUONG_THAC] = AllFilter.getNgaySaoDuongThac(
      this.state.isCheckedNgaySaoDuongThac, lunarMonth, canOfDayId, chiOfDayId
    )

    dayStatusList[NGAY_SAO_QUY_KHOC] = AllFilter.getNgaySaoQuyKhoc(
      this.state.isCheckedNgaySaoQuyKhoc, chiOfDayId
    )

    return dayStatusList
  }

  testxxx = () => {
  }

  render() {
    const {
      currentDate,
      currentLunarDate,
      selectedDate,
      tietKhiDatesAndTrucKienDates,
      currentSeasonStartDate,
      is30DaysInMonth,
      canChiDMY,
      currentLucThapHoaGiap,
      canHour0,
      currentTruc,
      currentSao
    } = this.state

    // Mảng chứa các hàng trong bảng, 1 hàng gồm 7 ngày
    let rowList = []

    // Mảng chứa các ngày (D/M/YYYY) hiển thị trong bảng
    const cellList = this.getCellList()

    // Tách mảng cellList bên trên vào thành mảng có 6 phần tử, mỗi phần tử lại chứa 7 phần tử con
    // Tương ứng 7 hàng và 6 cột (tổng cộng sẽ có 42 ngày trên bảng)
    const rowTotal = Math.ceil(cellList.length / 7)

    for (let row = 1; row <= rowTotal; row++) {
      const start = 7 * (row - 1)
      let end = start + 7 - 1

      if (end > cellList.length) {
        end = cellList.length
      }

      rowList.push(cellList.slice(start, end + 1))
    }

    // console.log('rowList', rowList);

    if (!Array.isArray(rowList)) {
      return null
    }

    // Tiến hành in tr và td
    const trElements = rowList.map((dateItem, idx) => {
      return (
        <tr key={ idx }>
          { dateItem }
        </tr>
      )
    })

    const elementCheckboxDateTypeList = FILTERED_DATE_LIST.map((item, index) => {
      return (
        <Col key={ index } xs={24} md={12} xl={8}>
          <Checkbox
            name={ item.checkboxValue }
            checked={ this.state[item.checkboxValue] }
            onChange={ this.onChangeCheckDateType }
          >
            Ngày { item.name }
          </Checkbox>
        </Col>
      )
    })

    return (
      <>
        <div className="month-calendar-page">
          <div className="container">
            <div className="calendar-month">
              <div className="calendar-table">
                {/* <InputNumber
                  size="large"
                  min={1}
                  max={10}
                  style={{ width: 100 }}
                  defaultValue={3}
                  name="inputNumber"
                /> */}

                <div>
                  <div className="box-header">
                    <div className="date-lg">
                      { DAYS_IN_WEEK[currentDate.isoWeekday() - 1].name },&nbsp;
                      Ngày { currentDate.format('D') }&nbsp;
                      Tháng { currentDate.format('M') }&nbsp;
                      Năm { currentDate.format('YYYY') }
                    </div>

                    <div className="date-md">
                      <div>
                        <strong>Ngày âm:</strong> Ngày { currentLunarDate.day }&nbsp;
                        Tháng {
                          currentLunarDate.month === 1 ? 'Giêng' :
                            currentLunarDate.month === 12 ? 'Chạp' :
                              currentLunarDate.month
                        }
                        { is30DaysInMonth ? ' (Đủ)' : ' (Thiếu)' }
                        { currentLunarDate.leap === 1 ? ' (Nhuận)' : '' }&nbsp;
                        Năm { canChiDMY && canChiDMY.yearName }
                      </div>

                      <div>
                        <strong>Bát tự:</strong>&nbsp;
                        { canHour0 && 'Giờ ' + canHour0 }
                        { canChiDMY && ', Ngày ' + canChiDMY.dayName }
                        { canChiDMY && ', Tháng ' + canChiDMY.monthName }
                        { canChiDMY && ', Năm ' + canChiDMY.yearName }
                      </div>

                      <div>
                        { currentLucThapHoaGiap && 'Hành ' + currentLucThapHoaGiap.menh.primaryName }
                        { currentSao && ', Sao '+ currentSao.name}
                        { currentTruc && ', Trực '+ currentTruc.name }
                      </div>

                      <div>
                        {
                          currentSeasonStartDate ?
                            'Tiết ' + currentSeasonStartDate.name + ' bắt đầu từ ngày ' +
                            currentSeasonStartDate.lunarDate.day + '/' +
                            currentSeasonStartDate.lunarDate.month + '/' +
                            currentSeasonStartDate.lunarDate.year +
                            (currentSeasonStartDate.lunarDate.leap === 1 ? ' (Nhuận)' : '') +
                            ' (ÂL)' : ''
                        }
                      </div>
                    </div>
                  </div>

                  <div className="box-control">
                    <Row>
                      <Col sm={24} md={12} className="col-left">
                        <Button type="primary" onClick={() => { this.changeYear(-1) }}>
                          <DoubleLeftOutlined />
                        </Button>&nbsp;
                        <Button type="primary" onClick={() => { this.changeYear(1) }}>
                          <DoubleRightOutlined />
                        </Button>&nbsp;
                        <Button type="primary" onClick={() => { this.changeMonth(-1) }}>
                          <LeftOutlined />
                        </Button>&nbsp;
                        <Button type="primary" onClick={() => { this.changeMonth(1) }}>
                          <RightOutlined />
                        </Button>&nbsp;
                        <Button type="primary" onClick={() => { this.goToday() }}>Hôm nay</Button>
                      </Col>

                      <Col sm={24} md={12} className="col-right">
                        &nbsp;
                        <span>
                          <input type="number" ref={ this.refInputMonth } />
                          <button
                            onClick={() => { this.changeMonth(0, +this.refInputMonth.current.value) }}
                          >
                            Set month
                          </button>
                        </span>

                        &nbsp;
                        <span>
                          Set year:
                          <input type="number" ref={ this.refInputYear } />
                          <button
                            onClick={() => { this.changeYear(0, +this.refInputYear.current.value) }}
                          >
                            Set year
                          </button>
                        </span>

                        &nbsp;

                        <Button type="primary" onClick={() => { this.testxxx() }}>Testxxx</Button>&nbsp;
                        <Button
                          type="primary"
                          onClick={() => {
                            this.setState({ isVisibleDateTypeList: !this.state.isVisibleDateTypeList })
                          }}
                        >
                          Chọn ngày tốt/xấu <DownOutlined rotate={ this.state.isVisibleDateTypeList ? 180 : 0 } />
                        </Button>
                      </Col>
                    </Row>
                  </div>

                  {
                    this.state.isVisibleDateTypeList &&
                    <Card size="small" style={{ marginBottom: 10 }}>
                      <div className="box-checklist">
                        <div className="box-card-header">
                          <Button type="primary" ghost onClick={ () => this.onChangeCheckAllDateType(true) }>
                            Chọn tất cả
                          </Button>
                          &nbsp;
                          <Button type="primary" ghost onClick={ () => this.onChangeCheckAllDateType(false) }>
                            Bỏ chọn tất cả
                          </Button>
                        </div>

                        <Row>
                          { elementCheckboxDateTypeList }
                        </Row>
                      </div>
                    </Card>
                  }

                  {/*<div className="box-current-day">
                    { moment(tietKhiDatesAndTrucKienDates.thanhMinhDate).format(DATE_FORMAT) }
                  </div>*/}
                </div>
                <table>
                  <thead>
                    <tr>
                      <th>Hai</th>
                      <th>Ba</th>
                      <th>Tư</th>
                      <th>Năm</th>
                      <th>Sáu</th>
                      <th>Bảy</th>
                      <th>C.N</th>
                    </tr>
                  </thead>
                  <tbody>
                    { trElements }
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>

        <DateDetailModal
          ref={ this.refDateDetailModal }
          selectedDate={ selectedDate }
          tietKhiDatesAndTrucKienDates={ tietKhiDatesAndTrucKienDates }
        />
      </>
    )
  }
}
