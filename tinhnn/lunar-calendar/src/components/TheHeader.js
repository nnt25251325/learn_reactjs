import React from 'react'
import { Layout } from 'antd'

import TheMainNav from './TheMainNav'

const { Header } = Layout

const TheHeader = () => (
  <Header>
    <div className="container">
      <TheMainNav />
    </div>
  </Header>
)

export default TheHeader
