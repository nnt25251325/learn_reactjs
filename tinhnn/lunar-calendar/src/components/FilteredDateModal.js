/**
 * Chepvang Studio
 */

import React from 'react'
import PropTypes from 'prop-types'
// import moment from 'moment'

import {
  // Row,
  // Col,
  Button,
  // Card,
  // Checkbox,
  Modal
  // Typography,
  // InputNumber
} from 'antd'

import {
// DoubleLeftOutlined,
// DoubleRightOutlined,
// LeftOutlined,
// RightOutlined,
// DownOutlined
} from '@ant-design/icons'

import {
// LIKE_MONTH_LIKE_DATE
// LIKE_SEASON_LIKE_DATE
} from '../constants'

export default class FilteredDateModal extends React.Component {
  constructor(props) {
    super(props)

    // console.log('props', props)

    this.state = {
      isVisible: false,
      width: null,
      filteredDate: null
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log(222, nextProps)
    // console.log(2223, prevState)

    if (nextProps.filteredDate !== prevState.filteredDate) {
      const { width, filteredDate } = nextProps

      return {
        width,
        filteredDate
      }
    }

    else {
      return null
    }
  }

  onCancel = () => {
    this.setState({
      isVisible: false,
      filteredDate: null
    })
  }

  onOpen = () => {
    this.setState({
      isVisible: true
    })
  }

  // testxxx = () => {
  // }

  render() {
    const {
      isVisible,
      width,
      filteredDate
    } = this.state

    let list = null
    let referenceBookList = null

    if (
      filteredDate &&
      Array.isArray(filteredDate.list) &&
      filteredDate.list.length
    ) {
      if (filteredDate.viewType === 2) {
        list = filteredDate.list.map((item, index) => {
          return (
            <li key={ index } className="item">
              { item.name && <span className="name">{ item.name } </span> }
              { filteredDate.textBridge && <span className="text-bridge">{ filteredDate.textBridge } </span> }
              { item.values && <span className="values"><strong>{ item.values }</strong></span> }
            </li>
          )
        })
      } else {
        list = filteredDate.list.map((item, index) => {
          return (
            <li key={ index } className="item">
              { item.name && <div className="name">{ item.name }:</div> }
              {
                item.values &&
                <div className="values">
                  { filteredDate.textBridge && <span className="text-bridge">{ filteredDate.textBridge } </span> }
                  { item.values }
                </div>
              }
            </li>
          )
        })
      }
    }

    if (filteredDate && filteredDate.referenceBooks) {
      referenceBookList = filteredDate.referenceBooks.map((item, index) => {
        return (
          <li key={ index }>
            { item }
          </li>
        )
      })
    }

    return (
      <>
        <Modal
          title="Chi tiết bộ lọc"
          visible={ isVisible }
          wrapClassName="filtered-date-modal"
          width= { width }
          onCancel={ this.onCancel }
          footer={[
            <Button key="back" onClick={ this.onCancel }>
              Đóng
            </Button>
          ]}
        >
          {
            filteredDate && filteredDate.name &&
            <h4 className={ `txt-title-top ${filteredDate.goodDayStatus ? 'good-day' : 'bad-day'}` }>
              { filteredDate.name }
            </h4>
          }

          {
            Array.isArray(list) && list.length &&
            <div className={ `box-list view-type-${filteredDate.viewType}` }>
              <ul className="list">
                { list }
              </ul>
            </div>
          }

          {
            Array.isArray(referenceBookList) && referenceBookList.length &&
            <div className="box-reference-books">
              <h4 className="txt-title">Tham khảo sách:</h4>
              <ul>
                { referenceBookList }
              </ul>
            </div>
          }
        </Modal>
      </>
    )
  }
}

FilteredDateModal.propTypes = {
  width: PropTypes.number,
  filteredDate: PropTypes.object
}
