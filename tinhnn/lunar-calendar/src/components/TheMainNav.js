import React from 'react'
import PropTypes from 'prop-types'
import {
  Link,
  useRouteMatch
} from 'react-router-dom'

const MainNav = () => (
  <nav className="main-nav">
    <ul>
      <ActiveLink to="/" label="Trang chủ" activeOnlyWhenExact={ true } />
      <ActiveLink to="/month-calendar" label="Lịch dương" />
    </ul>
  </nav>
)

const ActiveLink = ({ label, to, activeOnlyWhenExact }) => {
  let match = useRouteMatch({
    path: to,
    exact: activeOnlyWhenExact
  })

  return (
    <li className={ match ? 'active' : '' }>
      <Link to={ to } className="nav-link">{ label }</Link>
    </li>
  )
}

ActiveLink.propTypes = {
  label: PropTypes.string,
  to: PropTypes.string,
  activeOnlyWhenExact: PropTypes.bool
}

export default MainNav
