import React from 'react'
import { Layout } from 'antd'

const { Footer } = Layout

const TheFooter = () => (
  <Footer>
    <div className="container">
      <div className="copyright">Chepvang Studio @ 2020</div>
    </div>
  </Footer>
)

export default TheFooter
