/**
 * Chepvang Studio
 */

import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

import {
  // Row,
  // Col,
  Button,
  // Card,
  // Checkbox,
  Modal
  // Typography,
  // InputNumber
} from 'antd'

import {
// DoubleLeftOutlined,
// DoubleRightOutlined,
// LeftOutlined,
// RightOutlined,
// DownOutlined
} from '@ant-design/icons'

import {
  // DATE_FORMAT,
  DAYS_IN_WEEK,
  TRUC_LIST
  // SAO_LIST,
  // MENH_LIST,
  // LUC_THAP_HOA_GIAP_LIST
} from '../constants'

import {
  getLunarDate,
  check30DaysInMonth,
  getCanChiDMY,
  getTietKhi,
  getCurrentTruc,
  getCanHour0,
  getSao,
  getLucThapHoaGiap
} from '../utils/lunar'

export default class DateDetailModal extends React.Component {
  constructor(props) {
    super(props)

    // console.log('props', props)

    this.state = {
      isVisible: false,
      width: null,

      selectedDate: null,
      currentLunarDate: null,
      tietKhiDatesAndTrucKienDates: null,
      is30DaysInMonth: false,
      canChiDMY: null,
      canHour0: null,
      currentTietKhi: null,
      currentTruc: null,
      currentSao: null,
      currentLucThapHoaGiap: null
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // console.log(222, nextProps)
    // console.log(2223, prevState)

    if (
      nextProps.selectedDate !== prevState.selectedDate &&
      nextProps.tietKhiDatesAndTrucKienDates !== prevState.tietKhiDatesAndTrucKienDates
    ) {
      const { width, selectedDate, tietKhiDatesAndTrucKienDates } = nextProps
      const currentLunarDate = getLunarDate(
        +moment(selectedDate).format('D'),
        +moment(selectedDate).format('M'),
        +moment(selectedDate).format('YYYY')
      )
      const { tietKhiDateList, trucKienDateList } = tietKhiDatesAndTrucKienDates
      const is30DaysInMonth = check30DaysInMonth(currentLunarDate)
      const canChiDMY = getCanChiDMY(currentLunarDate)
      const canHour0 = getCanHour0(currentLunarDate.jd)
      const currentTietKhi = getTietKhi(currentLunarDate.jd)
      const currentTruc = getCurrentTruc(
        selectedDate, trucKienDateList, tietKhiDateList, TRUC_LIST
      )
      const currentSao = getSao(selectedDate)
      const currentLucThapHoaGiap = getLucThapHoaGiap(canChiDMY.day.can.id, canChiDMY.day.chi.id)

      return {
        width,
        selectedDate,
        currentLunarDate,
        tietKhiDatesAndTrucKienDates,
        is30DaysInMonth,
        canChiDMY,
        canHour0,
        currentTietKhi,
        currentTruc,
        currentSao,
        currentLucThapHoaGiap
      }
    }

    else {
      return null
    }
  }

  onCancel = () => {
    this.setState({
      isVisible: false,
      selectedDate: null,
      currentLunarDate: null,
      tietKhiDatesAndTrucKienDates: null,
      is30DaysInMonth: false,
      canChiDMY: null,
      canHour0: null,
      currentTietKhi: null,
      currentTruc: null,
      currentSao: null,
      currentLucThapHoaGiap: null
    })
  }

  onOpen = () => {
    this.setState({
      isVisible: true
    })
  }

  // testxxx = () => {
  // }

  render() {
    const {
      isVisible,
      width,
      selectedDate,
      currentLunarDate,
      is30DaysInMonth,
      canChiDMY,
      currentTietKhi,
      canHour0,
      currentTruc,
      currentSao,
      currentLucThapHoaGiap
    } = this.state

    return (
      <>
        <Modal
          title="Chi tiết ngày"
          visible={ isVisible }
          wrapClassName="date-detail-modal"
          width= { width }
          onCancel={ this.onCancel }
          footer={[
            <Button key="back" onClick={ this.onCancel }>
              Đóng
            </Button>
          ]}
        >
          <div className="box-top mb-3">
            { selectedDate && <div className="text-center">
              <strong>Ngày dương: </strong>
              { DAYS_IN_WEEK[selectedDate.isoWeekday() - 1].name },&nbsp;
              Ngày { selectedDate && selectedDate.format('D') }&nbsp;
              Tháng { selectedDate && selectedDate.format('M') }&nbsp;
              Năm { selectedDate && selectedDate.format('YYYY') }
            </div> }

            { currentLunarDate && <div className="text-center">
              <strong>Ngày âm: </strong>
              Ngày { currentLunarDate.day }&nbsp;
              Tháng {
                currentLunarDate.month === 1 ? 'Giêng' :
                  currentLunarDate.month === 12 ? 'Chạp' :
                    currentLunarDate.month
              }
              { is30DaysInMonth ? ' (Đủ)' : ' (Thiếu)' }
              { currentLunarDate.leap === 1 ? ' (Nhuận)' : '' }&nbsp;
              Năm { canChiDMY && canChiDMY.yearName }
            </div> }

            <div className="text-center">
              <strong>Bát tự: </strong>
              { canHour0 && 'Giờ ' + canHour0 }
              { canChiDMY && ', Ngày ' + canChiDMY.dayName }
              { canChiDMY && ', Tháng ' + canChiDMY.monthName }
              { canChiDMY && ', Năm ' + canChiDMY.yearName }
            </div>
          </div>

          <div className="box-info-table mb-3">
            <table>
              <tbody>
                { currentTietKhi && <tr>
                  <td>Tiết khí</td>
                  <td>{ currentTietKhi.name }</td>
                </tr> }

                { currentTruc && <tr>
                  <td>Trực</td>
                  <td>{ currentTruc.name }</td>
                </tr> }

                { currentSao && <tr>
                  <td>Sao nhị thập bát tú</td>
                  <td>{ currentSao.name }</td>
                </tr> }

                { currentLucThapHoaGiap && <tr>
                  <td>Mệnh ngày</td>
                  <td>
                    {currentLucThapHoaGiap.menh.name}&nbsp;
                    ({currentLucThapHoaGiap.menh.interpretation})
                  </td>
                </tr> }
              </tbody>
            </table>
          </div>

        </Modal>
      </>
    )
  }
}

DateDetailModal.propTypes = {
  width: PropTypes.number,
  selectedDate: PropTypes.object,
  tietKhiDatesAndTrucKienDates: PropTypes.object
}
