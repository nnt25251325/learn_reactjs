// import React from 'react'
// import NotFound from '../pages/not-found';
import MonthCalendar from '../pages/month-calendar'
import MonthLunarCalendar from '../pages/month-lunar-calendar'

const routes = [
  {
    path: '/',
    exact: true,
    component: MonthLunarCalendar
  },
  {
    path: '/month-calendar',
    component: MonthCalendar
  }
  // {
  //   path: '/404',
  //   component: () => <NotFound />
  // }
]

export default routes
