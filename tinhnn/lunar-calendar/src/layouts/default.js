import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import { Layout } from 'antd'

import Routes from '../routes'
import TheHeader from '../components/TheHeader'
import TheFooter from '../components/TheFooter'

const { Content } = Layout

export default function DefaultLayout() {
  return (
    <Router>
      <Layout className="default-layout">
        <TheHeader />

        <Content>
          { contentMenuList(Routes) }
        </Content>

        <TheFooter />
      </Layout>
    </Router>
  )
}

const contentMenuList = (routes) => {
  let routeList = null

  if (routes.length > 0) {
    routeList = routes.map((route, index) => {
      return (
        <Route
          key={ index }
          path={ route.path }
          exact={ route.exact || false }
          component={ route.component }
        />
      )
    })
  }

  return (
    <Switch>
      { routeList }
      <Redirect to="/" />
      {/* <Redirect to="/404" /> */}
    </Switch>
  )
}
