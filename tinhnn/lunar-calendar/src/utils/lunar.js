/**
 * Chepvang Studio
 */

import {
  TK19,
  TK20,
  TK21,
  TK22,
  DATE_FORMAT,
  FIRST_DAY,
  LAST_DAY,
  CAN_LIST,
  CHI_LIST,
  TIET_KHI_LIST,
  SAO_LIST,
  SAO_GIAC_DATE,

  CHI_TYS,
  CHI_SUU,
  CHI_DAN,
  CHI_MAO,
  CHI_THIN,
  CHI_TYJ,
  CHI_NGO,
  CHI_MUI,
  CHI_THAN,
  CHI_DAU,
  CHI_TUAT,
  CHI_HOI,

  TIET_KHI_XUAN_PHAN,
  TIET_KHI_THANH_MINH,
  TIET_KHI_LAP_HA,
  TIET_KHI_MANG_CHUNG,
  TIET_KHI_HA_CHI,
  TIET_KHI_TIEU_THU,
  TIET_KHI_LAP_THU,
  TIET_KHI_BACH_LO,
  TIET_KHI_THU_PHAN,
  TIET_KHI_HAN_LO,
  TIET_KHI_LAP_DONG,
  TIET_KHI_DAI_TUYET,
  TIET_KHI_DONG_CHI,
  TIET_KHI_TIEU_HAN,
  TIET_KHI_LAP_XUAN,
  TIET_KHI_KINH_TRAP,

  MENH_LIST,
  LUC_THAP_HOA_GIAP_LIST
  // TRUC_LIST
} from '../constants'

import moment from 'moment'

/**
 * Số PI
 */
const PI = Math.PI

/* Create lunar date object, stores (lunar) date, month, year, leap month indicator, and Julian date number */
function LunarDate(dd, mm, yy, leap, jd) {
  this.day = dd
  this.month = mm
  this.year = yy
  this.leap = leap
  this.jd = jd
}

function jdn(dd, mm, yy) {
  const a = Math.floor((14 - mm) / 12)
  const y = yy+4800-a
  const m = mm+12*a-3
  const jd = dd + Math.floor((153*m+2)/5) + 365*y + Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400) - 32045

  return jd
}

function jdn2date (jd) {
  var Z,
    A,
    alpha,
    B,
    C,
    D,
    E,
    dd,
    mm,
    yyyy

  Z = jd
  if (Z < 2299161) {
    A = Z
  } else {
    alpha = Math.floor((Z - 1867216.25) / 36524.25)
    A = Z + 1 + alpha - Math.floor(alpha / 4)
  }
  B = A + 1524
  C = Math.floor((B - 122.1) / 365.25)
  D = Math.floor(365.25 * C)
  E = Math.floor((B - D) / 30.6001 )
  dd = Math.floor(B - D - Math.floor(30.6001 * E))

  if (E < 14) {
    mm = E - 1
  } else {
    mm = E - 13
  }
  if (mm < 3) {
    yyyy = C - 4715
  } else {
    yyyy = C - 4716
  }

  return [dd, mm, yyyy]
}

/**
 * Ngày bắt đầu trong ứng dụng lịch, Tết âm lịch năm 1800
 */
const FIRST_DAY_JDN = jdn(
  +FIRST_DAY.split('/')[0],
  +FIRST_DAY.split('/')[1],
  +FIRST_DAY.split('/')[2]
)

/**
 * Ngày kết thúc trong ứng dụng lịch
 */
const LAST_DAY_JDN = jdn(
  +LAST_DAY.split('/')[0],
  +LAST_DAY.split('/')[1],
  +LAST_DAY.split('/')[2]
)

function decodeLunarYear(yyyy, yearCodes) {
  let regularMonths,
    offsetOfTet,
    leapMonth,
    leapMonthLength,
    solarNY,
    currentJD,
    j
  let ly = []
  const monthLengths = [29, 30]

  regularMonths = [12]
  offsetOfTet = yearCodes >> 17

  leapMonth = yearCodes & 0xf
  // eslint-disable-next-line
  leapMonthLength = monthLengths[yearCodes >> 16 & 0x1];
  solarNY = jdn(1, 1, yyyy)
  currentJD = solarNY + offsetOfTet
  j = yearCodes >> 4

  for(let i = 0; i < 12; i++) {
    regularMonths[12 - i - 1] = monthLengths[j & 0x1]
    j >>= 1
  }

  if (leapMonth === 0) {
    for(let mm = 1; mm <= 12; mm++) {
      ly.push(new LunarDate(1, mm, yyyy, 0, currentJD))
      currentJD += regularMonths[mm - 1]
    }
  } else {
    for(let mm = 1; mm <= leapMonth; mm++) {
      ly.push(new LunarDate(1, mm, yyyy, 0, currentJD))
      currentJD += regularMonths[mm-1]
    }

    ly.push(new LunarDate(1, leapMonth, yyyy, 1, currentJD))
    currentJD += leapMonthLength

    for(let mm = leapMonth+1; mm <= 12; mm++) {
      ly.push(new LunarDate(1, mm, yyyy, 0, currentJD))
      currentJD += regularMonths[mm-1]
    }
  }

  // console.log(decodeLunarYear, ly);

  return ly
}

function getYearInfo(yyyy) {
  var yearCodes

  if (yyyy < 1900) {
    yearCodes = TK19[yyyy - 1800]
  } else if (yyyy < 2000) {
    yearCodes = TK20[yyyy - 1900]
  } else if (yyyy < 2100) {
    yearCodes = TK21[yyyy - 2000]
  } else {
    yearCodes = TK22[yyyy - 2100]
  }

  // console.log('getYearInfo', decodeLunarYear(yyyy, yearCodes));
  return decodeLunarYear(yyyy, yearCodes)
}

function findLunarDate(jd, ly) {
  if (jd > LAST_DAY_JDN || jd < FIRST_DAY_JDN || ly[0].jd > jd) {
    return new LunarDate(0, 0, 0, 0, jd)
  }

  let i = ly.length-1
  while (jd < ly[i].jd) {
    i--
  }

  const off = jd - ly[i].jd
  // console.log('findLunarDate off', off);
  const ret = new LunarDate(ly[i].day+off, ly[i].month, ly[i].year, ly[i].leap, jd)
  // console.log('findLunarDate', ret);

  return ret
}

/* Compute the longitude of the sun at any time.
 * Parameter: floating number jdn, the number of days since 1/1/4713 BC noon
 * Algorithm from: "Astronomical Algorithms" by Jean Meeus, 1998
 */
function sunLongitude(jdn) {
  let T,
    T2,
    dr,
    M,
    L0,
    DL,
    lambda,
    theta,
    omega

  T = (jdn - 2451545.0 ) / 36525 // Time in Julian centuries from 2000-01-01 12:00:00 GMT
  T2 = T*T
  dr = PI/180 // degree to radian
  M = 357.52910 + 35999.05030*T - 0.0001559*T2 - 0.00000048*T*T2 // mean anomaly, degree
  L0 = 280.46645 + 36000.76983*T + 0.0003032*T2 // mean longitude, degree
  DL = (1.914600 - 0.004817*T - 0.000014*T2)*Math.sin(dr*M)
  DL = DL + (0.019993 - 0.000101*T)*Math.sin(dr*2*M) + 0.000290*Math.sin(dr*3*M)
  theta = L0 + DL // true longitude, degree

  // obtain apparent longitude by correcting for nutation and aberration
  omega = 125.04 - 1934.136 * T
  lambda = theta - 0.00569 - 0.00478 * Math.sin(omega * dr)

  // Convert to radians
  lambda = lambda*dr
  lambda = lambda - PI*2*(Math.floor(lambda/(PI*2))) // Normalize to (0, 2*PI)

  return lambda
}

/* Compute the sun segment at start (00:00) of the day with the given integral Julian day number.
 * The time zone if the time difference between local time and UTC: 7.0 for UTC+7:00.
 * The function returns a number between 0 and 23.
 * From the day after March equinox and the 1st major term after March equinox, 0 is returned.
 * After that, return 1, 2, 3 ...
 */
function getSunLongitude(dayNumber, timeZone) {
  return Math.floor(sunLongitude(dayNumber - 0.5 - timeZone/24.0) / PI * 12)
}

/*
 * getLunarDate
 */
export function getLunarDate(dd, mm, yyyy) {
  // console.log('xxx,', dd, mm, yyyy);

  let ly,
    jd

  if (yyyy < 1800 || 2199 < yyyy) {
    return new LunarDate(0, 0, 0, 0, 0)
  }

  ly = getYearInfo(yyyy)
  jd = jdn(dd, mm, yyyy)

  if (jd < ly[0].jd) {
    ly = getYearInfo(yyyy - 1)
  }

  // console.log('dd, mm, yyyy, getLunarDate ->', dd, mm, yyyy, findLunarDate(jd, ly));

  return findLunarDate(jd, ly)
}

/*
 * getSolarDate
 *
 * @params {Number} dd - Ngày âm lịch
 * @params {Number} mm - Tháng âm lịch
 * @params {Number} yyyy - Năm âm lịch
 */
export function getSolarDate(dd, mm, yyyy) {
  if (yyyy < 1800 || yyyy > 2199) {
    return new LunarDate(0, 0, 0, 0, 0)
  }

  const ly = getYearInfo(yyyy)
  let lm = ly[mm - 1]

  if (lm.month !== mm) {
    lm = ly[mm]
  }

  const ld = lm.jd + dd - 1

  return jdn2date(ld)
}

/*
 * Kiểm tra tháng đủ/tháng thiếu
 *
 * @params {Object} currentLunarDate - Đối tượng ngày âm
 * @return {Boolean} True nếu là tháng đủ 30 ngày
 */
export function check30DaysInMonth(currentLunarDate) {
  if (!currentLunarDate) {
    return
  }

  // - Hàm getSolarDate sẽ nhận vào ngày tháng năm âm, trả về ngày tháng năm dương thực tế là sonarDate,
  // ngày âm của ngày sonarDate có thể là ngày 30 của tháng hiện tại hoặc là ngày 29 của tháng kế tiếp,
  // vì vậy, để biết thực hư thế này, ta lấy kết quả được trả về (ngày dương sonarDate),
  // cho qua hàm tìm ngày âm getLunarDate, trả về ngày tháng năm âm thực tế và chỉ số năm nhuận leap.
  // nếu như currentLunarDate là tháng đủ, thì kết quả được trả về (lunarDate) sẽ là lunarDate.day = 30,
  // còn ngược lại thì lunarDate.day = 1,
  // vì currentLunarDate là tháng thiếu nên tiến 30 ngày sẽ nhảy sang mùng 1 của tháng tiếp theo.
  //
  // - Nếu là tháng nhuận, để tìm được số ngày trong tháng nhuận,
  // dùng ngày dương sonarDate cộng số ngày trong tháng không nhuận trước đó,
  // sẽ được ngày dương mới là sonarDate2,
  // lại dùng hàm getSolarDate để biết được ngày âm thực tế, lunarDate2.day = 30 hoặc = 1
  //
  // - Ví dụ: Tìm xem tháng 4/2020 ÂL là tháng đủ hay tháng thiếu:
  // truyền 30/4/2020 vào getSolarDate sẽ được sonarDate là 22/5/2020 DL,
  // truyền 22/5/2020 DL vào getLunarDate sẽ được lunarDate là 30/4/2020 ÂL,
  // lunarDate.day = 30 chứng tỏ tháng 4/2020 ÂL là tháng đủ 30 ngày.
  // - Khi lặp đế ngày 23/5/2020 thì thấy currentLunarDate.leap = 1,
  // vậy năm 2020 ÂL sẽ nhuận vào tháng 4, giờ tìm số ngày trong tháng nhuận 4/2020 ÂL,
  // ta đã biết ngày 22/5/2020 DL có ngày âm là 30/4/2020 ÂL và tháng 4/2020 ÂL là tháng đủ,
  // lấy 22/5/2020 DL cộng thêm số ngày của tháng không nhuận trước đó (tháng đủ nên cộng 30 ngày),
  // sẽ ra ngày dương sonarDate2 = 21/6/2020 DL,
  // truyền 21/6/2020 DL vào getLunarDate sẽ được lunarDate2 là 1/5/2020 ÂL,
  // lunarDate2.day = 1 chứng tỏ tháng nhuận 4/2020 ÂL là tháng thiếu nên có 29 ngày.
  const sonarDate = getSolarDate(30, currentLunarDate.month, currentLunarDate.year)
  const lunarDate = getLunarDate(sonarDate[0], sonarDate[1], sonarDate[2])

  if (lunarDate.day === 30 && currentLunarDate.leap === 0) {
    // Kiểm tra trường hợp tháng không nhuận
    // lunarDate.day sẽ luôn bằng 30 hoặc bằng 1
    return true
  }

  if (currentLunarDate.leap === 1) {
    // Kiểm tra trường hợp tháng nhuận
    const addDays = lunarDate.day === 30 ? 30 : 29
    const sonarDate2 = moment(`${sonarDate[0]}/${sonarDate[1]}/${sonarDate[2]}`, DATE_FORMAT)
      .add(addDays, 'day')
    const lunarDate2 = getLunarDate(
      +moment(sonarDate2).format('D'),
      +moment(sonarDate2).format('M'),
      +moment(sonarDate2).format('YYYY')
    )

    // So sánh số ngày trong tháng nhuận với 30
    if (lunarDate2.day === 30) {
      return true
    }
  }

  return false
}

/*
 * getCanChiDMY
 */
export function getCanChiDMY(lunarDate) {
  if (!lunarDate) {
    return
  }

  const canDay = CAN_LIST.find(item => item.id === (lunarDate.jd + 9) % 10 + 1)
  const canMonth = CAN_LIST.find(item => {
    return item.id === (lunarDate.year * 12 + lunarDate.month + 3) % 10 + 1
  })
  const canYear = CAN_LIST.find(item => item.id === (lunarDate.year + 6) % 10 + 1)

  const chiDay = CHI_LIST.find(item => item.id === (lunarDate.jd + 1) % 12 + 1)
  const chiMonth = CHI_LIST.find(item => item.id === (lunarDate.month + 1) % 12 + 1)
  const chiYear = CHI_LIST.find(item => item.id === (lunarDate.year + 8) % 12 + 1)

  return {
    day: {
      can: canDay ? canDay : null,
      chi: chiDay ? chiDay : null
    },
    month: {
      can: canMonth ? canMonth : null,
      chi: chiMonth ? chiMonth : null
    },
    year: {
      can: canYear ? canYear : null,
      chi: chiYear ? chiYear : null
    },
    dayName: canDay && chiDay ? canDay.name + ' ' + chiDay.name : null,
    monthName: canMonth && chiMonth ? canMonth.name + ' ' + chiMonth.name : null,
    yearName: canYear && chiYear ? canYear.name + ' ' + chiYear.name : null
  }
}

/*
 * getTietKhi
 */
export function getTietKhi(jd) {
  if (!jd) {
    return
  }

  const tietKhi = TIET_KHI_LIST.find(item => {
    return item.id === getSunLongitude(jd + 1, 7.0) + 1
  })

  return tietKhi ? tietKhi : null
}

/*
 * Tìm tiết khí và ngày khởi đầu trực Kiến.
 * Một năm có 24 tiết khí nhưng chỉ tìm 16 tiết khí vì 16 là đủ cho tìm ngày.
 * 4 tiết khí Xuân phân, Hạ chí, Thu phân, Đông chí để cho việc tìm 4 ngày tứ Ly,
 * nên không cần tìm ngày khởi đầu trực Kiến cho 4 tiết khí trên.
 * Cứ thay đổi năm là sẽ phải gọi lại hàm này.
 */
export function getTietKhiDatesAndTrucKienDates(currentYear) {
  console.log('getTietKhiDatesAndTrucKienDates ttt', currentYear)
  let lapDongNamTruocDate = null
  let daiTuyetNamTruocDate = null
  let dongChiNamTruocDate = null
  let tieuHanDate = null
  let lapXuanDate = null
  let kinhTrapDate = null
  let xuanPhanDate = null
  let thanhMinhDate = null
  let lapHaDate = null
  let mangchungDate = null
  let haChiDate = null
  let tieuThuDate = null
  let lapThuDate = null
  let bachLoDate = null
  let thuPhanDate = null
  let hanLoDate = null
  let lapDongDate = null
  let daiTuyetDate = null
  let dongChiDate = null
  let tieuHanNamSauDate = null
  let lapXuanNamSauDate = null

  let trucKienSauLapDongNamTruocDate = null
  let trucKienSauDaiTuyetNamTruocDate = null
  let trucKienSauLapXuanDate = null
  let trucKienSauKinhTrapDate = null
  let trucKienSauThanhMinhDate = null
  let trucKienSauLapHaDate = null
  let trucKienSauMangchungDate = null
  let trucKienSauTieuThuDate = null
  let trucKienSauLapThuDate = null
  let trucKienSauBachLoDate = null
  let trucKienSauHanLoDate = null
  let trucKienSauLapDongDate = null
  let trucKienSauDaiTuyetDate = null
  let trucKienSauTieuHanDate = null
  let trucKienSauTieuHanNamSauDate = null
  let trucKienSauLapXuanNamSauDate = null

  // Định dạng của item trong mảng: [day, month, year, position of year]
  const dateArray = [
    // Tìm Lập đông năm trước, thường là 7/11, 8/11
    [6, 11, +currentYear - 1, -1],
    [7, 11, +currentYear - 1, -1],
    [8, 11, +currentYear - 1, -1],

    // Tìm Đại tuyết năm trước, thường là 7/12, 8/12
    [6, 12, +currentYear - 1, -1],
    [7, 12, +currentYear - 1, -1],
    [8, 12, +currentYear - 1, -1],

    // Tìm Đông chí năm trước, thường là 21/12, 22/12
    [21, 12, +currentYear - 1, -1],
    [22, 12, +currentYear - 1, -1],
    [23, 12, +currentYear - 1, -1],

    // Tìm Tiểu hàn, thường là 5/1, 6/1
    [4, 1, +currentYear],
    [5, 1, +currentYear],
    [6, 1, +currentYear],
    [7, 1, +currentYear],

    // Tìm Lập xuân, thường là 4/2, 5/2
    [3, 2, +currentYear],
    [4, 2, +currentYear],
    [5, 2, +currentYear],

    // Tìm Kinh trập, thường là 5/3, 6/3
    [4, 3, +currentYear],
    [5, 3, +currentYear],
    [6, 3, +currentYear],
    [7, 3, +currentYear],

    // Tìm Xuân phân, thường là 20/3, 21/3
    [19, 3, +currentYear],
    [20, 3, +currentYear],
    [21, 3, +currentYear],
    [22, 3, +currentYear],

    // Tìm Thanh minh, thường là 4/4, 5/4
    [4, 4, +currentYear],
    [5, 4, +currentYear],
    [6, 4, +currentYear],

    // Tìm Lập hạ, thường là 5/5, 6/5
    [4, 5, +currentYear],
    [5, 5, +currentYear],
    [6, 5, +currentYear],
    [7, 5, +currentYear],

    // Tìm Mang chủng, thường là 5/6, 6/6
    [4, 6, +currentYear],
    [5, 6, +currentYear],
    [6, 6, +currentYear],
    [7, 6, +currentYear],

    // Tìm Hạ chí, thường là 21/6, 22/6
    [20, 6, +currentYear],
    [21, 6, +currentYear],
    [22, 6, +currentYear],

    // Tìm Tiểu thử, thường là 7/7, 8/7
    [6, 7, +currentYear],
    [7, 7, +currentYear],
    [8, 7, +currentYear],

    // Tìm Lập thu, thường là 7/8, 8/8
    [6, 8, +currentYear],
    [7, 8, +currentYear],
    [8, 8, +currentYear],

    // Tìm Bạch lộ, thường là 7/9, 8/9
    [6, 9, +currentYear],
    [7, 9, +currentYear],
    [8, 9, +currentYear],
    [9, 9, +currentYear],

    // Tìm Thu phân, thường là 22/9, 23/9
    [22, 9, +currentYear],
    [23, 9, +currentYear],
    [24, 9, +currentYear],

    // Tìm Hàn lộ, thường là 8/10, 9/10
    [7, 10, +currentYear],
    [8, 10, +currentYear],
    [9, 10, +currentYear],

    // Tìm Lập đông, thường là 7/11, 8/11
    [6, 11, +currentYear],
    [7, 11, +currentYear],
    [8, 11, +currentYear],

    // Tìm Đại tuyết, thường là 7/12, 8/12
    [6, 12, +currentYear],
    [7, 12, +currentYear],
    [8, 12, +currentYear],

    // Tìm Đông chí, thường là 21/12, 22/12
    [21, 12, +currentYear],
    [22, 12, +currentYear],
    [23, 12, +currentYear],

    // Tìm Tiểu hàn năm sau, thường là 5/1, 6/1
    [4, 1, +currentYear + 1, 1],
    [5, 1, +currentYear + 1, 1],
    [6, 1, +currentYear + 1, 1],
    [7, 1, +currentYear + 1, 1],

    // Tìm Lập xuân năm sau, thường là 4/1, 5/1
    [3, 2, +currentYear + 1, 1],
    [4, 2, +currentYear + 1, 1],
    [5, 2, +currentYear + 1, 1]
  ]

  // Hàm findTrucKienDate để tìm ngày mà trực Kiến khởi đầu.
  // tietKhiDate là ngày bắt đầu 1 tiết khí.
  // CHI là chi của ngày khởi đầu trực Kiến.
  // Quy luật: Sau 12 tiết khí dưới đây, đều có ngày khởi đầu của trực Kiến.
  // -> Sau Lập xuân --- Trực Kiến khởi đầu tại ngày Dần.
  // -> Sau Kinh trập --- Trực Kiến khởi đầu tại ngày Mão.
  // -> Sau Thanh minh --- Trực Kiến khởi đầu tại ngày Thìn.
  // -> Sau Lập hạ --- Trực Kiến khởi đầu tại ngày Tỵ.
  // -> Sau Mang chủng --- Trực Kiến khởi đầu tại ngày Ngọ.
  // -> Sau Tiểu thử --- Trực Kiến khởi đầu tại ngày Mùi.
  // -> Sau Lập thu --- Trực Kiến khởi đầu tại ngày Thân.
  // -> Sau Bạch lộ --- Trực Kiến khởi đầu tại ngày Dậu.
  // -> Sau Hàn lộ --- Trực Kiến khởi đầu tại ngày Tuất.
  // -> Sau Lập đông --- Trực Kiến khởi đầu tại ngày Hợi.
  // -> Sau Đại tuyết --- Trực Kiến khởi đầu tại ngày Tý.
  // -> Sau Tiểu hàn --- Trực Kiến khởi đầu tại ngày Sửu.
  // Sau Lập Xuân (Nghĩa là ngày bắt đầu Lập xuân).
  // jd là Julian date của ngày tiết khí tietKhiDate.
  // chiCurrent - Tìm chi của ngày tiết khí tietKhiDate.
  // Tìm khoangCachDenTrucKien, là số ngày từ ngày tiết khí tietKhiDate đến ngày khởi đầu trực Kiến.
  // Mỗi ngày 1 chi, lặp lần lượt từ Tý tới Hợi, biết được chi của ngày tiết khí tietKhiDate,
  // Sẽ tính được số ngày đến một chi nhất định.
  // Ví dụ: Ngày 5/3/2020 (Ngày Đinh Mùi) là ngày bắt đầu của tiết Kinh trập,
  // Mà "Sau Kinh trập --- Trực Kiến khởi đầu tại ngày Mão".
  // Vậy sau 5/3/2020, ngày Mão gần nhất là ngày 13/3/2020 (Ngày Ất Mão).
  // Vậy phải tính số khoangCachDenTrucKien, cộng với ngày 5/3/2020, sẽ ra được ngày 13/3/2020.
  const findTrucKienDate = (tietKhiDate, CHI, jd) => {
    const chiCurrent = CHI_LIST.find(item => item.id === (jd + 1) % 12 + 1)
    const khoangCachDenTrucKien = CHI > chiCurrent.id ?
      CHI - chiCurrent.id :
      12 - chiCurrent.id + CHI

    return moment(tietKhiDate).add(khoangCachDenTrucKien, 'day')
  }

  const forTheEndOf2199 = (month, year, yearPosition, lunarDate) => {
    // Tìm Tiểu hàn năm 2200 (Thêm phần này để dữ liệu hiển thị Trực của tháng 12/2199 không bị lỗi)
    if (
      yearPosition === 1 && !tieuHanNamSauDate && month === 1 &&
      year === 2200
    ) {
      console.log('5/1/2200')
      tieuHanNamSauDate = moment('5/1/2200', DATE_FORMAT)
      trucKienSauTieuHanNamSauDate = findTrucKienDate(tieuHanNamSauDate, CHI_SUU, lunarDate.jd)
    }

    // Tìm Lập xuân năm 2200 (Thêm phần này để dữ liệu hiển thị Trực của tháng 12/2199 không bị lỗi)
    if (
      yearPosition === 1 && !lapXuanNamSauDate && month === 2 &&
      year === 2200
    ) {
      console.log('4/2/2200')
      lapXuanNamSauDate = moment('4/2/2200', DATE_FORMAT)
      trucKienSauLapXuanNamSauDate = findTrucKienDate(lapXuanNamSauDate, CHI_DAN, lunarDate.jd)
    }
  }

  // Lặp từ qua từng phần tử của mảng dateArray, các phần tử sắp xếp theo thứ tự thời gian tăng dần.
  // Trong 2 đến 4 ngày Dương lịch liên tiếp (đã được định nghĩa trong mảng),
  // sẽ luôn có một ngày rơi vào ngày bắt đầu của 1 tiết khí trong năm (Dùng hàm getTietKhi để xác định),
  // mỗi tiết khi sẽ luôn nằm trong 1 tháng nhất định,
  // tìm được ngày nào đầu tiên thì ngày đó sẽ là ngày bắt đầu 1 tiết khí.
  // Chẳng hạn Lập xuân, luôn rơi vào 3/2 hoặc 4/2 hoặc 5/2 Dương lịch.
  dateArray.forEach(item => {
    const day = item[0]
    const month = item[1]
    const year = item[2]
    const yearPosition = item[3]
    const lunarDate = getLunarDate(day, month, year)
    const tietKhi = getTietKhi(lunarDate.jd)

    forTheEndOf2199(month, year, yearPosition, lunarDate)

    if (!lunarDate || !tietKhi) {
      return
    }

    // Tìm Lập đông năm trước
    if (
      yearPosition === -1 && !lapDongNamTruocDate && month === 11 &&
      tietKhi.id === TIET_KHI_LAP_DONG
    ) {
      lapDongNamTruocDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauLapDongNamTruocDate = findTrucKienDate(lapDongNamTruocDate, CHI_HOI, lunarDate.jd)
    }

    // Tìm Đại tuyết năm trước
    if (
      yearPosition === -1 && !daiTuyetNamTruocDate && month === 12 &&
      tietKhi.id === TIET_KHI_DAI_TUYET
    ) {
      daiTuyetNamTruocDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauDaiTuyetNamTruocDate = findTrucKienDate(daiTuyetNamTruocDate, CHI_TYS, lunarDate.jd)
    }

    // Tìm Đông chí năm trước
    if (
      yearPosition === -1 && !dongChiNamTruocDate && month === 12 &&
      tietKhi.id === TIET_KHI_DONG_CHI
    ) {
      dongChiNamTruocDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
    }

    // Tìm Tiểu hàn
    if (
      !yearPosition && !tieuHanDate && month === 1 &&
      tietKhi.id === TIET_KHI_TIEU_HAN
    ) {
      tieuHanDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauTieuHanDate = findTrucKienDate(tieuHanDate, CHI_SUU, lunarDate.jd)
    }

    // Tìm Lập xuân
    if (
      !yearPosition && !lapXuanDate && month === 2 &&
      tietKhi.id === TIET_KHI_LAP_XUAN
    ) {
      lapXuanDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauLapXuanDate = findTrucKienDate(lapXuanDate, CHI_DAN, lunarDate.jd)
    }

    // Tìm Kinh trập
    if (
      !yearPosition && !kinhTrapDate && month === 3 &&
      tietKhi.id === TIET_KHI_KINH_TRAP
    ) {
      kinhTrapDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauKinhTrapDate = findTrucKienDate(kinhTrapDate, CHI_MAO, lunarDate.jd)
    }

    // Tìm Xuân phân
    if (
      !yearPosition && !xuanPhanDate && month === 3 &&
      tietKhi.id === TIET_KHI_XUAN_PHAN
    ) {
      xuanPhanDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
    }

    // Tìm Thanh minh
    if (
      !yearPosition && !thanhMinhDate && month === 4 &&
      tietKhi.id === TIET_KHI_THANH_MINH
    ) {
      thanhMinhDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauThanhMinhDate = findTrucKienDate(thanhMinhDate, CHI_THIN, lunarDate.jd)
    }

    // Tìm Lập hạ
    if (
      !yearPosition && !lapHaDate && month === 5 &&
      tietKhi.id === TIET_KHI_LAP_HA
    ) {
      lapHaDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauLapHaDate = findTrucKienDate(lapHaDate, CHI_TYJ, lunarDate.jd)
    }

    // Tìm Mang chủng
    if (
      !yearPosition && !mangchungDate && month === 6 &&
      tietKhi.id === TIET_KHI_MANG_CHUNG
    ) {
      mangchungDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauMangchungDate = findTrucKienDate(mangchungDate, CHI_NGO, lunarDate.jd)
    }

    // Tìm Hạ chí
    if (
      !yearPosition && !haChiDate && month === 6 &&
      tietKhi.id === TIET_KHI_HA_CHI
    ) {
      haChiDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
    }

    // Tìm Tiểu thử
    if (
      !yearPosition && !tieuThuDate && month === 7 &&
      tietKhi.id === TIET_KHI_TIEU_THU
    ) {
      tieuThuDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauTieuThuDate = findTrucKienDate(tieuThuDate, CHI_MUI, lunarDate.jd)
    }

    // Tìm Lập thu
    if (
      !yearPosition && !lapThuDate && month === 8 &&
      tietKhi.id === TIET_KHI_LAP_THU
    ) {
      lapThuDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauLapThuDate = findTrucKienDate(lapThuDate, CHI_THAN, lunarDate.jd)
    }

    // Tìm Bạch lộ
    if (
      !yearPosition && !bachLoDate && month === 9 &&
      tietKhi.id === TIET_KHI_BACH_LO
    ) {
      bachLoDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauBachLoDate = findTrucKienDate(bachLoDate, CHI_DAU, lunarDate.jd)
    }

    // Tìm Thu phân
    if (
      !yearPosition && !thuPhanDate && month === 9 &&
      tietKhi.id === TIET_KHI_THU_PHAN
    ) {
      thuPhanDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
    }

    // Tìm Hàn lộ
    if (
      !yearPosition && !hanLoDate && month === 10 &&
      tietKhi.id === TIET_KHI_HAN_LO
    ) {
      hanLoDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauHanLoDate = findTrucKienDate(hanLoDate, CHI_TUAT, lunarDate.jd)
    }

    // Tìm Lập đông
    if (
      !yearPosition && !lapDongDate && month === 11 &&
      tietKhi.id === TIET_KHI_LAP_DONG
    ) {
      lapDongDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauLapDongDate = findTrucKienDate(lapDongDate, CHI_HOI, lunarDate.jd)
    }

    // Tìm Đại tuyết
    if (
      !yearPosition && !daiTuyetDate && month === 12 &&
      tietKhi.id === TIET_KHI_DAI_TUYET
    ) {
      daiTuyetDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauDaiTuyetDate = findTrucKienDate(daiTuyetDate, CHI_TYS, lunarDate.jd)
    }

    // Tìm Đông chí
    if (
      !yearPosition && !dongChiDate && month === 12 &&
      tietKhi.id === TIET_KHI_DONG_CHI
    ) {
      dongChiDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
    }

    // Tìm Tiểu hàn năm sau
    if (
      yearPosition === 1 && !tieuHanNamSauDate && month === 1 &&
      tietKhi.id === TIET_KHI_TIEU_HAN
    ) {
      tieuHanNamSauDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauTieuHanNamSauDate = findTrucKienDate(tieuHanNamSauDate, CHI_SUU, lunarDate.jd)
    }

    // Tìm Lập xuân năm sau
    if (
      yearPosition === 1 && !lapXuanNamSauDate && month === 2 &&
      tietKhi.id === TIET_KHI_LAP_XUAN
    ) {
      lapXuanNamSauDate = moment(`${day}/${month}/${year}`, DATE_FORMAT)
      trucKienSauLapXuanNamSauDate = findTrucKienDate(lapXuanNamSauDate, CHI_DAN, lunarDate.jd)
    }
  })

  return {
    lapDongNamTruocDate,
    daiTuyetNamTruocDate,
    dongChiNamTruocDate,
    tieuHanDate,
    lapXuanDate,
    kinhTrapDate,
    xuanPhanDate,
    thanhMinhDate,
    lapHaDate,
    mangchungDate,
    haChiDate,
    tieuThuDate,
    lapThuDate,
    bachLoDate,
    thuPhanDate,
    hanLoDate,
    lapDongDate,
    daiTuyetDate,
    dongChiDate,
    tieuHanNamSauDate,
    lapXuanNamSauDate,

    // Mảng tietKhiDateList và trucKienDateList.
    // Để sau này xác định trực của ngày hiện tại trong vòng lặp.
    // Xem hàm getCurrentTruc để hiểu rõ hơn.
    tietKhiDateList: [
      lapDongNamTruocDate,
      daiTuyetNamTruocDate,
      tieuHanDate,
      lapXuanDate,
      kinhTrapDate,
      thanhMinhDate,
      lapHaDate,
      mangchungDate,
      tieuThuDate,
      lapThuDate,
      bachLoDate,
      hanLoDate,
      lapDongDate,
      daiTuyetDate,
      tieuHanNamSauDate,
      lapXuanNamSauDate
    ],

    // Mỗi phần tử trong tietKhiDateList sẽ tương ứng với 1 phần tử trong tietKhiDateList
    trucKienDateList: [
      trucKienSauLapDongNamTruocDate,
      trucKienSauDaiTuyetNamTruocDate,
      trucKienSauTieuHanDate,
      trucKienSauLapXuanDate,
      trucKienSauKinhTrapDate,
      trucKienSauThanhMinhDate,
      trucKienSauLapHaDate,
      trucKienSauMangchungDate,
      trucKienSauTieuThuDate,
      trucKienSauLapThuDate,
      trucKienSauBachLoDate,
      trucKienSauHanLoDate,
      trucKienSauLapDongDate,
      trucKienSauDaiTuyetDate,
      trucKienSauTieuHanNamSauDate,
      trucKienSauLapXuanNamSauDate
    ],

    seasonStartDateList: [
      {
        id: TIET_KHI_LAP_DONG,
        date: lapDongNamTruocDate
      },
      {
        id: TIET_KHI_LAP_XUAN,
        date: lapXuanDate
      },
      {
        id: TIET_KHI_LAP_HA,
        date: lapHaDate
      },
      {
        id: TIET_KHI_LAP_THU,
        date: lapThuDate
      },
      {
        id: TIET_KHI_LAP_DONG,
        date: lapDongDate
      },
      {
        id: TIET_KHI_LAP_XUAN,
        date: lapXuanNamSauDate
      }
    ],

    currentYear: +currentYear
  }
}


/*
 * Tìm ngày bắt đầu trong một mùa
 */
export function getSeasonStartDate(dateInLoop, seasonStartDateList) {
  if (!dateInLoop.isValid() || !seasonStartDateList) {
    return
  }

  let seasonStartDate = null

  for (let i = 0; i < seasonStartDateList.length; i++) {
    if (dateInLoop >= seasonStartDateList[i].date && dateInLoop < seasonStartDateList[i+1].date) {
      const seasonId = seasonStartDateList[i].id
      const startDate = seasonStartDateList[i].date
      const lunarDate = getLunarDate(
        +moment(startDate).format('D'),
        +moment(startDate).format('M'),
        +moment(startDate).format('YYYY')
      )

      seasonStartDate = {
        id: seasonId,
        name: TIET_KHI_LIST.find(item => item.id === seasonId).name,
        date: startDate,
        lunarDate: lunarDate,
        status: dateInLoop.isSame(startDate)
      }

      break
    }
  }

  return seasonStartDate ? seasonStartDate : null
}


/*
 * Tìm trực của ngày
 */
export function getCurrentTruc(dateInLoop, trucKienDateList, tietKhiDateList, trucList) {
  if (!dateInLoop.isValid() || !trucKienDateList || !tietKhiDateList || !trucList) {
    return
  }


  let currentTruc = null

  // Lặp qua danh sách ngày khởi đầu trực Kiến sau mỗi tiết khí (Gồm 12 tiết khí để xác định trực).
  // Tìm xem ngày hiện tại đang nằm giữa 2 ngày khởi đầu trực Kiến nào.
  // Tính số ngày từ ngày hiện tại về lại ngày trực Kiến gần nhất (trong quá khứ).
  // Từ đó chỉ lấy phần dư để tính được trực của ngày hiện tại.
  // Có một quy luật nữa là giao giữa 2 tiết khí là 2 ngày liên tiếp (giả sử ngày a và ngày b) sẽ có trực giống nhau.
  // Ngày b có trực giống với ngày a, chính là đoạn code dateInLoop.isSameOrAfter(tietKhiDateList[i+1])
  // Khi lặp bảng qua ngày giao nhau này, từ ngày bắt đầu tiết khí mới phải phải có khoảng cách từ trực Kiến trực đi 1.
  for (let i = 0; i < trucKienDateList.length; i++) {
    if (dateInLoop >= trucKienDateList[i] && dateInLoop < trucKienDateList[i+1]) {
      let distanceFromTrucKien = dateInLoop.diff(trucKienDateList[i], 'days')

      if (dateInLoop.isSameOrAfter(tietKhiDateList[i+1])) {
        distanceFromTrucKien--
      }

      currentTruc = trucList.find(item => item.id === distanceFromTrucKien % 12 + 1)

      break
    }
  }

  return currentTruc ? currentTruc : null
}

/*
 * Tìm trực và tiết khí của 2 ngày giao giữa hai tiết khí
 */
export function getTietKhiAndTrucOf2Days(dateInLoop, trucKienDateList, tietKhiDateList, trucList, jd) {
  if (!dateInLoop.isValid() || !trucKienDateList || !tietKhiDateList || !trucList || !jd) {
    return
  }

  let currentTruc = null
  let currentTietKhi = null

  for (let i = 0; i < trucKienDateList.length; i++) {
    if (dateInLoop >= trucKienDateList[i] && dateInLoop < trucKienDateList[i+1]) {
      let distanceFromTrucKien = dateInLoop.diff(trucKienDateList[i], 'days')

      if (dateInLoop.diff(tietKhiDateList[i+1], 'days') === -1) {
        currentTruc = trucList.find(item => item.id === distanceFromTrucKien % 12 + 1)
        currentTietKhi = getTietKhi(jd)
      }

      if (dateInLoop.isSame(tietKhiDateList[i+1])) {
        distanceFromTrucKien--
        currentTruc = trucList.find(item => item.id === distanceFromTrucKien % 12 + 1)
        currentTietKhi = getTietKhi(jd)
      }

      break
    }
  }

  return {
    currentTietKhi: currentTietKhi ? currentTietKhi : null,
    currentTruc: currentTruc ? currentTruc : null
  }
}

/*
 * Can của giờ chính Tý (00:00) - Giờ đầu ngày
 */
export function getCanHour0(jd) {
  if (!jd) {
    return
  }

  const can = CAN_LIST.find(item => item.id === (jd - 1) * 2 % 10 + 1)
  const chi = CHI_LIST.find(item => item.id === 1)

  return can && chi ? can.name + ' ' + chi.name : null
}

/*
 * Tìm sao của ngày (Có 28 sao trong Nhị thập bát tú).
 * Các thứ trong tuần để định ước vị trí trực chiếu của các sao,
 * một tuần có bảy ngày thì mỗi sao quản lý một ngày.
 * Nếu biết được 1 ngày được xác định trước là sao Giác,
 * thì xem số chênh lệch giữa ngày đã xác đinh trước và ngày hiện tại là biết được sao của ngày hiện tại.
 * Thứ 2: Nguy, Tất, Trương, Tâm.
 * Thứ 3: Vĩ, Thất, Chủy, Dực.
 * Thứ 4: Cơ, Bích, Sâm, Chẩn.
 * Thứ 5: Giác, Đẩu, Khuê, Tỉnh.
 * Thứ 6: Cang, Ngưu, Lâu, Quỷ.
 * Thứ 7: Đê, Nữ, Vị, Liễu.
 * Chủ nhật: Phòng, Hư, Mão, Tinh.
 */
export function getSao(dateInLoop) {
  if (!dateInLoop.isValid()) {
    return
  }

  let sao = null
  const days = dateInLoop.diff(moment(SAO_GIAC_DATE, DATE_FORMAT), 'days')

  if (days >= 0) {
    sao = SAO_LIST.find(item => item.id === days % 28 + 1)
  } else {
    sao = SAO_LIST.find(item => item.id === 28 - Math.abs(days % 28) + 1)
  }

  return sao ? sao : null
}

/*
 * Tìm Hoa giáp trong 60 Hoa giáp.
 */
export function getLucThapHoaGiap(canId, chiId) {
  if (!canId || !chiId) {
    return
  }

  const hoaGiap = LUC_THAP_HOA_GIAP_LIST.find(item => item.canId === canId && item.chiId === chiId)
  const menh = hoaGiap && MENH_LIST.find(item => item.id === hoaGiap.menhId)

  // Cách viết bên trên (const menh = hoaGiap && abc) tương ứng với cách viết bên dưới
  // let menh = null;
  // if (hoaGiap) {
  //   menh = MENH_LIST.find(item => item.id === hoaGiap.menhId);
  // }

  return {
    hoaGiap: hoaGiap ? hoaGiap : null,
    menh: menh ? menh : null
  }
}
