/**
 * Chepvang Studio
 */

// import moment from 'moment';

import {
  // DATE_FORMAT,
  // TRUC_LIST,
  // CAN_LIST,
  // CHI_LIST,
  // TIET_KHI_LIST,

  CAN_GIAP,
  CAN_AT,
  CAN_BINH,
  CAN_DINH,
  CAN_MAU,
  CAN_KY,
  CAN_CANH,
  CAN_TAN,
  CAN_NHAM,
  CAN_QUY,

  CHI_TYS,
  CHI_SUU,
  CHI_DAN,
  CHI_MAO,
  CHI_THIN,
  CHI_TYJ,
  CHI_NGO,
  CHI_MUI,
  CHI_THAN,
  CHI_DAU,
  CHI_TUAT,
  CHI_HOI,

  GIO_TYS,
  GIO_SUU,
  GIO_DAN,
  GIO_MAO,
  GIO_THIN,
  GIO_TYJ,
  GIO_NGO,
  GIO_MUI,
  GIO_THAN,
  GIO_DAU,
  // GIO_TUAT,
  // GIO_HOI,

  // TRUC_KIEN,
  // TRUC_TRU,
  TRUC_MAN,
  // TRUC_BINH,
  // TRUC_DINH,
  // TRUC_CHAP,
  TRUC_PHA
  // TRUC_NGUY,
  // TRUC_THANH,
  // TRUC_THU,
  // TRUC_KHAI,
  // TRUC_BE,

  // LUC_THAP_HOA_GIAP_LIST,

  // TIET_KHI_XUAN_PHAN,
  // TIET_KHI_THANH_MINH,
  // TIET_KHI_COC_VU,
  // TIET_KHI_LAP_HA,
  // TIET_KHI_TIEU_MAN,
  // TIET_KHI_MANG_CHUNG,
  // TIET_KHI_HA_CHI,
  // TIET_KHI_TIEU_THU,
  // TIET_KHI_DAI_THU,
  // TIET_KHI_LAP_THU,
  // TIET_KHI_XU_THU,
  // TIET_KHI_BACH_LO,
  // TIET_KHI_THU_PHAN,
  // TIET_KHI_HAN_LO,
  // TIET_KHI_SUONG_GIANG,
  // TIET_KHI_LAP_DONG,
  // TIET_KHI_TIEU_TUYET,
  // TIET_KHI_DAI_TUYET,
  // TIET_KHI_DONG_CHI,
  // TIET_KHI_TIEU_HAN,
  // TIET_KHI_DAI_HAN,
  // TIET_KHI_LAP_XUAN,
  // TIET_KHI_VU_THUY,
  // TIET_KHI_KINH_TRAP
} from '../constants'

// import {
//   getLunarDate,
//   // getSolarDate,
//   // check30DaysInMonth,
//   getCanChiDMY,
//   getTietKhi,
//   // getTietKhiDatesAndTrucKienDates,
//   getSeasonStartDate,
//   getCurrentTruc,
//   getCanHour0,
//   getSao,
//   getLucThapHoaGiap,
// } from './lunar';

/*
 * Tìm ngày Bất tương (Ngày tốt dùng cho hôn nhân, cưới gả)
 *
 * @return {Boolean} True nếu là ngày Bất tương
 */
export function getNgayBatTuong(isCheckedNgayBatTuong, month, can, chi) {
  if (!isCheckedNgayBatTuong || !month || !can || !chi) {
    return
  }

  return (
    ((month === 1) && (
      (can === CAN_BINH && chi === CHI_DAN) ||
      (can === CAN_DINH && chi === CHI_MAO) ||
      (can === CAN_BINH && chi === CHI_TYS) ||
      (can === CAN_KY && chi === CHI_MAO) ||
      (can === CAN_MAU && chi === CHI_TYS) ||
      (can === CAN_CANH && chi === CHI_DAN) ||
      (can === CAN_TAN && chi === CHI_MAO)
    )) ||

    ((month === 2) && (
      (can === CAN_AT && chi === CHI_SUU) ||
      (can === CAN_BINH && chi === CHI_DAN) ||
      (can === CAN_DINH && chi === CHI_SUU) ||
      (can === CAN_BINH && chi === CHI_TUAT) ||
      (can === CAN_MAU && chi === CHI_DAN) ||
      (can === CAN_KY && chi === CHI_SUU) ||
      (can === CAN_MAU && chi === CHI_TUAT) ||
      (can === CAN_CANH && chi === CHI_TUAT)
    )) ||

    ((month === 3) && (
      (can === CAN_AT && chi === CHI_SUU) ||
      (can === CAN_DINH && chi === CHI_SUU) ||
      (can === CAN_KY && chi === CHI_TYJ) ||
      (can === CAN_DINH && chi === CHI_DAU)
    )) ||

    ((month === 4) && (
      (can === CAN_GIAP && chi === CHI_TYS) ||
      (can === CAN_GIAP && chi === CHI_TUAT) ||
      (can === CAN_BINH && chi === CHI_TYS) ||
      (can === CAN_AT && chi === CHI_DAU) ||
      (can === CAN_BINH && chi === CHI_TUAT) ||
      (can === CAN_MAU && chi === CHI_TYS) ||
      (can === CAN_BINH && chi === CHI_THAN) ||
      (can === CAN_DINH && chi === CHI_DAU) ||
      (can === CAN_MAU && chi === CHI_TUAT)
    )) ||

    ((month === 5) && (
      (can === CAN_QUY && chi === CHI_DAU) ||
      (can === CAN_GIAP && chi === CHI_TUAT) ||
      (can === CAN_GIAP && chi === CHI_THAN) ||
      (can === CAN_AT && chi === CHI_DAU) ||
      (can === CAN_BINH && chi === CHI_TUAT) ||
      (can === CAN_AT && chi === CHI_MUI) ||
      (can === CAN_BINH && chi === CHI_THAN) ||
      (can === CAN_MAU && chi === CHI_THAN) ||
      (can === CAN_KY && chi === CHI_MUI)
    )) ||

    ((month === 6) && (
      (can === CAN_NHAM && chi === CHI_THAN) ||
      (can === CAN_QUY && chi === CHI_DAU) ||
      (can === CAN_GIAP && chi === CHI_TUAT) ||
      (can === CAN_NHAM && chi === CHI_NGO) ||
      (can === CAN_QUY && chi === CHI_MUI) ||
      (can === CAN_GIAP && chi === CHI_THAN) ||
      (can === CAN_AT && chi === CHI_DAU) ||
      (can === CAN_GIAP && chi === CHI_NGO) ||
      (can === CAN_AT && chi === CHI_MUI) ||
      (can === CAN_NHAM && chi === CHI_TUAT)
    )) ||

    ((month === 7) && (
      (can === CAN_NHAM && chi === CHI_THAN) ||
      (can === CAN_QUY && chi === CHI_DAU) ||
      (can === CAN_NHAM && chi === CHI_TUAT) ||
      (can === CAN_QUY && chi === CHI_MUI) ||
      (can === CAN_GIAP && chi === CHI_THAN) ||
      (can === CAN_AT && chi === CHI_DAU) ||
      (can === CAN_GIAP && chi === CHI_TUAT) ||
      (can === CAN_AT && chi === CHI_MUI) ||
      (can === CAN_QUY && chi === CHI_TYJ) ||
      (can === CAN_AT && chi === CHI_TYJ) ||
      (can === CAN_KY && chi === CHI_MUI)
    )) ||

    ((month === 8) && (
      (can === CAN_MAU && chi === CHI_THIN) ||
      (can === CAN_TAN && chi === CHI_MUI) ||
      (can === CAN_GIAP && chi === CHI_THAN) ||
      (can === CAN_NHAM && chi === CHI_THIN) ||
      (can === CAN_QUY && chi === CHI_TYJ) ||
      (can === CAN_GIAP && chi === CHI_NGO) ||
      (can === CAN_GIAP && chi === CHI_THIN) ||
      (can === CAN_MAU && chi === CHI_NGO) ||
      (can === CAN_TAN && chi === CHI_TYJ) ||
      (can === CAN_NHAM && chi === CHI_NGO) ||
      (can === CAN_QUY && chi === CHI_MUI)
    )) ||

    ((month === 9) && (
      (can === CAN_KY && chi === CHI_TYJ) ||
      (can === CAN_CANH && chi === CHI_NGO) ||
      (can === CAN_TAN && chi === CHI_MUI) ||
      (can === CAN_AT && chi === CHI_MAO) ||
      (can === CAN_TAN && chi ===CHI_TYJ) ||
      (can === CAN_NHAM && chi === CHI_NGO) ||
      (can === CAN_QUY && chi === CHI_MUI) ||
      (can === CAN_QUY && chi === CHI_TYJ) ||
      (can === CAN_QUY && chi === CHI_MAO) ||
      (can === CAN_MAU && chi === CHI_NGO) ||
      (can === CAN_KY && chi === CHI_MUI)
    )) ||

    ((month === 10) && (
      (can === CAN_MAU && chi === CHI_THIN) ||
      (can === CAN_CANH && chi === CHI_NGO) ||
      (can === CAN_CANH && chi === CHI_THIN) ||
      (can === CAN_NHAM && chi === CHI_NGO) ||
      (can === CAN_CANH && chi === CHI_DAN) ||
      (can === CAN_TAN && chi === CHI_MAO) ||
      (can === CAN_NHAM && chi === CHI_THIN) ||
      (can === CAN_NHAM && chi === CHI_DAN) ||
      (can === CAN_QUY && chi === CHI_MAO) ||
      (can === CAN_MAU && chi === CHI_NGO)
    )) ||

    ((month === 11) && (
      (can === CAN_DINH && chi === CHI_MAO) ||
      (can === CAN_MAU && chi === CHI_THIN) ||
      (can === CAN_KY && chi === CHI_TYJ) ||
      (can === CAN_DINH && chi === CHI_SUU) ||
      (can === CAN_KY && chi === CHI_MAO) ||
      (can === CAN_CANH && chi === CHI_THIN) ||
      (can === CAN_TAN && chi === CHI_TYJ) ||
      (can === CAN_NHAM && chi === CHI_THIN) ||
      (can === CAN_TAN && chi === CHI_SUU) ||
      (can === CAN_DINH && chi === CHI_TYJ)
    )) ||

    ((month === 12) && (
      (can === CAN_BINH && chi === CHI_DAN) ||
      (can === CAN_DINH && chi === CHI_MAO) ||
      (can === CAN_MAU && chi === CHI_THIN) ||
      (can === CAN_DINH && chi === CHI_SUU) ||
      (can === CAN_MAU && chi === CHI_DAN) ||
      (can === CAN_KY && chi === CHI_MAO) ||
      (can === CAN_CANH && chi === CHI_THIN) ||
      (can === CAN_AT && chi === CHI_SUU) ||
      (can === CAN_CANH && chi === CHI_DAN) ||
      (can === CAN_TAN && chi === CHI_MAO) ||
      (can === CAN_BINH && chi === CHI_THIN) ||
      (can === CAN_TAN && chi === CHI_SUU)
    ))
  )
}

/*
 * Tìm ngày có sao Sát cống (Sao tốt)
 *
 * @return {Boolean} True nếu là ngày có sao Sát cống
 */
export function getNgaySaoSatCong(isCheckedNgaySaoSatCong, month, can, chi) {
  if (!isCheckedNgaySaoSatCong || !month || !can || !chi) {
    return
  }

  return (
    ((month === 1 || month === 4 || month === 7 || month === 10) && (
      (can === CAN_DINH && chi === CHI_MAO) ||
      (can === CAN_BINH && chi === CHI_TYS) ||
      (can === CAN_AT && chi === CHI_DAU) ||
      (can === CAN_GIAP && chi === CHI_NGO) ||
      (can === CAN_QUY && chi === CHI_MAO) ||
      (can === CAN_NHAM && chi === CHI_TYJ) ||
      (can === CAN_TAN && chi === CHI_DAU)
    )) ||

    ((month === 2 || month === 5 || month === 8 || month === 11) && (
      (can === CAN_BINH && chi === CHI_DAN) ||
      (can === CAN_AT && chi === CHI_HOI) ||
      (can === CAN_GIAP && chi === CHI_THAN) ||
      (can === CAN_QUY && chi === CHI_TYJ) ||
      (can === CAN_NHAM && chi === CHI_DAN) ||
      (can === CAN_TAN && chi === CHI_HOI) ||
      (can === CAN_CANH && chi === CHI_THAN)
    )) ||

    ((month === 3 || month === 6 || month === 9 || month === 12) && (
      (can === CAN_AT && chi === CHI_SUU) ||
      (can === CAN_GIAP && chi === CHI_TUAT) ||
      (can === CAN_QUY && chi === CHI_MUI) ||
      (can === CAN_NHAM && chi === CHI_THIN) ||
      (can === CAN_TAN && chi === CHI_SUU) ||
      (can === CAN_CANH && chi === CHI_TUAT) ||
      (can === CAN_KY && chi === CHI_MUI)
    ))
  )
}

/*
 * Tìm ngày có sao Trực tinh (Sao tốt)
 * Theo Sách Coi tuổi làm nhà và dựng vợ gả chồng - Chiêm gia tinh Huỳnh Liên
 *
 * @return {Boolean} True nếu là ngày có sao Trực tinh
 */
export function getNgaySaoTrucTinh(isCheckedNgaySaoTrucTinh, month, can, chi) {
  if (!isCheckedNgaySaoTrucTinh || !month || !can || !chi) {
    return
  }

  return (
    ((month === 1 || month === 4 || month === 7 || month === 10) && (
      (can === CAN_MAU && chi === CHI_THIN) ||
      (can === CAN_DINH && chi === CHI_SUU) ||
      (can === CAN_BINH && chi === CHI_TUAT) ||
      (can === CAN_AT && chi === CHI_MUI) ||
      (can === CAN_GIAP && chi === CHI_THIN) ||
      (can === CAN_QUY && chi === CHI_SUU) ||
      (can === CAN_NHAM && chi === CHI_TUAT)
    )) ||

    ((month === 2 || month === 5 || month === 8 || month === 11) && (
      (can === CAN_DINH && chi === CHI_MAO) ||
      (can === CAN_BINH && chi === CHI_TYS) ||
      (can === CAN_AT && chi === CHI_DAU) ||
      (can === CAN_GIAP && chi === CHI_NGO) ||
      (can === CAN_QUY && chi === CHI_MAO) ||
      (can === CAN_NHAM && chi === CHI_TYS) ||
      (can === CAN_TAN && chi === CHI_DAU)
    )) ||

    ((month === 3 || month === 6 || month === 9 || month === 12) && (
      (can === CAN_BINH && chi === CHI_DAN) ||
      (can === CAN_AT && chi === CHI_HOI) ||
      (can === CAN_GIAP && chi === CHI_THAN) ||
      (can === CAN_QUY && chi === CHI_TYJ) ||
      (can === CAN_NHAM && chi === CHI_DAN) ||
      (can === CAN_TAN && chi === CHI_MAO) ||
      (can === CAN_CANH && chi === CHI_THAN)
    ))
  )
}

/*
 * Tìm ngày có sao Nhân duyên (Sao tốt)
 * Theo Sách Coi tuổi làm nhà và dựng vợ gả chồng - Chiêm gia tinh Huỳnh Liên
 *
 * @return {Boolean} True nếu là ngày có sao Nhân duyên
 */
export function getNgaySaoNhanDuyen(isCheckedNgaySaoNhanDuyen, month, can, chi) {
  if (!isCheckedNgaySaoNhanDuyen || !month || !can|| !chi) {
    return
  }

  return (
    ((month === 1 || month === 4 || month === 7 || month === 10) && (
      (can === CAN_TAN && chi === CHI_MUI) ||
        (can === CAN_CANH && chi === CHI_THIN) ||
        (can === CAN_KY && chi === CHI_SUU) ||
        (can === CAN_MAU && chi === CHI_TUAT) ||
        (can === CAN_DINH && chi === CHI_MUI) ||
        (can === CAN_BINH && chi === CHI_THIN)
    )) ||

    ((month === 2 || month === 5 || month === 8 || month === 11) && (
      (can === CAN_CANH && chi === CHI_NGO) ||
      (can === CAN_KY && chi === CHI_MAO) ||
      (can === CAN_NHAM && chi === CHI_TYS) ||
      (can === CAN_DINH && chi === CHI_DAU) ||
      (can === CAN_BINH && chi === CHI_NGO) ||
      (can === CAN_AT && chi === CHI_MAO)
    )) ||

    ((month === 3 || month === 6 || month === 9 || month === 12) && (
      (can === CAN_KY && chi === CHI_TYJ) ||
      (can === CAN_MAU && chi === CHI_DAN) ||
      (can === CAN_KY && chi === CHI_HOI) ||
      (can === CAN_BINH && chi === CHI_THAN) ||
      (can === CAN_AT && chi === CHI_TYJ) ||
      (can === CAN_GIAP && chi === CHI_DAN) ||
      (can === CAN_QUY && chi === CHI_HOI)
    ))
  )
}

/*
 * Tìm ngày có sao Thiên ân (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có sao Thiên ân
 */
export function getNgaySaoThienAn(isCheckedNgaySaoThienAn, can, chi) {
  if (!isCheckedNgaySaoThienAn || !can|| !chi) {
    return
  }

  return (can === CAN_GIAP && chi === CHI_TYS) ||
    (can === CAN_AT && chi === CHI_SUU) ||
    (can === CAN_BINH && chi === CHI_DAN) ||
    (can === CAN_DINH && chi === CHI_MAO) ||
    (can === CAN_MAU && chi === CHI_THIN) ||
    (can === CAN_KY && chi === CHI_MAO) ||
    (can === CAN_CANH && chi === CHI_THIN) ||
    (can === CAN_CANH && chi === CHI_TUAT) ||
    (can === CAN_TAN && chi === CHI_TYJ) ||
    (can === CAN_TAN && chi === CHI_HOI) ||
    (can === CAN_NHAM && chi === CHI_NGO) ||
    (can === CAN_NHAM && chi === CHI_THAN) ||
    (can === CAN_QUY && chi === CHI_SUU) ||
    (can === CAN_QUY && chi === CHI_MUI)
}

/*
 * Tìm ngày có sao Thiên thụy (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có sao Thiên thụy
 */
export function getNgaySaoThienThuy(isCheckedNgaySaoThienThuy, can, chi) {
  if (!isCheckedNgaySaoThienThuy || !can|| !chi) {
    return
  }

  return (can === CAN_MAU && chi === CHI_DAN) ||
    (can === CAN_KY && chi === CHI_MAO) ||
    (can === CAN_CANH && chi === CHI_DAN) ||
    (can === CAN_TAN && chi === CHI_TYJ) ||
    (can === CAN_NHAM && chi === CHI_TYS)
}

/*
 * Tìm ngày có sao Ngũ hợp (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có sao Ngũ hợp
 */
export function getNgaySaoNguHop(isCheckedNgaySaoNguHop, can, chi) {
  if (!isCheckedNgaySaoNguHop || !can|| !chi) {
    return
  }

  return (can === CAN_MAU && chi === CHI_NGO) ||
    (can === CAN_KY && chi === CHI_MUI) ||
    (can === CAN_TAN && chi === CHI_DAU) ||
    (can === CAN_QUY && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Thiên đức (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên đức
 */
export function getNgaySaoThienDuc(isCheckedNgaySaoThienDuc, month, can, chi) {
  if (!isCheckedNgaySaoThienDuc || !month || !can || !chi) {
    return
  }

  return (month === 1 && can === CAN_DINH) ||
    (month === 2 && chi === CHI_THAN) ||
    (month === 3 && can === CAN_NHAM) ||
    (month === 4 && can === CAN_TAN) ||
    (month === 5 && chi === CHI_HOI) ||
    (month === 6 && can === CAN_GIAP) ||
    (month === 7 && can === CAN_QUY) ||
    (month === 8 && chi === CHI_DAN) ||
    (month === 9 && can === CAN_BINH) ||
    (month === 10 && can === CAN_AT) ||
    (month === 11 && chi === CHI_TYJ) ||
    (month === 12 && can === CAN_CANH)
}

/*
 * Tìm ngày có Sao Thiên đức hợp (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên đức hợp
 */
export function getNgaySaoThienDucHop(isCheckedNgaySaoThienDucHop, month, can, chi) {
  if (!isCheckedNgaySaoThienDucHop || !month || !can || !chi) {
    return
  }

  return (month === 1 && can === CAN_NHAM) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && can === CAN_DINH) ||
    (month === 4 && can === CAN_BINH) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && can === CAN_KY) ||
    (month === 7 && can === CAN_MAU) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && can === CAN_TAN) ||
    (month === 10 && can === CAN_CANH) ||
    (month === 11 && chi === CHI_THAN) ||
    (month === 12 && can === CAN_AT)
}

/*
 * Tìm ngày có Sao Nguyệt đức (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt đức
 */
export function getNgaySaoNguyetDuc(isCheckedNgaySaoNguyetDuc, month, can) {
  if (!isCheckedNgaySaoNguyetDuc || !month || !can) {
    return
  }

  return ((month === 1 || month === 5 || month === 9) && can === CAN_BINH) ||
    ((month === 2 || month === 6 || month === 10) && can === CAN_GIAP) ||
    ((month === 3 || month === 7 || month === 11) && can === CAN_NHAM) ||
    ((month === 4 || month === 8 || month === 12) && can === CAN_CANH)
}

/*
 * Tìm ngày có Sao Nguyệt đức hợp (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt đức hợp
 */
export function getNgaySaoNguyetDucHop(isCheckedNgaySaoNguyetDucHop, month, can) {
  if (!isCheckedNgaySaoNguyetDucHop || !month || !can) {
    return
  }

  return ((month === 1 || month === 5 || month === 9) && can === CAN_TAN) ||
    ((month === 2 || month === 6 || month === 10) && can === CAN_KY) ||
    ((month === 3 || month === 7 || month === 11) && can === CAN_DINH) ||
    ((month === 4 || month === 8 || month === 12) && can === CAN_AT)
}

/*
 * Tìm ngày có Sao Thiên hỷ (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên hỷ
 */
export function getNgaySaoThienHy(isCheckedNgaySaoThienHy, month, chi) {
  if (!isCheckedNgaySaoThienHy || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TUAT) ||
    (month === 2 && chi === CHI_HOI) ||
    (month === 3 && chi === CHI_TYS) ||
    (month === 4 && chi === CHI_SUU) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_MAO) ||
    (month === 7 && chi === CHI_THIN) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_NGO) ||
    (month === 10 && chi === CHI_MUI) ||
    (month === 11 && chi === CHI_THAN) ||
    (month === 12 && chi === CHI_DAU)
}

/*
 * Tìm ngày có Sao Thiên phú (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên phú
 */
export function getNgaySaoThienPhu(isCheckedNgaySaoThienPhu, month, chi) {
  if (!isCheckedNgaySaoThienPhu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THIN) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_NGO) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_THAN) ||
    (month === 6 && chi === CHI_DAU) ||
    (month === 7 && chi === CHI_TUAT) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_MAO)
}

/*
 * Tìm ngày có Sao Thiên quý (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên quý
 */
export function getNgaySaoThienQuy(
  isCheckedNgaySaoThienQuy,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  can
) {
  if (
    !isCheckedNgaySaoThienQuy ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !can
  ) {
    return
  }

  return (isMuaXuan && can === CAN_GIAP) ||
    (isMuaXuan && can === CAN_AT) ||

    (isMuaHa && can === CAN_BINH) ||
    (isMuaHa && can === CAN_DINH) ||

    (isMuaThu && can === CAN_CANH) ||
    (isMuaThu && can === CAN_TAN) ||

    (isMuaDong && can === CAN_NHAM) ||
    (isMuaDong && can === CAN_QUY)
}

/*
 * Tìm ngày có Sao Thiên xá (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên xá
 */
export function getNgaySaoThienXa(isCheckedNgaySaoThienXa, month, can, chi) {
  if (!isCheckedNgaySaoThienXa || !month || !can || !chi) {
    return
  }

  return (
    ((month === 1 || month === 2 || month === 3) && (
      (can === CAN_MAU && chi === CHI_DAN)
    )) ||

    ((month === 4 || month === 6) && (
      (can === CAN_GIAP && chi === CHI_NGO)
    )) ||

    ((month === 7 || month === 8 || month === 9) && (
      (can === CAN_MAU && chi === CHI_THAN)
    )) ||

    ((month === 10 || month === 12) && (
      (can === CAN_GIAP && chi === CHI_TYS)
    ))
  )
}

/*
 * Tìm ngày có Sao Sinh khí (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Sinh khí
 */
export function getNgaySaoSinhKhi(isCheckedNgaySaoSinhKhi, month, chi) {
  if (!isCheckedNgaySaoSinhKhi || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYS) ||
    (month === 2 && chi === CHI_SUU) ||
    (month === 3 && chi === CHI_DAN) ||
    (month === 4 && chi === CHI_MAO) ||
    (month === 5 && chi === CHI_THIN) ||
    (month === 6 && chi === CHI_TYJ) ||
    (month === 7 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_MUI) ||
    (month === 9 && chi === CHI_THAN) ||
    (month === 10 && chi === CHI_DAU) ||
    (month === 11 && chi === CHI_TUAT) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Thiên phúc (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên phúc
 */
export function getNgaySaoThienPhuc(isCheckedNgaySaoThienPhuc, month, can) {
  if (!isCheckedNgaySaoThienPhuc || !month || !can) {
    return
  }

  return (month === 1 && can === CAN_KY) ||
    (month === 2 && can === CAN_MAU) ||
    (month === 4 && (can === CAN_TAN || can === CAN_QUY)) ||
    (month === 5 && (can === CAN_CANH || can === CAN_NHAM)) ||
    (month === 7 && can === CAN_AT) ||
    (month === 8 && can === CAN_GIAP) ||
    (month === 10 && can === CAN_DINH) ||
    (month === 11 && can === CAN_BINH)
}

/*
 * Tìm ngày có Sao Thiên thành (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên thành
 */
export function getNgaySaoThienThanh(isCheckedNgaySaoThienThanh, month, chi) {
  if (!isCheckedNgaySaoThienThanh || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_MUI) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_HOI) ||
    (month === 4 && chi === CHI_SUU) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_TYJ) ||
    (month === 7 && chi === CHI_MUI) ||
    (month === 8 && chi === CHI_DAU) ||
    (month === 9 && chi === CHI_HOI) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Thiên quan (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên quan
 */
export function getNgaySaoThienQuan(isCheckedNgaySaoThienQuan, month, chi) {
  if (!isCheckedNgaySaoThienQuan || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TUAT) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_DAN) ||
    (month === 4 && chi === CHI_THIN) ||
    (month === 5 && chi === CHI_NGO) ||
    (month === 6 && chi === CHI_THAN) ||
    (month === 7 && chi === CHI_TUAT) ||
    (month === 8 && chi === CHI_TYS) ||
    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_THIN) ||
    (month === 11 && chi === CHI_NGO) ||
    (month === 12 && chi === CHI_THAN)
}

/*
 * Tìm ngày có Sao Thiên mã (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên mã
 */
export function getNgaySaoThienMa(isCheckedNgaySaoThienMa, month, chi) {
  if (!isCheckedNgaySaoThienMa || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_NGO) ||
    (month === 2 && chi === CHI_THAN) ||
    (month === 3 && chi === CHI_TUAT) ||
    (month === 4 && chi === CHI_TYS) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_THIN) ||

    (month === 7 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_THAN) ||
    (month === 9 && chi === CHI_TUAT) ||
    (month === 10 && chi === CHI_TYS) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_THIN)
}

/*
 * Tìm ngày có Sao Thiên tài (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên tài
 */
export function getNgaySaoThienTai(isCheckedNgaySaoThienTai, month, chi) {
  if (!isCheckedNgaySaoThienTai || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THIN) ||
    (month === 2 && chi === CHI_NGO) ||
    (month === 3 && chi === CHI_THAN) ||
    (month === 4 && chi === CHI_TUAT) ||
    (month === 5 && chi === CHI_TYS) ||
    (month === 6 && chi === CHI_DAN) ||
    (month === 7 && chi === CHI_THIN) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_THAN) ||
    (month === 10 && chi === CHI_TUAT) ||
    (month === 11 && chi === CHI_TYS) ||
    (month === 12 && chi === CHI_DAN)
}

/*
 * Tìm ngày có Sao Địa tài (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Địa tài
 */
export function getNgaySaoDiaTai(isCheckedNgaySaoDiaTai, month, chi) {
  if (!isCheckedNgaySaoDiaTai || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_MUI) ||
    (month === 3 && chi === CHI_DAU) ||
    (month === 4 && chi === CHI_HOI) ||
    (month === 5 && chi === CHI_SUU) ||
    (month === 6 && chi === CHI_MAO) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_MUI) ||
    (month === 9 && chi === CHI_DAU) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_SUU) ||
    (month === 12 && chi === CHI_MAO)
}

/*
 * Tìm ngày có Sao Nguyệt tài (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt tài
 */
export function getNgaySaoNguyetTai(isCheckedNgaySaoNguyetTai, month, chi) {
  if (!isCheckedNgaySaoNguyetTai || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_NGO) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_TYJ) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_MUI) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Nguyệt ân (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt ân
 */
export function getNgaySaoNguyetAn(isCheckedNgaySaoNguyetAn, month, can) {
  if (!isCheckedNgaySaoNguyetAn || !month || !can) {
    return
  }

  return (month === 1 && can === CAN_BINH) ||
    (month === 2 && can === CAN_DINH) ||
    (month === 3 && can === CAN_CANH) ||
    (month === 4 && can === CAN_KY) ||
    (month === 5 && can === CAN_MAU) ||
    (month === 6 && can === CAN_TAN) ||
    (month === 7 && can === CAN_NHAM) ||
    (month === 8 && can === CAN_QUY) ||
    (month === 9 && can === CAN_CANH) ||
    (month === 10 && can === CAN_AT) ||
    (month === 11 && can === CAN_GIAP) ||
    (month === 12 && can === CAN_TAN)
}

/*
 * Tìm ngày có Sao Nguyệt không (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt không
 */
export function getNgaySaoNguyetKhong(isCheckedNgaySaoNguyetKhong, month, can) {
  if (!isCheckedNgaySaoNguyetKhong || !month || !can) {
    return
  }

  return (month === 1 && can === CAN_NHAM) ||
    (month === 2 && can === CAN_CANH) ||
    (month === 3 && can === CAN_BINH) ||
    (month === 4 && can === CAN_GIAP) ||
    (month === 5 && can === CAN_NHAM) ||
    (month === 6 && can === CAN_CANH) ||
    (month === 7 && can === CAN_BINH) ||
    (month === 8 && can === CAN_GIAP) ||
    (month === 9 && can === CAN_NHAM) ||
    (month === 10 && can === CAN_CANH) ||
    (month === 11 && can === CAN_BINH) ||
    (month === 12 && can === CAN_GIAP)
}

/*
 * Tìm ngày có Sao Minh tinh (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Minh tinh
 */
export function getNgaySaoMinhTinh(isCheckedNgaySaoMinhTinh, month, chi) {
  if (!isCheckedNgaySaoMinhTinh || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THAN) ||
    (month === 2 && chi === CHI_TUAT) ||
    (month === 3 && chi === CHI_TYS) ||
    (month === 4 && chi === CHI_DAN) ||
    (month === 5 && chi === CHI_THIN) ||
    (month === 6 && chi === CHI_NGO) ||
    (month === 7 && chi === CHI_THAN) ||
    (month === 8 && chi === CHI_TUAT) ||
    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_DAN) ||
    (month === 11 && chi === CHI_THIN) ||
    (month === 12 && chi === CHI_NGO)
}

/*
 * Tìm ngày có Sao Thánh tâm (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thánh tâm
 */
export function getNgaySaoThanhTam(isCheckedNgaySaoThanhTam, month, chi) {
  if (!isCheckedNgaySaoThanhTam || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_HOI) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_TYS) ||
    (month === 4 && chi === CHI_NGO) ||
    (month === 5 && chi === CHI_SUU) ||
    (month === 6 && chi === CHI_MUI) ||
    (month === 7 && chi === CHI_DAN) ||
    (month === 8 && chi === CHI_THAN) ||
    (month === 9 && chi === CHI_MAO) ||
    (month === 10 && chi === CHI_DAU) ||
    (month === 11 && chi === CHI_THIN) ||
    (month === 12 && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Ngũ phú (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Ngũ phú
 */
export function getNgaySaoNguPhu(isCheckedNgaySaoNguPhu, month, chi) {
  if (!isCheckedNgaySaoNguPhu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_HOI) ||
    (month === 2 && chi === CHI_DAN) ||
    (month === 3 && chi === CHI_TYJ) ||
    (month === 4 && chi === CHI_THAN) ||
    (month === 5 && chi === CHI_HOI) ||
    (month === 6 && chi === CHI_DAN) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_THAN) ||
    (month === 9 && chi === CHI_HOI) ||
    (month === 10 && chi === CHI_DAN) ||
    (month === 11 && chi === CHI_TYJ) ||
    (month === 12 && chi === CHI_THAN)
}

/*
 * Tìm ngày có Sao Lộc khố (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Lộc khố
 */
export function getNgaySaoLocKho(isCheckedNgaySaoLocKho, month, chi) {
  if (!isCheckedNgaySaoLocKho || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THIN) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_NGO) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_THAN) ||
    (month === 6 && chi === CHI_DAU) ||
    (month === 7 && chi === CHI_TUAT) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_MAO)
}

/*
 * Tìm ngày có Sao Phúc sinh (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Phúc sinh
 */
export function getNgaySaoPhucSinh(isCheckedNgaySaoPhucSinh, month, chi) {
  if (!isCheckedNgaySaoPhucSinh || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAU) ||
    (month === 2 && chi === CHI_MAO) ||
    (month === 3 && chi === CHI_TUAT) ||
    (month === 4 && chi === CHI_THIN) ||
    (month === 5 && chi === CHI_HOI) ||
    (month === 6 && chi === CHI_TYJ) ||
    (month === 7 && chi === CHI_TYS) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_SUU) ||
    (month === 10 && chi === CHI_MUI) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_THAN)
}

/*
 * Tìm ngày có Sao Cát khánh (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Cát khánh
 */
export function getNgaySaoCatKhanh(isCheckedNgaySaoCatKhanh, month, chi) {
  if (!isCheckedNgaySaoCatKhanh || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAU) ||
    (month === 2 && chi === CHI_DAN) ||
    (month === 3 && chi === CHI_HOI) ||
    (month === 4 && chi === CHI_THIN) ||
    (month === 5 && chi === CHI_SUU) ||
    (month === 6 && chi === CHI_NGO) ||
    (month === 7 && chi === CHI_MAO) ||
    (month === 8 && chi === CHI_THAN) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_TUAT) ||
    (month === 11 && chi === CHI_MUI) ||
    (month === 12 && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Âm đức (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Âm đức
 */
export function getNgaySaoAmDuc(isCheckedNgaySaoAmDuc, month, chi) {
  if (!isCheckedNgaySaoAmDuc || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAU) ||
    (month === 2 && chi === CHI_MUI) ||
    (month === 3 && chi === CHI_TYJ) ||
    (month === 4 && chi === CHI_MAO) ||
    (month === 5 && chi === CHI_SUU) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_DAU) ||
    (month === 8 && chi === CHI_MUI) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_MAO) ||
    (month === 11 && chi === CHI_SUU) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao U vi tinh (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao U vi tinh
 */
export function getNgaySaoUViTinh(isCheckedNgaySaoUViTinh, month, chi) {
  if (!isCheckedNgaySaoUViTinh || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_HOI) ||
    (month === 2 && chi === CHI_THIN) ||
    (month === 3 && chi === CHI_SUU) ||
    (month === 4 && chi === CHI_NGO) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_THAN) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_TUAT) ||
    (month === 9 && chi === CHI_MUI) ||
    (month === 10 && chi === CHI_TYS) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_DAN)
}

/*
 * Tìm ngày có Sao Mãn đức tinh (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Mãn đức tinh
 */
export function getNgaySaoManDucTinh(isCheckedNgaySaoManDucTinh, month, chi) {
  if (!isCheckedNgaySaoManDucTinh || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAN) ||
    (month === 2 && chi === CHI_MUI) ||
    (month === 3 && chi === CHI_THIN) ||
    (month === 4 && chi === CHI_DAU) ||
    (month === 5 && chi === CHI_NGO) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_THAN) ||
    (month === 8 && chi === CHI_SUU) ||
    (month === 9 && chi === CHI_TUAT) ||
    (month === 10 && chi === CHI_MAO) ||
    (month === 11 && chi === CHI_TYS) ||
    (month === 12 && chi === CHI_TYJ)
}

/*
 * Tìm ngày có Sao Kinh tâm (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Kinh tâm
 */
export function getNgaySaoKinhTam(isCheckedNgaySaoKinhTam, month, chi) {
  if (!isCheckedNgaySaoKinhTam || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_MUI) ||
    (month === 2 && chi === CHI_SUU) ||
    (month === 3 && chi === CHI_THAN) ||
    (month === 4 && chi === CHI_DAN) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_MAO) ||
    (month === 7 && chi === CHI_TUAT) ||
    (month === 8 && chi === CHI_THIN) ||
    (month === 9 && chi === CHI_HOI) ||
    (month === 10 && chi === CHI_TYJ) ||
    (month === 11 && chi === CHI_TYS) ||
    (month === 12 && chi === CHI_NGO)
}

/*
 * Tìm ngày có Sao Tuế hợp (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tuế hợp
 */
export function getNgaySaoTueHop(isCheckedNgaySaoTueHop, month, chi) {
  if (!isCheckedNgaySaoTueHop || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_SUU) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_HOI) ||
    (month === 4 && chi === CHI_TUAT) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_THAN) ||
    (month === 7 && chi === CHI_MUI) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_THIN) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_DAN)
}

/*
 * Tìm ngày có Sao Nguyệt giải (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt giải
 */
export function getNgaySaoNguyetGiai(isCheckedNgaySaoNguyetGiai, month, chi) {
  if (!isCheckedNgaySaoNguyetGiai || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THAN) ||
    (month === 2 && chi === CHI_THAN) ||
    (month === 3 && chi === CHI_DAU) ||
    (month === 4 && chi === CHI_DAU) ||
    (month === 5 && chi === CHI_TUAT) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && chi === CHI_NGO) ||
    (month === 10 && chi === CHI_NGO) ||
    (month === 11 && chi === CHI_MUI) ||
    (month === 12 && chi === CHI_MUI)
}

/*
 * Tìm ngày có Sao Quan nhật (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Quan nhật
 */
export function getNgaySaoQuanNhat(isCheckedNgaySaoQuanNhat, month, chi) {
  if (!isCheckedNgaySaoQuanNhat || !month || !chi) {
    return
  }

  return (month === 2 && chi === CHI_MAO) ||
    (month === 5 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_DAU) ||
    (month === 11 && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Hoạt điệu (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Hoạt điệu
 */
export function getNgaySaoHoatDieu(isCheckedNgaySaoHoatDieu, month, chi) {
  if (!isCheckedNgaySaoHoatDieu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_TUAT) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_TYS) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_DAN) ||
    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_THIN) ||
    (month === 9 && chi === CHI_SUU) ||
    (month === 10 && chi === CHI_NGO) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_THAN)
}

/*
 * Tìm ngày có Sao Giải thần (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Giải thần
 */
export function getNgaySaoGiaiThan(isCheckedNgaySaoGiaiThan, month, chi) {
  if (!isCheckedNgaySaoGiaiThan || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THAN) ||
    (month === 2 && chi === CHI_THAN) ||
    (month === 3 && chi === CHI_TUAT) ||
    (month === 4 && chi === CHI_TUAT) ||
    (month === 5 && chi === CHI_TYS) ||
    (month === 6 && chi === CHI_TYS) ||
    (month === 7 && chi === CHI_DAN) ||
    (month === 8 && chi === CHI_DAN) ||
    (month === 9 && chi === CHI_THIN) ||
    (month === 10 && chi === CHI_THIN) ||
    (month === 11 && chi === CHI_NGO) ||
    (month === 12 && chi === CHI_NGO)
}

/*
 * Tìm ngày có Sao Phổ hộ (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Phổ hộ
 */
export function getNgaySaoPhoBo(isCheckedNgaySaoPhoBo, month, chi) {
  if (!isCheckedNgaySaoPhoBo || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THAN) ||
    (month === 2 && chi === CHI_DAN) ||
    (month === 3 && chi === CHI_DAU) ||
    (month === 4 && chi === CHI_MAO) ||
    (month === 5 && chi === CHI_TUAT) ||
    (month === 6 && chi === CHI_THIN) ||
    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_NGO) ||
    (month === 11 && chi === CHI_SUU) ||
    (month === 12 && chi === CHI_MUI)
}

/*
 * Tìm ngày có Sao Ích hậu (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Ích hậu
 */
export function getNgaySaoIchHau(isCheckedNgaySaoIchHau, month, chi) {
  if (!isCheckedNgaySaoIchHau || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYS) ||
    (month === 2 && chi === CHI_NGO) ||
    (month === 3 && chi === CHI_SUU) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_THAN) ||
    (month === 7 && chi === CHI_MAO) ||
    (month === 8 && chi === CHI_DAU) ||
    (month === 9 && chi === CHI_THIN) ||
    (month === 10 && chi === CHI_TUAT) ||
    (month === 11 && chi === CHI_TYJ) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Tục thể (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tục thể
 */
export function getNgaySaoTucThe(isCheckedNgaySaoTucThe, month, chi) {
  if (!isCheckedNgaySaoTucThe || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_SUU) ||
    (month === 2 && chi === CHI_MUI) ||
    (month === 3 && chi === CHI_DAN) ||
    (month === 4 && chi === CHI_THAN) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_DAU) ||
    (month === 7 && chi === CHI_THIN) ||
    (month === 8 && chi === CHI_TUAT) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_NGO) ||
    (month === 12 && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Yếu yên (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Yếu yên
 */
export function getNgaySaoYeuYen(isCheckedNgaySaoYeuYen, month, chi) {
  if (!isCheckedNgaySaoYeuYen || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAN) ||
    (month === 2 && chi === CHI_THAN) ||
    (month === 3 && chi === CHI_MAO) ||
    (month === 4 && chi === CHI_DAU) ||
    (month === 5 && chi === CHI_THIN) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && chi === CHI_NGO) ||
    (month === 10 && chi === CHI_TYS) ||
    (month === 11 && chi === CHI_MUI) ||
    (month === 12 && chi === CHI_SUU)
}

/*
 * Tìm ngày có Sao Dịch mã (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Dịch mã
 */
export function getNgaySaoDichMa(isCheckedNgaySaoDichMa, month, chi) {
  if (!isCheckedNgaySaoDichMa || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THAN) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_DAN) ||
    (month === 4 && chi === CHI_HOI) ||
    (month === 5 && chi === CHI_THAN) ||
    (month === 6 && chi === CHI_TYJ) ||
    (month === 7 && chi === CHI_DAN) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && chi === CHI_THAN) ||
    (month === 10 && chi === CHI_TYJ) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Tam hợp (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tam hợp
 */
export function getNgaySaoTamHop(isCheckedNgaySaoTamHop, month, chi) {
  if (!isCheckedNgaySaoTamHop || !month || !chi) {
    return
  }

  return (month === 1 && (chi === CHI_NGO || chi === CHI_TUAT)) ||
    (month === 2 && (chi === CHI_MUI || chi === CHI_HOI)) ||
    (month === 3 && (chi === CHI_THAN || chi === CHI_TYS)) ||
    (month === 4 && (chi === CHI_DAU || chi === CHI_SUU)) ||
    (month === 5 && (chi === CHI_TUAT || chi === CHI_DAN)) ||
    (month === 6 && (chi === CHI_HOI || chi === CHI_MAO)) ||
    (month === 7 && (chi === CHI_TYS || chi === CHI_THIN)) ||
    (month === 8 && (chi === CHI_SUU || chi === CHI_TYJ)) ||
    (month === 9 && (chi === CHI_DAN || chi === CHI_NGO)) ||
    (month === 10 && (chi === CHI_MAO || chi === CHI_MUI)) ||
    (month === 11 && (chi === CHI_THIN || chi === CHI_THAN)) ||
    (month === 12 && (chi === CHI_TYJ || chi === CHI_DAU))
}

/*
 * Tìm ngày có Sao Lục hợp (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Lục hợp
 */
export function getNgaySaoLucHop(isCheckedNgaySaoLucHop, month, chi) {
  if (!isCheckedNgaySaoLucHop || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_HOI) ||
    (month === 2 && chi === CHI_TUAT) ||
    (month === 3 && chi === CHI_DAU) ||
    (month === 4 && chi === CHI_THAN) ||
    (month === 5 && chi === CHI_MUI) ||
    (month === 6 && chi === CHI_NGO) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_THIN) ||
    (month === 9 && chi === CHI_MAO) ||
    (month === 10 && chi === CHI_DAN) ||
    (month === 11 && chi === CHI_SUU) ||
    (month === 12 && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Mẫu thương (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Mẫu thương
 */
export function getNgaySaoMauThuong(
  isCheckedNgaySaoMauThuong,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoMauThuong ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_HOI) ||
    (isMuaXuan && chi === CHI_TYS) ||

    (isMuaHa && chi === CHI_DAN) ||
    (isMuaHa && chi === CHI_MAO) ||

    (isMuaThu && chi === CHI_THIN) ||
    (isMuaThu && chi === CHI_SUU) ||

    (isMuaDong && chi === CHI_THAN) ||
    (isMuaDong && chi === CHI_DAU)
}

/*
 * Tìm ngày có Sao Phúc hậu (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Phúc hậu
 */
export function getNgaySaoPhucHau(
  isCheckedNgaySaoPhucHau,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoPhucHau ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_DAN) ||
    (isMuaHa && chi === CHI_TYJ) ||
    (isMuaThu && chi === CHI_THAN) ||
    (isMuaDong && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Đại hồng sa (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Đại hồng sa
 */
export function getNgaySaoDaiHongSa(
  isCheckedNgaySaoDaiHongSa,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoDaiHongSa ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_TYS) ||
    (isMuaXuan && chi === CHI_SUU) ||

    (isMuaHa && chi === CHI_THIN) ||
    (isMuaHa && chi === CHI_TYJ) ||

    (isMuaThu && chi === CHI_NGO) ||
    (isMuaThu && chi === CHI_MUI) ||

    (isMuaDong && chi === CHI_THAN) ||
    (isMuaDong && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Dân nhật - thời đức (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Dân nhật - thời đức
 */
export function getNgaySaoDanNhatThoiDuc(
  isCheckedNgaySaoDanNhatThoiDuc,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoDanNhatThoiDuc ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_NGO) ||
    (isMuaHa && chi === CHI_DAU) ||
    (isMuaThu && chi === CHI_TYS) ||
    (isMuaDong && chi === CHI_MAO)
}

/*
 * Tìm ngày có Sao Hoàng ân (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Hoàng ân
 */
export function getNgaySaoHoangAn(isCheckedNgaySaoHoangAn, month, chi) {
  if (!isCheckedNgaySaoHoangAn || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TUAT) ||
    (month === 2 && chi === CHI_SUU) ||
    (month === 3 && chi === CHI_DAN) ||
    (month === 4 && chi === CHI_TYJ) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_MAO) ||
    (month === 7 && chi === CHI_TYS) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_HOI) ||
    (month === 10 && chi === CHI_THIN) ||
    (month === 11 && chi === CHI_THAN) ||
    (month === 12 && chi === CHI_MUI)
}

/*
 * Tìm ngày có Sao Thanh long (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thanh long
 */
export function getNgaySaoThanhLong(isCheckedNgaySaoThanhLong, month, chi) {
  if (!isCheckedNgaySaoThanhLong || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYS) ||
    (month === 2 && chi === CHI_DAN) ||
    (month === 3 && chi === CHI_THIN) ||
    (month === 4 && chi === CHI_NGO) ||
    (month === 5 && chi === CHI_THAN) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_TYS) ||
    (month === 8 && chi === CHI_DAN) ||
    (month === 9 && chi === CHI_THIN) ||
    (month === 10 && chi === CHI_NGO) ||
    (month === 11 && chi === CHI_THAN) ||
    (month === 12 && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Minh đường (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Minh đường
 */
export function getNgaySaoMinhDuong(isCheckedNgaySaoMinhDuong, month, chi) {
  if (!isCheckedNgaySaoMinhDuong || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_SUU) ||
    (month === 2 && chi === CHI_MAO) ||
    (month === 3 && chi === CHI_TYJ) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_SUU) ||
    (month === 8 && chi === CHI_MAO) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_MUI) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Kim đường (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Kim đường
 */
export function getNgaySaoKimDuong(isCheckedNgaySaoKimDuong, month, chi) {
  if (!isCheckedNgaySaoKimDuong || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_MUI) ||
    (month === 3 && chi === CHI_DAU) ||
    (month === 4 && chi === CHI_HOI) ||
    (month === 5 && chi === CHI_SUU) ||
    (month === 6 && chi === CHI_MAO) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_MUI) ||
    (month === 9 && chi === CHI_DAU) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_SUU) ||
    (month === 12 && chi === CHI_MAO)
}

/*
 * Tìm ngày có Sao Ngọc đường (Sao tốt)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Ngọc đường
 */
export function getNgaySaoNgocDuong(isCheckedNgaySaoNgocDuong, month, chi) {
  if (!isCheckedNgaySaoNgocDuong || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_MUI) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_HOI) ||
    (month === 4 && chi === CHI_SUU) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_TYJ) ||
    (month === 7 && chi === CHI_MUI) ||
    (month === 8 && chi === CHI_DAU) ||
    (month === 9 && chi === CHI_HOI) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_TYJ)
}

/*
 * Tìm ngày Sát chủ (Ngày xấu, kỵ làm nhà cửa, cưới gả)
 *
 * @return {Boolean} True nếu là ngày Sát chủ
 */
export function getNgaySatChu(isCheckedNgaySatChu, month, chi) {
  if (!isCheckedNgaySatChu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_MAO) ||
    (month === 5 && chi === CHI_THAN) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_SUU) ||
    (month === 9 && chi === CHI_NGO) ||
    (month === 10 && chi === CHI_DAU) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_THIN)
}

/*
 * Tìm ngày Tháng sát chủ (Ngày xấu, kỵ làm nhà cửa, cưới gả)
 *
 * @return {Boolean} True nếu là ngày Tháng sát chủ
 */
export function getNgayThangSatChu(isCheckedNgayThangSatChu, month, chi) {
  if (!isCheckedNgayThangSatChu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYS) ||
    ((month === 2 || month === 3 || month === 7 || month === 9) && chi === CHI_SUU) ||
    (month === 4 && chi === CHI_TUAT) ||
    (month === 11 && chi === CHI_MUI) ||
    ((month === 5 || month === 6 || month === 8 || month === 10 || month === 12) && chi === CHI_THIN)
}

/*
 * Tìm ngày Sát chủ trong 4 mùa (Ngày xấu, kỵ làm nhà cửa, cưới gả)
 *
 * @return {Boolean} True nếu là ngày Sát chủ trong 4 mùa
 */
export function getNgaySatChuTrong4Mua(
  isCheckedNgaySatChuTrong4Mua,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySatChuTrong4Mua ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_NGO) ||
    (isMuaHa && chi === CHI_TYS) ||
    (isMuaThu && chi === CHI_MUI) ||
    (isMuaDong && chi === CHI_MAO)
}

/*
 * Tìm ngày Bốn mùa sát chủ (Ngày xấu, kỵ làm nhà cửa, cưới gả)
 *
 * @return {Boolean} True nếu là ngày Bốn mùa sát chủ
 */
export function getNgayBonMuaSatChu(isCheckedNgayBonMuaSatChu, month, chi) {
  if (!isCheckedNgayBonMuaSatChu || !month || !chi) {
    return
  }

  return ((month === 1 || month === 5 || month === 9) && (chi === CHI_TYS)) ||
    ((month === 2 || month === 8 || month === 10) && (chi === CHI_MAO)) ||
    ((month === 3 || month === 7 || month === 11) && (chi === CHI_NGO)) ||
    ((month === 4 || month === 6 || month === 12) && (chi === CHI_DAU))
}

/*
 * Tìm ngày Thọ tử (Ngày xấu, kỵ làm nhà cửa, cưới gả)
 *
 * @return {Boolean} True nếu là ngày Thọ tử
 */
export function getNgayThoTu(isCheckedNgayThoTu, month, can, chi) {
  if (!isCheckedNgayThoTu || !month || !can || !chi) {
    return
  }

  return (month === 1 && can === CAN_BINH && chi === CHI_TUAT) ||
    (month === 2 && can === CAN_NHAM && chi === CHI_THIN) ||
    (month === 3 && can === CAN_TAN && chi === CHI_HOI) ||
    (month === 4 && can === CAN_DINH && chi === CHI_TYJ) ||
    (month === 5 && can === CAN_MAU && chi === CHI_TYS) ||
    (month === 6 && can === CAN_BINH && chi === CHI_NGO) ||
    (month === 7 && can === CAN_AT && chi === CHI_SUU) ||
    (month === 8 && can === CAN_QUY && chi === CHI_MUI) ||
    (month === 9 && can === CAN_GIAP && chi === CHI_DAN) ||
    (month === 10 && can === CAN_MAU && chi === CHI_THAN) ||
    (month === 11 && can === CAN_TAN && chi === CHI_MAO) ||
    (month === 12 && can === CAN_TAN && chi === CHI_DAU)
}

/*
 * Tìm ngày Ly sào (Ngày xấu, 14 ngày)
 *
 * @return {Boolean} True nếu là ngày Ly sào
 */
export function getNgayLySao(isCheckedNgayLySao, can, chi) {
  if (!isCheckedNgayLySao  || !can || !chi) {
    return
  }

  return (can === CAN_MAU && chi === CHI_THIN) ||
    (can === CAN_MAU && chi === CHI_DAN) ||
    (can === CAN_MAU && chi === CHI_NGO) ||
    (can === CAN_MAU && chi === CHI_TYS) ||
    (can === CAN_MAU && chi === CHI_TUAT) ||
    (can === CAN_MAU && chi === CHI_THAN) ||
    (can === CAN_TAN && chi === CHI_MAO) ||
    (can === CAN_TAN && chi === CHI_TYJ) ||
    (can === CAN_TAN && chi === CHI_SUU) ||
    (can === CAN_KY && chi === CHI_TYJ) ||
    (can === CAN_KY && chi === CHI_SUU) ||
    (can === CAN_KY && chi === CHI_HOI) || // ? Khác với Bàn về lịch vạn niên, ngày Kỷ Dậu
    (can === CAN_NHAM && chi === CHI_NGO) || // ? Khác với Bàn về lịch vạn niên, ko có ngày này
    (can === CAN_NHAM && chi === CHI_TUAT) ||
    (can === CAN_QUY && chi === CHI_TYJ)
}

/*
 * Tìm ngày Vãng vong (Ngày xấu, kỵ làm nhà cửa, cưới gả)
 *
 * @return {Boolean} True nếu là ngày Vãng vong
 */
export function getNgayVangVong(isCheckedNgayVangVong, month, chi) {
  if (!isCheckedNgayVangVong || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAN) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_THAN) ||
    (month === 4 && chi === CHI_HOI) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_NGO) ||
    (month === 7 && chi === CHI_DAU) ||
    (month === 8 && chi === CHI_TYS) ||
    (month === 9 && chi === CHI_THIN) ||
    (month === 10 && chi === CHI_MUI) ||
    (month === 11 && chi === CHI_TUAT) ||
    (month === 12 && chi === CHI_SUU)
}

/*
 * Tìm ngày Xích tòng tử (Ngày xấu)
 * Sách sưu tầm
 *
 * @return {Boolean} True nếu là ngày Xích tòng tử
 */
export function getNgayXichTongTu(isCheckedNgayXichTongTu, day, month) {
  if (!isCheckedNgayXichTongTu || !day || !month) {
    return
  }

  return (month === 1 && (day === 7 || day === 11)) ||
    (month === 2 && (day === 9 || day === 19)) ||
    (month === 3 && (day === 15 || day === 16)) ||
    (month === 4 && (day === 9 || day === 22)) ||
    (month === 5 && (day === 9 || day === 14)) ||
    (month === 6 && (day === 10 || day === 20)) ||
    (month === 7 && (day === 8 || day === 23)) ||
    (month === 8 && (day === 18 || day === 29)) ||
    (month === 9 && (day === 2 || day === 30)) ||
    (month === 10 && (day === 1 || day === 14)) ||
    (month === 11 && (day === 2 || day === 21)) ||
    (month === 12 && (day === 1 || day === 30))
}

/*
 * Tìm ngày Không vong (Ngày xấu)
 * Sách sưu tầm
 *
 * @return {Boolean} True nếu là ngày Không vong
 */
export function getNgayKhongVong(isCheckedNgayKhongVong, day, month, is30DaysInMonth) {
  if (!isCheckedNgayKhongVong || !day || !month) {
    return
  }

  if (is30DaysInMonth) {
    // Tháng đủ
    return (month === 1 && (day === 14 || day === 23 || day === 20)) ||
      (month === 2 && (day === 17 || day === 21 || day === 29)) ||
      (month === 3 && (day === 12 || day === 20 || day === 29)) ||
      (month === 4 && (day === 11 || day === 19 || day === 22)) ||
      (month === 5 && (day === 10 || day === 18 || day === 26)) ||
      (month === 6 && (day === 9 || day === 17 || day === 25)) ||
      (month === 7 && (day === 8 || day === 16 || day === 24)) ||
      (month === 8 && (day === 7 || day === 15 || day === 23)) ||
      (month === 9 && (day === 2 || day === 10 || day === 18)) ||
      (month === 10 && (day === 2 || day === 12 || day === 29)) ||
      (month === 11 && (day === 8 || day === 12 || day === 28)) ||
      (month === 12 && (day === 7 || day === 11 || day === 27))
  } else {
    // Tháng thiếu
    return (month === 1 && (day === 1 || day === 10 || day === 18)) ||
      (month === 2 && (day === 1 || day === 9 || day === 17)) ||
      (month === 3 && (day === 8 || day === 16 || day === 24)) ||
      (month === 4 && (day === 3 || day === 13 || day === 25)) ||
      (month === 5 && (day === 6 || day === 14 || day === 22)) ||
      (month === 6 && (day === 5 || day === 13 || day === 21)) ||
      (month === 7 && (day === 4 || day === 12 || day === 20)) ||
      (month === 8 && (day === 2 || day === 11 || day === 19)) ||
      (month === 9 && (day === 12 || day === 14 || day === 25)) ||
      (month === 10 && (day === 1 || day === 9 || day === 17)) ||
      (month === 11 && (day === 9 || day === 16 || day === 24)) ||
      (month === 12 && (day === 7 || day === 15 || day === 23))
  }
}

/*
 * Tìm ngày Hung bại (Ngày xấu)
 * Sách sưu tầm
 *
 * @return {Boolean} True nếu là ngày Hung bại
 */
export function getNgayHungBai(isCheckedNgayHungBai, day, month) {
  if (!isCheckedNgayHungBai || !day || !month) {
    return
  }

  return (month === 1 &&
      (day === 3 || day === 9 || day === 6 || day === 15 || day === 21)) ||

    (month === 2 &&
      (day === 2 || day === 5 || day === 8 || day === 14 || day === 19)) ||

    (month === 3 &&
      (day === 1 || day === 4 || day === 7 || day === 12 || day === 22)) ||

    (month === 4 &&
      (day === 2 || day === 6 || day === 8 || day === 12 || day === 25)) ||

    (month === 5 &&
      (day === 2 || day === 5 || day === 11 || day === 17 || day === 25)) ||

    (month === 6 &&
      (day === 1 || day === 6 || day === 10 || day === 16 || day === 20)) ||

    (month === 7 &&
      (day === 3 || day === 9 || day === 15 || day === 21)) ||

    (month === 8 &&
      (day === 2 || day === 8 || day === 14 || day === 28)) ||

    (month === 9 &&
      (day === 1 || day === 6 || day === 7 || day === 12 || day === 16)) ||

    (month === 10 &&
      (day === 6 || day === 10 || day === 12 || day === 14)) ||

    (month === 11 &&
      (day === 4 || day === 14)) ||

    (month === 12 &&
      (day === 3 || day === 5))
}

/*
 * Tìm ngày Xích khẩu (Ngày xấu, hay cãi nhau)
 * Sách sưu tầm
 *
 * @return {Boolean} True nếu là ngày Xích khẩu
 */
export function getNgayXichKhau(isCheckedNgayXichKhau, day, month) {
  if (!isCheckedNgayXichKhau || !day || !month) {
    return
  }

  return (month === 1 && (day === 7 || day === 13)) ||
    (month === 2 && (day === 5 || day === 14)) ||
    (month === 3 && (day === 2 || day === 9)) ||
    (month === 4 && (day === 1 || day === 9)) ||
    (month === 5 && (day === 5 || day === 15)) ||
    (month === 6 && (day === 3)) ||
    (month === 7 && (day === 1 || day === 18)) ||
    (month === 8 && (day === 2 || day === 27)) ||
    (month === 9 && (day === 2 || day === 26)) ||
    (month === 10 && (day === 1 || day === 23)) ||
    (month === 11 && (day === 15 || day === 21)) ||
    (month === 12 && (day === 9 || day === 19))
}

/*
 * Tìm ngày Hoang ốc trong 4 mùa (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Hoang ốc trong 4 mùa
 */
export function getNgayHoangOc4Mua(
  isCheckedNgayHoangOc4Mua,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgayHoangOc4Mua ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_THAN) ||
    (isMuaHa && chi === CHI_DAN) ||
    (isMuaThu && chi === CHI_MAO) ||
    (isMuaDong && chi === CHI_MUI)
}

/*
 * Tìm ngày Giá ốc trong 4 mùa (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Giá ốc trong 4 mùa
 */
export function getNgayGiaOc4Mua(
  isCheckedNgayGiaOc4Mua,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgayGiaOc4Mua ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_THAN) ||
    (isMuaHa && chi === CHI_DAN) ||
    (isMuaThu && chi === CHI_TYS) ||
    (isMuaDong && chi === CHI_HOI)
}

/*
 * Tìm ngày Hỏa tinh (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Hỏa tinh
 */
export function getNgayHoaTinh(isCheckedNgayHoaTinh, month, can, chi) {
  if (!isCheckedNgayHoaTinh || !month || !can || !chi) {
    return
  }

  return (month === 1 && can === CAN_AT && chi === CHI_SUU) ||

    (month === 2 && can === CAN_GIAP && chi === CHI_TYS) ||
    (month === 2 && can === CAN_TAN && chi === CHI_MAO) ||

    (month === 3 && can === CAN_NHAM && chi === CHI_THAN) ||
    (month === 3 && can === CAN_TAN && chi === CHI_TYJ) ||

    (month === 4 && can === CAN_QUY && chi === CHI_MUI) ||
    (month === 4 && can === CAN_NHAM && chi === CHI_THIN) ||

    (month === 5 && can === CAN_QUY && chi === CHI_DAU) ||
    (month === 5 && can === CAN_CANH && chi === CHI_TYS) ||

    (month === 6 && can === CAN_CANH && chi === CHI_DAN) ||
    (month === 6 && can === CAN_KY && chi === CHI_HOI) ||

    (month === 7 && can === CAN_TAN && chi === CHI_SUU) ||
    (month === 7 && can === CAN_CANH && chi === CHI_TUAT) ||

    (month === 8 && can === CAN_NHAM && chi === CHI_NGO) ||
    (month === 8 && can === CAN_KY && chi === CHI_DAU) ||

    (month === 9 && can === CAN_MAU && chi === CHI_THAN) ||
    (month === 9 && can === CAN_DINH && chi === CHI_TYJ) ||

    (month === 10 && can === CAN_KY && chi === CHI_MUI) ||

    (month === 11 && can === CAN_MAU && chi === CHI_NGO)
}

/*
 * Tìm ngày Thiên hỏa (Thiên ngục) (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày Thiên hỏa (Thiên ngục)
 */
export function getNgayThienHoa(isCheckedNgayThienHoa, month, chi) {
  if (!isCheckedNgayThienHoa || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYS) ||
    (month === 2 && chi === CHI_MAO) ||
    (month === 3 && chi === CHI_NGO) ||
    (month === 4 && chi === CHI_DAU) ||

    (month === 5 && chi === CHI_TYS) ||
    (month === 6 && chi === CHI_MAO) ||
    (month === 7 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_DAU) ||

    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_MAO) ||
    (month === 11 && chi === CHI_NGO) ||
    (month === 12 && chi === CHI_DAU)
}

/*
 * Tìm ngày Địa hỏa (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Địa hỏa
 */
export function getNgayDiaHoa(isCheckedNgayDiaHoa, month, chi) {
  if (!isCheckedNgayDiaHoa || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TUAT) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_THAN) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_NGO) ||
    (month === 6 && chi === CHI_TYJ) ||
    (month === 7 && chi === CHI_THIN) ||
    (month === 8 && chi === CHI_MAO) ||
    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_TYS) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày Độc hỏa (Nguyệt hỏa) (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày Độc hỏa (Nguyệt hỏa)
 */
export function getNgayDocHoa(isCheckedNgayDocHoa, month, chi) {
  if (!isCheckedNgayDocHoa || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_THIN) ||
    (month === 3 && chi === CHI_MAO) ||
    (month === 4 && chi === CHI_DAN) ||
    (month === 5 && chi === CHI_SUU) ||
    (month === 6 && chi === CHI_TYS) ||
    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_TUAT) ||
    (month === 9 && chi === CHI_DAU) ||
    (month === 10 && chi === CHI_THAN) ||
    (month === 11 && chi === CHI_MUI) ||
    (month === 12 && chi === CHI_NGO)
}

/*
 * Tìm ngày có Sao hỏa (Ngày xấu, sao xấu, kỵ làm nhà cửa)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 38)
 * Theo Sách Bàn về lịch vạn niên thì đây là ngày Hỏa tinh
 *
 * @return {Boolean} True nếu là ngày Sao hỏa
 */
export function getNgaySaoHoa(isCheckedNgaySaoHoa, month, can, chi) {
  if (!isCheckedNgaySaoHoa || !month || !can || !chi) {
    return
  }

  return (
    ((month === 1 || month === 4 || month === 7 || month === 10) && (
      (can === CAN_AT && chi === CHI_SUU) ||
      (can === CAN_GIAP && chi === CHI_TUAT) ||
      (can === CAN_QUY && chi === CHI_MUI) ||
      (can === CAN_NHAM && chi === CHI_THIN) ||
      (can === CAN_TAN && chi === CHI_SUU) ||
      (can === CAN_CANH && chi === CHI_TUAT) ||
      (can === CAN_KY && chi === CHI_MUI)
    )) ||

    ((month === 2 || month === 5 || month === 8 || month === 11) && (
      (can === CAN_GIAP && chi === CHI_TYS) ||
      (can === CAN_QUY && chi === CHI_DAU) ||
      (can === CAN_NHAM && chi === CHI_NGO) ||
      (can === CAN_TAN && chi === CHI_MAO) ||
      (can === CAN_CANH && chi === CHI_TYS) ||
      (can === CAN_KY && chi === CHI_DAU) ||
      (can === CAN_MAU && chi === CHI_NGO)
    )) ||

    ((month === 3 || month === 6 || month === 9 || month === 12) && (
      (can === CAN_NHAM && chi === CHI_THAN) ||
      (can === CAN_CANH && chi === CHI_DAN) ||
      (can === CAN_KY && chi === CHI_HOI) ||
      (can === CAN_MAU && chi === CHI_THAN) ||
      (can === CAN_DINH && chi === CHI_TYJ) // Trong sách Bàn về lịch vạn niên thì là ngày Tân Tỵ, với có thêm Quý Hợi
    ))
  )
}

/*
 * Tìm ngày Thiên tai đại họa (Ngày xấu, kỵ làm nhà cửa, cưới gả)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Thiên tai đại họa
 */
export function getNgayThienTaiDaiHoa(isCheckedNgayThienTaiDaiHoa, month, chi) {
  if (!isCheckedNgayThienTaiDaiHoa || !month || !chi) {
    return
  }

  return ((month === 1 || month === 5 || month === 9) && (chi === CHI_TYS)) ||
    ((month === 2 || month === 6 || month === 10) && (chi === CHI_MAO)) ||
    ((month === 3 || month === 7 || month === 11) && (chi === CHI_NGO)) ||
    ((month === 4 || month === 8 || month === 12) && (chi === CHI_DAU))
}

/*
 * Tìm ngày Lôi giang (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 37)
 *
 * @return {Boolean} True nếu là ngày Lôi giang
 */
export function getNgayLoiGiang(isCheckedNgayLoiGiang, month, chi) {
  if (!isCheckedNgayLoiGiang || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAN) ||
    (month === 2 && chi === CHI_SUU) ||
    (month === 3 && chi === CHI_TUAT) ||
    (month === 4 && chi === CHI_TYJ) ||
    (month === 5 && chi === CHI_THIN) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_THAN) ||
    (month === 8 && chi === CHI_MUI) ||
    (month === 9 && chi === CHI_MAO) ||
    (month === 10 && chi === CHI_TYS) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_NGO)
}

/*
 * Tìm ngày Lôi đình chính sát (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 37)
 *
 * @return {Boolean} True nếu là ngày Lôi đình chính sát
 */
export function getNgayLoiDinhChinhSat(isCheckedNgayLoiDinhChinhSat, month, chi) {
  if (!isCheckedNgayLoiDinhChinhSat || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_SUU) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_HOI) ||
    (month === 4 && chi === CHI_TUAT) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_THAN) ||
    (month === 7 && chi === CHI_MUI) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_THIN) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_DAN)
}

/*
 * Tìm ngày Lôi đình sát chủ (Ngày xấu, kỵ làm nhà cửa)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 37)
 *
 * @return {Boolean} True nếu là ngày Lôi đình sát chủ
 */
export function getNgayLoiDinhSatChu(isCheckedNgayLoiDinhSatChu, month, chi) {
  if (!isCheckedNgayLoiDinhSatChu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_SUU) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_THAN) ||
    (month === 4 && chi === CHI_TYJ) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_DAU) ||
    (month === 7 && chi === CHI_THAN) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_DAU) ||
    (month === 11 && chi === CHI_THAN) ||
    (month === 12 && chi === CHI_TYS)
}

/*
 * Tìm ngày Sát sư (Ngày xấu, kỵ cho thầy cúng)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Sát sư
 */
export function getNgaySatSu(isCheckedNgaySatSu, month, chi) {
  if (!isCheckedNgaySatSu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_MAO) ||
    (month === 5 && chi === CHI_HOI) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_THAN) ||
    (month === 9 && chi === CHI_SUU) ||
    (month === 10 && chi === CHI_DAU) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_THIN)
}

/*
 * Tìm ngày Tháng sát sư (Ngày xấu, kỵ cho thầy cúng)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Tháng sát sư
 */
export function getNgayThangSatSu(isCheckedNgayThangSatSu, month, chi) {
  if (!isCheckedNgayThangSatSu || !month || !chi) {
    return
  }

  return ((month === 1 || month === 11) && (chi === CHI_TYS)) ||
    ((month === 2) && (chi === CHI_SUU)) ||
    ((month === 4) && (chi === CHI_MAO)) ||
    ((month === 3 || month === 6 || month === 7) && (chi === CHI_MUI)) ||
    ((month === 9) && (chi === CHI_NGO)) ||
    (
      (month === 5 || month === 8 || month === 10 || month === 12) &&
      (chi === CHI_DAU)
    )
}

/*
 * Tìm ngày Nguyệt Kỵ (còn gọi là ngày Lý nhân) (Ngày xấu, mọi việc)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 5)
 *
 * @return {Boolean} True nếu là ngày Nguyệt Kỵ
 */
export function getNgayNguyetKy(isCheckedNgayNguyetKy, day) {
  if (!isCheckedNgayNguyetKy || !day) {
    return
  }

  return day === 5 || day === 14 || day === 23
}

/*
 * Tìm ngày Thiên mã tam cường (Ngày xấu, mọi việc)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 5)
 *
 * @return {Boolean} True nếu là ngày Thiên mã tam cường
 */
export function getNgayThienMaTamCuong(isCheckedNgayThienMaTamCuong, day) {
  if (!isCheckedNgayThienMaTamCuong || !day) {
    return
  }

  return day === 8 || day === 18 || day === 28
}

/*
 * Tìm ngày Tam nương sát (Ngày xấu, mọi việc)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan
 *
 * @return {Boolean} True nếu là ngày Tam nương sát
 */
export function getNgayTamNuongSat(isCheckedNgayTamNuongSat, day) {
  if (!isCheckedNgayTamNuongSat || !day) {
    return
  }

  return day === 3 || day === 7 ||
    day === 13 || day === 18 ||
    day === 22 || day === 27
}

/*
 * Tìm ngày Bất tương xấu (Ngày xấu, kỵ thăm quan, nhận chức)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan
 *
 * @return {Boolean} True nếu là ngày Bất tương xấu
 */
export function getNgayBatTuongXau(isCheckedNgayBatTuongXau, day) {
  if (!isCheckedNgayBatTuongXau || !day) {
    return
  }

  return day === 7 || day === 16 || day === 19 || day === 28
}

/*
 * Tìm ngày Nguyệt tận (Ngày xấu, ngày cuối các tháng)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày Nguyệt tận
 */
export function getNgayNguyetTan(isCheckedNgayNguyetTan, day, is30DaysInMonth) {
  if (
    !isCheckedNgayNguyetTan ||
    !day ||
    typeof is30DaysInMonth !== 'boolean'
  ) {
    return
  }

  return (is30DaysInMonth && day === 30) || (!is30DaysInMonth && day === 29)
}

/*
 * Tìm ngày Ngưu Lang Chức Nữ trong 4 mùa (Ngày xấu)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Ngưu Lang Chức Nữ trong 4 mùa
 */
export function getNgayNguuLangChucNu4Mua(
  isCheckedNgayNguuLangChucNu4Mua,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgayNguuLangChucNu4Mua ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_DAU) ||
    (isMuaHa && chi === CHI_MAO) ||
    (isMuaThu && chi === CHI_THAN) ||
    (isMuaDong && chi === CHI_DAN)
}

/*
 * Tìm ngày  (Ngày xấu, kỵ cưới gả)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Không sàng trong 4 mùa
 */
export function getNgayKhongSang4Mua(
  isCheckedNgayKhongSang4Mua,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgayKhongSang4Mua ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_THIN) ||
    (isMuaHa && chi === CHI_MUI) ||
    (isMuaThu && chi === CHI_TUAT) ||
    (isMuaDong && chi === CHI_SUU)
}

/*
 * Tìm ngày  (Ngày xấu, kỵ cưới gả)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Không phòng trong 4 mùa
 */
export function getNgayKhongPhong4Mua(
  isCheckedNgayKhongPhong4Mua,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgayKhongPhong4Mua ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_THIN) ||
    (isMuaXuan && chi === CHI_TYJ) ||
    (isMuaXuan && chi === CHI_TYS) ||

    (isMuaHa && chi === CHI_TUAT) ||
    (isMuaHa && chi === CHI_HOI) ||
    (isMuaHa && chi === CHI_MUI) ||

    (isMuaThu && chi === CHI_MAO) ||
    (isMuaThu && chi === CHI_DAN) ||
    (isMuaThu && chi === CHI_NGO) ||

    (isMuaDong && chi === CHI_DAU) ||
    (isMuaDong && chi === CHI_THAN) ||
    (isMuaDong && chi === CHI_SUU)
}

/*
 * Tìm ngày Tu La đoạt giá (Ngày xấu, Kỵ cưới gả)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Tu La đoạt giá
 */
export function getNgayTuLaDoatGia(isCheckedNgayTuLaDoatGia, month, chi) {
  if (!isCheckedNgayTuLaDoatGia || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THIN) ||
    (month === 2 && chi === CHI_SUU) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_TYS) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_TUAT) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_THAN) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_NGO)
}

/*
 * Tìm ngày Thập ác đại bại (Ngày xấu)
 *
 * @return {Boolean} True nếu là ngày Thập ác đại bại
 */
export function getNgayThapAcDaiBai(
  isCheckedNgayThapAcDaiBai,
  month,
  can,
  chi,
  canOfYearId
) {
  if (
    !isCheckedNgayThapAcDaiBai ||
    !month ||
    !can ||
    !chi ||
    !canOfYearId
  ) {
    return
  }

  return ( // Năm Giáp và năm Kỷ
    (canOfYearId === CAN_GIAP || canOfYearId === CAN_KY) &&
      (month === 3 && can === CAN_MAU && chi === CHI_TUAT)
  ) ||
    (
      (canOfYearId === CAN_GIAP || canOfYearId === CAN_KY) &&
      (month === 7 && can === CAN_QUY && chi === CHI_HOI)
    ) ||
    (
      (canOfYearId === CAN_GIAP || canOfYearId === CAN_KY) &&
      (month === 10 && can === CAN_BINH && chi === CHI_THAN)
    ) ||
    (
      (canOfYearId === CAN_GIAP || canOfYearId === CAN_KY) &&
      (month === 11 && can === CAN_DINH && chi === CHI_HOI)
    ) ||

    // Năm Ất và năm Canh
    (
      (canOfYearId === CAN_AT || canOfYearId === CAN_CANH) &&
      (month === 4 && can === CAN_NHAM && chi === CHI_THAN)
    ) ||
    (
      (canOfYearId === CAN_AT || canOfYearId === CAN_CANH) &&
      (month === 9 && can === CAN_AT && chi === CHI_TYJ)
    ) ||

    // Năm Bính và năm Tân
    (
      (canOfYearId === CAN_BINH || canOfYearId === CAN_TAN) &&
      (month === 3 && can === CAN_TAN && chi === CHI_TYJ)
    ) ||
    (
      (canOfYearId === CAN_BINH || canOfYearId === CAN_TAN) &&
      (month === 9 && can === CAN_CANH && chi === CHI_THIN)
    ) ||
    ( // binh, binh
      (canOfYearId === CAN_BINH || canOfYearId === CAN_TAN) &&
      (month === 10 && can === CAN_BINH && chi === CHI_THIN)
    ) ||

    // Năm Mậu và năm Quý
    (
      (canOfYearId === CAN_MAU || canOfYearId === CAN_QUY) &&
      (month === 6 && can === CAN_KY && chi === CHI_SUU)
    )
}

/*
 * Tìm ngày Nhập mộ trong 4 mùa (Ngày xấu, đau ốm vào ngày này thì xấu)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Boolean} True nếu là ngày Nhập mộ trong 4 mùa
 */
export function getNgayNhapMo4Mua(
  isCheckedNgayNhapMo4Mua,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgayNhapMo4Mua ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_DAU) ||
    (isMuaHa && chi === CHI_TYS) ||
    (isMuaThu && chi === CHI_MAO) ||
    (isMuaDong && chi === CHI_NGO)
}

/*
 * Tìm ngày Thần Hào (Ngày Xấu)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 31)
 *
 * @return {Boolean} True nếu là ngày Thần Hào
 */
export function getNgayThanHao(isCheckedNgayThanHao, month, chi) {
  if (!isCheckedNgayThanHao || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TUAT) ||
    (month === 2 && chi === CHI_HOI) ||
    (month === 3 && chi === CHI_TYS) ||
    (month === 4 && chi === CHI_SUU) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_MAO) ||
    (month === 7 && chi === CHI_THIN) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_NGO) ||
    (month === 10 && chi === CHI_MUI) ||
    (month === 11 && chi === CHI_THAN) ||
    (month === 12 && chi === CHI_DAU)
}

/*
 * Tìm ngày Quỷ Khốc (Ngày Xấu)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 31)
 *
 * @return {Boolean} True nếu là ngày Quỷ Khốc
 */
export function getNgayQuyKhoc(isCheckedNgayQuyKhoc, month, chi) {
  if (!isCheckedNgayQuyKhoc || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_MUI) ||
    (month === 2 && chi === CHI_THAN) ||
    (month === 3 && chi === CHI_DAU) ||
    (month === 4 && chi === CHI_TUAT) ||
    (month === 5 && chi === CHI_HOI) ||
    (month === 6 && chi === CHI_TYS) ||
    (month === 7 && chi === CHI_SUU) ||
    (month === 8 && chi === CHI_DAN) ||
    (month === 9 && chi === CHI_MAO) ||
    (month === 10 && chi === CHI_THIN) ||
    (month === 11 && chi === CHI_TYJ) ||
    (month === 12 && chi === CHI_NGO)
}

/*
 * Tìm ngày Thiên môn bế tắc (Ngày xấu, tránh là cổng, cửa, xây lát đường đi)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 32)
 *
 * @return {Boolean} True nếu là ngày Thiên môn bế tắc
 */
export function getNgayThienMonBeTac(isCheckedNgayThienMonBeTac, can, trucOfDay) {
  if (!isCheckedNgayThienMonBeTac || !can || !trucOfDay) {
    return
  }

  return can === CAN_MAU || can === CAN_KY ||
    trucOfDay.id === TRUC_MAN || trucOfDay.id === TRUC_PHA
}

/*
 * Tìm 4 ngày Tứ Ly hoặc 4 ngày Tứ Tuyệt (Ngày xấu)
 * 4 ngày Tứ Ly: trước một ngày những tiết xuân phân, hạ chí, thu phân, đông chí.
 * 4 ngày Tứ Tuyệt: trước một ngày những tiết lập xuân, lập hạ, lập thu và lập đông.
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 38)
 *
 * @return {Boolean} True nếu là ngày Tứ Ly hoặc Tứ Tuyệt
 */
export function getNgayTuLyOrTuTuyet(isCheckedNgayTuLyOrTuTuyet, dateInLoop, dateList) {
  if (
    !isCheckedNgayTuLyOrTuTuyet ||
    !dateInLoop.isValid() ||
    !(Array.isArray(dateList) && dateList.length)
  ) {
    return
  }

  for (var i = 0; i < dateList.length; i++) {
    if (dateInLoop.diff(dateList[i], 'days') === -1) {
      return true
    }
  }
}

/*
 * Tìm ngày có Sao Cửu thổ quỷ (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Cửu thổ quỷ
 */
export function getNgaySaoCuuThoQuy(isCheckedNgaySaoCuuThoQuy, can, chi) {
  if (!isCheckedNgaySaoCuuThoQuy || !can|| !chi) {
    return
  }

  return (can === CAN_AT && chi === CHI_DAU) ||
    (can === CAN_QUY && chi === CHI_TYJ) ||
    (can === CAN_GIAP && chi === CHI_NGO) ||
    (can === CAN_TAN && chi === CHI_SUU) ||
    (can === CAN_NHAM && chi === CHI_DAN) ||
    (can === CAN_KY && chi === CHI_DAU) ||
    (can === CAN_CANH && chi === CHI_TUAT) ||
    (can === CAN_DINH && chi === CHI_SUU) ||
    (can === CAN_MAU && chi === CHI_NGO)
}

/*
 * Tìm ngày có Sao Thiên cương (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên cương
 */
export function getNgaySaoThienCuong(isCheckedNgaySaoThienCuong, month, chi) {
  if (!isCheckedNgaySaoThienCuong || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_DAN) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_THIN) ||
    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_SUU) ||
    (month === 10 && chi === CHI_THAN) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Thiên lại (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên lại
 */
export function getNgaySaoThienLai(isCheckedNgaySaoThienLai, month, chi) {
  if (!isCheckedNgaySaoThienLai || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAU) ||
    (month === 2 && chi === CHI_NGO) ||
    (month === 3 && chi === CHI_MAO) ||
    (month === 4 && chi === CHI_TYS) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_NGO) ||
    (month === 7 && chi === CHI_MAO) ||
    (month === 8 && chi === CHI_TYS) ||
    (month === 9 && chi === CHI_DAU) ||
    (month === 10 && chi === CHI_NGO) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Tiểu hồng sa (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tiểu hồng sa
 */
export function getNgaySaoTieuHongSa(isCheckedNgaySaoTieuHongSa, month, chi) {
  if (!isCheckedNgaySaoTieuHongSa || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_SUU) ||

    (month === 4 && chi === CHI_TYJ) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_SUU) ||

    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_DAU) ||
    (month === 9 && chi === CHI_SUU) ||

    (month === 10 && chi === CHI_TYJ) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_SUU)
}

/*
 * Tìm ngày có Sao Đại hao (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Đại hao
 */
export function getNgaySaoDaiHao(isCheckedNgaySaoDaiHao, month, chi) {
  if (!isCheckedNgaySaoDaiHao || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_NGO) ||
    (month === 2 && chi === CHI_MUI) ||
    (month === 3 && chi === CHI_THAN) ||
    (month === 4 && chi === CHI_DAU) ||
    (month === 5 && chi === CHI_TUAT) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_TYS) ||
    (month === 8 && chi === CHI_SUU) ||
    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_MAO) ||
    (month === 11 && chi === CHI_THIN) ||
    (month === 12 && chi === CHI_TYJ)
}

/*
 * Tìm ngày có Sao Tiểu hao (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tiểu hao
 */
export function getNgaySaoTieuHao(isCheckedNgaySaoTieuHao, month, chi) {
  if (!isCheckedNgaySaoTieuHao || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_NGO) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_THAN) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_TYS) ||
    (month === 9 && chi === CHI_SUU) ||
    (month === 10 && chi === CHI_DAN) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_THIN)
}

/*
 * Tìm ngày có Sao Nguyệt phá (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt phá
 */
export function getNgaySaoNguyetPha(isCheckedNgaySaoNguyetPha, month, chi) {
  if (!isCheckedNgaySaoNguyetPha || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THAN) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_TUAT) ||
    (month === 4 && chi === CHI_HOI) ||
    (month === 5 && chi === CHI_TYS) ||
    (month === 6 && chi === CHI_SUU) ||
    (month === 7 && chi === CHI_DAN) ||
    (month === 8 && chi === CHI_MAO) ||
    (month === 9 && chi === CHI_THIN) ||
    (month === 10 && chi === CHI_TYJ) ||
    (month === 11 && chi === CHI_NGO) ||
    (month === 12 && chi === CHI_MUI)
}

/*
 * Tìm ngày có Sao Kiếp sát (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Kiếp sát
 */
export function getNgaySaoKiepSat(isCheckedNgaySaoKiepSat, month, chi) {
  if (!isCheckedNgaySaoKiepSat || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_HOI) ||
    (month === 2 && chi === CHI_THAN) ||
    (month === 3 && chi === CHI_TYJ) ||
    (month === 4 && chi === CHI_DAN) ||
    (month === 5 && chi === CHI_HOI) ||
    (month === 6 && chi === CHI_THAN) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_DAN) ||
    (month === 9 && chi === CHI_HOI) ||
    (month === 10 && chi === CHI_THAN) ||
    (month === 11 && chi === CHI_TYJ) ||
    (month === 12 && chi === CHI_DAN)
}

/*
 * Tìm ngày có Sao Địa phá (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Địa phá
 */
export function getNgaySaoDiaPha(isCheckedNgaySaoDiaPha, month, chi) {
  if (!isCheckedNgaySaoDiaPha || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_HOI) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_SUU) ||
    (month === 4 && chi === CHI_DAN) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_THIN) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_MUI) ||
    (month === 10 && chi === CHI_THAN) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Thổ phủ (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thổ phủ
 */
export function getNgaySaoThoPhu(isCheckedNgaySaoThoPhu, month, chi) {
  if (!isCheckedNgaySaoThoPhu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAN) ||
    (month === 2 && chi === CHI_MAO) ||
    (month === 3 && chi === CHI_THIN) ||
    (month === 4 && chi === CHI_TYJ) ||
    (month === 5 && chi === CHI_NGO) ||
    (month === 6 && chi === CHI_MUI) ||
    (month === 7 && chi === CHI_THAN) ||
    (month === 8 && chi === CHI_DAU) ||
    (month === 9 && chi === CHI_TUAT) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_TYS) ||
    (month === 12 && chi === CHI_SUU)
}

/*
 * Tìm ngày có Sao Thổ ôn (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thổ ôn
 */
export function getNgaySaoThoOn(isCheckedNgaySaoThoOn, month, chi) {
  if (!isCheckedNgaySaoThoOn || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THIN) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_NGO) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_THAN) ||
    (month === 6 && chi === CHI_DAU) ||
    (month === 7 && chi === CHI_TUAT) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_MAO)
}

/*
 * Tìm ngày có Sao Thiên ôn (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên ôn
 */
export function getNgaySaoThienOn(isCheckedNgaySaoThienOn, month, chi) {
  if (!isCheckedNgaySaoThienOn || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_MUI) ||
    (month === 2 && chi === CHI_TUAT) ||
    (month === 3 && chi === CHI_THIN) ||
    (month === 4 && chi === CHI_DAN) ||
    (month === 5 && chi === CHI_NGO) ||
    (month === 6 && chi === CHI_TYS) ||
    (month === 7 && chi === CHI_DAU) ||
    (month === 8 && chi === CHI_THAN) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_SUU) ||
    (month === 12 && chi === CHI_MAO)
}

/*
 * Tìm ngày có Sao Thụ tử (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thụ tử
 */
export function getNgaySaoThuTu(isCheckedNgaySaoThuTu, month, chi) {
  if (!isCheckedNgaySaoThuTu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TUAT) ||
    (month === 2 && chi === CHI_THIN) ||
    (month === 3 && chi === CHI_HOI) ||
    (month === 4 && chi === CHI_TYJ) ||
    (month === 5 && chi === CHI_TYS) ||
    (month === 6 && chi === CHI_NGO) ||
    (month === 7 && chi === CHI_SUU) ||
    (month === 8 && chi === CHI_MUI) ||
    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_THAN) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_DAU)
}

/*
 * Tìm ngày có Sao Hoang vu (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Hoang vu
 */
export function getNgaySaoHoangVu(
  isCheckedNgaySaoHoangVu,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoHoangVu ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_TYJ) ||
    (isMuaXuan && chi === CHI_DAU) ||
    (isMuaXuan && chi === CHI_SUU) ||

    (isMuaHa && chi === CHI_THAN) ||
    (isMuaHa && chi === CHI_TYS) ||
    (isMuaHa && chi === CHI_THIN) ||

    (isMuaThu && chi === CHI_HOI) ||
    (isMuaThu && chi === CHI_MAO) ||
    (isMuaThu && chi === CHI_MUI) ||

    (isMuaDong && chi === CHI_DAN) ||
    (isMuaDong && chi === CHI_NGO) ||
    (isMuaDong && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Thiên tặc (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên tặc
 */
export function getNgaySaoThienTac(isCheckedNgaySaoThienTac, month, chi) {
  if (!isCheckedNgaySaoThienTac || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THIN) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_DAN) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_TYS) ||
    (month === 6 && chi === CHI_TYJ) ||
    (month === 7 && chi === CHI_TUAT) ||
    (month === 8 && chi === CHI_MAO) ||
    (month === 9 && chi === CHI_THAN) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_NGO) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Địa tặc (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Địa tặc
 */
export function getNgaySaoDiaTac(isCheckedNgaySaoDiaTac, month, chi) {
  if (!isCheckedNgaySaoDiaTac || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_SUU) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_HOI) ||
    (month === 4 && chi === CHI_TUAT) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_THAN) ||
    (month === 7 && chi === CHI_MUI) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_THIN) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_DAN)
}

/*
 * Tìm ngày có Sao Hoả tai (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Hoả tai
 */
export function getNgaySaoHoaTai(isCheckedNgaySaoHoaTai, month, chi) {
  if (!isCheckedNgaySaoHoaTai || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_SUU) ||
    (month === 2 && chi === CHI_MUI) ||
    (month === 3 && chi === CHI_DAN) ||
    (month === 4 && chi === CHI_THAN) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_DAU) ||
    (month === 7 && chi === CHI_THIN) ||
    (month === 8 && chi === CHI_TUAT) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_NGO) ||
    (month === 12 && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Nguyệt yếm (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt yếm
 */
export function getNgaySaoNguyetYem(isCheckedNgaySaoNguyetYem, month, chi) {
  if (!isCheckedNgaySaoNguyetYem || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TUAT) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_THAN) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_NGO) ||
    (month === 6 && chi === CHI_TYJ) ||
    (month === 7 && chi === CHI_THIN) ||
    (month === 8 && chi === CHI_MAO) ||
    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_TYS) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Nguyệt hư (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt hư
 */
export function getNgaySaoNguyetHu(isCheckedNgaySaoNguyetHu, month, chi) {
  if (!isCheckedNgaySaoNguyetHu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_SUU) ||
    (month === 2 && chi === CHI_TUAT) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_THIN) ||

    (month === 5 && chi === CHI_SUU) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_MUI) ||
    (month === 8 && chi === CHI_THIN) ||

    (month === 9 && chi === CHI_SUU) ||
    (month === 10 && chi === CHI_TUAT) ||
    (month === 11 && chi === CHI_MUI) ||
    (month === 12 && chi === CHI_THIN)
}

/*

 * Tìm ngày có Sao Hoàng sa (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Hoàng sa
 */
export function getNgaySaoHoangSa(isCheckedNgaySaoHoangSa, month, chi) {
  if (!isCheckedNgaySaoHoangSa || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_NGO) ||
    (month === 2 && chi === CHI_DAN) ||
    (month === 3 && chi === CHI_TYS) ||

    (month === 4 && chi === CHI_NGO) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_TYS) ||

    (month === 7 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_DAN) ||
    (month === 9 && chi === CHI_TYS) ||

    (month === 10 && chi === CHI_NGO) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Lục bất thành (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Lục bất thành
 */
export function getNgaySaoLucBatThanh(isCheckedNgaySaoLucBatThanh, month, chi) {
  if (!isCheckedNgaySaoLucBatThanh || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAN) ||
    (month === 2 && chi === CHI_NGO) ||
    (month === 3 && chi === CHI_TUAT) ||

    (month === 4 && chi === CHI_TYJ) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_SUU) ||

    (month === 7 && chi === CHI_THAN) ||
    (month === 8 && chi === CHI_TYS) ||
    (month === 9 && chi === CHI_THIN) ||

    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_MUI)
}

/*
 * Tìm ngày có Sao Nhân cách (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nhân cách
 */
export function getNgaySaoNhanCach(isCheckedNgaySaoNhanCach, month, chi) {
  if (!isCheckedNgaySaoNhanCach || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAU) ||
    (month === 2 && chi === CHI_MUI) ||
    (month === 3 && chi === CHI_TYJ) ||
    (month === 4 && chi === CHI_MAO) ||
    (month === 5 && chi === CHI_SUU) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_DAU) ||
    (month === 8 && chi === CHI_MUI) ||
    (month === 9 && chi === CHI_TYJ) ||
    (month === 10 && chi === CHI_MAO) ||
    (month === 11 && chi === CHI_SUU) ||
    (month === 12 && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Thần cách (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thần cách
 */
export function getNgaySaoThanCach(isCheckedNgaySaoThanCach, month, chi) {
  if (!isCheckedNgaySaoThanCach || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_MAO) ||
    (month === 3 && chi === CHI_SUU) ||
    (month === 4 && chi === CHI_HOI) ||
    (month === 5 && chi === CHI_DAU) ||
    (month === 6 && chi === CHI_MUI) ||

    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_MAO) ||
    (month === 9 && chi === CHI_SUU) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_MUI)
}

/*
 * Tìm ngày có Sao Phi ma sát (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Phi ma sát
 */
export function getNgaySaoPhiMaSat(isCheckedNgaySaoPhiMaSat, month, chi) {
  if (!isCheckedNgaySaoPhiMaSat || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYS) ||
    (month === 2 && chi === CHI_DAU) ||
    (month === 3 && chi === CHI_NGO) ||
    (month === 4 && chi === CHI_MAO) ||

    (month === 5 && chi === CHI_TYS) ||
    (month === 6 && chi === CHI_DAU) ||
    (month === 7 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_MAO) ||

    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_DAU) ||
    (month === 11 && chi === CHI_NGO) ||
    (month === 12 && chi === CHI_MAO)
}
/*
 * Tìm ngày có Sao Ngũ quỷ (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Ngũ quỷ
 */
export function getNgaySaoNguQuy(isCheckedNgaySaoNguQuy, month, chi) {
  if (!isCheckedNgaySaoNguQuy || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_NGO) ||
    (month === 2 && chi === CHI_DAN) ||
    (month === 3 && chi === CHI_THIN) ||
    (month === 4 && chi === CHI_DAU) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_THAN) ||
    (month === 7 && chi === CHI_SUU) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_MUI) ||
    (month === 12 && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Băng tiêu ngoạ hãm (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Băng tiêu ngoạ hãm
 */
export function getNgaySaoBangTieuNgoaHam(isCheckedNgaySaoBangTieuNgoaHam, month, chi) {
  if (!isCheckedNgaySaoBangTieuNgoaHam || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_SUU) ||
    (month === 4 && chi === CHI_DAN) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_MUI) ||
    (month === 10 && chi === CHI_THAN) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_THIN)
}

/*
 * Tìm ngày có Sao Hà khôi cẩu giảo (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Hà khôi cẩu giảo
 */
export function getNgaySaoHaKhoiCauGiao(isCheckedNgaySaoHaKhoiCauGiao, month, chi) {
  if (!isCheckedNgaySaoHaKhoiCauGiao || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_HOI) ||
    (month === 2 && chi === CHI_NGO) ||
    (month === 3 && chi === CHI_SUU) ||
    (month === 4 && chi === CHI_THAN) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_TUAT) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_TYS) ||
    (month === 9 && chi === CHI_MUI) ||
    (month === 10 && chi === CHI_DAN) ||
    (month === 11 && chi === CHI_DAU) ||
    (month === 12 && chi === CHI_THIN)
}

/*
 * Tìm ngày có Sao Cửu không (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Cửu không
 */
export function getNgaySaoCuuKhong(isCheckedNgaySaoCuuKhong, month, chi) {
  if (!isCheckedNgaySaoCuuKhong || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THIN) ||
    (month === 2 && chi === CHI_SUU) ||
    (month === 3 && chi === CHI_TUAT) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_MAO) ||
    (month === 6 && chi === CHI_TYS) ||
    (month === 7 && chi === CHI_DAU) ||
    (month === 8 && chi === CHI_NGO) ||
    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_THAN) ||
    (month === 12 && chi === CHI_TYJ)
}

/*
 * Tìm ngày có Sao Trùng tang (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Trùng tang
 */
export function getNgaySaoTrungTang(isCheckedNgaySaoTrungTang, month, can) {
  if (!isCheckedNgaySaoTrungTang || !month || !can) {
    return
  }

  return (month === 1 && can === CAN_GIAP) ||
    (month === 2 && can === CAN_AT) ||
    (month === 3 && can === CAN_KY) ||
    (month === 4 && can === CAN_BINH) ||
    (month === 5 && can === CAN_DINH) ||
    (month === 6 && can === CAN_KY) ||
    (month === 7 && can === CAN_CANH) ||
    (month === 8 && can === CAN_TAN) ||
    (month === 9 && can === CAN_KY) ||
    (month === 10 && can === CAN_NHAM) ||
    (month === 11 && can === CAN_QUY) ||
    (month === 12 && can === CAN_KY)
}

/*
 * Tìm ngày có Sao Trùng phục (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Trùng phục
 */
export function getNgaySaoTrungPhuc(isCheckedNgaySaoTrungPhuc, month, can) {
  if (!isCheckedNgaySaoTrungPhuc || !month || !can) {
    return
  }

  return (month === 1 && can === CAN_CANH) ||
    (month === 2 && can === CAN_TAN) ||
    (month === 3 && can === CAN_KY) ||
    (month === 4 && can === CAN_NHAM) ||
    (month === 5 && can === CAN_QUY) ||
    (month === 6 && can === CAN_MAU) ||
    (month === 7 && can === CAN_GIAP) ||
    (month === 8 && can === CAN_AT) ||
    (month === 9 && can === CAN_KY) ||
    (month === 10 && can === CAN_NHAM) ||
    (month === 11 && can === CAN_QUY) ||
    (month === 12 && can === CAN_KY)
}

/*
 * Tìm ngày có Sao Chu tước hắc đạo (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Chu tước hắc đạo
 */
export function getNgaySaoChuTuocHacDao(isCheckedNgaySaoChuTuocHacDao, month, chi) {
  if (!isCheckedNgaySaoChuTuocHacDao || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_MAO) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_DAU) ||
    (month === 5 && chi === CHI_HOI) ||
    (month === 6 && chi === CHI_SUU) ||

    (month === 7 && chi === CHI_MAO) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_MUI) ||
    (month === 10 && chi === CHI_DAU) ||
    (month === 11 && chi === CHI_HOI) ||
    (month === 12 && chi === CHI_SUU)
}

/*
 * Tìm ngày có Sao Bạch hổ (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Bạch hổ
 */
export function getNgaySaoBachHo(isCheckedNgaySaoBachHo, month, chi) {
  if (!isCheckedNgaySaoBachHo || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_NGO) ||
    (month === 2 && chi === CHI_THAN) ||
    (month === 3 && chi === CHI_TUAT) ||
    (month === 4 && chi === CHI_TYS) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_THIN) ||

    (month === 7 && chi === CHI_NGO) ||
    (month === 8 && chi === CHI_THAN) ||
    (month === 9 && chi === CHI_TUAT) ||
    (month === 10 && chi === CHI_TYS) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_THIN)
}

/*
 * Tìm ngày có Sao Huyền vũ (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Huyền vũ
 */
export function getNgaySaoHuyenVu(isCheckedNgaySaoHuyenVu, month, chi) {
  if (!isCheckedNgaySaoHuyenVu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAU) ||
    (month === 2 && chi === CHI_HOI) ||
    (month === 3 && chi === CHI_SUU) ||
    (month === 4 && chi === CHI_MAO) ||
    (month === 5 && chi === CHI_TYJ) ||
    (month === 6 && chi === CHI_MUI) ||

    (month === 7 && chi === CHI_DAU) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && chi === CHI_SUU) ||
    (month === 10 && chi === CHI_MAO) ||
    (month === 11 && chi === CHI_TYJ) ||
    (month === 12 && chi === CHI_MUI)
}

/*
 * Tìm ngày có Sao Câu trận (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Câu trận
 */
export function getNgaySaoCauTran(isCheckedNgaySaoCauTran, month, chi) {
  if (!isCheckedNgaySaoCauTran || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_HOI) ||
    (month === 2 && chi === CHI_SUU) ||
    (month === 3 && chi === CHI_MAO) ||
    (month === 4 && chi === CHI_TYJ) ||
    (month === 5 && chi === CHI_MUI) ||
    (month === 6 && chi === CHI_DAU) ||

    (month === 7 && chi === CHI_HOI) ||
    (month === 8 && chi === CHI_SUU) ||
    (month === 9 && chi === CHI_MAO) ||
    (month === 10 && chi === CHI_TYJ) ||
    (month === 11 && chi === CHI_MUI) ||
    (month === 12 && chi === CHI_DAU)
}

/*
 * Tìm ngày có Sao Lôi công (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Lôi công
 */
export function getNgaySaoLoiCong(isCheckedNgaySaoLoiCong, month, chi) {
  if (!isCheckedNgaySaoLoiCong || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_DAN) ||
    (month === 2 && chi === CHI_HOI) ||
    (month === 3 && chi === CHI_TYJ) ||
    (month === 4 && chi === CHI_THAN) ||

    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_HOI) ||
    (month === 7 && chi === CHI_TYJ) ||
    (month === 8 && chi === CHI_THAN) ||

    (month === 9 && chi === CHI_DAN) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_TYJ) ||
    (month === 12 && chi === CHI_THAN)
}

/*
 * Tìm ngày có Sao Cô thần (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Cô thần
 */
export function getNgaySaoCoThan(isCheckedNgaySaoCoThan, month, chi) {
  if (!isCheckedNgaySaoCoThan || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TUAT) ||
    (month === 2 && chi === CHI_HOI) ||
    (month === 3 && chi === CHI_TYS) ||
    (month === 4 && chi === CHI_SUU) ||
    (month === 5 && chi === CHI_DAN) ||
    (month === 6 && chi === CHI_MAO) ||
    (month === 7 && chi === CHI_THIN) ||
    (month === 8 && chi === CHI_TYJ) ||
    (month === 9 && chi === CHI_NGO) ||
    (month === 10 && chi === CHI_MUI) ||
    (month === 11 && chi === CHI_THAN) ||
    (month === 12 && chi === CHI_DAU)
}

/*
 * Tìm ngày có Sao Quả tú (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Quả tú
 */
export function getNgaySaoQuaTu(isCheckedNgaySaoQuaTu, month, chi) {
  if (!isCheckedNgaySaoQuaTu || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_THIN) ||
    (month === 2 && chi === CHI_TYJ) ||
    (month === 3 && chi === CHI_NGO) ||
    (month === 4 && chi === CHI_MUI) ||
    (month === 5 && chi === CHI_THAN) ||
    (month === 6 && chi === CHI_DAU) ||
    (month === 7 && chi === CHI_TUAT) ||
    (month === 8 && chi === CHI_HOI) ||
    (month === 9 && chi === CHI_TYS) ||
    (month === 10 && chi === CHI_SUU) ||
    (month === 11 && chi === CHI_DAN) ||
    (month === 12 && chi === CHI_MAO)
}

/*
 * Tìm ngày có Sao Nguyệt hình (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt hình
 */
export function getNgaySaoNguyetHinh(isCheckedNgaySaoNguyetHinh, month, chi) {
  if (!isCheckedNgaySaoNguyetHinh || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_TYJ) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_THIN) ||
    (month === 4 && chi === CHI_THAN) ||
    (month === 5 && chi === CHI_NGO) ||
    (month === 6 && chi === CHI_SUU) ||
    (month === 7 && chi === CHI_DAN) ||
    (month === 8 && chi === CHI_DAU) ||
    (month === 9 && chi === CHI_MUI) ||
    (month === 10 && chi === CHI_HOI) ||
    (month === 11 && chi === CHI_MAO) ||
    (month === 12 && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Tội chỉ (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tội chỉ
 */
export function getNgaySaoToiChi(isCheckedNgaySaoToiChi, month, chi) {
  if (!isCheckedNgaySaoToiChi || !month || !chi) {
    return
  }

  return (month === 1 && chi === CHI_NGO) ||
    (month === 2 && chi === CHI_TYS) ||
    (month === 3 && chi === CHI_MUI) ||
    (month === 4 && chi === CHI_SUU) ||
    (month === 5 && chi === CHI_THAN) ||
    (month === 6 && chi === CHI_DAN) ||
    (month === 7 && chi === CHI_DAU) ||
    (month === 8 && chi === CHI_MAO) ||
    (month === 9 && chi === CHI_TUAT) ||
    (month === 10 && chi === CHI_THIN) ||
    (month === 11 && chi === CHI_HOI) ||
    (month === 12 && chi === CHI_TYJ)
}

/*
 * Tìm ngày có Sao Nguyệt Kiến chuyển sát (Sao xấu)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 5)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Nguyệt Kiến chuyển sát
 */
export function getNgaySaoNguyetKienChuyenSat(
  isCheckedNgaySaoNguyetKienChuyenSat,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoNguyetKienChuyenSat ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_MAO) ||
    (isMuaHa && chi === CHI_NGO) ||
    (isMuaThu && chi === CHI_DAU) ||
    (isMuaDong && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Thiên địa chính chuyển (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên địa chính chuyển
 */
export function getNgaySaoThienDiaChinhChuyen(
  isCheckedNgaySaoThienDiaChinhChuyen,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  can,
  chi
) {
  if (
    !isCheckedNgaySaoThienDiaChinhChuyen ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !can ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && can === CAN_QUY && chi === CHI_MAO) ||
    (isMuaHa && can === CAN_BINH && chi === CHI_NGO) ||
    (isMuaThu && can === CAN_DINH && chi === CHI_DAU) ||
    (isMuaDong && can === CAN_CANH && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Thiên địa chuyển sát (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thiên địa chuyển sát
 */
export function getNgaySaoThienDiaChuyenSat(
  isCheckedNgaySaoThienDiaChuyenSat,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  can,
  chi
) {
  if (
    !isCheckedNgaySaoThienDiaChuyenSat ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !can ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && can === CAN_AT && chi === CHI_MAO) ||
    (isMuaHa && can === CAN_BINH && chi === CHI_NGO) ||
    (isMuaThu && can === CAN_TAN && chi === CHI_DAU) ||
    (isMuaDong && can === CAN_NHAM && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Lỗ ban sát (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Lỗ ban sát
 */
export function getNgaySaoLoBanSat(
  isCheckedNgaySaoLoBanSat,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoLoBanSat ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_TYS) ||
    (isMuaHa && chi === CHI_MAO) ||
    (isMuaThu && chi === CHI_NGO) ||
    (isMuaDong && chi === CHI_DAU)
}

/*
 * Tìm ngày có Sao Phủ đầu sát (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Phủ đầu sát
 */
export function getNgaySaoPhuDauSat(
  isCheckedNgaySaoPhuDauSat,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoPhuDauSat ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_THIN) ||
    (isMuaHa && chi === CHI_MUI) ||
    (isMuaThu && chi === CHI_DAU) ||
    (isMuaDong && chi === CHI_TYS)
}

/*
 * Tìm ngày có Sao Tam tang (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tam tang
 */
export function getNgaySaoTamTang(
  isCheckedNgaySaoTamTang,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoTamTang ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_THIN) ||
    (isMuaHa && chi === CHI_MUI) ||
    (isMuaThu && chi === CHI_TUAT) ||
    (isMuaDong && chi === CHI_SUU)
}

/*
 * Tìm ngày có Sao Ngũ hư (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Ngũ hư
 */
export function getNgaySaoNguHu(
  isCheckedNgaySaoNguHu,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoNguHu ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_TYJ) ||
    (isMuaXuan && chi === CHI_DAU) ||
    (isMuaXuan && chi === CHI_SUU) ||

    (isMuaHa && chi === CHI_THAN) ||
    (isMuaHa && chi === CHI_TYS) ||
    (isMuaHa && chi === CHI_THIN) ||

    (isMuaThu && chi === CHI_HOI) ||
    (isMuaThu && chi === CHI_MAO) ||
    (isMuaThu && chi === CHI_MUI) ||

    (isMuaDong && chi === CHI_DAN) ||
    (isMuaDong && chi === CHI_NGO) ||
    (isMuaDong && chi === CHI_TUAT)
}
// export function getNgaySaoNguHu(isCheckedNgaySaoNguHu, month, chi) {
//   if (!isCheckedNgaySaoNguHu || !month || !chi) {
//     return;
//   }

//   return (month === 1 && chi === CHI_TYJ) ||
//     (month === 2 && chi === CHI_DAU) ||
//     (month === 3 && chi === CHI_SUU) ||

//     (month === 4 && chi === CHI_THAN) ||
//     (month === 5 && chi === CHI_TYS) ||
//     (month === 6 && chi === CHI_THIN) ||

//     (month === 7 && chi === CHI_HOI) ||
//     (month === 8 && chi === CHI_MAO) ||
//     (month === 9 && chi === CHI_MUI) ||

//     (month === 10 && chi === CHI_DAN) ||
//     (month === 11 && chi === CHI_NGO) ||
//     (month === 12 && chi === CHI_TUAT);
// }

/*
 * Tìm ngày có Sao Tứ thời đại mộ (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tứ thời đại mộ
 */
export function getNgaySaoTuThoiDaiMo(
  isCheckedNgaySaoTuThoiDaiMo,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  can,
  chi
) {
  if (
    !isCheckedNgaySaoTuThoiDaiMo ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !can ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && can === CAN_AT && chi === CHI_MUI) ||
    (isMuaHa && can === CAN_BINH && chi === CHI_TUAT) ||
    (isMuaThu && can === CAN_TAN && chi === CHI_SUU) ||
    (isMuaDong && can === CAN_NHAM && chi === CHI_THIN)
}

/*
 * Tìm ngày có Sao Thổ cấm (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Thổ cấm
 */
export function getNgaySaoThoCam(
  isCheckedNgaySaoThoCam,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoThoCam ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_HOI) ||
    (isMuaHa && chi === CHI_DAN) ||
    (isMuaThu && chi === CHI_TYJ) ||
    (isMuaDong && chi === CHI_THAN)
}

/*
 * Tìm ngày có Sao Ly sàng (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Ly sàng
 */
export function getNgaySaoLySang(
  isCheckedNgaySaoLySang,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoLySang ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_DAU) ||
    (isMuaHa && (chi === CHI_DAN || chi === CHI_NGO)) ||
    (isMuaThu && chi === CHI_TUAT) ||
    (isMuaDong && chi === CHI_TYJ)
}

/*
 * Tìm ngày có Sao Tứ thời cô quả (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Tứ thời cô quả
 */
export function getNgaySaoTuThoiCoQua(
  isCheckedNgaySaoTuThoiCoQua,
  isMuaXuan,
  isMuaHa,
  isMuaThu,
  isMuaDong,
  chi
) {
  if (
    !isCheckedNgaySaoTuThoiCoQua ||
    typeof isMuaXuan !== 'boolean' ||
    typeof isMuaHa !== 'boolean' ||
    typeof isMuaThu !== 'boolean' ||
    typeof isMuaDong !== 'boolean' ||
    !chi
  ) {
    return
  }

  return (isMuaXuan && chi === CHI_SUU) ||
    (isMuaHa && chi === CHI_THIN) ||
    (isMuaThu && chi === CHI_MUI) ||
    (isMuaDong && chi === CHI_TUAT)
}

/*
 * Tìm ngày có Sao Âm thác (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Âm thác
 */
export function getNgaySaoAmThac(isCheckedNgaySaoAmThac, month, can, chi) {
  if (!isCheckedNgaySaoAmThac || !month || !can || !chi) {
    return
  }

  return (month === 1 && can === CAN_CANH && chi === CHI_TUAT) ||
    (month === 2 && can === CAN_TAN && chi === CHI_DAU) ||
    (month === 3 && can === CAN_CANH && chi === CHI_THAN) ||
    (month === 4 && can === CAN_DINH && chi === CHI_MUI) ||
    (month === 5 && can === CAN_BINH && chi === CHI_NGO) ||
    (month === 6 && can === CAN_DINH && chi === CHI_TYJ) ||
    (month === 7 && can === CAN_GIAP && chi === CHI_THIN) ||
    (month === 8 && can === CAN_AT && chi === CHI_MAO) ||
    (month === 9 && can === CAN_GIAP && chi === CHI_DAN) ||
    (month === 10 && can === CAN_QUY && chi === CHI_SUU) ||
    (month === 11 && can === CAN_NHAM && chi === CHI_TYS) ||
    (month === 12 && can === CAN_QUY && chi === CHI_HOI)
}

/*
 * Tìm ngày có Sao Dương thác (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Dương thác
 */
export function getNgaySaoDuongThac(isCheckedNgaySaoDuongThac, month, can, chi) {
  if (!isCheckedNgaySaoDuongThac || !month || !can || !chi) {
    return
  }

  return (month === 1 && can === CAN_GIAP && chi === CHI_DAN) ||
    (month === 2 && can === CAN_AT && chi === CHI_MAO) ||
    (month === 3 && can === CAN_GIAP && chi === CHI_THIN) ||
    (month === 4 && can === CAN_DINH && chi === CHI_TYJ) ||
    (month === 5 && can === CAN_BINH && chi === CHI_NGO) ||
    (month === 6 && can === CAN_DINH && chi === CHI_MUI) ||
    (month === 7 && can === CAN_CANH && chi === CHI_THAN) ||
    (month === 8 && can === CAN_TAN && chi === CHI_DAU) ||
    (month === 9 && can === CAN_CANH && chi === CHI_TUAT) ||
    (month === 10 && can === CAN_QUY && chi === CHI_HOI) ||
    (month === 11 && can === CAN_NHAM && chi === CHI_TYS) ||
    (month === 12 && can === CAN_QUY && chi === CHI_SUU)
}

/*
 * Tìm ngày có Sao Quỷ khốc (Sao xấu)
 * Theo Sách Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009)
 *
 * @return {Boolean} True nếu là ngày có Sao Quỷ khốc
 */
export function getNgaySaoQuyKhoc(isCheckedNgaySaoQuyKhoc, chi) {
  if (!isCheckedNgaySaoQuyKhoc || !chi) {
    return
  }

  return chi === CHI_TUAT
}



/*
 * Tìm giờ Sát chủ (Giờ xấu, kỵ làm nhà cửa, cưới gả)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Number} Id của giờ Sát chủ
 */
export function getGioSatchu(isCheckGioSatchu, month) {
  if (!isCheckGioSatchu || !month) {
    return
  }

  switch (month) {
    case 1:
    case 7:
      return GIO_DAN

    case 2:
    case 8:
      return GIO_TYJ

    case 3:
    case 9:
      return GIO_THAN

    case 4:
    case 10:
      return GIO_THIN

    case 5:
    case 11:
      return GIO_DAU

    case 6:
    case 12:
      return GIO_MAO

    default:
      return null
  }
}

/*
 * Tìm giờ Thọ tử (Giờ xấu, kỵ làm nhà cửa, cưới gả)
 * Theo Sách Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát
 *
 * @return {Number} Id của giờ Thọ tử
 */
export function getGioThoTu(isCheckGioThoTu, chi) {
  if (!isCheckGioThoTu || !chi) {
    return
  }

  switch (chi) {
    case 1:
    case 7:
      return GIO_DAN

    case 2:
    case 8:
      return GIO_TYJ

    case 3:
    case 9:
      return GIO_THAN

    case 4:
    case 10:
      return GIO_THIN

    case 5:
    case 11:
      return GIO_DAU

    case 6:
    case 12:
      return GIO_MAO

    default:
      return null
  }
}

/*
 * Tìm giờ Không vong (Giờ xấu, kỵ làm nhà cửa, cưới gả)
 * Theo Sách Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan
 *
 * @return {Array} Mảng Id của giờ Không vong
 */
export function getGioKhongVong(isCheckGioKhongVong, can, chi) {
  if (!isCheckGioKhongVong || !can || !chi) {
    return
  }

  switch (can) {
    case CAN_GIAP:
    case CAN_KY:
      return [GIO_THAN, GIO_DAU]

    case CAN_AT:
    case CAN_CANH:
      return [GIO_NGO, GIO_MUI]

    case CAN_BINH:
    case CAN_TAN:
      return [GIO_THIN, GIO_TYJ]

    case CAN_DINH:
    case CAN_NHAM:
      return [GIO_DAN, GIO_MAO]

    case CAN_QUY:
    case CHI_DAU:
      return [GIO_TYS, GIO_SUU]

    default:
      return []
  }
}

export default {
  // Tốt
  getNgayBatTuong,
  getNgaySaoSatCong,
  getNgaySaoTrucTinh,
  getNgaySaoNhanDuyen,
  getNgaySaoThienAn,
  getNgaySaoThienThuy,
  getNgaySaoNguHop,
  getNgaySaoThienDuc,
  getNgaySaoThienDucHop,
  getNgaySaoNguyetDuc,
  getNgaySaoNguyetDucHop,
  getNgaySaoThienHy,
  getNgaySaoThienPhu,
  getNgaySaoThienQuy,
  getNgaySaoThienXa,
  getNgaySaoSinhKhi,
  getNgaySaoThienPhuc,
  getNgaySaoThienThanh,
  getNgaySaoThienQuan,
  getNgaySaoThienMa,
  getNgaySaoThienTai,
  getNgaySaoDiaTai,
  getNgaySaoNguyetTai,
  getNgaySaoNguyetAn,
  getNgaySaoNguyetKhong,
  getNgaySaoMinhTinh,
  getNgaySaoThanhTam,
  getNgaySaoNguPhu,
  getNgaySaoLocKho,
  getNgaySaoPhucSinh,
  getNgaySaoCatKhanh,
  getNgaySaoAmDuc,
  getNgaySaoUViTinh,
  getNgaySaoManDucTinh,
  getNgaySaoKinhTam,
  getNgaySaoTueHop,
  getNgaySaoNguyetGiai,
  getNgaySaoQuanNhat,
  getNgaySaoHoatDieu,
  getNgaySaoGiaiThan,
  getNgaySaoPhoBo,
  getNgaySaoIchHau,
  getNgaySaoTucThe,
  getNgaySaoYeuYen,
  getNgaySaoDichMa,
  getNgaySaoTamHop,
  getNgaySaoLucHop,
  getNgaySaoMauThuong,
  getNgaySaoPhucHau,
  getNgaySaoDaiHongSa,
  getNgaySaoDanNhatThoiDuc,
  getNgaySaoHoangAn,
  getNgaySaoThanhLong,
  getNgaySaoMinhDuong,
  getNgaySaoKimDuong,
  getNgaySaoNgocDuong,

  // Xấu
  getNgaySatChu,
  getNgayThangSatChu,
  getNgaySatChuTrong4Mua,
  getNgayBonMuaSatChu,
  getNgayThoTu,
  getNgayLySao,
  getNgayVangVong,
  getNgayXichTongTu,
  getNgayKhongVong,
  getNgayHungBai,
  getNgayXichKhau,
  getNgayHoangOc4Mua,
  getNgayGiaOc4Mua,
  getNgayHoaTinh,
  getNgayThienHoa,
  getNgayDiaHoa,
  getNgayDocHoa,
  getNgaySaoHoa,
  getNgayThienTaiDaiHoa,
  getNgayLoiGiang,
  getNgayLoiDinhChinhSat,
  getNgayLoiDinhSatChu,
  getNgaySatSu,
  getNgayThangSatSu,
  getNgayNguyetKy,
  getNgayThienMaTamCuong,
  getNgayTamNuongSat,
  getNgayBatTuongXau,
  getNgayNguyetTan,
  getNgayNguuLangChucNu4Mua,
  getNgayKhongSang4Mua,
  getNgayKhongPhong4Mua,
  getNgayTuLaDoatGia,
  getNgayThapAcDaiBai,
  getNgayNhapMo4Mua,
  getNgayThanHao,
  getNgayQuyKhoc,
  getNgayThienMonBeTac,
  getNgayTuLyOrTuTuyet,
  getNgaySaoCuuThoQuy,
  getNgaySaoThienCuong,
  getNgaySaoThienLai,
  getNgaySaoTieuHongSa,
  getNgaySaoDaiHao,
  getNgaySaoTieuHao,
  getNgaySaoNguyetPha,
  getNgaySaoKiepSat,
  getNgaySaoDiaPha,
  getNgaySaoThoPhu,
  getNgaySaoThoOn,
  getNgaySaoThienOn,
  getNgaySaoThuTu,
  getNgaySaoHoangVu,
  getNgaySaoThienTac,
  getNgaySaoDiaTac,
  getNgaySaoHoaTai,
  getNgaySaoNguyetYem,
  getNgaySaoNguyetHu,
  getNgaySaoHoangSa,
  getNgaySaoLucBatThanh,
  getNgaySaoNhanCach,
  getNgaySaoThanCach,
  getNgaySaoPhiMaSat,
  getNgaySaoNguQuy,
  getNgaySaoBangTieuNgoaHam,
  getNgaySaoHaKhoiCauGiao,
  getNgaySaoCuuKhong,
  getNgaySaoTrungTang,
  getNgaySaoTrungPhuc,
  getNgaySaoChuTuocHacDao,
  getNgaySaoBachHo,
  getNgaySaoHuyenVu,
  getNgaySaoCauTran,
  getNgaySaoLoiCong,
  getNgaySaoCoThan,
  getNgaySaoQuaTu,
  getNgaySaoNguyetHinh,
  getNgaySaoToiChi,
  getNgaySaoNguyetKienChuyenSat,
  getNgaySaoThienDiaChinhChuyen,
  getNgaySaoThienDiaChuyenSat,
  getNgaySaoLoBanSat,
  getNgaySaoPhuDauSat,
  getNgaySaoTamTang,
  getNgaySaoNguHu,
  getNgaySaoTuThoiDaiMo,
  getNgaySaoThoCam,
  getNgaySaoLySang,
  getNgaySaoTuThoiCoQua,
  getNgaySaoAmThac,
  getNgaySaoDuongThac,
  getNgaySaoQuyKhoc

  // xxx
}
