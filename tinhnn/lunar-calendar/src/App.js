import React from 'react'
import DefaultLayout from './layouts/default'
// import 'antd/dist/antd.css';
import './assets/scss/styles.scss'

export default function App() {
  return (
    <DefaultLayout />
  )
}
