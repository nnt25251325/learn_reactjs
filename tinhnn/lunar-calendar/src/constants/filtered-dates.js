import {
  NGAY_BAT_TUONG,
  NGAY_SAO_SAT_CONG,
  NGAY_SAO_TRUC_TINH,
  NGAY_SAO_NHAN_DUYEN,
  NGAY_SAO_THIEN_AN,
  NGAY_SAO_THIEN_THUY,
  NGAY_SAO_NGU_HOP,
  NGAY_SAO_THIEN_DUC,
  NGAY_SAO_THIEN_DUC_HOP,
  NGAY_SAO_NGUYET_DUC,
  NGAY_SAO_NGUYET_DUC_HOP,
  NGAY_SAO_THIEN_HY,
  NGAY_SAO_THIEN_PHU,
  NGAY_SAO_THIEN_QUY,
  NGAY_SAO_THIEN_XA,
  NGAY_SAO_SINH_KHI,
  NGAY_SAO_THIEN_PHUC,
  NGAY_SAO_THIEN_THANH,
  NGAY_SAO_THIEN_QUAN,
  NGAY_SAO_THIEN_MA,
  NGAY_SAO_THIEN_TAI,
  NGAY_SAO_DIA_TAI,
  NGAY_SAO_NGUYET_TAI,
  NGAY_SAO_NGUYET_AN,
  NGAY_SAO_NGUYET_KHONG,
  NGAY_SAO_MINH_TINH,
  NGAY_SAO_THANH_TAM,
  NGAY_SAO_NGU_PHU,
  NGAY_SAO_LOC_KHO,
  NGAY_SAO_PHUC_SINH,
  NGAY_SAO_CAT_KHANH,
  NGAY_SAO_AM_DUC,
  NGAY_SAO_U_VI_TINH,
  NGAY_SAO_MAN_DUC_TINH,
  NGAY_SAO_KINH_TAM,
  NGAY_SAO_TUE_HOP,
  NGAY_SAO_NGUYET_GIAI,
  NGAY_SAO_QUAN_NHAT,
  NGAY_SAO_HOAT_DIEU,
  NGAY_SAO_GIAI_THAN,
  NGAY_SAO_PHO_BO,
  NGAY_SAO_ICH_HAU,
  NGAY_SAO_TUC_THE,
  NGAY_SAO_YEU_YEN,
  NGAY_SAO_DICH_MA,
  NGAY_SAO_TAM_HOP,
  NGAY_SAO_LUC_HOP,
  NGAY_SAO_MAU_THUONG,
  NGAY_SAO_PHUC_HAU,
  NGAY_SAO_DAI_HONG_SA,
  NGAY_SAO_DAN_NHAT_THOI_DUC,
  NGAY_SAO_HOANG_AN,
  NGAY_SAO_THANH_LONG,
  NGAY_SAO_MINH_DUONG,
  NGAY_SAO_KIM_DUONG,
  NGAY_SAO_NGOC_DUONG,
  NGAY_SAT_CHU,
  NGAY_THANG_SAT_CHU,
  NGAY_SAT_CHU_TRONG_4_MUA,
  NGAY_BON_MUA_SAT_CHU,
  NGAY_THO_TU,
  NGAY_LY_SAO,
  NGAY_VANG_VONG,
  NGAY_XICH_TONG_TU,
  NGAY_KHONG_VONG,
  NGAY_HUNG_BAI,
  NGAY_XICH_KHAU,
  NGAY_HOANG_OC_4_MUA,
  NGAY_GIA_OC_4_MUA,
  NGAY_HOA_TINH,
  NGAY_THIEN_HOA,
  NGAY_DIA_HOA,
  NGAY_DOC_HOA,
  NGAY_THIEN_TAI_DAI_HOA,
  NGAY_SAO_HOA,
  NGAY_LOI_GIANG,
  NGAY_LOI_DINH_CHINH_SAT,
  NGAY_LOI_DINH_SAT_CHU,
  NGAY_SAT_SU,
  NGAY_THANG_SAT_SU,
  NGAY_NGUYET_KY,
  NGAY_THIEN_MA_TAM_CUONG,
  NGAY_TAM_NUONG_SAT,
  NGAY_BAT_TUONG_XAU,
  NGAY_NGUYET_TAN,
  NGAY_NGUU_LANG_CHUC_NU_4_MUA,
  NGAY_KHONG_SANG4_MUA,
  NGAY_KHONG_PHONG4_MUA,
  NGAY_TU_LA_DOAT_GIA,
  NGAY_THAP_AC_DAI_BAI,
  NGAY_NHAP_MO4_MUA,
  NGAY_THAN_HAO,
  NGAY_QUY_KHOC,
  NGAY_THIEN_MON_BE_TAC,
  NGAY_TU_LY,
  NGAY_TU_TUYET,
  NGAY_SAO_CUU_THO_QUY,
  NGAY_SAO_THIEN_CUONG,
  NGAY_SAO_THIEN_LAI,
  NGAY_SAO_TIEU_HONG_SA,
  NGAY_SAO_DAI_HAO,
  NGAY_SAO_TIEU_HAO,
  NGAY_SAO_NGUYET_PHA,
  NGAY_SAO_KIEP_SAT,
  NGAY_SAO_DIA_PHA,
  NGAY_SAO_THO_PHU,
  NGAY_SAO_THO_ON,
  NGAY_SAO_THIEN_ON,
  NGAY_SAO_THU_TU,
  NGAY_SAO_HOANG_VU,
  NGAY_SAO_THIEN_TAC,
  NGAY_SAO_DIA_TAC,
  NGAY_SAO_HOA_TAI,
  NGAY_SAO_NGUYET_YEM,
  NGAY_SAO_NGUYET_HU,
  NGAY_SAO_HOANG_SA,
  NGAY_SAO_LUC_BAT_THANH,
  NGAY_SAO_NHAN_CACH,
  NGAY_SAO_THAN_CACH,
  NGAY_SAO_PHI_MA_SAT,
  NGAY_SAO_NGU_QUY,
  NGAY_SAO_BANG_TIEU_NGOA_HAM,
  NGAY_SAO_HA_KHOI_CAU_GIAO,
  NGAY_SAO_CUU_KHONG,
  NGAY_SAO_TRUNG_TANG,
  NGAY_SAO_TRUNG_PHUC,
  NGAY_SAO_CHU_TUOC_HAC_DAO,
  NGAY_SAO_BACH_HO,
  NGAY_SAO_HUYEN_VU,
  NGAY_SAO_CAU_TRAN,
  NGAY_SAO_LOI_CONG,
  NGAY_SAO_CO_THAN,
  NGAY_SAO_QUA_TU,
  NGAY_SAO_NGUYET_HINH,
  NGAY_SAO_TOI_CHI,
  NGAY_SAO_NGUYET_KIEN_CHUYEN_SAT,
  NGAY_SAO_THIEN_DIA_CHINH_CHUYEN,
  NGAY_SAO_THIEN_DIA_CHUYEN_SAT,
  NGAY_SAO_LO_BAN_SAT,
  NGAY_SAO_PHU_DAU_SAT,
  NGAY_SAO_TAM_TANG,
  NGAY_SAO_NGU_HU,
  NGAY_SAO_TU_THOI_DAI_MO,
  NGAY_SAO_THO_CAM,
  NGAY_SAO_LY_SANG,
  NGAY_SAO_TU_THOI_CO_QUA,
  NGAY_SAO_AM_THAC,
  NGAY_SAO_DUONG_THAC,
  NGAY_SAO_QUY_KHOC

  // LIKE_MONTH_LIKE_DATE,
  // LIKE_SEASON_LIKE_DATE
} from './index'

/**
 * Danh sách loại ngày trong bộ lọc
 */
export const FILTERED_DATE_LIST = [
  // --------------------------------------------------------------------------------------- //
  // Tốt ----------------------------------------------------------------------------------- //
  // --------------------------------------------------------------------------------------- //
  {
    name: 'Ngày Bất tương (tốt)',
    code: NGAY_BAT_TUONG,
    checkboxValue: 'isCheckedNgayBatTuong',
    labelName: 'Ngày Bất tương',
    labelClass: 'ngay-bat-tuong',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: 'Tháng 1',
        values: 'Bính Dần, Đinh Mão, Bính Tý, Kỷ Mão, Mậu Tý, Canh Dần, Tân Mão'
      },
      {
        name: 'Tháng 2',
        values: 'Ất Sửu, Bính Dần, Đinh Sửu, Bính Tuất, Mậu Dần, Kỷ Sửu, Mậu Tuất, Canh Tuất'
      },
      {
        name: 'Tháng 3',
        values: 'Ất Sửu, Đinh Sửu, Kỷ Tỵ, Đinh Dậu'
      },
      {
        name: 'Tháng 4',
        values: 'Giáp Tý, Giáp Tuất, Bính Tý, Ất Dậu, Bính Tuất, Mậu Tý, Bính Thân, Đinh Dậu, Mậu Tuất'
      },
      {
        name: 'Tháng 5',
        values: 'Quý Dậu, Giáp Tuất, Giáp Thân, Ất Dậu, Bính Tuất, Ất Mùi, Bính Thân, Mậu Thân, Kỷ Mùi'
      },
      {
        name: 'Tháng 6',
        values: 'Nhâm Thân, Quý Dậu, Giáp Tuất, Nhâm Ngọ, Quý Mùi, GiápThân, Ất Dậu, Giáp Ngọ, Ất Mùi, Nhâm Tuất'
      },
      {
        name: 'Tháng 7',
        values: 'Nhâm Thân, Quý Dậu, Nhâm Tuất, Quý Mùi, Giáp Thân, Ất Dậu, Giáp Tuất, Ất Mùi, Quý Tỵ, Ất Tỵ, Kỷ Mùi'
      },
      {
        name: 'Tháng 8',
        values: 'Mậu Thìn, Tân Mùi, Giáp Thân, Nhâm Thìn, Quý Tỵ, Giáp Ngọ, Giáp Thìn, Mậu Ngọ, Tân Tỵ, Nhâm Ngọ, Quý Mùi'
      },
      {
        name: 'Tháng 9',
        values: 'Kỷ Tỵ, Canh Ngọ, Tân Mùi, Ất Mão, Tân Tỵ, Nhâm Ngọ, Quý Mùi, Quý Tỵ, Quý Mão, Mậu Ngọ, Kỷ Mùi.'
      },
      {
        name: 'Tháng 10',
        values: 'Mậu Thìn, Canh Ngọ, Canh Thìn, Nhâm Ngọ, Canh Dần, Tân Mão, Nhâm Thìn, Nhâm Dần, Quý Mão, Mậu Ngọ'
      },
      {
        name: 'Tháng 11',
        values: 'Đinh Mão, Mậu Thìn, Kỷ Tỵ, Đinh Sửu, Kỷ Mão, Canh Thìn, Tân Tỵ, Nhâm Thìn, Tân Sửu, Đinh Tỵ'
      },
      {
        name: 'Tháng 12',
        values: 'Bính Dần, Đinh Mão, Mậu Thìn, Đinh Sửu, Mậu Dần, Kỷ Mão, Canh Thìn, Ất Sửu, Canh Dần, Tân Mão, Tân Sửu, Bính Thìn'
      }
    ],
    referenceBooks: [
      'Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát (Trang 116)'
    ]
  },
  {
    name: 'Ngày có sao Sát cống (tốt)',
    code: NGAY_SAO_SAT_CONG,
    checkboxValue: 'isCheckedNgaySaoSatCong',
    labelName: 'Sao Sát cống',
    labelClass: 'ngay-sao-sat-cong',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: 'Tháng 1, 4, 7, 10',
        values: 'Đinh Mão, Bính Tý, Ất Dậu, Giáp Ngọ, Quý Mão, Nhâm Tý, Tân Dậu' // Khác với Vạn sự bất cần nhân, ở đinh dậu
      },
      {
        name: 'Tháng 2, 5, 8, 11',
        values: 'Bính Dần, Ất Hợi, Giáp Thân, Quý Tỵ, Nhâm Dần, Tân Hợi, Canh Thân'
      },
      {
        name: 'Tháng 3, 6, 9, 12',
        values: 'Ất Sửu, Giáp Tuất, Quý Mùi, Nhâm Thìn, Tân Sửu, Canh Tuất, Kỷ Mùi' // Khác với kim oanh ký, ở Kỷ sửu (ất sửu)
      }
    ],
    referenceBooks: [
      'Coi tuổi làm nhà và dựng vợ gả chồng - Chiêm gia tinh Huỳnh Liên (Trang 59)',
      'Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009) (Trang 69)',
      'Kim Oanh Ký - Thái Kim Oanh (Trang 291)',
      'Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 45)'
    ]
  },
  {
    name: 'Ngày có sao Trực tinh (tốt)',
    code: NGAY_SAO_TRUC_TINH,
    checkboxValue: 'isCheckedNgaySaoTrucTinh',
    labelName: 'Sao Trực tinh',
    labelClass: 'ngay-sao-truc-tinh',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: 'Tháng 1, 4, 7, 10',
        values: 'Mậu Thìn, Đinh Sửu, Bính Tuất, Ất Mùi, Giáp Thìn, Quý Sửu, Nhâm Tuất'
      },
      {
        name: 'Tháng 2, 5, 8, 11',
        values: 'Đinh Mão, Bính Tý, Ất Dậu, Giáp Ngọ, Quý Mão, Nhâm Tý, Tân Dậu' // Khác với Vạn sự bất cần nhân, quý sửu, thêm mậu dần, ko có nhâm tý
      },
      {
        name: 'Tháng 3, 6, 9, 12',
        values: 'Bính Dần, Ất Hợi, Giáp Thân, Quý Ty, Nhâm Dần, Tân Mão, Canh Thân'
      }
    ],
    referenceBooks: [
      'Coi tuổi làm nhà và dựng vợ gả chồng - Chiêm gia tinh Huỳnh Liên (Trang 59)',
      'Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009) (Trang 69)',
      'Kim Oanh Ký - Thái Kim Oanh (Trang 291)',
      'Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 45)'
    ]
  },
  {
    name: 'Ngày có sao Nhân duyên (tốt)',
    code: NGAY_SAO_NHAN_DUYEN,
    checkboxValue: 'isCheckedNgaySaoNhanDuyen',
    labelName: 'Sao Nhân duyên',
    labelClass: 'ngay-sao-nhan-duyen',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: 'Tháng 1, 4, 7, 10',
        values: 'Tân Mùi, Canh Thìn, Kỷ Sửu, Mậu Tuất, Đinh Mùi, Bính Thìn'
      },
      {
        name: 'Tháng 2, 5, 8, 11',
        values: 'Canh Ngọ, Kỷ Mão, Nhâm Tý, Đinh Dậu, Bính Ngọ, Ất Mão' // Khác với Vạn sự bất cần nhân, ở mậu tý
      },
      {
        name: 'Tháng 3, 6, 9, 12',
        values: 'Kỷ Tỵ, Mậu Dần, Kỷ Hợi, Bính Thân, Ất Tỵ, Giáp Dần, Quý Hợi' // Khác với Vạn sự bất cần nhân, ở đinh hợi
      }
    ],
    referenceBooks: [
      'Coi tuổi làm nhà và dựng vợ gả chồng - Chiêm gia tinh Huỳnh Liên (Trang 59)',
      'Bàn về lịch vạn niên - Tân Việt, Thiều Phong (2005 - 2009) (Trang 69)',
      'Kim Oanh Ký - Thái Kim Oanh (Trang 291)',
      'Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 45)'
    ]
  },
  {
    name: 'Ngày có sao Thiên ân (tốt)',
    code: NGAY_SAO_THIEN_AN,
    checkboxValue: 'isCheckedNgaySaoThienAn',
    labelName: 'Sao Thiên ân',
    labelClass: 'ngay-sao-thien-an',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên thụy (tốt)',
    code: NGAY_SAO_THIEN_THUY,
    checkboxValue: 'isCheckedNgaySaoThienThuy',
    labelName: 'Sao Thiên thụy',
    labelClass: 'ngay-sao-thien-thuy',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Ngũ hợp (tốt)',
    code: NGAY_SAO_NGU_HOP,
    checkboxValue: 'isCheckedNgaySaoNguHop',
    labelName: 'Sao Ngũ hợp',
    labelClass: 'ngay-sao-ngu-hop',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên đức (tốt)',
    code: NGAY_SAO_THIEN_DUC,
    checkboxValue: 'isCheckedNgaySaoThienDuc',
    labelName: 'Sao Thiên đức',
    labelClass: 'ngay-sao-thien-duc',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên đức hợp (tốt)',
    code: NGAY_SAO_THIEN_DUC_HOP,
    checkboxValue: 'isCheckedNgaySaoThienDucHop',
    labelName: 'Sao Thiên đức hợp',
    labelClass: 'ngay-sao-thien-duc-hop',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Nguyệt đức (tốt)',
    code: NGAY_SAO_NGUYET_DUC,
    checkboxValue: 'isCheckedNgaySaoNguyetDuc',
    labelName: 'Sao Nguyệt đức',
    labelClass: 'ngay-sao-nguyet-duc',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Nguyệt đức hợp (tốt)',
    code: NGAY_SAO_NGUYET_DUC_HOP,
    checkboxValue: 'isCheckedNgaySaoNguyetDucHop',
    labelName: 'Sao Nguyệt đức hợp',
    labelClass: 'ngay-sao-nguyet-duc-hop',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên hỷ (tốt)',
    code: NGAY_SAO_THIEN_HY,
    checkboxValue: 'isCheckedNgaySaoThienHy',
    labelName: 'Sao Thiên hỷ',
    labelClass: 'ngay-sao-thien-hy',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên phú (tốt)',
    code: NGAY_SAO_THIEN_PHU,
    checkboxValue: 'isCheckedNgaySaoThienPhu',
    labelName: 'Sao Thiên phú',
    labelClass: 'ngay-sao-thien-phu',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên quý (tốt)',
    code: NGAY_SAO_THIEN_QUY,
    checkboxValue: 'isCheckedNgaySaoThienQuy',
    labelName: 'Sao Thiên quý',
    labelClass: 'ngay-sao-thien-quy',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên xá (tốt)',
    code: NGAY_SAO_THIEN_XA,
    checkboxValue: 'isCheckedNgaySaoThienXa',
    labelName: 'Sao Thiên xá',
    labelClass: 'ngay-sao-thien-xa',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Sinh khí (tốt)',
    code: NGAY_SAO_SINH_KHI,
    checkboxValue: 'isCheckedNgaySaoSinhKhi',
    labelName: 'Sao Sinh khí',
    labelClass: 'ngay-sao-sinh-khi',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên phúc (tốt)',
    code: NGAY_SAO_THIEN_PHUC,
    checkboxValue: 'isCheckedNgaySaoThienPhuc',
    labelName: 'Sao Thiên phúc',
    labelClass: 'ngay-sao-thien-phuc',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên thành (tốt)',
    code: NGAY_SAO_THIEN_THANH,
    checkboxValue: 'isCheckedNgaySaoThienThanh',
    labelName: 'Sao Thiên thành',
    labelClass: 'ngay-sao-thien-thanh',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên quan (tốt)',
    code: NGAY_SAO_THIEN_QUAN,
    checkboxValue: 'isCheckedNgaySaoThienQuan',
    labelName: 'Sao Thiên quan',
    labelClass: 'ngay-sao-thien-quan',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên mã (tốt)',
    code: NGAY_SAO_THIEN_MA,
    checkboxValue: 'isCheckedNgaySaoThienMa',
    labelName: 'Sao Thiên mã',
    labelClass: 'ngay-sao-thien-ma',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thiên tài (tốt)',
    code: NGAY_SAO_THIEN_TAI,
    checkboxValue: 'isCheckedNgaySaoThienTai',
    labelName: 'Sao Thiên tài',
    labelClass: 'ngay-sao-thien-tai',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Địa tài (tốt)',
    code: NGAY_SAO_DIA_TAI,
    checkboxValue: 'isCheckedNgaySaoDiaTai',
    labelName: 'Sao Địa tài',
    labelClass: 'ngay-sao-dia-tai',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Nguyệt tài (tốt)',
    code: NGAY_SAO_NGUYET_TAI,
    checkboxValue: 'isCheckedNgaySaoNguyetTai',
    labelName: 'Sao Nguyệt tài',
    labelClass: 'ngay-sao-nguyet-tai',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Nguyệt ân (tốt)',
    code: NGAY_SAO_NGUYET_AN,
    checkboxValue: 'isCheckedNgaySaoNguyetAn',
    labelName: 'Sao Nguyệt ân',
    labelClass: 'ngay-sao-nguyet-an',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Nguyệt không (tốt)',
    code: NGAY_SAO_NGUYET_KHONG,
    checkboxValue: 'isCheckedNgaySaoNguyetKhong',
    labelName: 'Sao Nguyệt không',
    labelClass: 'ngay-sao-nguyet-khong',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Minh tinh (tốt)',
    code: NGAY_SAO_MINH_TINH,
    checkboxValue: 'isCheckedNgaySaoMinhTinh',
    labelName: 'Sao Minh tinh',
    labelClass: 'ngay-sao-minh-tinh',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thánh tâm (tốt)',
    code: NGAY_SAO_THANH_TAM,
    checkboxValue: 'isCheckedNgaySaoThanhTam',
    labelName: 'Sao Thánh tâm',
    labelClass: 'ngay-sao-thanh-tam',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Ngũ phú (tốt)',
    code: NGAY_SAO_NGU_PHU,
    checkboxValue: 'isCheckedNgaySaoNguPhu',
    labelName: 'Sao Ngũ phú',
    labelClass: 'ngay-sao-ngu-phu',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Lộc khố (tốt)',
    code: NGAY_SAO_LOC_KHO,
    checkboxValue: 'isCheckedNgaySaoLocKho',
    labelName: 'Sao Lộc khố',
    labelClass: 'ngay-sao-loc-kho',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Phúc sinh (tốt)',
    code: NGAY_SAO_PHUC_SINH,
    checkboxValue: 'isCheckedNgaySaoPhucSinh',
    labelName: 'Sao Phúc sinh',
    labelClass: 'ngay-sao-phuc-sinh',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Cát khánh (tốt)',
    code: NGAY_SAO_CAT_KHANH,
    checkboxValue: 'isCheckedNgaySaoCatKhanh',
    labelName: 'Sao Cát khánh',
    labelClass: 'ngay-sao-cat-khanh',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Âm đức (tốt)',
    code: NGAY_SAO_AM_DUC,
    checkboxValue: 'isCheckedNgaySaoAmDuc',
    labelName: 'Sao Âm đức',
    labelClass: 'ngay-sao-am-duc',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao U vi tinh (tốt)',
    code: NGAY_SAO_U_VI_TINH,
    checkboxValue: 'isCheckedNgaySaoUViTinh',
    labelName: 'Sao U vi tinh',
    labelClass: 'ngay-sao-u-vi-tinh',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Mãn đức tinh (tốt)',
    code: NGAY_SAO_MAN_DUC_TINH,
    checkboxValue: 'isCheckedNgaySaoManDucTinh',
    labelName: 'Sao Mãn đức tinh',
    labelClass: 'ngay-sao-man-duc-tinh',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Kinh tâm (tốt)',
    code: NGAY_SAO_KINH_TAM,
    checkboxValue: 'isCheckedNgaySaoKinhTam',
    labelName: 'Sao Kinh tâm',
    labelClass: 'ngay-sao-kinh-tam',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Tuế hợp (tốt)',
    code: NGAY_SAO_TUE_HOP,
    checkboxValue: 'isCheckedNgaySaoTueHop',
    labelName: 'Sao Tuế hợp',
    labelClass: 'ngay-sao-tue-hop',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Nguyệt giải (tốt)',
    code: NGAY_SAO_NGUYET_GIAI,
    checkboxValue: 'isCheckedNgaySaoNguyetGiai',
    labelName: 'Sao Nguyệt giải',
    labelClass: 'ngay-sao-nguyet-giai',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Quan nhật (tốt)',
    code: NGAY_SAO_QUAN_NHAT,
    checkboxValue: 'isCheckedNgaySaoQuanNhat',
    labelName: 'Sao Quan nhật',
    labelClass: 'ngay-sao-quan-nhat',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Hoạt điệu (tốt)',
    code: NGAY_SAO_HOAT_DIEU,
    checkboxValue: 'isCheckedNgaySaoHoatDieu',
    labelName: 'Sao Hoạt điệu',
    labelClass: 'ngay-sao-hoat-dieu',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Giải thần (tốt)',
    code: NGAY_SAO_GIAI_THAN,
    checkboxValue: 'isCheckedNgaySaoGiaiThan',
    labelName: 'Sao Giải thần',
    labelClass: 'ngay-sao-giai-than',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Phổ hộ (tốt)',
    code: NGAY_SAO_PHO_BO,
    checkboxValue: 'isCheckedNgaySaoPhoBo',
    labelName: 'Sao Phổ hộ',
    labelClass: 'ngay-sao-pho-bo',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Ích hậu (tốt)',
    code: NGAY_SAO_ICH_HAU,
    checkboxValue: 'isCheckedNgaySaoIchHau',
    labelName: 'Sao Ích hậu',
    labelClass: 'ngay-sao-ich-hau',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Tục thể (tốt)',
    code: NGAY_SAO_TUC_THE,
    checkboxValue: 'isCheckedNgaySaoTucThe',
    labelName: 'Sao Tục thể',
    labelClass: 'ngay-sao-tuc-the',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Yếu yên (tốt)',
    code: NGAY_SAO_YEU_YEN,
    checkboxValue: 'isCheckedNgaySaoYeuYen',
    labelName: 'Sao Yếu yên',
    labelClass: 'ngay-sao-yeu-yen',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Dịch mã (tốt)',
    code: NGAY_SAO_DICH_MA,
    checkboxValue: 'isCheckedNgaySaoDichMa',
    labelName: 'Sao Dịch mã',
    labelClass: 'ngay-sao-dich-ma',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Tam hợp (tốt)',
    code: NGAY_SAO_TAM_HOP,
    checkboxValue: 'isCheckedNgaySaoTamHop',
    labelName: 'Sao Tam hợp',
    labelClass: 'ngay-sao-tam-hop',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Lục hợp (tốt)',
    code: NGAY_SAO_LUC_HOP,
    checkboxValue: 'isCheckedNgaySaoLucHop',
    labelName: 'Sao Lục hợp',
    labelClass: 'ngay-sao-luc-hop',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Mẫu thương (tốt)',
    code: NGAY_SAO_MAU_THUONG,
    checkboxValue: 'isCheckedNgaySaoMauThuong',
    labelName: 'Sao Mẫu thương',
    labelClass: 'ngay-sao-mau-thuong',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Phúc hậu (tốt)',
    code: NGAY_SAO_PHUC_HAU,
    checkboxValue: 'isCheckedNgaySaoPhucHau',
    labelName: 'Sao Phúc hậu',
    labelClass: 'ngay-sao-phuc-hau',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Đại hồng sa (tốt)',
    code: NGAY_SAO_DAI_HONG_SA,
    checkboxValue: 'isCheckedNgaySaoDaiHongSa',
    labelName: 'Sao Đại hồng sa',
    labelClass: 'ngay-sao-dai-hong-sa',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Dân nhật - thời đức (tốt)',
    code: NGAY_SAO_DAN_NHAT_THOI_DUC,
    checkboxValue: 'isCheckedNgaySaoDanNhatThoiDuc',
    labelName: 'Sao Dân nhật - thời đức',
    labelClass: 'ngay-sao-dan-nhat-thoi-duc',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Hoàng ân (tốt)',
    code: NGAY_SAO_HOANG_AN,
    checkboxValue: 'isCheckedNgaySaoHoangAn',
    labelName: 'Sao Hoàng ân',
    labelClass: 'ngay-sao-hoang-an',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Thanh long (tốt)',
    code: NGAY_SAO_THANH_LONG,
    checkboxValue: 'isCheckedNgaySaoThanhLong',
    labelName: 'Sao Thanh long',
    labelClass: 'ngay-sao-thanh-long',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Minh đường (tốt)',
    code: NGAY_SAO_MINH_DUONG,
    checkboxValue: 'isCheckedNgaySaoMinhDuong',
    labelName: 'Sao Minh đường',
    labelClass: 'ngay-sao-minh-duong',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Kim đường (tốt)',
    code: NGAY_SAO_KIM_DUONG,
    checkboxValue: 'isCheckedNgaySaoKimDuong',
    labelName: 'Sao Kim đường',
    labelClass: 'ngay-sao-kim-duong',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có sao Ngọc đường (tốt)',
    code: NGAY_SAO_NGOC_DUONG,
    checkboxValue: 'isCheckedNgaySaoNgocDuong',
    labelName: 'Sao Ngọc đường',
    labelClass: 'ngay-sao-ngoc-duong',
    goodDayStatus: true,
    viewType: 1,
    textBridge: 'Ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },

  // --------------------------------------------------------------------------------------- //
  // Xấu ----------------------------------------------------------------------------------- //
  // --------------------------------------------------------------------------------------- //
  {
    name: 'Ngày Sát chủ (xấu)',
    code: NGAY_SAT_CHU,
    checkboxValue: 'isCheckedNgaySatChu',
    labelName: 'Ngày Sát chủ',
    labelClass: 'ngay-sat-chu',
    goodDayStatus: false,
    viewType: 2,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: 'Tháng 1',
        values: 'Tỵ'
      },
      {
        name: 'Tháng 2',
        values: 'Tý'
      },
      {
        name: 'Tháng 3',
        values: 'Mùi'
      },
      {
        name: 'Tháng 4',
        values: 'Mão'
      },
      {
        name: 'Tháng 5',
        values: 'Thân'
      },
      {
        name: 'Tháng 6',
        values: 'Tuất'
      },
      {
        name: 'Tháng 7',
        values: 'Hợi'
      },
      {
        name: 'Tháng 8',
        values: 'Sửu'
      },
      {
        name: 'Tháng 9',
        values: 'Ngọ'
      },
      {
        name: 'Tháng 10',
        values: 'Dậu'
      },
      {
        name: 'Tháng 11',
        values: 'Dần'
      },
      {
        name: 'Tháng 12',
        values: 'Thìn'
      }
    ],
    referenceBooks: [
      'Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát (Trang 128)'
    ]
  },
  {
    name: 'Ngày Tháng sát chủ (xấu)',
    code: NGAY_THANG_SAT_CHU,
    checkboxValue: 'isCheckedNgayThangSatChu',
    labelName: 'Ngày Tháng sát chủ',
    labelClass: 'ngay-thang-sat-chu',
    goodDayStatus: false,
    viewType: 2,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: 'Tháng 1',
        values: 'Tý'
      },
      {
        name: 'Tháng 2, 3, 7, 9',
        values: 'Sửu'
      },
      {
        name: 'Tháng 4',
        values: 'Tuất'
      },
      {
        name: 'Tháng 11',
        values: 'Mùi'
      },
      {
        name: 'Tháng 5, 6, 8, 10, 12',
        values: 'Thìn'
      }
    ],
    referenceBooks: [
      'Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát (Trang 128)'
    ]
  },
  {
    name: 'Ngày Sát chủ trong 4 mùa (xấu)',
    code: NGAY_SAT_CHU_TRONG_4_MUA,
    checkboxValue: 'isCheckedNgaySatChuTrong4Mua',
    labelName: 'Ngày Sát chủ trong 4 mùa',
    labelClass: 'ngay-sat-chu-trong-4-mua',
    goodDayStatus: false,
    viewType: 2,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: 'Mùa Xuân',
        values: 'Ngọ'
      },
      {
        name: 'Mùa Hạ',
        values: 'Tý'
      },
      {
        name: 'Mùa Thu',
        values: 'Mùi'
      },
      {
        name: 'Mùa Đông',
        values: 'Mão'
      }
    ],
    referenceBooks: [
      'Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát (Trang 130)'
    ]
  },
  {
    name: 'Ngày Bốn mùa sát chủ (xấu)',
    code: NGAY_BON_MUA_SAT_CHU,
    checkboxValue: 'isCheckedNgayBonMuaSatChu',
    labelName: 'Ngày Bốn mùa sát chủ',
    labelClass: 'ngay-bon-mua-sat-chu',
    goodDayStatus: false,
    viewType: 2,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: 'Tháng 1, 5, 9',
        values: 'Tý'
      },
      {
        name: 'Tháng 2, 8, 10',
        values: 'Mão'
      },
      {
        name: 'Tháng 3, 7, 11',
        values: 'Ngọ'
      },
      {
        name: 'Tháng 4, 6, 12',
        values: 'Dậu'
      }
    ],
    referenceBooks: [
      'Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát (Trang 130)'
    ]
  },
  {
    name: 'Ngày Thọ tử (xấu)',
    code: NGAY_THO_TU,
    checkboxValue: 'isCheckedNgayThoTu',
    labelName: 'Ngày Thọ tử',
    labelClass: 'ngay-tho-tu',
    goodDayStatus: false,
    viewType: 2,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: 'Tháng 1',
        values: 'Bính Tuất'
      },
      {
        name: 'Tháng 2',
        values: 'Nhâm Thìn'
      },
      {
        name: 'Tháng 3',
        values: 'Tân Hợi'
      },
      {
        name: 'Tháng 4',
        values: 'Đinh Tỵ'
      },
      {
        name: 'Tháng 5',
        values: 'Mậu Tý'
      },
      {
        name: 'Tháng 6',
        values: 'Bính Ngọ'
      },
      {
        name: 'Tháng 7',
        values: 'Ất Sửu'
      },
      {
        name: 'Tháng 8',
        values: 'Quý Mùi'
      },
      {
        name: 'Tháng 9',
        values: 'Giáp Dần'
      },
      {
        name: 'Tháng 10',
        values: 'Mậu Thân'
      },
      {
        name: 'Tháng 11',
        values: 'Tân Mão'
      },
      {
        name: 'Tháng 12',
        values: 'Tân Dậu'
      }
    ],
    referenceBooks: [
      'Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát (Trang 130)'
    ]
  },
  {
    name: 'Ngày Ly sào (xấu)',
    code: NGAY_LY_SAO,
    checkboxValue: 'isCheckedNgayLySao',
    labelName: 'Ngày Ly sào',
    labelClass: 'ngay-ly-sao',
    goodDayStatus: false,
    viewType: 1,
    textBridge: '',
    list: [
      {
        name: '',
        values: 'Tân Mão, Mậu Thìn, Kỷ Tỵ, Tân Tỵ, Mậu Dần, Mậu Ngọ, Nhâm Ngọ, Mậu Tý, Kỷ Sửu, Tân Sửu, Mậu Tuất, Nhâm Tuất, Quý Tỵ, Tân Tỵ, Kỷ Hợi, Mậu Thân'
      }
    ],
    referenceBooks: [
      'Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 36)',
      'Kim Oanh Ký - Thái Kim Oanh (Trang 294)'
    ]
  },
  {
    name: 'Ngày Vãng vong (xấu)',
    code: NGAY_VANG_VONG,
    checkboxValue: 'isCheckedNgayVangVong',
    labelName: 'Ngày Vãng vong',
    labelClass: 'ngay-vang-vong',
    goodDayStatus: false,
    viewType: 2,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: 'Tháng 1',
        values: 'Dần'
      },
      {
        name: 'Tháng 2',
        values: 'Tỵ'
      },
      {
        name: 'Tháng 3',
        values: 'Thân'
      },
      {
        name: 'Tháng 4',
        values: 'Hợi'
      },
      {
        name: 'Tháng 5',
        values: 'Mão'
      },
      {
        name: 'Tháng 6',
        values: 'Ngọ'
      },
      {
        name: 'Tháng 7',
        values: 'Dậu'
      },
      {
        name: 'Tháng 8',
        values: 'Tý'
      },
      {
        name: 'Tháng 9',
        values: 'Thìn'
      },
      {
        name: 'Tháng 10',
        values: 'Mùi'
      },
      {
        name: 'Tháng 11',
        values: 'Tuất'
      },
      {
        name: 'Tháng 12',
        values: 'Sửu'
      }
    ],
    referenceBooks: [
      'Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát (Trang 132)'
    ]
  },
  {
    name: 'Ngày Xích tòng tử (xấu)',
    code: NGAY_XICH_TONG_TU,
    checkboxValue: 'isCheckedNgayXichTongTu',
    labelName: 'Ngày Xích tòng tử',
    labelClass: 'ngay-xich-tong-tu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
      '(Sưu tầm)'
    ]
  },
  {
    name: 'Ngày Không vong (xấu)',
    code: NGAY_KHONG_VONG,
    checkboxValue: 'isCheckedNgayKhongVong',
    labelName: 'Ngày Không vong',
    labelClass: 'ngay-khong-vong',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Hung bại (xấu)',
    code: NGAY_HUNG_BAI,
    checkboxValue: 'isCheckedNgayHungBai',
    labelName: 'Ngày Hung bại',
    labelClass: 'ngay-hung-bai',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Xích khẩu (xấu)',
    code: NGAY_XICH_KHAU,
    checkboxValue: 'isCheckedNgayXichKhau',
    labelName: 'Ngày Xích khẩu',
    labelClass: 'ngay-xich-khau',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Hoang ốc trong 4 mùa (xấu)',
    code: NGAY_HOANG_OC_4_MUA,
    checkboxValue: 'isCheckedNgayHoangOc4Mua',
    labelName: 'Ngày Hoang ốc',
    labelClass: 'ngay-hoang-oc',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Giá ốc trong 4 mùa (xấu)',
    code: NGAY_GIA_OC_4_MUA,
    checkboxValue: 'isCheckedNgayGiaOc4Mua',
    labelName: 'Ngày Giá ốc',
    labelClass: 'ngay-gia-oc',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Hỏa tinh (xấu)',
    code: NGAY_HOA_TINH,
    checkboxValue: 'isCheckedNgayHoaTinh',
    labelName: 'Ngày Hỏa tinh',
    labelClass: 'ngay-hoa-tinh',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Thiên hỏa (Thiên ngục) (xấu)',
    code: NGAY_THIEN_HOA,
    checkboxValue: 'isCheckedNgayThienHoa',
    labelName: 'Ngày Thiên hỏa (Thiên ngục)',
    labelClass: 'ngay-thien-hoa',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Địa hỏa (xấu)',
    code: NGAY_DIA_HOA,
    checkboxValue: 'isCheckedNgayDiaHoa',
    labelName: 'Ngày Địa hỏa',
    labelClass: 'ngay-dia-hoa',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Độc hỏa (Nguyệt hoả) (xấu)',
    code: NGAY_DOC_HOA,
    checkboxValue: 'isCheckedNgayDocHoa',
    labelName: 'Ngày Độc hỏa (Nguyệt hoả)',
    labelClass: 'ngay-doc-hoa',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Thiên tai địa họa (xấu)',
    code: NGAY_THIEN_TAI_DAI_HOA,
    checkboxValue: 'isCheckedNgayThienTaiDaiHoa',
    labelName: 'Ngày Thiên tai đại họa',
    labelClass: 'ngay-thien-tai-dai-hoa',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày sao hỏa (xấu)',
    code: NGAY_SAO_HOA,
    checkboxValue: 'isCheckedNgaySaoHoa',
    labelName: 'Ngày Sao hỏa',
    labelClass: 'ngay-sao-hoa',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Lôi giang (xấu)',
    code: NGAY_LOI_GIANG,
    checkboxValue: 'isCheckedNgayLoiGiang',
    labelName: 'Ngày Lôi giang',
    labelClass: 'ngay-loi-giang',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Lôi đình chính sát (xấu)',
    code: NGAY_LOI_DINH_CHINH_SAT,
    checkboxValue: 'isCheckedNgayLoiDinhChinhSat',
    labelName: 'Ngày Lôi đình chính sát',
    labelClass: 'ngay-loi-dinh-chinh-sat',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Lôi đình sát chủ (xấu)',
    code: NGAY_LOI_DINH_SAT_CHU,
    checkboxValue: 'isCheckedNgayLoiDinhSatChu',
    labelName: 'Ngày Lôi đình sát chủ',
    labelClass: 'ngay-loi-dinh-sat-chu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sát sư (xấu)',
    code: NGAY_SAT_SU,
    checkboxValue: 'isCheckedNgaySatSu',
    labelName: 'Ngày Sát sư',
    labelClass: 'ngay-sat-su',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Tháng sát sư (xấu)',
    code: NGAY_THANG_SAT_SU,
    checkboxValue: 'isCheckedNgayThangSatSu',
    labelName: 'Ngày Tháng sát sư',
    labelClass: 'ngay-thang-sat-su',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Nguyệt kỵ (5, 14, 23) (xấu)',
    code: NGAY_NGUYET_KY,
    checkboxValue: 'isCheckedNgayNguyetKy',
    labelName: 'Ngày Nguyệt kỵ',
    labelClass: 'ngay-nguyet-ky',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Thiên mã tam cường (8, 18, 28) (xấu)',
    code: NGAY_THIEN_MA_TAM_CUONG,
    checkboxValue: 'isCheckedNgayThienMaTamCuong',
    labelName: 'Ngày Thiên mã tam cường',
    labelClass: 'ngay-thien-ma-tam-cuong',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Tam nương sát (3, 7, 13, 18, 22, 27) (xấu)',
    code: NGAY_TAM_NUONG_SAT,
    checkboxValue: 'isCheckedNgayTamNuongSat',
    labelName: 'Ngày Tam nương sát',
    labelClass: 'ngay-tam-nuong-sat',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Bất tương xấu (7, 16, 19, 28, kỵ thăm quan, nhận chức)',
    code: NGAY_BAT_TUONG_XAU,
    checkboxValue: 'isCheckedNgayBatTuongXau',
    labelName: 'Ngày Bất tương xấu',
    labelClass: 'ngay-bat-tuong-xau',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Nguyệt tận (Ngày cuối tháng)',
    code: NGAY_NGUYET_TAN,
    checkboxValue: 'isCheckedNgayNguyetTan',
    labelName: 'Ngày Nguyệt tận',
    labelClass: 'ngay-nguyet-tan',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Ngưu Lang Chức Nữ trong 4 mùa (xấu)',
    code: NGAY_NGUU_LANG_CHUC_NU_4_MUA,
    checkboxValue: 'isCheckedNgayNguuLangChucNu4Mua',
    labelName: 'Ngày Ngưu Lang Chức Nữ',
    labelClass: 'ngay-nguu-lang-chuc-nu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Không sàng trong 4 mùa (xấu)',
    code: NGAY_KHONG_SANG4_MUA,
    checkboxValue: 'isCheckedNgayKhongSang4Mua',
    labelName: 'Ngày Không sàng',
    labelClass: 'ngay-khong-sang',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Không phòng trong 4 mùa (xấu)',
    code: NGAY_KHONG_PHONG4_MUA,
    checkboxValue: 'isCheckedNgayKhongPhong4Mua',
    labelName: 'Ngày Không phòng',
    labelClass: 'ngay-khong-phong',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Tu la đoạt giá (xấu)',
    code: NGAY_TU_LA_DOAT_GIA,
    checkboxValue: 'isCheckedNgayTuLaDoatGia',
    labelName: 'Ngày Tu La đoạt giá',
    labelClass: 'ngay-tu-la-doat-gia',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Thập ác đại bại (xấu)',
    code: NGAY_THAP_AC_DAI_BAI,
    checkboxValue: 'isCheckedNgayThapAcDaiBai',
    labelName: 'Ngày Thập ác đại bại',
    labelClass: 'ngay-thap-ac-dai-bai',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: 'Năm Giáp và năm Kỷ',
        values: 'Tháng 3 kỵ ngày Mậu Tuất\n Tháng 7 kỵ ngày Quý Hợi\n Tháng 10 kỵ ngày Bính Thân\n Tháng 11 kỵ ngày Đinh Hợi'
      },
      {
        name: 'Năm Ất và năm Canh',
        values: 'Tháng 4 kỵ ngày Nhâm Thân\n Tháng 9 kỵ ngày Ất Tỵ'
      },
      {
        name: 'Năm Bính và năm Tân',
        values: 'Tháng 3 kỵ ngày Tân Tỵ\n Tháng 9 kỵ ngày Canh Thìn\n Tháng 11 kỵ ngày Giáp Thìn'
      },
      {
        name: 'Năm Mậu và năm Quý',
        values: 'Tháng 6 kỵ ngày Kỷ Sửu'
      },
      {
        name: 'Năm Đinh và năm Nhâm',
        values: 'Không kỵ ngày nào'
      }
    ],
    referenceBooks: [
      'Ngọc hạp chánh tông - Viên Tài Hà Tấn Phát (Trang 122)',
      'Vạn sự bất cầu nhân - Quảng Luân Nguyệt Lan (Trang 5)'
    ]
  },
  {
    name: 'Ngày Nhập mộ trong 4 mùa (kỵ đau ốm) (xấu)',
    code: NGAY_NHAP_MO4_MUA,
    checkboxValue: 'isCheckedNgayNhapMo4Mua',
    labelName: 'Ngày Nhập mộ',
    labelClass: 'ngay-nhap-mo',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Thần Hào (xấu)',
    code: NGAY_THAN_HAO,
    checkboxValue: 'isCheckedNgayThanHao',
    labelName: 'Ngày Thần Hào',
    labelClass: 'ngay-than-hao',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Quỷ Khốc (xấu)',
    code: NGAY_QUY_KHOC,
    checkboxValue: 'isCheckedNgayQuyKhoc',
    labelName: 'Ngày Quỷ Khốc',
    labelClass: 'ngay-quy-khoc',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Thiên môn bế tắc (xấu)',
    code: NGAY_THIEN_MON_BE_TAC,
    checkboxValue: 'isCheckedNgayThienMonBeTac',
    labelName: 'Ngày Thiên môn bế tắc',
    labelClass: 'ngay-thien-mon-be-tac',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Tứ ly (xấu)',
    code: NGAY_TU_LY,
    checkboxValue: 'isCheckedNgayTuLy',
    labelName: 'Ngày Tứ ly',
    labelClass: 'ngay-tu-ly',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Tứ tuyệt (xấu)',
    code: NGAY_TU_TUYET,
    checkboxValue: 'isCheckedNgayTuTuyet',
    labelName: 'Ngày Tứ tuyệt',
    labelClass: 'ngay-tu-tuyet',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Cửu thổ quỷ (xấu)',
    code: NGAY_SAO_CUU_THO_QUY,
    checkboxValue: 'isCheckedNgaySaoCuuThoQuy',
    labelName: 'Sao Cửu thổ quỷ',
    labelClass: 'ngay-sao-cuu-tho-quy',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Thiên cương (xấu)',
    code: NGAY_SAO_THIEN_CUONG,
    checkboxValue: 'isCheckedNgaySaoThienCuong',
    labelName: 'Sao Thiên cương (Diệt môn)',
    labelClass: 'ngay-sao-thien-cuong',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Thiên lại (xấu)',
    code: NGAY_SAO_THIEN_LAI,
    checkboxValue: 'isCheckedNgaySaoThienLai',
    labelName: 'Sao Thiên lại',
    labelClass: 'ngay-sao-thien-lai',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Tiểu hồng sa (xấu)',
    code: NGAY_SAO_TIEU_HONG_SA,
    checkboxValue: 'isCheckedNgaySaoTieuHongSa',
    labelName: 'Sao Tiểu hồng sa',
    labelClass: 'ngay-sao-tieu-hong-sa',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Đại hao (xấu)',
    code: NGAY_SAO_DAI_HAO,
    checkboxValue: 'isCheckedNgaySaoDaiHao',
    labelName: 'Sao Đại hao (Tử khí quan phù)',
    labelClass: 'ngay-sao-dai-hao',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Tiểu hao (xấu)',
    code: NGAY_SAO_TIEU_HAO,
    checkboxValue: 'isCheckedNgaySaoTieuHao',
    labelName: 'Sao Tiểu hao',
    labelClass: 'ngay-sao-tieu-hao',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Nguyệt phá (xấu)',
    code: NGAY_SAO_NGUYET_PHA,
    checkboxValue: 'isCheckedNgaySaoNguyetPha',
    labelName: 'Sao Nguyệt phá',
    labelClass: 'ngay-sao-nguyet-pha',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Kiếp sát (xấu)',
    code: NGAY_SAO_KIEP_SAT,
    checkboxValue: 'isCheckedNgaySaoKiepSat',
    labelName: 'Sao Kiếp sát',
    labelClass: 'ngay-sao-kiep-sat',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Địa phá (xấu)',
    code: NGAY_SAO_DIA_PHA,
    checkboxValue: 'isCheckedNgaySaoDiaPha',
    labelName: 'Sao Địa phá',
    labelClass: 'ngay-sao-dia-pha',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Thổ phủ (xấu)',
    code: NGAY_SAO_THO_PHU,
    checkboxValue: 'isCheckedNgaySaoThoPhu',
    labelName: 'Sao Thổ phủ',
    labelClass: 'ngay-sao-tho-phu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Thổ ôn (xấu)',
    code: NGAY_SAO_THO_ON,
    checkboxValue: 'isCheckedNgaySaoThoOn',
    labelName: 'Sao Thổ ôn (Thiên cẩu)',
    labelClass: 'ngay-sao-tho-on',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Thiên ôn (xấu)',
    code: NGAY_SAO_THIEN_ON,
    checkboxValue: 'isCheckedNgaySaoThienOn',
    labelName: 'Sao Thiên ôn',
    labelClass: 'ngay-sao-thien-on',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Thụ tử (xấu)',
    code: NGAY_SAO_THU_TU,
    checkboxValue: 'isCheckedNgaySaoThuTu',
    labelName: 'Sao Thụ tử',
    labelClass: 'ngay-sao-thu-tu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Hoang vu (xấu)',
    code: NGAY_SAO_HOANG_VU,
    checkboxValue: 'isCheckedNgaySaoHoangVu',
    labelName: 'Sao Hoang vu',
    labelClass: 'ngay-sao-hoang-vu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Thiên tặc (xấu)',
    code: NGAY_SAO_THIEN_TAC,
    checkboxValue: 'isCheckedNgaySaoThienTac',
    labelName: 'Sao Thiên tặc',
    labelClass: 'ngay-sao-thien-tac',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Địa tặc (xấu)',
    code: NGAY_SAO_DIA_TAC,
    checkboxValue: 'isCheckedNgaySaoDiaTac',
    labelName: 'Sao Địa tặc',
    labelClass: 'ngay-sao-dia-tac',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Hoả tai (xấu)',
    code: NGAY_SAO_HOA_TAI,
    checkboxValue: 'isCheckedNgaySaoHoaTai',
    labelName: 'Sao Hoả tai',
    labelClass: 'ngay-sao-hoa-tai',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Nguyệt yếm (xấu)',
    code: NGAY_SAO_NGUYET_YEM,
    checkboxValue: 'isCheckedNgaySaoNguyetYem',
    labelName: 'Sao Nguyệt yếm',
    labelClass: 'ngay-sao-nguyet-yem',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Nguyệt hư (xấu)',
    code: NGAY_SAO_NGUYET_HU,
    checkboxValue: 'isCheckedNgaySaoNguyetHu',
    labelName: 'Sao Nguyệt hư',
    labelClass: 'ngay-sao-nguyet-hu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Hoàng sa (xấu)',
    code: NGAY_SAO_HOANG_SA,
    checkboxValue: 'isCheckedNgaySaoHoangSa',
    labelName: 'Sao Hoàng sa',
    labelClass: 'ngay-sao-hoang-sa',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Lục bất thành (xấu)',
    code: NGAY_SAO_LUC_BAT_THANH,
    checkboxValue: 'isCheckedNgaySaoLucBatThanh',
    labelName: 'Sao Lục bất thành',
    labelClass: 'ngay-sao-luc-bat-thanh',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Nhân cách (xấu)',
    code: NGAY_SAO_NHAN_CACH,
    checkboxValue: 'isCheckedNgaySaoNhanCach',
    labelName: 'Sao Nhân cách',
    labelClass: 'ngay-sao-nhan-cach',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Thần cách (xấu)',
    code: NGAY_SAO_THAN_CACH,
    checkboxValue: 'isCheckedNgaySaoThanCach',
    labelName: 'Sao Thần cách',
    labelClass: 'ngay-sao-than-cach',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Phi ma sát (xấu)',
    code: NGAY_SAO_PHI_MA_SAT,
    checkboxValue: 'isCheckedNgaySaoPhiMaSat',
    labelName: 'Sao Phi ma sát',
    labelClass: 'ngay-sao-phi-ma-sat',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Ngũ quỷ (xấu)',
    code: NGAY_SAO_NGU_QUY,
    checkboxValue: 'isCheckedNgaySaoNguQuy',
    labelName: 'Sao Ngũ quỷ',
    labelClass: 'ngay-sao-ngu-quy',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Băng tiêu ngoạ hãm (xấu)',
    code: NGAY_SAO_BANG_TIEU_NGOA_HAM,
    checkboxValue: 'isCheckedNgaySaoBangTieuNgoaHam',
    labelName: 'Sao Băng tiêu ngoạ hãm',
    labelClass: 'ngay-sao-bang-tieu-ngoa-ham',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Hà khôi cẩu giảo (xấu)',
    code: NGAY_SAO_HA_KHOI_CAU_GIAO,
    checkboxValue: 'isCheckedNgaySaoHaKhoiCauGiao',
    labelName: 'Sao Hà khôi cẩu giảo',
    labelClass: 'ngay-sao-ha-khoi-cau-giao',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Cửu không (xấu)',
    code: NGAY_SAO_CUU_KHONG,
    checkboxValue: 'isCheckedNgaySaoCuuKhong',
    labelName: 'Sao Cửu không',
    labelClass: 'ngay-sao-cuu-khong',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Trùng tang (xấu)',
    code: NGAY_SAO_TRUNG_TANG,
    checkboxValue: 'isCheckedNgaySaoTrungTang',
    labelName: 'Sao Trùng tang',
    labelClass: 'ngay-sao-trung-tang',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Trùng phục (xấu)',
    code: NGAY_SAO_TRUNG_PHUC,
    checkboxValue: 'isCheckedNgaySaoTrungPhuc',
    labelName: 'Sao Trùng phục',
    labelClass: 'ngay-sao-trung-phuc',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Chu tước hắc đạo (xấu)',
    code: NGAY_SAO_CHU_TUOC_HAC_DAO,
    checkboxValue: 'isCheckedNgaySaoChuTuocHacDao',
    labelName: 'Sao Chu tước hắc đạo',
    labelClass: 'ngay-sao-chu-tuoc-hac-dao',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Bạch hổ (xấu)',
    code: NGAY_SAO_BACH_HO,
    checkboxValue: 'isCheckedNgaySaoBachHo',
    labelName: 'Sao Bạch hổ',
    labelClass: 'ngay-sao-bach-ho',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Huyền vũ (xấu)',
    code: NGAY_SAO_HUYEN_VU,
    checkboxValue: 'isCheckedNgaySaoHuyenVu',
    labelName: 'Sao Huyền vũ',
    labelClass: 'ngay-sao-huyen-vu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Câu trận (xấu)',
    code: NGAY_SAO_CAU_TRAN,
    checkboxValue: 'isCheckedNgaySaoCauTran',
    labelName: 'Sao Câu trận',
    labelClass: 'ngay-sao-cau-tran',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Lôi công (xấu)',
    code: NGAY_SAO_LOI_CONG,
    checkboxValue: 'isCheckedNgaySaoLoiCong',
    labelName: 'Sao Lôi công',
    labelClass: 'ngay-sao-loi-cong',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Cô thần (xấu)',
    code: NGAY_SAO_CO_THAN,
    checkboxValue: 'isCheckedNgaySaoCoThan',
    labelName: 'Sao Cô thần',
    labelClass: 'ngay-sao-co-than',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Quả tú (xấu)',
    code: NGAY_SAO_QUA_TU,
    checkboxValue: 'isCheckedNgaySaoQuaTu',
    labelName: 'Sao Quả tú',
    labelClass: 'ngay-sao-qua-tu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Nguyệt hình (xấu)',
    code: NGAY_SAO_NGUYET_HINH,
    checkboxValue: 'isCheckedNgaySaoNguyetHinh',
    labelName: 'Sao Nguyệt hình',
    labelClass: 'ngay-sao-nguyet-hinh',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày có Sao Tội chỉ (xấu)',
    code: NGAY_SAO_TOI_CHI,
    checkboxValue: 'isCheckedNgaySaoToiChi',
    labelName: 'Sao Tội chỉ',
    labelClass: 'ngay-sao-toi-chi',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Nguyệt Kiến chuyển sát (xấu)',
    code: NGAY_SAO_NGUYET_KIEN_CHUYEN_SAT,
    checkboxValue: 'isCheckedNgaySaoNguyetKienChuyenSat',
    labelName: 'Sao Nguyệt Kiến chuyển sát',
    labelClass: 'ngay-sao-nguyet-kien-chuyen-sat',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Thiên địa chính chuyển (xấu)',
    code: NGAY_SAO_THIEN_DIA_CHINH_CHUYEN,
    checkboxValue: 'isCheckedNgaySaoThienDiaChinhChuyen',
    labelName: 'Sao Thiên địa chính chuyển',
    labelClass: 'ngay-sao-thien-dia-chinh-chuyen',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Thiên địa chuyển sát (xấu)',
    code: NGAY_SAO_THIEN_DIA_CHUYEN_SAT,
    checkboxValue: 'isCheckedNgaySaoThienDiaChuyenSat',
    labelName: 'Sao Thiên địa chuyển sát',
    labelClass: 'ngay-sao-thien-dia-chuyen-sat',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Lỗ ban sát (xấu)',
    code: NGAY_SAO_LO_BAN_SAT,
    checkboxValue: 'isCheckedNgaySaoLoBanSat',
    labelName: 'Sao Lỗ ban sát',
    labelClass: 'ngay-sao-lo-ban-sat',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Phủ đầu sát (xấu)',
    code: NGAY_SAO_PHU_DAU_SAT,
    checkboxValue: 'isCheckedNgaySaoPhuDauSat',
    labelName: 'Sao Phủ đầu sát',
    labelClass: 'ngay-sao-phu-dau-sat',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Tam tang (xấu)',
    code: NGAY_SAO_TAM_TANG,
    checkboxValue: 'isCheckedNgaySaoTamTang',
    labelName: 'Sao Tam tang',
    labelClass: 'ngay-sao-tam-tang',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Ngũ hư (xấu)',
    code: NGAY_SAO_NGU_HU,
    checkboxValue: 'isCheckedNgaySaoNguHu',
    labelName: 'Sao Ngũ hư',
    labelClass: 'ngay-sao-ngu-hu',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Tứ thời đại mộ (xấu)',
    code: NGAY_SAO_TU_THOI_DAI_MO,
    checkboxValue: 'isCheckedNgaySaoTuThoiDaiMo',
    labelName: 'Sao Tứ thời đại mộ',
    labelClass: 'ngay-sao-tu-thoi-dai-mo',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Thổ cấm (xấu)',
    code: NGAY_SAO_THO_CAM,
    checkboxValue: 'isCheckedNgaySaoThoCam',
    labelName: 'Sao Thổ cấm',
    labelClass: 'ngay-sao-tho-cam',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Ly sàng (xấu)',
    code: NGAY_SAO_LY_SANG,
    checkboxValue: 'isCheckedNgaySaoLySang',
    labelName: 'Sao Ly sàng',
    labelClass: 'ngay-sao-ly-sang',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Tứ thời cô quả (xấu)',
    code: NGAY_SAO_TU_THOI_CO_QUA,
    checkboxValue: 'isCheckedNgaySaoTuThoiCoQua',
    labelName: 'Sao Tứ thời cô quả',
    labelClass: 'ngay-sao-tu-thoi-co-qua',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Âm thác (xấu)',
    code: NGAY_SAO_AM_THAC,
    checkboxValue: 'isCheckedNgaySaoAmThac',
    labelName: 'Sao Âm thác',
    labelClass: 'ngay-sao-am-thac',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Dương thác (xấu)',
    code: NGAY_SAO_DUONG_THAC,
    checkboxValue: 'isCheckedNgaySaoDuongThac',
    labelName: 'Sao Dương thác',
    labelClass: 'ngay-sao-duong-thac',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  },
  {
    name: 'Ngày Sao Quỷ khốc (xấu)',
    code: NGAY_SAO_QUY_KHOC,
    checkboxValue: 'isCheckedNgaySaoQuyKhoc',
    labelName: 'Sao Quỷ khốc',
    labelClass: 'ngay-sao-quy-khoc',
    goodDayStatus: false,
    viewType: 1,
    textBridge: 'kỵ ngày',
    list: [
      {
        name: '',
        values: ''
      }
    ],
    referenceBooks: [
    ]
  }
]
