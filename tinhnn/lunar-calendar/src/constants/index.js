/**
 * Dữ liệu lịch âm
 */
export const TK19 = [
  0x30baa3, 0x56ab50, 0x422ba0, 0x2cab61, 0x52a370, 0x3c51e8, 0x60d160, 0x4ae4b0, 0x376926, 0x58daa0,
  0x445b50, 0x3116d2, 0x562ae0, 0x3ea2e0, 0x28e2d2, 0x4ec950, 0x38d556, 0x5cb520, 0x46b690, 0x325da4,
  0x5855d0, 0x4225d0, 0x2ca5b3, 0x52a2b0, 0x3da8b7, 0x60a950, 0x4ab4a0, 0x35b2a5, 0x5aad50, 0x4455b0,
  0x302b74, 0x562570, 0x4052f9, 0x6452b0, 0x4e6950, 0x386d56, 0x5e5aa0, 0x46ab50, 0x3256d4, 0x584ae0,
  0x42a570, 0x2d4553, 0x50d2a0, 0x3be8a7, 0x60d550, 0x4a5aa0, 0x34ada5, 0x5a95d0, 0x464ae0, 0x2eaab4,
  0x54a4d0, 0x3ed2b8, 0x64b290, 0x4cb550, 0x385757, 0x5e2da0, 0x4895d0, 0x324d75, 0x5849b0, 0x42a4b0,
  0x2da4b3, 0x506a90, 0x3aad98, 0x606b50, 0x4c2b60, 0x359365, 0x5a9370, 0x464970, 0x306964, 0x52e4a0,
  0x3cea6a, 0x62da90, 0x4e5ad0, 0x392ad6, 0x5e2ae0, 0x4892e0, 0x32cad5, 0x56c950, 0x40d4a0, 0x2bd4a3,
  0x50b690, 0x3a57a7, 0x6055b0, 0x4c25d0, 0x3695b5, 0x5a92b0, 0x44a950, 0x2ed954, 0x54b4a0, 0x3cb550,
  0x286b52, 0x4e55b0, 0x3a2776, 0x5e2570, 0x4852b0, 0x32aaa5, 0x56e950, 0x406aa0, 0x2abaa3, 0x50ab50
] /* Years 1800-1899 */

export const TK20 = [
  0x3c4bd8, 0x624ae0, 0x4ca570, 0x3854d5, 0x5cd260, 0x44d950, 0x315554, 0x5656a0, 0x409ad0, 0x2a55d2,
  0x504ae0, 0x3aa5b6, 0x60a4d0, 0x48d250, 0x33d255, 0x58b540, 0x42d6a0, 0x2cada2, 0x5295b0, 0x3f4977,
  0x644970, 0x4ca4b0, 0x36b4b5, 0x5c6a50, 0x466d50, 0x312b54, 0x562b60, 0x409570, 0x2c52f2, 0x504970,
  0x3a6566, 0x5ed4a0, 0x48ea50, 0x336a95, 0x585ad0, 0x442b60, 0x2f86e3, 0x5292e0, 0x3dc8d7, 0x62c950,
  0x4cd4a0, 0x35d8a6, 0x5ab550, 0x4656a0, 0x31a5b4, 0x5625d0, 0x4092d0, 0x2ad2b2, 0x50a950, 0x38b557,
  0x5e6ca0, 0x48b550, 0x355355, 0x584da0, 0x42a5b0, 0x2f4573, 0x5452b0, 0x3ca9a8, 0x60e950, 0x4c6aa0,
  0x36aea6, 0x5aab50, 0x464b60, 0x30aae4, 0x56a570, 0x405260, 0x28f263, 0x4ed940, 0x38db47, 0x5cd6a0,
  0x4896d0, 0x344dd5, 0x5a4ad0, 0x42a4d0, 0x2cd4b4, 0x52b250, 0x3cd558, 0x60b540, 0x4ab5a0, 0x3755a6,
  0x5c95b0, 0x4649b0, 0x30a974, 0x56a4b0, 0x40aa50, 0x29aa52, 0x4e6d20, 0x39ad47, 0x5eab60, 0x489370,
  0x344af5, 0x5a4970, 0x4464b0, 0x2c74a3, 0x50ea50, 0x3d6a58, 0x6256a0, 0x4aaad0, 0x3696d5, 0x5c92e0
] /* Years 1900-1999 */

export const TK21 = [
  0x46c960, 0x2ed954, 0x54d4a0, 0x3eda50, 0x2a7552, 0x4e56a0, 0x38a7a7, 0x5ea5d0, 0x4a92b0, 0x32aab5,
  0x58a950, 0x42b4a0, 0x2cbaa4, 0x50ad50, 0x3c55d9, 0x624ba0, 0x4ca5b0, 0x375176, 0x5c5270, 0x466930,
  0x307934, 0x546aa0, 0x3ead50, 0x2a5b52, 0x504b60, 0x38a6e6, 0x5ea4e0, 0x48d260, 0x32ea65, 0x56d520,
  0x40daa0, 0x2d56a3, 0x5256d0, 0x3c4afb, 0x6249d0, 0x4ca4d0, 0x37d0b6, 0x5ab250, 0x44b520, 0x2edd25,
  0x54b5a0, 0x3e55d0, 0x2a55b2, 0x5049b0, 0x3aa577, 0x5ea4b0, 0x48aa50, 0x33b255, 0x586d20, 0x40ad60,
  0x2d4b63, 0x525370, 0x3e49e8, 0x60c970, 0x4c54b0, 0x3768a6, 0x5ada50, 0x445aa0, 0x2fa6a4, 0x54aad0,
  0x4052e0, 0x28d2e3, 0x4ec950, 0x38d557, 0x5ed4a0, 0x46d950, 0x325d55, 0x5856a0, 0x42a6d0, 0x2c55d4,
  0x5252b0, 0x3ca9b8, 0x62a930, 0x4ab490, 0x34b6a6, 0x5aad50, 0x4655a0, 0x2eab64, 0x54a570, 0x4052b0,
  0x2ab173, 0x4e6930, 0x386b37, 0x5e6aa0, 0x48ad50, 0x332ad5, 0x582b60, 0x42a570, 0x2e52e4, 0x50d160,
  0x3ae958, 0x60d520, 0x4ada90, 0x355aa6, 0x5a56d0, 0x462ae0, 0x30a9d4, 0x54a2d0, 0x3ed150, 0x28e952
] /* Years 2000-2099 */

export const TK22 = [
  0x4eb520, 0x38d727, 0x5eada0, 0x4a55b0, 0x362db5, 0x5a45b0, 0x44a2b0, 0x2eb2b4, 0x54a950, 0x3cb559,
  0x626b20, 0x4cad50, 0x385766, 0x5c5370, 0x484570, 0x326574, 0x5852b0, 0x406950, 0x2a7953, 0x505aa0,
  0x3baaa7, 0x5ea6d0, 0x4a4ae0, 0x35a2e5, 0x5aa550, 0x42d2a0, 0x2de2a4, 0x52d550, 0x3e5abb, 0x6256a0,
  0x4c96d0, 0x3949b6, 0x5e4ab0, 0x46a8d0, 0x30d4b5, 0x56b290, 0x40b550, 0x2a6d52, 0x504da0, 0x3b9567,
  0x609570, 0x4a49b0, 0x34a975, 0x5a64b0, 0x446a90, 0x2cba94, 0x526b50, 0x3e2b60, 0x28ab61, 0x4c9570,
  0x384ae6, 0x5cd160, 0x46e4a0, 0x2eed25, 0x54da90, 0x405b50, 0x2c36d3, 0x502ae0, 0x3a93d7, 0x6092d0,
  0x4ac950, 0x32d556, 0x58b4a0, 0x42b690, 0x2e5d94, 0x5255b0, 0x3e25fa, 0x6425b0, 0x4e92b0, 0x36aab6,
  0x5c6950, 0x4674a0, 0x31b2a5, 0x54ad50, 0x4055a0, 0x2aab73, 0x522570, 0x3a5377, 0x6052b0, 0x4a6950,
  0x346d56, 0x585aa0, 0x42ab50, 0x2e56d4, 0x544ae0, 0x3ca570, 0x2864d2, 0x4cd260, 0x36eaa6, 0x5ad550,
  0x465aa0, 0x30ada5, 0x5695d0, 0x404ad0, 0x2aa9b3, 0x50a4d0, 0x3ad2b7, 0x5eb250, 0x48b540, 0x33d556
] /* Years 2100-2199 */

/**
 * Định dạng ngày trong chuyển đổi qua lại của hàm moment
 */
export const DATE_FORMAT = 'D/M/YYYY'

/**
 * Ngày bắt đầu trong ứng dụng lịch này, Tết âm lịch năm 1800
 */
export const FIRST_DAY = '25/1/1800'

/**
 * Ngày kết thúc trong ứng dụng lịch này
 */
export const LAST_DAY = '31/12/2199'

/**
 * Danh sách ngày trong tuần
 */
export const DAYS_IN_WEEK = [
  {
    id: 1,
    name: 'Thứ Hai'
  },
  {
    id: 2,
    name: 'Thứ Ba'
  },
  {
    id: 3,
    name: 'Thứ Tư'
  },
  {
    id: 4,
    name: 'Thứ Năm'
  },
  {
    id: 5,
    name: 'Thứ Sáu'
  },
  {
    id: 6,
    name: 'Thứ Bảy'
  },
  {
    id: 7,
    name: 'Chủ Nhật'
  }
]

/**
 * Danh sách 10 can
 */
export const CAN_LIST = [
  {
    id: 1,
    name: 'Giáp'
  },
  {
    id: 2,
    name: 'Ất'
  },
  {
    id: 3,
    name: 'Bính'
  },
  {
    id: 4,
    name: 'Đinh'
  },
  {
    id: 5,
    name: 'Mậu'
  },
  {
    id: 6,
    name: 'Kỷ'
  },
  {
    id: 7,
    name: 'Canh'
  },
  {
    id: 8,
    name: 'Tân'
  },
  {
    id: 9,
    name: 'Nhâm'
  },
  {
    id: 10,
    name: 'Quý'
  }
]

export const CAN_GIAP = 1
export const CAN_AT = 2
export const CAN_BINH = 3
export const CAN_DINH = 4
export const CAN_MAU = 5
export const CAN_KY = 6
export const CAN_CANH = 7
export const CAN_TAN = 8
export const CAN_NHAM = 9
export const CAN_QUY = 10

/**
 * Danh sách 12 chi
 */
export const CHI_LIST = [
  {
    id: 1,
    name: 'Tý'
  },
  {
    id: 2,
    name: 'Sửu'
  },
  {
    id: 3,
    name: 'Dần'
  },
  {
    id: 4,
    name: 'Mão'
  },
  {
    id: 5,
    name: 'Thìn'
  },
  {
    id: 6,
    name: 'Tỵ'
  },
  {
    id: 7,
    name: 'Ngọ'
  },
  {
    id: 8,
    name: 'Mùi'
  },
  {
    id: 9,
    name: 'Thân'
  },
  {
    id: 10,
    name: 'Dậu'
  },
  {
    id: 11,
    name: 'Tuất'
  },
  {
    id: 12,
    name: 'Hợi'
  }
]

export const CHI_TYS = 1
export const CHI_SUU = 2
export const CHI_DAN = 3
export const CHI_MAO = 4
export const CHI_THIN = 5
export const CHI_TYJ = 6
export const CHI_NGO = 7
export const CHI_MUI = 8
export const CHI_THAN = 9
export const CHI_DAU = 10
export const CHI_TUAT = 11
export const CHI_HOI = 12

/**
 * Danh sách 24 tiết khí
 */
export const TIET_KHI_LIST = [
  {
    id: 1,
    name: 'Xuân phân'
  },
  {
    id: 2,
    name: 'Thanh minh'
  },
  {
    id: 3,
    name: 'Cốc vũ'
  },
  {
    id: 4,
    name: 'Lập hạ'
  },
  {
    id: 5,
    name: 'Tiểu mãn'
  },
  {
    id: 6,
    name: 'Mang chủng'
  },
  {
    id: 7,
    name: 'Hạ chí'
  },
  {
    id: 8,
    name: 'Tiểu thử'
  },
  {
    id: 9,
    name: 'Đại thử'
  },
  {
    id: 10,
    name: 'Lập thu'
  },
  {
    id: 11,
    name: 'Xử thử'
  },
  {
    id: 12,
    name: 'Bạch lộ'
  },
  {
    id: 13,
    name: 'Thu phân'
  },
  {
    id: 14,
    name: 'Hàn lộ'
  },
  {
    id: 15,
    name: 'Sương giáng'
  },
  {
    id: 16,
    name: 'Lập đông'
  },
  {
    id: 17,
    name: 'Tiểu tuyết'
  },
  {
    id: 18,
    name: 'Đại tuyết'
  },
  {
    id: 19,
    name: 'Đông chí'
  },
  {
    id: 20,
    name: 'Tiểu hàn'
  },
  {
    id: 21,
    name: 'Đại hàn'
  },
  {
    id: 22,
    name: 'Lập xuân'
  },
  {
    id: 23,
    name: 'Vũ thủy'
  },
  {
    id: 24,
    name: 'Kinh trập'
  }
]

export const TIET_KHI_XUAN_PHAN = 1
export const TIET_KHI_THANH_MINH = 2
export const TIET_KHI_COC_VU = 3
export const TIET_KHI_LAP_HA = 4
export const TIET_KHI_TIEU_MAN = 5
export const TIET_KHI_MANG_CHUNG = 6
export const TIET_KHI_HA_CHI = 7
export const TIET_KHI_TIEU_THU = 8
export const TIET_KHI_DAI_THU = 9
export const TIET_KHI_LAP_THU = 10
export const TIET_KHI_XU_THU = 11
export const TIET_KHI_BACH_LO = 12
export const TIET_KHI_THU_PHAN = 13
export const TIET_KHI_HAN_LO = 14
export const TIET_KHI_SUONG_GIANG = 15
export const TIET_KHI_LAP_DONG = 16
export const TIET_KHI_TIEU_TUYET = 17
export const TIET_KHI_DAI_TUYET = 18
export const TIET_KHI_DONG_CHI = 19
export const TIET_KHI_TIEU_HAN = 20
export const TIET_KHI_DAI_HAN = 21
export const TIET_KHI_LAP_XUAN = 22
export const TIET_KHI_VU_THUY = 23
export const TIET_KHI_KINH_TRAP = 24

/**
 * Danh sách 12 trực
 */
export const TRUC_LIST = [
  {
    id: 1,
    name: 'Kiến'
  },
  {
    id: 2,
    name: 'Trừ'
  },
  {
    id: 3,
    name: 'Mãn'
  },
  {
    id: 4,
    name: 'Bình'
  },
  {
    id: 5,
    name: 'Định'
  },
  {
    id: 6,
    name: 'Chấp'
  },
  {
    id: 7,
    name: 'Phá'
  },
  {
    id: 8,
    name: 'Nguy'
  },
  {
    id: 9,
    name: 'Thành'
  },
  {
    id: 10,
    name: 'Thu'
  },
  {
    id: 11,
    name: 'Khai'
  },
  {
    id: 12,
    name: 'Bế'
  }
]

export const TRUC_KIEN = 1
export const TRUC_TRU = 2
export const TRUC_MAN = 3
export const TRUC_BINH = 4
export const TRUC_DINH = 5
export const TRUC_CHAP = 6
export const TRUC_PHA = 7
export const TRUC_NGUY = 8
export const TRUC_THANH = 9
export const TRUC_THU = 10
export const TRUC_KHAI = 11
export const TRUC_BE = 12

/**
 * Danh sách 28 sao (Nhị thập bát tú)
 */
export const SAO_LIST = [
  {
    id: 1,
    name: 'Giác'
  },
  {
    id: 2,
    name: 'Cang'
  },
  {
    id: 3,
    name: 'Đê'
  },
  {
    id: 4,
    name: 'Phòng'
  },
  {
    id: 5,
    name: 'Tâm'
  },
  {
    id: 6,
    name: 'Vĩ'
  },
  {
    id: 7,
    name: 'Cơ'
  },
  {
    id: 8,
    name: 'Đẩu'
  },
  {
    id: 9,
    name: 'Ngưu'
  },
  {
    id: 10,
    name: 'Nữ'
  },
  {
    id: 11,
    name: 'Hư'
  },
  {
    id: 12,
    name: 'Nguy'
  },
  {
    id: 13,
    name: 'Thất'
  },
  {
    id: 14,
    name: 'Bích'
  },
  {
    id: 15,
    name: 'Khuê'
  },
  {
    id: 16,
    name: 'Lâu'
  },
  {
    id: 17,
    name: 'Vị'
  },
  {
    id: 18,
    name: 'Mão'
  },
  {
    id: 19,
    name: 'Tất'
  },
  {
    id: 20,
    name: 'Chủy'
  },
  {
    id: 21,
    name: 'Sâm'
  },
  {
    id: 22,
    name: 'Tỉnh'
  },
  {
    id: 23,
    name: 'Quỷ'
  },
  {
    id: 24,
    name: 'Liễu'
  },
  {
    id: 25,
    name: 'Tinh'
  },
  {
    id: 26,
    name: 'Trương'
  },
  {
    id: 27,
    name: 'Dực'
  },
  {
    id: 28,
    name: 'Chẩn'
  }
]

export const SAO_GIAC_DATE = '8/1/1800' // Thứ 5 ngày 8/1/1800 DL

/**
 * Danh sách 12 giờ
 */
export const GIO_TYS = 1
export const GIO_SUU = 2
export const GIO_DAN = 3
export const GIO_MAO = 4
export const GIO_THIN = 5
export const GIO_TYJ = 6
export const GIO_NGO = 7
export const GIO_MUI = 8
export const GIO_THAN = 9
export const GIO_DAU = 10
export const GIO_TUAT = 11
export const GIO_HOI = 12

/**
 * Danh sách 5 loại mệnh chính, chia ra làm 30 loại mệnh phụ
 */
export const MENH_LIST = [
  {
    id: 1,
    name: 'Sa Trung Kim',
    interpretation: 'Vàng trong cát',
    primaryName: 'Kim'
  },
  {
    id: 2,
    name: 'Hải Trung Kim',
    interpretation: 'Vàng trong biển',
    primaryName: 'Kim'
  },
  {
    id: 3,
    name: 'Kim Bạch Kim',
    interpretation: 'Vàng trắng',
    primaryName: 'Kim'
  },
  {
    id: 4,
    name: 'Thoa Xuyến Kim',
    interpretation: 'Vàng trang sức',
    primaryName: 'Kim'
  },
  {
    id: 5,
    name: 'Bạch Lạp Kim',
    interpretation: 'Chân đèn bằng vàng',
    primaryName: 'Kim'
  },
  {
    id: 6,
    name: 'Kiếm Phong Kim',
    interpretation: 'Kiếm bằng vàng',
    primaryName: 'Kim'
  },
  {
    id: 7,
    name: 'Giáng Hạ Thủy',
    interpretation: 'Nước cuối suối',
    primaryName: 'Thủy'
  },
  {
    id: 8,
    name: 'Tuyền Trung Thủy',
    interpretation: 'Nước trong suối',
    primaryName: 'Thủy'
  },
  {
    id: 9,
    name: 'Trường Lưu Thủy',
    interpretation: 'Nước chảy dài',
    primaryName: 'Thủy'
  },
  {
    id: 10,
    name: 'Đại Khê Thủy',
    interpretation: 'Nước suối lớn',
    primaryName: 'Thủy'
  },
  {
    id: 11,
    name: 'Đại Hải Thủy',
    interpretation: 'Nước biển lớn',
    primaryName: 'Thủy'
  },
  {
    id: 12,
    name: 'Thiên Hà Thủy',
    interpretation: 'Nước trên trời',
    primaryName: 'Thủy'
  },
  {
    id: 13,
    name: 'Bình Địa Mộc',
    interpretation: 'Cây ở đồng bằng',
    primaryName: 'Mộc'
  },
  {
    id: 14,
    name: 'Tang Đố Mộc',
    interpretation: 'Cây Dâu Tằm',
    primaryName: 'Mộc'
  },
  {
    id: 15,
    name: 'Dương Liễu Mộc',
    interpretation: 'Cây Dương Liễu',
    primaryName: 'Mộc'
  },
  {
    id: 16,
    name: 'Thạch Lưu Mộc',
    interpretation: 'Cây Lựu mọc trên đá',
    primaryName: 'Mộc'
  },
  {
    id: 17,
    name: 'Tùng Bách Mộc',
    interpretation: 'Cây Tùng cổ thụ',
    primaryName: 'Mộc'
  },
  {
    id: 18,
    name: 'Đại Lâm Mộc',
    interpretation: 'Cây ở rừng lớn',
    primaryName: 'Mộc'
  },
  {
    id: 19,
    name: 'Phú Đăng Hỏa',
    interpretation: 'Lửa đèn dầu',
    primaryName: 'Hỏa'
  },
  {
    id: 20,
    name: 'Lư Trung Hỏa',
    interpretation: 'Lửa trong lò',
    primaryName: 'Hỏa'
  },
  {
    id: 21,
    name: 'Sơn Hạ Hỏa',
    interpretation: 'Lửa dưới núi',
    primaryName: 'Hỏa'
  },
  {
    id: 22,
    name: 'Sơn Đầu Hỏa',
    interpretation: 'Lửa trên đỉnh núi',
    primaryName: 'Hỏa'
  },
  {
    id: 23,
    name: 'Tích Lịch Hỏa',
    interpretation: 'Lửa sấm sét',
    primaryName: 'Hỏa'
  },
  {
    id: 24,
    name: 'Thiên Thượng Hỏa',
    interpretation: 'Lửa trên trời',
    primaryName: 'Hỏa'
  },
  {
    id: 25,
    name: 'Lộ bàng thổ',
    interpretation: 'Đất ven đường',
    primaryName: 'Thổ'
  },
  {
    id: 26,
    name: 'Sa trung thổ',
    interpretation: 'Đất lẫn trong cát',
    primaryName: 'Thổ'
  },
  {
    id: 27,
    name: 'Bích thượng thổ',
    interpretation: 'Đất trên vách',
    primaryName: 'Thổ'
  },
  {
    id: 28,
    name: 'Thành đầu thổ',
    interpretation: 'Đất trên thành',
    primaryName: 'Thổ'
  },
  {
    id: 29,
    name: 'Ốc thượng thổ',
    interpretation: 'Đất ngói trên mái nhà',
    primaryName: 'Thổ'
  },
  {
    id: 30,
    name: 'Đại trạch thổ',
    interpretation: 'Đất nền nhà lớn',
    primaryName: 'Thổ'
  }
]

export const MENH_SA_TRUNG_KIM = 1
export const MENH_HAI_TRUNG_KIM = 2
export const MENH_KIM_BACH_KIM = 3
export const MENH_THOA_XUYEN_KIM = 4
export const MENH_BACH_LAP_KIM = 5
export const MENH_KIEM_PHONG_KIM = 6
export const MENH_GIANG_HA_THUY = 7
export const MENH_TUYEN_TRUNG_THUY = 8
export const MENH_TRUONG_LUU_THUY = 9
export const MENH_DAI_KHE_THUY = 10
export const MENH_DAI_HAI_THUY = 11
export const MENH_THIEN_HA_THUY = 12
export const MENH_BINH_DIA_MOC = 13
export const MENH_TANG_DO_MOC = 14
export const MENH_DUONG_LIEU_MOC = 15
export const MENH_THACH_LUU_MOC = 16
export const MENH_TUNG_BACH_MOC = 17
export const MENH_DAI_LAM_MOC = 18
export const MENH_PHU_DANG_HOA = 19
export const MENH_LU_TRUNG_HOA = 20
export const MENH_SON_HA_HOA = 21
export const MENH_SON_DAU_HOA = 22
export const MENH_TICH_LICH_HOA = 23
export const MENH_THIEN_THUONG_HOA = 24
export const MENH_LO_BANG_THO = 25
export const MENH_SA_TRUNG_THO = 26
export const MENH_BICH_THUONG_THO = 27
export const MENH_THANH_DAU_THO = 28
export const MENH_OC_THUONG_THO = 29
export const MENH_DAI_TRACH_THO = 30

/**
 * Danh sách 60 Lục thập hoa giáp
 */
export const LUC_THAP_HOA_GIAP_LIST = [
  {
    id: 1,
    name: 'Giáp Tý',
    canId: CAN_GIAP,
    chiId: CHI_TYS,
    menhId: MENH_HAI_TRUNG_KIM
  },
  {
    id: 2,
    name: 'Ất Sửu',
    canId: CAN_AT,
    chiId: CHI_SUU,
    menhId: MENH_HAI_TRUNG_KIM
  },
  {
    id: 3,
    name: 'Bính Dần',
    canId: CAN_BINH,
    chiId: CHI_DAN,
    menhId: MENH_LU_TRUNG_HOA
  },
  {
    id: 4,
    name: 'Đinh Mão',
    canId: CAN_DINH,
    chiId: CHI_MAO,
    menhId: MENH_LU_TRUNG_HOA
  },
  {
    id: 5,
    name: 'Mậu Thìn',
    canId: CAN_MAU,
    chiId: CHI_THIN,
    menhId: MENH_DAI_LAM_MOC
  },
  {
    id: 6,
    name: 'Kỷ Tỵ',
    canId: CAN_KY,
    chiId: CHI_TYJ,
    menhId: MENH_DAI_LAM_MOC
  },
  {
    id: 7,
    name: 'Canh Ngọ',
    canId: CAN_CANH,
    chiId: CHI_NGO,
    menhId: MENH_LO_BANG_THO
  },
  {
    id: 8,
    name: 'Tân Mùi',
    canId: CAN_TAN,
    chiId: CHI_MUI,
    menhId: MENH_LO_BANG_THO
  },
  {
    id: 9,
    name: 'Nhâm Thân',
    canId: CAN_NHAM,
    chiId: CHI_THAN,
    menhId: MENH_KIEM_PHONG_KIM
  },
  {
    id: 10,
    name: 'Quý Dậu',
    canId: CAN_QUY,
    chiId: CHI_DAU,
    menhId: MENH_KIEM_PHONG_KIM
  },
  {
    id: 11,
    name: 'Giáp Tuất',
    canId: CAN_GIAP,
    chiId: CHI_TUAT,
    menhId: MENH_SON_DAU_HOA
  },
  {
    id: 12,
    name: 'Ất Hợi',
    canId: CAN_AT,
    chiId: CHI_HOI,
    menhId: MENH_SON_DAU_HOA
  },
  {
    id: 13,
    name: 'Bính Tý',
    canId: CAN_BINH,
    chiId: CHI_TYS,
    menhId: MENH_GIANG_HA_THUY
  },
  {
    id: 14,
    name: 'Đinh Sửu',
    canId: CAN_DINH,
    chiId: CHI_SUU,
    menhId: MENH_GIANG_HA_THUY
  },
  {
    id: 15,
    name: 'Mậu Dần',
    canId: CAN_MAU,
    chiId: CHI_DAN,
    menhId: MENH_THANH_DAU_THO
  },
  {
    id: 16,
    name: 'Kỷ Mão',
    canId: CAN_KY,
    chiId: CHI_MAO,
    menhId: MENH_THANH_DAU_THO
  },
  {
    id: 17,
    name: 'Canh Thìn',
    canId: CAN_CANH,
    chiId: CHI_THIN,
    menhId: MENH_BACH_LAP_KIM
  },
  {
    id: 18,
    name: 'Tân Tỵ',
    canId: CAN_TAN,
    chiId: CHI_TYJ,
    menhId: MENH_BACH_LAP_KIM
  },
  {
    id: 19,
    name: 'Nhâm Ngọ',
    canId: CAN_NHAM,
    chiId: CHI_NGO,
    menhId: MENH_DUONG_LIEU_MOC
  },
  {
    id: 20,
    name: 'Quý Mùi',
    canId: CAN_QUY,
    chiId: CHI_MUI,
    menhId: MENH_DUONG_LIEU_MOC
  },
  {
    id: 21,
    name: 'Giáp Thân',
    canId: CAN_GIAP,
    chiId: CHI_THAN,
    menhId: MENH_TUYEN_TRUNG_THUY
  },
  {
    id: 22,
    name: 'Ất Dậu',
    canId: CAN_AT,
    chiId: CHI_DAU,
    menhId: MENH_TUYEN_TRUNG_THUY
  },
  {
    id: 23,
    name: 'Bính Tuất',
    canId: CAN_BINH,
    chiId: CHI_TUAT,
    menhId: MENH_OC_THUONG_THO
  },
  {
    id: 24,
    name: 'Đinh Hợi',
    canId: CAN_DINH,
    chiId: CHI_HOI,
    menhId: MENH_OC_THUONG_THO
  },
  {
    id: 25,
    name: 'Mậu Tý',
    canId: CAN_MAU,
    chiId: CHI_TYS,
    menhId: MENH_TICH_LICH_HOA
  },
  {
    id: 26,
    name: 'Kỷ Sửu',
    canId: CAN_KY,
    chiId: CHI_SUU,
    menhId: MENH_TICH_LICH_HOA
  },
  {
    id: 27,
    name: 'Canh Dần',
    canId: CAN_CANH,
    chiId: CHI_DAN,
    menhId: MENH_TUNG_BACH_MOC
  },
  {
    id: 28,
    name: 'Tân Mão',
    canId: CAN_TAN,
    chiId: CHI_MAO,
    menhId: MENH_TUNG_BACH_MOC
  },
  {
    id: 29,
    name: 'Nhâm Thìn',
    canId: CAN_NHAM,
    chiId: CHI_THIN,
    menhId: MENH_TRUONG_LUU_THUY
  },
  {
    id: 30,
    name: 'Quý Tỵ',
    canId: CAN_QUY,
    chiId: CHI_TYJ,
    menhId: MENH_TRUONG_LUU_THUY
  },
  {
    id: 31,
    name: 'Giáp Ngọ',
    canId: CAN_GIAP,
    chiId: CHI_NGO,
    menhId: MENH_SA_TRUNG_KIM
  },
  {
    id: 32,
    name: 'Ất Mùi',
    canId: CAN_AT,
    chiId: CHI_MUI,
    menhId: MENH_SA_TRUNG_KIM
  },
  {
    id: 33,
    name: 'Bính Thân',
    canId: CAN_BINH,
    chiId: CHI_THAN,
    menhId: MENH_SON_HA_HOA
  },
  {
    id: 34,
    name: 'Đinh Dậu',
    canId: CAN_DINH,
    chiId: CHI_DAU,
    menhId: MENH_SON_HA_HOA
  },
  {
    id: 35,
    name: 'Mậu Tuất',
    canId: CAN_MAU,
    chiId: CHI_TUAT,
    menhId: MENH_BINH_DIA_MOC
  },
  {
    id: 36,
    name: 'Kỷ Hợi',
    canId: CAN_KY,
    chiId: CHI_HOI,
    menhId: MENH_BINH_DIA_MOC
  },
  {
    id: 37,
    name: 'Canh Tý',
    canId: CAN_CANH,
    chiId: CHI_TYS,
    menhId: MENH_BICH_THUONG_THO
  },
  {
    id: 38,
    name: 'Tân Sửu',
    canId: CAN_TAN,
    chiId: CHI_SUU,
    menhId: MENH_BICH_THUONG_THO
  },
  {
    id: 39,
    name: 'Nhâm Dần',
    canId: CAN_NHAM,
    chiId: CHI_DAN,
    menhId: MENH_KIM_BACH_KIM
  },
  {
    id: 40,
    name: 'Quý Mão',
    canId: CAN_QUY,
    chiId: CHI_MAO,
    menhId: MENH_KIM_BACH_KIM
  },
  {
    id: 41,
    name: 'Giáp Thìn',
    canId: CAN_GIAP,
    chiId: CHI_THIN,
    menhId: MENH_PHU_DANG_HOA
  },
  {
    id: 42,
    name: 'Ất Tỵ',
    canId: CAN_AT,
    chiId: CHI_TYJ,
    menhId: MENH_PHU_DANG_HOA
  },
  {
    id: 43,
    name: 'Bính Ngọ',
    canId: CAN_BINH,
    chiId: CHI_NGO,
    menhId: MENH_THIEN_HA_THUY
  },
  {
    id: 44,
    name: 'Đinh Mùi',
    canId: CAN_DINH,
    chiId: CHI_MUI,
    menhId: MENH_THIEN_HA_THUY
  },
  {
    id: 45,
    name: 'Mậu Thân',
    canId: CAN_MAU,
    chiId: CHI_THAN,
    menhId: MENH_DAI_TRACH_THO
  },
  {
    id: 46,
    name: 'Kỷ Dậu',
    canId: CAN_KY,
    chiId: CHI_DAU,
    menhId: MENH_DAI_TRACH_THO
  },
  {
    id: 47,
    name: 'Canh Tuất',
    canId: CAN_CANH,
    chiId: CHI_TUAT,
    menhId: MENH_THOA_XUYEN_KIM
  },
  {
    id: 48,
    name: 'Tân Hợi',
    canId: CAN_TAN,
    chiId: CHI_HOI,
    menhId: MENH_THOA_XUYEN_KIM
  },
  {
    id: 49,
    name: 'Nhâm Tý',
    canId: CAN_NHAM,
    chiId: CHI_TYS,
    menhId: MENH_TANG_DO_MOC
  },
  {
    id: 50,
    name: 'Quý Sửu',
    canId: CAN_QUY,
    chiId: CHI_SUU,
    menhId: MENH_TANG_DO_MOC
  },
  {
    id: 51,
    name: 'Giáp Dần',
    canId: CAN_GIAP,
    chiId: CHI_DAN,
    menhId: MENH_DAI_KHE_THUY
  },
  {
    id: 52,
    name: 'Ất Mão',
    canId: CAN_AT,
    chiId: CHI_MAO,
    menhId: MENH_DAI_KHE_THUY
  },
  {
    id: 53,
    name: 'Bính Thìn',
    canId: CAN_BINH,
    chiId: CHI_THIN,
    menhId: MENH_SA_TRUNG_THO
  },
  {
    id: 54,
    name: 'Đinh Tỵ',
    canId: CAN_DINH,
    chiId: CHI_TYJ,
    menhId: MENH_SA_TRUNG_THO
  },
  {
    id: 55,
    name: 'Mậu Ngọ',
    canId: CAN_MAU,
    chiId: CHI_NGO,
    menhId: MENH_THIEN_THUONG_HOA
  },
  {
    id: 56,
    name: 'Kỷ Mùi',
    canId: CAN_KY,
    chiId: CHI_MUI,
    menhId: MENH_THIEN_THUONG_HOA
  },
  {
    id: 57,
    name: 'Canh Thân',
    canId: CAN_CANH,
    chiId: CHI_THAN,
    menhId: MENH_THACH_LUU_MOC
  },
  {
    id: 58,
    name: 'Tân Dậu',
    canId: CAN_TAN,
    chiId: CHI_DAU,
    menhId: MENH_THACH_LUU_MOC
  },
  {
    id: 59,
    name: 'Nhâm Tuất',
    canId: CAN_NHAM,
    chiId: CHI_TUAT,
    menhId: MENH_DAI_HAI_THUY
  },
  {
    id: 60,
    name: 'Quý Hợi',
    canId: CAN_QUY,
    chiId: CHI_HOI,
    menhId: MENH_DAI_HAI_THUY
  }
]

/**
 * Danh sách mã code của các loại ngày trong bộ lọc
 */
// Tốt
export const NGAY_BAT_TUONG = 'ngayBatTuong'
export const NGAY_SAO_SAT_CONG = 'ngaySaoSatCong'
export const NGAY_SAO_TRUC_TINH = 'ngaySaoTrucTinh'
export const NGAY_SAO_NHAN_DUYEN = 'ngaySaoNhanDuyen'
export const NGAY_SAO_THIEN_AN = 'ngaySaoThienAn'
export const NGAY_SAO_THIEN_THUY = 'ngaySaoThienThuy'
export const NGAY_SAO_NGU_HOP = 'ngaySaoNguHop'
export const NGAY_SAO_THIEN_DUC = 'ngaySaoThienDuc'
export const NGAY_SAO_THIEN_DUC_HOP = 'ngaySaoThienDucHop'
export const NGAY_SAO_NGUYET_DUC = 'ngaySaoNguyetDuc'
export const NGAY_SAO_NGUYET_DUC_HOP = 'ngaySaoNguyetDucHop'
export const NGAY_SAO_THIEN_HY = 'ngaySaoThienHy'
export const NGAY_SAO_THIEN_PHU = 'ngaySaoThienPhu'
export const NGAY_SAO_THIEN_QUY = 'ngaySaoThienQuy'
export const NGAY_SAO_THIEN_XA = 'ngaySaoThienXa'
export const NGAY_SAO_SINH_KHI = 'ngaySaoSinhKhi'
export const NGAY_SAO_THIEN_PHUC = 'ngaySaoThienPhuc'
export const NGAY_SAO_THIEN_THANH = 'ngaySaoThienThanh'
export const NGAY_SAO_THIEN_QUAN = 'ngaySaoThienQuan'
export const NGAY_SAO_THIEN_MA = 'ngaySaoThienMa'
export const NGAY_SAO_THIEN_TAI = 'ngaySaoThienTai'
export const NGAY_SAO_DIA_TAI = 'ngaySaoDiaTai'
export const NGAY_SAO_NGUYET_TAI = 'ngaySaoNguyetTai'
export const NGAY_SAO_NGUYET_AN = 'ngaySaoNguyetAn'
export const NGAY_SAO_NGUYET_KHONG = 'ngaySaoNguyetKhong'
export const NGAY_SAO_MINH_TINH = 'ngaySaoMinhTinh'
export const NGAY_SAO_THANH_TAM = 'ngaySaoThanhTam'
export const NGAY_SAO_NGU_PHU = 'ngaySaoNguPhu'
export const NGAY_SAO_LOC_KHO = 'ngaySaoLocKho'
export const NGAY_SAO_PHUC_SINH = 'ngaySaoPhucSinh'
export const NGAY_SAO_CAT_KHANH = 'ngaySaoCatKhanh'
export const NGAY_SAO_AM_DUC = 'ngaySaoAmDuc'
export const NGAY_SAO_U_VI_TINH = 'ngaySaoUViTinh'
export const NGAY_SAO_MAN_DUC_TINH = 'ngaySaoManDucTinh'
export const NGAY_SAO_KINH_TAM = 'ngaySaoKinhTam'
export const NGAY_SAO_TUE_HOP = 'ngaySaoTueHop'
export const NGAY_SAO_NGUYET_GIAI = 'ngaySaoNguyetGiai'
export const NGAY_SAO_QUAN_NHAT = 'ngaySaoQuanNhat'
export const NGAY_SAO_HOAT_DIEU = 'ngaySaoHoatDieu'
export const NGAY_SAO_GIAI_THAN = 'ngaySaoGiaiThan'
export const NGAY_SAO_PHO_BO = 'ngaySaoPhoBo'
export const NGAY_SAO_ICH_HAU = 'ngaySaoIchHau'
export const NGAY_SAO_TUC_THE = 'ngaySaoTucThe'
export const NGAY_SAO_YEU_YEN = 'ngaySaoYeuYen'
export const NGAY_SAO_DICH_MA = 'ngaySaoDichMa'
export const NGAY_SAO_TAM_HOP = 'ngaySaoTamHop'
export const NGAY_SAO_LUC_HOP = 'ngaySaoLucHop'
export const NGAY_SAO_MAU_THUONG = 'ngaySaoMauThuong'
export const NGAY_SAO_PHUC_HAU = 'ngaySaoPhucHau'
export const NGAY_SAO_DAI_HONG_SA = 'ngaySaoDaiHongSa'
export const NGAY_SAO_DAN_NHAT_THOI_DUC = 'ngaySaoDanNhatThoiDuc'
export const NGAY_SAO_HOANG_AN = 'ngaySaoHoangAn'
export const NGAY_SAO_THANH_LONG = 'ngaySaoThanhLong'
export const NGAY_SAO_MINH_DUONG = 'ngaySaoMinhDuong'
export const NGAY_SAO_KIM_DUONG = 'ngaySaoKimDuong'
export const NGAY_SAO_NGOC_DUONG = 'ngaySaoNgocDuong'

// Xấu
export const NGAY_SAT_CHU = 'ngaySatChu'
export const NGAY_THANG_SAT_CHU = 'ngayThangSatChu'
export const NGAY_SAT_CHU_TRONG_4_MUA = 'NgaySatChuTrong4Mua'
export const NGAY_BON_MUA_SAT_CHU = 'ngayBonMuaSatChu'
export const NGAY_THO_TU = 'ngayThoTu'
export const NGAY_LY_SAO = 'ngayLySao'
export const NGAY_VANG_VONG = 'ngayVangVong'
export const NGAY_XICH_TONG_TU = 'ngayXichTongTu'
export const NGAY_KHONG_VONG = 'ngayKhongVong'
export const NGAY_HUNG_BAI = 'ngayHungBai'
export const NGAY_XICH_KHAU = 'ngayXichKhau'
export const NGAY_HOANG_OC_4_MUA = 'ngayHoangOc4Mua'
export const NGAY_GIA_OC_4_MUA = 'ngayGiaOc4Mua'
export const NGAY_HOA_TINH = 'ngayHoaTinh'
export const NGAY_THIEN_HOA = 'ngayThienHoa'
export const NGAY_DIA_HOA = 'ngayDiaHoa'
export const NGAY_DOC_HOA = 'ngayDocHoa'
export const NGAY_THIEN_TAI_DAI_HOA = 'ngayThienTaiDaiHoa'
export const NGAY_SAO_HOA = 'ngaySaoHoa'
export const NGAY_LOI_GIANG = 'ngayLoiGiang'
export const NGAY_LOI_DINH_CHINH_SAT = 'ngayLoiDinhChinhSat'
export const NGAY_LOI_DINH_SAT_CHU = 'ngayLoiDinhSatChu'
export const NGAY_SAT_SU = 'ngaySatSu'
export const NGAY_THANG_SAT_SU = 'ngayThangSatSu'
export const NGAY_NGUYET_KY = 'ngayNguyetKy'
export const NGAY_THIEN_MA_TAM_CUONG = 'ngayThienMaTamCuong'
export const NGAY_TAM_NUONG_SAT = 'ngayTamNuongSat'
export const NGAY_BAT_TUONG_XAU = 'ngayBatTuongXau'
export const NGAY_NGUYET_TAN = 'ngayNguyetTan'
export const NGAY_NGUU_LANG_CHUC_NU_4_MUA = 'ngayNguuLangChucNu4Mua'
export const NGAY_KHONG_SANG4_MUA = 'ngayKhongSang4Mua'
export const NGAY_KHONG_PHONG4_MUA = 'ngayKhongPhong4Mua'
export const NGAY_TU_LA_DOAT_GIA = 'ngayTuLaDoatGia'
export const NGAY_THAP_AC_DAI_BAI = 'ngayThapAcDaiBai'
export const NGAY_NHAP_MO4_MUA = 'ngayNhapMo4Mua'
export const NGAY_THAN_HAO = 'ngayThanHao'
export const NGAY_QUY_KHOC = 'ngayQuyKhoc'
export const NGAY_THIEN_MON_BE_TAC = 'ngayThienMonBeTac'
export const NGAY_TU_LY = 'ngayTuLy'
export const NGAY_TU_TUYET = 'ngayTuTuyet'
export const NGAY_SAO_CUU_THO_QUY = 'ngaySaoCuuThoQuy'
export const NGAY_SAO_THIEN_CUONG = 'ngaySaoThienCuong'
export const NGAY_SAO_THIEN_LAI = 'ngaySaoThienLai'
export const NGAY_SAO_TIEU_HONG_SA = 'ngaySaoTieuHongSa'
export const NGAY_SAO_DAI_HAO = 'ngaySaoDaiHao'
export const NGAY_SAO_TIEU_HAO = 'ngaySaoTieuHao'
export const NGAY_SAO_NGUYET_PHA = 'ngaySaoNguyetPha'
export const NGAY_SAO_KIEP_SAT = 'ngaySaoKiepSat'
export const NGAY_SAO_DIA_PHA = 'ngaySaoDiaPha'
export const NGAY_SAO_THO_PHU = 'ngaySaoThoPhu'
export const NGAY_SAO_THO_ON = 'ngaySaoThoOn'
export const NGAY_SAO_THIEN_ON = 'ngaySaoThienOn'
export const NGAY_SAO_THU_TU = 'ngaySaoThuTu'
export const NGAY_SAO_HOANG_VU = 'ngaySaoHoangVu'
export const NGAY_SAO_THIEN_TAC = 'ngaySaoThienTac'
export const NGAY_SAO_DIA_TAC = 'ngaySaoDiaTac'
export const NGAY_SAO_HOA_TAI = 'ngaySaoHoaTai'
export const NGAY_SAO_NGUYET_YEM = 'ngaySaoNguyetYem'
export const NGAY_SAO_NGUYET_HU = 'ngaySaoNguyetHu'
export const NGAY_SAO_HOANG_SA = 'ngaySaoHoangSa'
export const NGAY_SAO_LUC_BAT_THANH = 'ngaySaoLucBatThanh'
export const NGAY_SAO_NHAN_CACH = 'ngaySaoNhanCach'
export const NGAY_SAO_THAN_CACH = 'ngaySaoThanCach'
export const NGAY_SAO_PHI_MA_SAT = 'ngaySaoPhiMaSat'
export const NGAY_SAO_NGU_QUY = 'ngaySaoNguQuy'
export const NGAY_SAO_BANG_TIEU_NGOA_HAM = 'ngaySaoBangTieuNgoaHam'
export const NGAY_SAO_HA_KHOI_CAU_GIAO = 'ngaySaoHaKhoiCauGiao'
export const NGAY_SAO_CUU_KHONG = 'ngaySaoCuuKhong'
export const NGAY_SAO_TRUNG_TANG = 'ngaySaoTrungTang'
export const NGAY_SAO_TRUNG_PHUC = 'ngaySaoTrungPhuc'
export const NGAY_SAO_CHU_TUOC_HAC_DAO = 'ngaySaoChuTuocHacDao'
export const NGAY_SAO_BACH_HO = 'ngaySaoBachHo'
export const NGAY_SAO_HUYEN_VU = 'ngaySaoHuyenVu'
export const NGAY_SAO_CAU_TRAN = 'ngaySaoCauTran'
export const NGAY_SAO_LOI_CONG = 'ngaySaoLoiCong'
export const NGAY_SAO_CO_THAN = 'ngaySaoCoThan'
export const NGAY_SAO_QUA_TU = 'ngaySaoQuaTu'
export const NGAY_SAO_NGUYET_HINH = 'ngaySaoNguyetHinh'
export const NGAY_SAO_TOI_CHI = 'ngaySaoToiChi'
export const NGAY_SAO_NGUYET_KIEN_CHUYEN_SAT = 'ngaySaoNguyetKienChuyenSat'
export const NGAY_SAO_THIEN_DIA_CHINH_CHUYEN = 'ngaySaoThienDiaChinhChuyen'
export const NGAY_SAO_THIEN_DIA_CHUYEN_SAT = 'ngaySaoThienDiaChuyenSat'
export const NGAY_SAO_LO_BAN_SAT = 'ngaySaoLoBanSat'
export const NGAY_SAO_PHU_DAU_SAT = 'ngaySaoPhuDauSat'
export const NGAY_SAO_TAM_TANG = 'ngaySaoTamTang'
export const NGAY_SAO_NGU_HU = 'ngaySaoNguHu'
export const NGAY_SAO_TU_THOI_DAI_MO = 'ngaySaoTuThoiDaiMo'
export const NGAY_SAO_THO_CAM = 'ngaySaoThoCam'
export const NGAY_SAO_LY_SANG = 'ngaySaoLySang'
export const NGAY_SAO_TU_THOI_CO_QUA = 'ngaySaoTuThoiCoQua'
export const NGAY_SAO_AM_THAC = 'ngaySaoAmThac'
export const NGAY_SAO_DUONG_THAC = 'ngaySaoDuongThac'
export const NGAY_SAO_QUY_KHOC = 'ngaySaoQuyKhoc'

// /**
//  * Loại bộ lọc
//  */
// export const LIKE_MONTH_LIKE_DATE = 1
// export const LIKE_SEASON_LIKE_DATE = 2
