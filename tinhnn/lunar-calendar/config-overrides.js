const { override, fixBabelImports, addLessLoader } = require('customize-cra');

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),

  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      '@font-family': 'Arial, "Helvetica Neue", Helvetica, sans-serif;',
      '@font-size-base': '16px',
      '@font-size-sm': '14px',
      '@text-color': '#000'
    },
  }),
);
