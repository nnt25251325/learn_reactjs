import React, { Suspense } from 'react';
import { StoreProvider } from 'easy-peasy';

import 'antd/dist/antd.css';
import '@Assets/scss/styles.scss';

import store from '@Core/create-store';

// import Layout from './layout';
import Pages from './pages';

const App = () => {
  return (
    <div className="App">
      <StoreProvider store={store}>
        <Suspense fallback={<div>Loading</div>}>
          {/* <Layout> */}
          <Pages />
          {/* </Layout> */}
        </Suspense>
      </StoreProvider>
    </div>
  );
};

export default App;
