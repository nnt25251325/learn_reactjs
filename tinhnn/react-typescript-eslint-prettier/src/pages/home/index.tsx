import React from 'react';
import { Route, Switch } from 'react-router';

import store from '../../core/create-store';
import { model } from './model';
import List from './components/p-list';
import Detail from './components/p-detail';

import './style.scss';

store.addModel('home', model);

export default () => {
  return (
    <Switch>
      <Route path="/home/detail" component={Detail} />
      <Route path="/home" component={List} />
    </Switch>
  );
};
