import React, { lazy } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import MainHeader from '@Components/MainHeader';

const Login = lazy(() => import('./login'));
const Home = lazy(() => import('./home'));
const Contact = lazy(() => import('./contact'));
const Page404 = lazy(() => import('./page-404'));

export default () => {
  return (
    <>
      <MainHeader />

      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/contact" component={Contact} />
        <Route path="/404" component={Page404} />
        <Redirect to="/404" />
      </Switch>
    </>
  );
};
