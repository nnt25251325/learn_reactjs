export function onFullfilled(response: any) {
  return Promise.resolve(response);
}

export function onRejected(error: any) {
  // if (error) {
  //   const { response } = error;

  //   if (response.status === 401) {
  //     window.location.href = '/login';
  //   }

  //   return Promise.reject(response.data);
  // }

  return Promise.reject();
}
