import React from 'react';
import { Container } from 'react-bootstrap';

export default () => {
  return (
    <div className="p-home">
      <div className="title-page">
        <Container>
          <h1>Home</h1>
        </Container>
      </div>

      <img
        src="https://loremflickr.com/1920/500"
        alt="Demo"
        className="w-100"
      />

      <div style={{ display: 'flex', flexWrap: 'nowrap' }}>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c1"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c2"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c3"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c4"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c5"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c6"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c7"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c8"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c9"></div>
        <div style={{ flex: '1 1 auto', height: 100 }} className="c10"></div>
      </div>
    </div>
  );
};
