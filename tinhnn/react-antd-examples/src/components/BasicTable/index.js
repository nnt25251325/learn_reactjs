import React, { useState, useEffect } from 'react';
import { Table, Spin } from 'antd';
import { Loading3QuartersOutlined } from '@ant-design/icons';

import { PAGE_SIZES, PAGE_SIZE, DEFAULT_SORT } from '../../constants';

import './style.scss';

export const BasicTable = ({
  columns = [],
  data = [],
  total = 0,
  currentPage = 1,
  currentLimit = PAGE_SIZE,
  rowKey = 'id',
  showTotal = (totalnumber, range) =>
    `${range[0]}-${range[1]} of ${totalnumber} record(s)`, // boolean or callback function
  defaultSortBy = DEFAULT_SORT,
  loading = false,
  isResponsive = true,
  className = '',
  restPagination,
  onSortChange,
  onTableChange,
  ...rest
}) => {
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(PAGE_SIZE);

  /**
   * Watching change of current page
   */
  useEffect(() => setPage(currentPage), [currentPage]);

  /**
   * Watching change of current limit
   */
  useEffect(() => setLimit(currentLimit), [currentLimit]);

  /**
   * Handle table change
   */
  const handleTableChange = (pagination, filters, sorter) => {
    const page = pagination.current;
    const limit = pagination.pageSize;
    const params = {
      page,
      limit,
      order: [defaultSortBy]
    };

    setPage(page);
    setLimit(limit);

    if (sorter.order) {
      params.order = [
        [sorter.field, sorter.order === 'ascend' ? 'ASC' : 'DESC']
      ];
    }

    if (typeof onSortChange === 'function') {
      onSortChange(sorter);
    }

    onTableChange(params);
  };

  return (
    <div className={isResponsive ? 'table-responsive' : ''}>
      {Array.isArray(data) && limit && (
        <Table
          dataSource={data}
          columns={columns}
          rowKey={rowKey} // Fix Warning: Each child in a list should have a unique "key" prop and design
          loading={{
            spinning: loading,
            indicator: <Spin indicator={<Loading3QuartersOutlined spin />} />
          }}
          pagination={{
            total,
            current: page,
            pageSize: limit,
            pageSizeOptions: PAGE_SIZES,
            showSizeChanger: true,
            showTotal,
            ...restPagination
          }}
          className={`c-basic-table ${className}`}
          onChange={handleTableChange}
          {...rest}
        />
      )}
    </div>
  );
};
