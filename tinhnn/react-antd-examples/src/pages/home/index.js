import React from 'react';
import { Card } from 'antd';

import store from '../../core/create-store';
import { model } from './model';
import { BasicBreadcrumb } from '../../components';

store.addModel('home', model);

export default () => {
  return (
    <div>
      <BasicBreadcrumb />

      <h3 className="ant-typography mb-4">Home</h3>

      <Card>
        <h1>Welcome to react examples</h1>
      </Card>
    </div>
  );
};
