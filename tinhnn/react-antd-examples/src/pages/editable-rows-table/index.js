/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import { Table, Input, InputNumber, Form, Button } from 'antd';

import { BasicBreadcrumb } from '../../components';

const originData = [];

for (let i = 0; i < 100; i++) {
  originData.push({
    key: i.toString(),
    name: `Edrward ${i}`,
    age: 32,
    address: `London Park no. ${i}`
  });
}

export default () => {
  const [form] = Form.useForm();
  const [data, setData] = useState(originData);
  const [editingKey, setEditingKey] = useState('');

  const isEditing = record => {
    return record.key === editingKey;
  };

  const columns = [
    {
      title: 'name',
      dataIndex: 'name',
      width: '25%',
      editable: true
    },
    {
      title: 'age',
      dataIndex: 'age',
      width: '15%',
      editable: true
    },
    {
      title: 'address',
      dataIndex: 'address',
      width: '40%',
      editable: true
    },
    {
      title: 'operation',
      dataIndex: 'operation',
      render: (_, record) => {
        const editable = isEditing(record);

        return editable ? (
          <>
            <Button
              size="small"
              type="primary"
              onClick={() => onSave(record.key)}
            >
              Save
            </Button>
            &nbsp;&nbsp;&nbsp;
            <Button size="small" onClick={onCancel}>
              Cancel
            </Button>
          </>
        ) : (
          <Button
            size="small"
            type="primary"
            disabled={editingKey !== ''}
            onClick={() => onEdit(record)}
          >
            Edit
          </Button>
        );
      }
    }
  ];

  const mergedColumns = columns.map(item => {
    if (!item.editable) return item;

    return {
      ...item,
      onCell: record => ({
        record,
        dataIndex: item.dataIndex,
        title: item.title,
        editing: isEditing(record),
        onClick: () => {
          onEdit(record, 'CELL_CLICKED');
        }
        // onKeyUp: e => { if (e.keyCode===13 && e.ctrlKey) onSave(record.key) }
      })
    };
  });

  const EditableCell = ({
    index,
    dataIndex,
    record,
    editing,
    children,
    ...restProps
  }) => {
    const inputNode =
      dataIndex === 'age' ? (
        <InputNumber onClick={e => e.stopPropagation()} />
      ) : dataIndex === 'address' ? (
        <Input.TextArea rows="3" onClick={e => e.stopPropagation()} />
      ) : (
        <Input onClick={e => e.stopPropagation()} />
      );

    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item
            name={dataIndex}
            style={{ margin: 0 }}
            rules={[
              {
                required: dataIndex === 'name',
                message: 'This field is required'
              }
            ]}
          >
            {inputNode}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  const onEdit = async (record, actionType) => {
    console.log('record', record);

    if (
      actionType === 'CELL_CLICKED' &&
      editingKey &&
      editingKey !== record.key
    ) {
      console.log(122111212, editingKey);
      const validate = await onSave(editingKey);

      if (!validate) return;

      // setTimeout(() => {
      //   form.setFieldsValue({
      //     name: '',
      //     age: '',
      //     address: '',
      //     ...record,
      //   });

      //   setEditingKey(record.key);
      // }, 1000);

      // return;
    }

    form.setFieldsValue({
      name: '',
      age: '',
      address: '',
      ...record
    });

    setEditingKey(record.key);
  };

  const onSave = async key => {
    try {
      const row = await form.validateFields();

      console.log('row', row);
      const newData = [...data];
      const index = newData.findIndex(item => key === item.key);

      if (index > -1) {
        const item = newData[index];
        console.log('item', item);
        newData.splice(index, 1, { ...item, ...row });
        console.log('newData 1', newData);
        setData(newData);
        setEditingKey('');
      } else {
        newData.push(row);
        console.log('newData 2', newData);
        setData(newData);
        setEditingKey('');
      }

      return true;
    } catch (err) {
      console.log('Validate Failed:', err);
      return false;
    }
  };

  const onCancel = () => {
    console.log('onCancel');
    setEditingKey('');
  };

  return (
    <div className="p-editable-rows-table">
      <BasicBreadcrumb />

      <h3 className="ant-typography mb-4">Editable rows table</h3>

      <div>editingKey: {editingKey}</div>
      <Form form={form} component={false}>
        <Table
          dataSource={data}
          columns={mergedColumns}
          components={{
            body: {
              cell: EditableCell
            }
          }}
          bordered
          pagination={{
            onChange: onCancel
          }}
        />
      </Form>
    </div>
  );
};
