import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, Input, Form, Checkbox } from 'antd';
import { UserOutlined, LockOutlined, LoginOutlined } from '@ant-design/icons';

import store from '../../core/create-store';
import { model } from './model';

store.addModel('login', model);

export default () => {
  const [form] = Form.useForm();

  /**
   * On login
   */
  const onLogin = values => {
    console.log('values', values);

    window.location.href = '/';
  };

  return (
    <Card className="card-login">
      <Form
        form={form}
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 24 }}
        className="pt-2 pb-2"
        onFinish={onLogin}
      >
        <div className="text-center mb-3">
          <div
            to="/"
            style={{ fontSize: '36px', fontWeight: 'bold', color: '#636363' }}
          >
            LOGO
          </div>
        </div>

        <div className="form-wrapper">
          <Form.Item
            label="Username"
            name="username"
            rules={[{ required: true, message: 'Please fill in this field' }]}
          >
            <Input
              prefix={<UserOutlined />}
              placeholder="Enter Username"
              className="only-bd-bottom"
            />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: 'Please fill in this field' }]}
          >
            <Input.Password
              prefix={<LockOutlined />}
              placeholder="Enter Password"
              className="only-bd-bottom"
            />
          </Form.Item>

          <div className="ant-row ant-row-space-between ant-row-middle ant-form-item">
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Link to="/forgot-password">Forgot password</Link>
          </div>

          <Form.Item className="mb-0">
            <Button
              type="primary"
              htmlType="submit"
              shape="round"
              size="large"
              block
              icon={<LoginOutlined />}
            >
              Log in
            </Button>
          </Form.Item>
        </div>
      </Form>
    </Card>
  );
};
