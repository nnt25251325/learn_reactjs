import React, { useState } from 'react';
import { Input, Tree } from 'antd';
import { FolderOutlined, FilterOutlined } from '@ant-design/icons';

import 'antd/dist/antd.css';
import './style.scss';

import {
  debounce,
  reAddExpandedKeys,
  findItemAndParentsOnTree,
  searchItemsAndParentKeysOnTree
} from '../../helpers/utils';
import { BasicBreadcrumb } from '../../components';

const { DirectoryTree } = Tree;
let typingTimerOfSearch = 0;

const listMock = [
  {
    title: 'Project Requirements (1-1)',
    key: '1-1',
    parentKey: null,
    icon: <FolderOutlined />,
    isFirstParent: true,
    children: [
      {
        title: 'BDD Requirements (1-1-1)',
        key: '1-1-1',
        parentKey: '1-1',
        isLeaf: true
      },
      {
        title: 'Email Requirements (1-1-2)',
        key: '1-1-2',
        parentKey: '1-1',
        isLeaf: true
      },
      {
        title: 'New (1-1-3)',
        key: '1-1-3',
        parentKey: '1-1',
        isLeaf: true
      }
    ]
  },
  {
    title: 'Requirement QQQ (1-2)',
    key: '1-2',
    parentKey: null,
    isFirstParent: true,
    icon: <FolderOutlined />,
    children: [
      {
        title: 'Email Requirements (1-2-1)',
        key: '1-2-1',
        parentKey: '1-2',
        isLeaf: true
      },
      {
        title: 'New Jira Sync (1-2-2)',
        key: '1-2-2',
        parentKey: '1-2',
        icon: <FolderOutlined />,
        children: [
          {
            title: 'Test Requirement 1 (1-2-2-1)',
            key: '1-2-2-1',
            parentKey: '1-2-2',
            isLeaf: true
          },
          {
            title: 'New Jira Sync (1-2-2-2)',
            key: '1-2-2-2',
            parentKey: '1-2-2',
            isLeaf: true
          },
          {
            title: 'Test 5 (1-2-2-3)',
            key: '1-2-2-3',
            parentKey: '1-2-2',
            icon: <FolderOutlined />,
            children: [
              {
                title: 'Test 51 (1-2-2-3-1)',
                key: '1-2-2-3-1',
                parentKey: '1-2-2-3',
                isLeaf: true
              },
              {
                title: 'Test 56 (1-2-2-3-2)',
                key: '1-2-2-3-2',
                parentKey: '1-2-2-3',
                isLeaf: true
              }
            ]
          }
        ]
      }
    ]
  },
  {
    title: 'Imported (1-3)',
    key: '1-3',
    parentKey: null,
    isFirstParent: true,
    isLeaf: true
  }
];

export default () => {
  // Other state
  const [treeData, setTreeData] = useState(listMock);
  const [expandedKeys, setExpandedKeys] = useState([]);
  const [autoExpandParent, setAutoExpandParent] = useState(true); // For filter on tree
  const [searchValue, setSearchValue] = useState(''); // For filter on tree
  const [isNoItemMatch, setIsNoItemMatch] = useState(false); // For filter on tree

  /**
   * Tree title render
   *
   * @return {Object} - Element
   */
  const TreeTitleRender = item => {
    if (item.isFirstParent) {
      return <strong>{item.title}</strong>;
    }

    return <div>{item.title}</div>;
  };

  /**
   * Convert tree data to highlight
   *
   * For filter on tree
   */
  const convertTreeDataToHighlight = (data, keyword) => {
    if (typeof keyword !== 'string') return;

    const str = keyword.toString().toLowerCase();

    const list = data.map(item => {
      const index =
        typeof item.title === 'string' &&
        item.title &&
        item.title.toString().toLowerCase().indexOf(str);
      const beforeStr = item.title.substr(0, index);
      const currentStr = item.title.substr(index, str.length);
      const afterStr = item.title.substr(index + str.length);
      const hasChildren = Array.isArray(item.children) && item.children.length;

      const title =
        index > -1 ? (
          <>
            {beforeStr}
            <span className="text-search-highlight">{currentStr}</span>
            {afterStr}
          </>
        ) : (
          <>{item.title}</>
        );

      return {
        ...item,
        title,
        children: hasChildren
          ? convertTreeDataToHighlight(item.children, keyword)
          : []
      };
    });

    return list;
  };

  /**
   * On filter on tree
   *
   * For filter on tree
   */
  const onFilterOnTree = (val = '') => {
    clearTimeout(typingTimerOfSearch);

    typingTimerOfSearch = setTimeout(
      debounce(() => {
        const result = searchItemsAndParentKeysOnTree(treeData, val);
        const { items, parentKeys } = result;
        const expandedKeys =
          Array.isArray(parentKeys) && parentKeys.length ? parentKeys : [];

        setExpandedKeys(val !== '' ? expandedKeys : []);
        setSearchValue(val);
        setIsNoItemMatch(!(Array.isArray(items) && items.length));
        setAutoExpandParent(true);
      }),
      300
    );
  };

  /**
   * Handle select tree item
   */
  const handleSelectTreeItem = keys => {
    reAddExpandedKeys(keys[0], expandedKeys, list => setExpandedKeys(list));

    if (Array.isArray(treeData)) {
      const result = findItemAndParentsOnTree(treeData, keys[0], 'key');
      console.log('handleSelectTreeItem', result);
    }
  };

  return (
    <div className="c-directory-tree">
      <BasicBreadcrumb />

      <h3 className="ant-typography mb-4">Filter on tree</h3>

      <div style={{ padding: '10px' }}>
        <Input
          allowClear
          suffix={<FilterOutlined />}
          placeholder="Filter here"
          onChange={e => onFilterOnTree(e.target.value)}
        />
        <br />
        {isNoItemMatch && <div>No items match</div>}
      </div>

      <DirectoryTree
        treeData={convertTreeDataToHighlight(treeData, searchValue)}
        expandedKeys={expandedKeys}
        titleRender={TreeTitleRender}
        autoExpandParent={autoExpandParent} // For filter on tree
        onExpand={keys => {
          setExpandedKeys(keys);
          setAutoExpandParent(false); // For filter on tree
        }}
        onSelect={handleSelectTreeItem}
      />
    </div>
  );
};
