import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { RouterLoading } from '../components/RouterLoading';
import Home from './home';
import Page404 from './page-404';
import Login from './login';
import ForgotPassword from './forgot-password';
import User from './user';
import DragAndDrop from './drag-and-drop';
import EditableRowsTable from './editable-rows-table';
import Transfer from './transfer';
import ResizePanels from './resize-panels';
import FilterOnTree from './filter-on-tree';

export default () => (
  <Suspense fallback={<RouterLoading />}>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/login" component={Login} />
      <Route path="/forgot-password" component={ForgotPassword} />
      <Route path="/user" component={User} />
      <Route path="/drag-and-drop" component={DragAndDrop} />
      <Route path="/editable-rows-table" component={EditableRowsTable} />
      <Route path="/filter-on-tree" component={FilterOnTree} />
      <Route path="/resize-panels" component={ResizePanels} />
      <Route path="/transfer" component={Transfer} />
      <Route path="/404" component={Page404} />
      <Redirect to="/404" />
    </Switch>
  </Suspense>
);
