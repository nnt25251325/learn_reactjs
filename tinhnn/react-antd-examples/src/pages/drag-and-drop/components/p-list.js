import React from 'react';
import { Button } from 'antd';

export default () => {
  return (
    <div>
      <Button href="/drag-and-drop/drag-table" type="primary" className="mx-2">
        Drag table
      </Button>

      <Button href="/drag-and-drop/multi-drag" type="primary" className="mx-2">
        Multi drag
      </Button>

      <Button
        href="/drag-and-drop/multi-table-drag"
        type="primary"
        className="mx-2"
      >
        Multi table drag
      </Button>
    </div>
  );
};
