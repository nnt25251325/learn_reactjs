import React, { useCallback, useEffect, useState } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';

import { Column } from './column';
import { mutliDragAwareReorder, multiSelectTo as multiSelect } from './utils';
import './style.css';
import { BasicBreadcrumb } from '../../../../components';

const entitiesMock = {
  tasks: [
    { id: '0', title: 'Task 0' },
    { id: '1', title: 'Task 1' },
    { id: '2', title: 'Task 2' },
    { id: '3', title: 'Task 3' },
    { id: '4', title: 'Task 4' },
    { id: '5', title: 'Task 5' },
    { id: '6', title: 'Task 6' },
    { id: '7', title: 'Task 7' },
    { id: '8', title: 'Task 8' },
    { id: '9', title: 'Task 9' },
    { id: '10', title: 'Task 10' },
    { id: '11', title: 'Task 11' },
    { id: '12', title: 'Task 12' },
    { id: '13', title: 'Task 13' },
    { id: '14', title: 'Task 14' },
    { id: '15', title: 'Task 15' },
    { id: '16', title: 'Task 16' },
    { id: '17', title: 'Task 17' },
    { id: '18', title: 'Task 18' },
    { id: '19', title: 'Task 19' }
  ],
  columnKey: ['todo1', 'progress1', 'done1'],
  columns: {
    todo1: {
      id: 'todo1',
      title: 'To do',
      taskIds: [
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        '11',
        '12',
        '13',
        '14',
        '15',
        '16',
        '17',
        '18',
        '19'
      ]
    },
    progress1: {
      id: 'progress1',
      title: 'Progress',
      taskIds: []
    },
    done1: {
      id: 'done1',
      title: 'Done',
      taskIds: []
    }
  }
};

const MultiDrag = () => {
  const [entities, setEntities] = useState(entitiesMock);
  const [selectedTaskIds, setSelectedTaskIds] = useState([]);
  const [draggingTaskId, setDraggingTaskId] = useState(null);

  const onWindowKeyDown = useCallback(e => {
    if (e.defaultPrevented) {
      return;
    }

    if (e.key === 'Escape') {
      unselectAll();
    }
  }, []);

  const onWindowClick = useCallback(e => {
    if (e.defaultPrevented) {
      return;
    }

    unselectAll();
  }, []);

  const onWindowTouchEnd = useCallback(e => {
    if (e.defaultPrevented) {
      return;
    }

    unselectAll();
  }, []);

  useEffect(() => {
    window.addEventListener('click', onWindowClick);
    window.addEventListener('keydown', onWindowKeyDown);
    window.addEventListener('touchend', onWindowTouchEnd);

    return () => {
      window.removeEventListener('click', onWindowClick);
      window.removeEventListener('keydown', onWindowKeyDown);
      window.removeEventListener('touchend', onWindowTouchEnd);
    };
  }, [onWindowClick, onWindowKeyDown, onWindowTouchEnd]);

  const getTasks = (entities, id) => {
    return entities.columns[id].taskIds.map(taskId =>
      entities.tasks.find(item => item.id === taskId)
    );
  };

  const onDragStart = start => {
    const id = start.draggableId;
    const selected = selectedTaskIds.find(taskId => taskId === id);

    // if dragging an item that is not selected - unselect all items
    if (!selected) {
      unselectAll();
    }

    setDraggingTaskId(start.draggableId);
  };

  const onDragEnd = result => {
    const destination = result.destination;
    const source = result.source;

    // nothing to do
    if (!destination || result.reason === 'CANCEL') {
      setDraggingTaskId(null);
      return;
    }

    const processed = mutliDragAwareReorder({
      entities,
      selectedTaskIds,
      source,
      destination
    });

    setEntities(processed.entities);
    setDraggingTaskId(null);
  };

  const toggleSelection = taskId => {
    const wasSelected = selectedTaskIds.includes(taskId);

    const newTaskIds = (() => {
      // Task was not previously selected
      // now will be the only selected item
      if (!wasSelected) {
        return [taskId];
      }

      // Task was part of a selected group
      // will now become the only selected item
      if (selectedTaskIds.length > 1) {
        return [taskId];
      }

      // task was previously selected but not in a group
      // we will now clear the selection
      return [];
    })();

    setSelectedTaskIds(newTaskIds);
  };

  const toggleSelectionInGroup = taskId => {
    const index = selectedTaskIds.indexOf(taskId);

    // if not selected - add it to the selected items
    if (index === -1) {
      setSelectedTaskIds([...selectedTaskIds, taskId]);

      return;
    }

    // it was previously selected and now needs to be removed from the group
    const shallow = [...selectedTaskIds];
    shallow.splice(index, 1);

    setSelectedTaskIds(shallow);
  };

  // This behaviour matches the MacOSX finder selection
  const multiSelectTo = newTaskId => {
    const updated = multiSelect(entities, selectedTaskIds, newTaskId);

    if (updated == null) {
      return;
    }

    setSelectedTaskIds(updated);
  };

  const unselectAll = () => {
    setSelectedTaskIds([]);
  };

  return (
    <div className="p-multi-drag">
      <BasicBreadcrumb />

      <h3 className="ant-typography mb-4">Multi drag</h3>

      <DragDropContext onDragStart={onDragStart} onDragEnd={onDragEnd}>
        <div style={{ display: 'flex', userSelect: 'none' }}>
          {entities.columnKey.map(id => (
            <Column
              key={id}
              column={entities.columns[id]}
              tasks={getTasks(entities, id)}
              selectedTaskIds={selectedTaskIds}
              draggingTaskId={draggingTaskId}
              toggleSelection={toggleSelection}
              toggleSelectionInGroup={toggleSelectionInGroup}
              multiSelectTo={multiSelectTo}
            />
          ))}
        </div>
      </DragDropContext>
    </div>
  );
};

export default MultiDrag;
