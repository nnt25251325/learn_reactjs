import React from 'react';
import { Droppable } from 'react-beautiful-dnd';

import { Task } from './task';

export const Column = ({
  column,
  tasks,
  selectedTaskIds,
  draggingTaskId,
  toggleSelection,
  toggleSelectionInGroup,
  multiSelectTo
}) => {
  // console.log('tasks', tasks);

  return (
    <div className="column">
      <h2 style={{ padding: "0 10px" }}>{column.title}</h2>

      <Droppable droppableId={column.id}>
        {(provided, snapshot) => (
          <div
            ref={provided.innerRef}
            {...provided.droppableProps}
            style={{
              padding: 8,
              minHeight: 200,
              transition: 'all 0.4s ease',
              background: snapshot.isDraggingOver ? '#6b778c' : ''
            }}
          >
            {tasks.map((task, index) => {
              const isSelected = selectedTaskIds.some(selectedTaskId => selectedTaskId === task.id);
              const isGhosting = isSelected && Boolean(draggingTaskId) && draggingTaskId !== task.id;

              return (
                <Task
                  index={index}
                  key={task.id}
                  task={task}
                  isSelected={isSelected}
                  isGhosting={isGhosting}
                  selectionCount={selectedTaskIds.length}
                  toggleSelection={toggleSelection}
                  toggleSelectionInGroup={toggleSelectionInGroup}
                  multiSelectTo={multiSelectTo}
                />
              );
            })}
          </div>
        )}
      </Droppable>
    </div>
  );
}
