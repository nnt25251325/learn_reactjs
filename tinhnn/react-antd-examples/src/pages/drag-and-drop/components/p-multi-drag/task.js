import React from 'react';
import { Draggable } from 'react-beautiful-dnd';

// https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
const primaryButton = 0;

export const Task = ({
  index,
  task,
  isSelected,
  isGhosting,
  selectionCount,
  toggleSelection,
  toggleSelectionInGroup,
  multiSelectTo
}) => {
  const onKeyDown = (event, provided, snapshot) => {
    if (event.defaultPrevented) {
      return;
    }

    if (snapshot.isDragging) {
      return;
    }

    // Press Enter
    if (event.keyCode !== 13) {
      return;
    }

    // we are using the event for selection
    event.preventDefault();
    performAction(event);
  };

  // Using onClick as it will be correctly
  // preventing if there was a drag
  const onClick = (event) => {
    if (event.defaultPrevented) {
      return;
    }

    if (event.button !== primaryButton) {
      return;
    }

    // marking the event as used
    event.preventDefault();
    performAction(event);
  };

  const onTouchEnd = (event) => {
    if (event.defaultPrevented) {
      return;
    }

    // marking the event as used
    // we would also need to add some extra logic to prevent the click
    // if this element was an anchor
    event.preventDefault();
    toggleSelectionInGroup(task.id);
  };

  // Determines if the platform specific toggle selection in group key was used
  const wasToggleInSelectionGroupKeyUsed = (event) => {
    const isUsingWindows = navigator.platform.indexOf('Win') >= 0;
    return isUsingWindows ? event.ctrlKey : event.metaKey;
  };

  // Determines if the multiSelect key was used
  const wasMultiSelectKeyUsed = (event) => event.shiftKey;

  const performAction = (event) => {
    if (wasToggleInSelectionGroupKeyUsed(event)) {
      toggleSelectionInGroup(task.id);
      return;
    }

    if (wasMultiSelectKeyUsed(event)) {
      multiSelectTo(task.id);
      return;
    }

    toggleSelection(task.id);
  };

  return (
    <Draggable draggableId={(task.id).toString()} index={index}>
      {(provided, snapshot) => {
        const shouldShowSelection = snapshot.isDragging && selectionCount > 1;

        return (
          <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            className={`item ${
              isSelected ? 'is-selected' : ''
            } ${
              isGhosting ? 'is-ghosting' : ''
            } ${
              snapshot.isDragging ? 'is-dragging' : ''
            }`}
            onClick={onClick}
            onTouchEnd={onTouchEnd}
            onKeyDown={event => onKeyDown(event, provided, snapshot)}
          >
            <div>{task.title}</div>
            {shouldShowSelection && <div className="selection-count">{selectionCount}</div>}
          </div>
        );
      }}
    </Draggable>
  );
}
