import React, { useState } from 'react';
import { Table } from 'antd';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';

import './style.css';
import { BasicBreadcrumb } from '../../../../components';

const DragTable = () => {
  const [dataSource, setDataSource] = useState([
    {
      index: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park'
    },
    {
      index: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      index: '3',
      name: 'Joe Black',
      age: 39,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      index: '4',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park'
    },
    {
      index: '5',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      index: '6',
      name: 'Joe Black',
      age: 39,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      index: '7',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      index: '8',
      name: 'Joe Black',
      age: 39,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      index: '9',
      name: 'Joe Black',
      age: 39,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      index: '10',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      index: '11',
      name: 'Joe Black',
      age: 39,
      address: 'Sidney No. 1 Lake Park'
    }
  ]);

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name'
    },
    {
      title: 'Age',
      dataIndex: 'age'
    },
    {
      title: 'Address',
      dataIndex: 'address'
    }
  ];

  const DroppableTableBody = props => {
    return (
      <Droppable droppableId="droppable">
        {(provided, snapshot) => {
          return (
            <tbody
              {...props}
              {...provided.droppableProps}
              ref={provided.innerRef}
            ></tbody>
          );
        }}
      </Droppable>
    );
  };

  const DragableTableRow = props => {
    const index = dataSource.findIndex(
      row => row.index === props['data-row-key']
    );

    return (
      <Draggable
        key={props['data-row-key']}
        draggableId={props['data-row-key'].toString()}
        index={index}
      >
        {(provided, snapshot) => {
          return (
            <tr
              {...props}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              ref={provided.innerRef}
              className={snapshot.isDragging ? 'row-dragging' : props.className}
            />
          );
        }}
      </Draggable>
    );
  };

  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = result => {
    if (!result.destination) {
      return;
    }

    console.log('result', result);

    const items = reorder(
      dataSource,
      result.source.index,
      result.destination.index
    );

    setDataSource(items);
  };

  return (
    <div className="p-drag-table">
      <BasicBreadcrumb />

      <h3 className="ant-typography mb-4">Drag table</h3>

      <DragDropContext onDragEnd={onDragEnd}>
        <Table
          dataSource={dataSource}
          columns={columns}
          rowKey="index"
          pagination={false}
          components={{
            body: {
              wrapper: DroppableTableBody,
              row: DragableTableRow
            }
          }}
        />
      </DragDropContext>
    </div>
  );
};

export default DragTable;
