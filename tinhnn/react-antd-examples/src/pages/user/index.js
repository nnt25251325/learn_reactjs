import React from 'react';
import { Route, Switch } from 'react-router';
import { useRouteMatch } from 'react-router-dom';

import store from '../../core/create-store';
import { model } from './model';
import List from './components/p-list';
import Sample from './components/p-sample';

import './style.scss';

store.addModel('user', model);

export default () => {
  const match = useRouteMatch();

  return (
    <Switch>
      <Route path={`${match.path}/sample`} component={Sample} />
      <Route exact path="/user*" component={List} />
    </Switch>
  );
};
