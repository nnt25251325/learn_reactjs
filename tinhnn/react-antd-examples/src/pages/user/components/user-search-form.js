import React from 'react';
import { Row, Col, Form, Input, Button, Tooltip } from 'antd';
import {
  SearchOutlined,
  PlusOutlined,
  ReloadOutlined
} from '@ant-design/icons';

import { DEFAULT_SORT, PAGE_SIZE } from '../../../constants';

export const UserSearchForm = ({
  loadingList = false,
  onShowDetail,
  onSearch,
  onReload
}) => {
  const [form] = Form.useForm();

  /**
   * On submit and search
   */
  const onSubmit = values => {
    if (loadingList) {
      return;
    }

    const where = {};

    if (values.id) {
      where.id = values.id;
    }

    if (values.name) {
      where.name = {
        $like: `%${values.name}%`
      };
    }

    onSearch({
      page: 1,
      where,
      actionType: 'SEARCH'
    });
  };

  return (
    <div className="c-user-search-form mb-2">
      <Form form={form} onFinish={onSubmit}>
        <Row justify="end" className="search-basic">
          <Tooltip placement="top" title="Add" destroyTooltipOnHide={true}>
            <Button
              type="primary"
              icon={<PlusOutlined />}
              className="btn-add mt-1 mb-1 ml-3"
              onClick={onShowDetail}
            ></Button>
          </Tooltip>

          <Tooltip placement="top" title="Reload" destroyTooltipOnHide={true}>
            <Button
              type="primary"
              ghost
              icon={<ReloadOutlined />}
              className="btn-reload mt-1 mb-1 ml-1"
              onClick={() => {
                form.resetFields();
                onReload({
                  page: 1,
                  limit: PAGE_SIZE,
                  where: null,
                  order: [DEFAULT_SORT]
                });
              }}
            ></Button>
          </Tooltip>
        </Row>

        <div className="search-advanced">
          <Row gutter={30}>
            <Col>
              <Form.Item label="ID" name="id" className="pr-4">
                <Input />
              </Form.Item>
            </Col>

            <Col>
              <Form.Item label="Name" name="name" className="pr-4">
                <Input />
              </Form.Item>
            </Col>

            <Col>
              <Button
                htmlType="submit"
                type="primary"
                icon={<SearchOutlined />}
                className="min-w-100"
              >
                Search
              </Button>
            </Col>
          </Row>
        </div>
      </Form>
    </div>
  );
};
