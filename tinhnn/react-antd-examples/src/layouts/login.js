import React, { Suspense } from 'react';

import { RouterLoading } from '../components';

export default ({ children }) => {
  return (
    <Suspense fallback={<RouterLoading />}>
      <div className="l-login">{children}</div>
    </Suspense>
  );
};
