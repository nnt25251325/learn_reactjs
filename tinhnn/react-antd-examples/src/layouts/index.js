import React from 'react';
import { useLocation } from 'react-router-dom';

import Default from './default';
import Login from './login';

import './style.scss';

export default ({ children }) => {
  const location = useLocation();

  console.log('location', location);

  if (
    location.pathname === '/login' ||
    location.pathname === '/forgot-password'
  ) {
    return <Login>{children}</Login>;
  }

  return <Default>{children}</Default>;
};
