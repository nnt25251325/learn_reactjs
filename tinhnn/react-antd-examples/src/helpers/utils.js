import { PAGE_SIZE, DEFAULT_SORT } from '../constants';

/**
 * Build and encode URI query for api
 * View: https://sequelize.org/v3/docs/querying/
 *
 * Example:
 * {
 *   offset: 0,
 *   limit: 10,
 *   order: [
 *     ['name', 'ASC']
 *   ],
 *   where: {
 *     id: 123,
 *     name: {
 *       $like: '%abc%'
 *     },
 *     $or: [ // search on multi attribute
 *       {
 *         name: {
 *           $like: '%tony%'
 *         }
 *       },
 *       {
 *         email: {
 *           $like: '%tony%'
 *         }
 *       }
 *     ],
 *     $and: [ // exclude record
 *       {
 *         id: { $ne: 63 }
 *       },
 *       {
 *         id: { $ne: 150 }
 *       }
 *     ]
 *   }
 * }
 *
 */

export const buildQuery = (params = {}) => {
  const query = {
    offset: 0, // Same page = 1
    limit: PAGE_SIZE // If limit is null => Don't send limit => Get all
  };

  // Add offset
  if (/^[0-9]*$/.test(params.page)) {
    query.offset = (params.page - 1) * (params.limit || query.limit);
  }

  // Add limit
  if (/^[0-9]*$/.test(params.limit)) {
    query.limit = params.limit;
  }

  // Add where
  if (
    params.where !== null &&
    typeof params.where === 'object' &&
    Object.keys(params.where).length
  ) {
    query.where = params.where;
  }

  // Add order (sort)
  if (!params.order) {
    query.order = [DEFAULT_SORT]; // Default is order by updatedAt: DESC
  } else if (Array.isArray(params.order) && params.order.length > 0) {
    query.order = params.order;
  }

  // Add attributes
  if (
    (Array.isArray(params.attributes) && params.attributes.length) ||
    (typeof params.attributes === 'object' &&
      Object.keys(params.attributes).length)
  ) {
    query.attributes = params.attributes;
  }

  // Don't add when value is null
  Object.keys(params).forEach(key =>
    params[key] === null ? delete query[key] : ''
  );

  console.log('query', query);

  return encodeURI(JSON.stringify(query));
};

/**
 * Build query for save to store
 */
export const buildQueryForSaveToStore = payload => {
  const query = {
    page: 1,
    limit: PAGE_SIZE,
    ...payload
  };

  if (!query.order) {
    query.order = [DEFAULT_SORT];
  }

  // Don't add when value is null
  Object.keys(query).forEach(key =>
    query[key] === null ? delete query[key] : ''
  );

  return query;
};

/**
 * Build path name list, for active nav link
 *
 * Example - Before: /parent/child-1/child-2
 * Example - After: ['/parent', '/parent/child-1', '/parent/child-1/child-2']
 */
export const buildPathNameList = pathName => {
  if (!pathName) {
    return [];
  }

  const pathList = [];
  const pathArray = pathName.substr(pathName.indexOf('/') + 1).split('/'); // Example: ['parent', 'child-1', 'child-2']

  pathArray.forEach((item, index) => {
    if (!pathList[index - 1]) {
      pathList.push('/' + item);
      return;
    }

    pathList.push(pathList[index - 1] + '/' + item);
  });

  return pathList;
};

/**
 * Build routes for breadcrumb
 *
 * Example - Before: /parent/child-1/child-2
 * Example - After:
 * [
 *   { path: "/", breadcrumbName: "Home" },
 *   { path: "/parent", breadcrumbName: "Parent" },
 *   { path: "/parent/child-1", breadcrumbName: "Child 1" },
 *   { path: "/parent/child-1/child-2", breadcrumbName: "Child 2" }
 * ]
 */
export const buildRoutesForBreadcrumb = (pathName, breadcrumbConfig) => {
  if (!pathName) {
    return [];
  }

  if (pathName === '/') {
    // Only home page
    return breadcrumbConfig.filter(item => item.path === '/');
  }

  const breadcrumbs = [];
  const pathList = [];
  const pathArray = pathName.split('/'); // Example: ['', 'parent', 'child-1', 'child-2']

  pathArray.forEach((item, index) => {
    if (!pathList[index - 1]) {
      const curentPath = '/' + item;
      pathList.push(curentPath);

      const found = breadcrumbConfig.find(breadcrumb => {
        return breadcrumb.path === curentPath && !breadcrumb.isHideOnBreadcrumb;
      });

      if (found) {
        breadcrumbs.push(found);
      }

      return;
    }

    const separator = pathList[index - 1] === '/' ? '' : '/';
    const curentPath = pathList[index - 1] + separator + item;
    pathList.push(curentPath);

    const found = breadcrumbConfig.find(breadcrumb => {
      return breadcrumb.path === curentPath && !breadcrumb.isHideOnBreadcrumb;
    });

    if (found) {
      breadcrumbs.push(found);
    }
  });

  return breadcrumbs;
};

/**
 * Convert permissions to menus
 */
export const convertPermissionsToMenus = (permissions, breadcrumbConfig) => {
  if (
    !(Array.isArray(permissions) && permissions.length) ||
    !(Array.isArray(breadcrumbConfig) && breadcrumbConfig.length)
  ) {
    return [];
  }

  const menuList = breadcrumbConfig.filter(item => {
    const menuItem = permissions.find(permission => {
      return permission.moduleId === item.moduleId && !item.isHideOnSidebarMenu;
    });

    return menuItem;
  });

  return menuList;
};

/**
 * Debounce function
 */
export const debounce = (func, wait, immediate) => {
  let timeout;

  return function () {
    const context = this;
    const args = arguments;

    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) func.apply(context, args);
  };
};

/**
 * For directory tree
 * Re add expanded keys, because when you click to tree item, this tree item is collapsed or expanded
 *
 * Expect: Always expand item when click it
 */
export const reAddExpandedKeys = (key, expandedKeys, callback) => {
  if (!key || !Array.isArray(expandedKeys) || typeof callback !== 'function') {
    return;
  }

  const count = expandedKeys.reduce((accumulator, current) => {
    return current === key ? accumulator + 1 : accumulator;
  }, 0);

  if (count >= 0) {
    const newExpandedKeys = [...expandedKeys.filter(item => item !== key), key];
    callback(newExpandedKeys);
  }
};

/**
 * Find item and parents on tree
 */
export const findItemAndParentsOnTree = (
  list,
  key,
  filterByAttribute = 'id'
) => {
  let found = null;
  const allParents = [];
  const parentList = [];

  if (found) return;

  const findItem = (list, key) => {
    if (found) return;

    for (let i = 0; i < list.length; i++) {
      const item = list[i];
      const hasChildren = Array.isArray(item.children) && item.children.length;

      if (item[filterByAttribute] === key) {
        found = item;
        break;
      }

      if (hasChildren) {
        allParents.push(item);
        findItem(item.children, key);
      }
    }
  };

  findItem(list, key);

  if (found && Array.isArray(allParents)) {
    const reverseList = [...allParents].reverse();

    // Find parent nested of found item
    reverseList.reduce((accumulator, current) => {
      if (
        Array.isArray(current.children) &&
        current.children.some(child => child[filterByAttribute] === accumulator)
      ) {
        parentList.push(current);
        accumulator = current.key;
      }

      return accumulator;
    }, found.key);
  }

  return {
    item: found,
    parentList: Array.isArray(parentList) ? parentList.reverse() : []
  };
};

/**
 * Search items and parent keys on tree
 *
 * "items" are the found items
 * "parentKeys" is the parent of the found items. One item has one parent
 * "parentKeys" for expand row on tree
 */
export const searchItemsAndParentKeysOnTree = (list, keyword) => {
  let items = [];
  let parentKeyList = [];

  const findItem = (list, keyword, parentKey) => {
    for (let i = 0; i < list.length; i++) {
      const item = list[i];
      const hasChildren = Array.isArray(item.children) && item.children.length;

      if (
        typeof item.title === 'string' &&
        typeof keyword === 'string' &&
        item.title &&
        item.title
          .toString()
          .toLowerCase()
          .indexOf(keyword.toString().toLowerCase()) > -1
      ) {
        items.push(item);
        parentKey && parentKeyList.push(parentKey);
      }

      if (hasChildren) findItem(item.children, keyword, item.key);
    }
  };

  // Run findItem function. Initialize parentKey is null
  findItem(list, keyword, null);

  // Remove duplicate key, remove null
  const parentKeys = parentKeyList.reduce((accumulator, current) => {
    if (!accumulator.some(sub => sub === current)) {
      accumulator.push(current);
    }

    return accumulator;
  }, []);

  return {
    items,
    parentKeys
  };
};
