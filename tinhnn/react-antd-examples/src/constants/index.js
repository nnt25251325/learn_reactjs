export * from './breadcrumb-config';
export * from './endpoints';

/**
 * List of number record in one page
 */
export const PAGE_SIZES = [10, 20, 50, 100];

/**
 * Number record in one page
 */
export const PAGE_SIZE = PAGE_SIZES[0];

/**
 * Default sort value
 */
export const DEFAULT_SORT = ['updatedAt', 'DESC'];

/**
 * Short date format
 */
export const SHORT_DATE_FORMAT = 'MM/DD/YYYY';
