import React from 'react';
import { HomeOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';

export const BREADCRUMB_ROUTES_CONFIG = [
  {
    path: '/',
    breadcrumbName: 'Home',
    icon: <HomeOutlined />
  },
  {
    path: '/drag-and-drop',
    breadcrumbName: 'Drag and drop',
    icon: <SettingOutlined />,
    moduleId: 2,
    isHideOnBreadcrumb: true,
    child: [
      {
        path: '/drag-and-drop/drag-table',
        breadcrumbName: 'Drag table',
        icon: <SettingOutlined />,
        moduleId: 2
      },
      {
        path: '/drag-and-drop/multi-drag',
        breadcrumbName: 'Multi drag',
        icon: <SettingOutlined />,
        moduleId: 2
      },
      {
        path: '/drag-and-drop/multi-table-drag',
        breadcrumbName: 'Multi table drag',
        icon: <SettingOutlined />,
        moduleId: 2
      }
    ]
  },
  {
    path: '/drag-and-drop/drag-table',
    breadcrumbName: 'Drag table',
    moduleId: 2,
    isHideOnSidebarMenu: true
  },
  {
    path: '/drag-and-drop/multi-drag',
    breadcrumbName: 'Multi drag',
    moduleId: 2,
    isHideOnSidebarMenu: true
  },
  {
    path: '/drag-and-drop/multi-table-drag',
    breadcrumbName: 'Multi table drag',
    moduleId: 2,
    isHideOnSidebarMenu: true
  },
  {
    path: '/editable-rows-table',
    breadcrumbName: 'Editable rows table',
    icon: <SettingOutlined />,
    moduleId: 2
  },
  {
    path: '/filter-on-tree',
    breadcrumbName: 'Filter on tree',
    icon: <SettingOutlined />,
    moduleId: 2
  },
  {
    path: '/resize-panels',
    breadcrumbName: 'Resize panels',
    icon: <SettingOutlined />,
    moduleId: 2
  },
  {
    path: '/transfer',
    breadcrumbName: 'Transfer',
    icon: <SettingOutlined />,
    moduleId: 2
  },
  {
    path: '/user',
    breadcrumbName: 'User',
    icon: <UserOutlined />,
    moduleId: 1
  },
  {
    path: '/forgot-password',
    breadcrumbName: 'Forgot password',
    icon: <UserOutlined />,
    moduleId: 1
  },
  {
    path: '/login',
    breadcrumbName: 'Login',
    icon: <UserOutlined />,
    moduleId: 1
  }
];
