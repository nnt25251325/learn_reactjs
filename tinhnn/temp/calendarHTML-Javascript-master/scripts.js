let today = new Date();
let currentMonth = today.getMonth();
let currentYear = today.getFullYear();
console.log(111, today);
console.log(111, currentMonth);
console.log(111, currentYear);
let selectYear = document.getElementById("year");
let selectMonth = document.getElementById("month");

let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

let monthAndYear = document.getElementById("monthAndYear");
showCalendar(currentMonth, currentYear);


function next() {
	currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
	currentMonth = (currentMonth + 1) % 12;
	showCalendar(currentMonth, currentYear);
}

function previous() {
	currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
	currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
	showCalendar(currentMonth, currentYear);
}

function onToday() {
	currentMonth = (new Date()).getMonth();
	currentYear = (new Date()).getFullYear();
	console.log(currentMonth, currentYear);

	showCalendar(currentMonth, currentYear);
}

function jump() {
	currentYear = +selectYear.value;
	currentMonth = +selectMonth.value;
	console.log(currentMonth, currentYear);

	showCalendar(currentMonth, currentYear);
}

function showCalendar(month, year) {
	let firstDay = (new Date(year, month)).getDay() - 1;
	let daysInMonth = 32 - new Date(year, month, 32).getDate();
  let daysInPreviousMonth = 32 - new Date(year, month - 1, 32).getDate();

	let tbl = document.getElementById("calendar-body"); // body of the calendar

	// console.log(111111, 'today', today);
	// console.log(2222222222, 'firstDay', firstDay);

	// clearing all previous cells
	tbl.innerHTML = "";

	// filing data about month and in the page via DOM.
	monthAndYear.innerHTML = months[month] + " " + year;
	selectYear.value = year;
	selectMonth.value = month;

	// creating all cells
	let date = 1;
	let nextDate = 1;
  let firstDateInFirstRow = daysInPreviousMonth - firstDay + 1;
  let totalCells = 42;
	for (let i = 0; i < 6; i++) {
		// creates a table row
		let row = document.createElement("tr");

		//creating individual cells, filing them up with data.
		for (let j = 0; j < 7; j++) {
			if (i === 0 && j < firstDay) {
				let cell = document.createElement("td");
				let cellText = document.createTextNode(firstDateInFirstRow);
				cell.appendChild(cellText);
				row.appendChild(cell);
				firstDateInFirstRow++;
			} else if (date > daysInMonth && date <= totalCells) {
				let cell = document.createElement("td");
				let cellText = document.createTextNode(nextDate);
				cell.appendChild(cellText);
				row.appendChild(cell);
				nextDate++;
			} else if (date > totalCells) {
				break;
			} else {
				let cell = document.createElement("td");
				let cellText = document.createTextNode(date);
				if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
					cell.classList.add("today");
				} // color today's date
				cell.appendChild(cellText);
				row.appendChild(cell);
				date++;
			}


		}

		tbl.appendChild(row); // appending each row into calendar body.
	}

}