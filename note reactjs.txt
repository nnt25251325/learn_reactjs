-- Gói cho Sublime Text
ReactJS
JSX
Babel

-- link
http://setting-color-react.herokuapp.com/
https://quan-ly-cong-viec.herokuapp.com/

http://nnt25251325.freevnn.com/learn_reactjs_todo_app/

-- Cài đặt:
npm install -g create-react-app
create-react-app my-app

-- Dùng npx
npm install -g npx
npx create-react-app my-app
cd my-app
npm start

--Cài redux
npm install --save redux react-redux

-- cấu trúc class
import React, { Component } from 'react';

export class ColorPicker extends Component {
	render() {
		return (
			<div></div>
		);
	}
}

-- cấu trúc constructor
constructor(props) {
	super(props);
}

constructor(props) {
	super(props);
	this.onAddToCart = this.onAddToCart.bind(this);
}

constructor(props) {
	super(props);
	this.state = {
		colors: ['red', 'green', 'blue', '#ccc']
	};
}

-- 2 cách viết giống nhau:
function Header() {
	return (
		<div>
			<h1>HeaderComponent sdf</h1>
		</div>
	);
}

class Header extends Component {
	render() {
		return (
			<div>
				<h1>
					Header
				</h1>
			</div>
		);
	}
}

-- Hàm map() - vòng lặp
class App extends Component {
	render() {
		var users = [
			{
				id: 1,
				name: 'Nguyen Van A',
				age: 19
			},
			{
				id: 2,
				name: 'Nguyen Tran B',
				age: 27
			},
			{
				id: 3,
				name: 'Nguyen Chi C',
				age: 36
			}
		];
		var elements = users.map((user, index) => {
			return (
				<div key={user.id}>
					<h1>{user.id}</h1>
					<h2>{ user.name }</h2>
					<p>{ user.age }</p>
				</div>
			);
		});
		return (
			<div>
				{ elements }
			</div>
		);
	}
}

-- Function trong component
class App extends Component {
	showInfoProduct(product) {
		if (product.status) {
			return (
				<h3>
					{product.id} <br/>
					{product.name} <br/>
					{product.price} <br/>
					{ product.status ? 'active' : 'pending'}
				</h3>
			)
		}
	}
	render() {
		var product = {
			"id": 1,
			"name": "6 plus",
			"price": 15000000,
			"status": true
		};
		return (
			<div>
				{ this.showInfoProduct(product) }
			</div>
		);
	}
}

-- vòng lặp và props
App.js
class App extends Component {
	render() {
		var products = [
			{
				id : 1,
				name: 'name 1',
				price: '15000000',
				image: 'images/img_demo_01.jpg',
				status: true
			},
			{
				id : 2,
				name: 'name 2',
				price: '16000000',
				image: 'images/img_demo_02.jpg',
				status: true
			},
			{
				id : 3,
				name: 'name 3',
				price: '17000000',
				image: 'images/img_demo_03.jpg',
				status: true
			}
		];

		var elements = products.map((product, index) => {
			var result = '';
			if(product.status) {
				result = 	<Product
										key={product.index}
										name={product.name}
										price={product.price}
										image={product.image}>
									</Product>
			}
			return result;
		});
		return (
			<div>
				<Header />
				<div className="container">
					<div className="row">
						<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							{elements}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

Product.js
class Product extends Component {
	render() {
		return (
			<div>
				<div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div className="thumbnail">
						<img src={ this.props.image } alt={this.props.name} />
						<div className="caption text-center">
							<h3>
								{this.props.name}
							</h3>
							<p>
								{this.props.price}
							</p>
							<p>
								<button type="button" className="btn btn-success">Mua hàng</button>
							</p>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

-- cách cách lấy giá trị của props
class Product extends Component {
	constructor(props) {
		super(props);
		this.onAddToCart = this.onAddToCart.bind(this);
	}

	onAddToCart() {
		alert(this.props.name);
	} // khi sử dụng constructor

	onAddToCart2 = () => {
		alert(this.props.name);
	} // sử dụng arrow function 1

	onAddToCart3(text) {
		alert(text);
	} // sử dụng arrow function 2

	render() {
		return (
			<div>
				<div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<div className="thumbnail">
						<img src={this.props.image} alt={this.props.name}/>
						<div className="caption text-center">
							<h3>{this.props.name}</h3>
							<p>{this.props.price}</p>
							<p className="text-center">
								<button type="button" className="btn btn-success" onClick={ this.onAddToCart }>Mua Hàng</button>
								<button type="button" className="btn btn-success" onClick={ this.onAddToCart2 }>Mua Hàng 2</button>
								<button type="button" className="btn btn-success" onClick={ () => { this.onAddToCart3(this.props.name) } }>Mua Hàng 3</button>
							</p>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

-- refs trong form
class App extends Component {
	constructor(props) {
		super(props);
		this.onAddProduct = this.onAddProduct.bind(this);
	}

	onAddProduct() {
		console.log(this.refs.name.value);
	}

	render() {
		return (
			<div>
				<div className="container">
					<div className="row">
						<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div className="panel panel-info">
								<div className="panel-heading">
									<h3 className="panel-title">Thêm sản phẩm</h3>
								</div>
								<div className="panel-body">
									<div className="form-group">
										<label htmlFor="">Tên sản phẩm</label>
										<input type="text" className="form-control" ref="name"/>
									</div>
									<button type="submit" className="btn btn-primary" onClick={ this.onAddProduct }>Lưu</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

-- refs trong form - NEW
class AutoFocusTextInput extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
  }

  componentDidMount() {
    this.textInput.current.focusTextInput();
  }

  render() {
    return (
      <CustomTextInput ref={this.textInput} />
    );
  }
}

-- state
import React, { Component } from 'react';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			products: [
				{
					id : 1,
					name: 'name 1',
					price: '15000000',
					image: 'images/img_demo_01.jpg',
					status: true
				},
				{
					id : 2,
					name: 'name 2',
					price: '16000000',
					image: 'images/img_demo_02.jpg',
					status: true
				},
				{
					id : 3,
					name: 'name 3',
					price: '17000000',
					image: 'images/img_demo_03.jpg',
					status: true
				}
			],
			isActive: true
		};
		this.onSetState = this.onSetState.bind(this);
	}

	onSetState() {
		this.setState({
			isActive: !this.state.isActive
		});
		// if(this.state.isActive === true) {
		// 	this.setState({
		// 		isActive: false
		// 	});
		// } else {
		// 	this.setState({
		// 		isActive: true
		// 	});
		// }
	}

	render() {
		var elements = this.state.products.map((product, index) => {
			var result = '';
			if(product.status) {
				result = 	<tr key={index}>
										<td>{index}</td>
										<td>{product.name}</td>
										<td>
											<span className="label label-info">{ product.price }</span>
										</td>
									</tr>
			}
			return result;
		});
		return (
			<div>
				<div className="container">
					<div className="row">
						<table className="table table-hover">
							<thead>
								<tr>
									<th>STT</th>
									<th>Tên sản phẩm</th>
									<th>Giá</th>
								</tr>
							</thead>
							<tbody>
								{elements}
							</tbody>
						</table>
						{/*<p>{this.state.products[1].name}</p>*/}
						<button type="button" className="btn btn-primary" onClick={this.onSetState}>
							Active: {this.state.isActive === true ? 'true' : 'false'}
						</button>
					</div>
				</div>
			</div>
		);
	}
}

export default App;

-- style và map
class ColorPicker extends Component {
	constructor(props) {
		super(props);
		this.state = {
			colors: ['red', 'green', 'blue', '#ccc']
		};
	}

	render() {
		var elmColors = this.state.colors.map((color, index) => {
			return <span key={index} style={{backgroundColor: (color)}} ></span>
		});

		return (
			<div>
				{elmColors}
			</div>
		);
	}
}

-- Sublime Emmet JSX Reactjs - How to properly get a TAB trigger working with Emmet inside of JSX
(Preferences -> Key Bindings - User)
[
	{
		"keys": ["tab"],
		"command": "expand_abbreviation_by_tab",

		// put comma-separated syntax selectors for which
		// you want to expandEmmet abbreviations into "operand" key
		// instead of SCOPE_SELECTOR.
		// Examples: source.js, text.html - source
		"context": [
			{
				"operand": "meta.group.braces.round.js, text.html",
				"operator": "equal",
				"match_all": true,
				"key": "selector"
			},

			// run only if there's no selected text
			{
				"match_all": true,
				"key": "selection_empty"
			},

			// don't work if there are active tabstops
			{
				"operator": "equal",
				"operand": false,
				"match_all": true,
				"key": "has_next_field"
			},

			// don't work if completion popup is visible and you
			// want to insert completion with Tab. If you want to
			// expand Emmet with Tab even if popup is visible --
			// remove this section
			{
				"operand": false,
				"operator": "equal",
				"match_all": true,
				"key": "auto_complete_visible"
			},
			{
				"match_all": true,
				"key": "is_abbreviation"
			}
		]
	}
]

-- form
import React, { Component } from 'react';
import './App.css';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			txtUsername: 'abc',
			txtPassword: '123',
			txtDecs: 'student',
			sltGender: 1,
			rdLang: 'en',
			chkbStatus: false
		};
		this.onHandleChange = this.onHandleChange.bind(this);
		this.onHandleSubmit = this.onHandleSubmit.bind(this);
	}

	onHandleChange(event) {
		var target = event.target;
		var name = target.name;
		var value = target.type === 'checkbox' ? target.checked : target.value;
		this.setState({
			[name]: value
		});
	}
	onHandleSubmit(event) {
		event.preventDefault();
		console.log(this.state);
	}
	render() {
		return (
			<div>
				<div className="container mt-30">
					<div className="row">
						<div className="col-xs-6 col-xs-push-3">
							<div className="panel panel-primary">
								<div className="panel-heading">
									<h3 className="panel-title">Form</h3>
								</div>
								<div className="panel-body">
									<form onSubmit={this.onHandleSubmit}>
										<div className="form-group">
											<label>Username</label>
											<input type="text" name="txtUsername" value={this.state.txtUsername} onChange={this.onHandleChange} className="form-control"/>
										</div>
										<div className="form-group">
											<label>Password</label>
											<input type="password" name="txtPassword" value={this.state.txtPassword} onChange={this.onHandleChange} className="form-control"/>
										</div>
										<div className="form-group">
											<label>Decscription</label>
											<textarea name="txtDecs" onChange={this.onHandleChange} value={this.state.txtDecs} className="form-control"></textarea>
										</div>
										<div className="form-group">
											<label>Gender</label>
											<select name="sltGender" value={this.state.sltGender} onChange={this.onHandleChange} className="form-control">
												<option value={0}>Female</option>
												<option value={1}>Male</option>
												<option value={2}>Other</option>
											</select>
										</div>
										<div className="form-group">
											<label>Language</label>
											<div className="radio">
												<label>
													<input type="radio" name="rdLang" value="en" onChange={this.onHandleChange} checked={this.state.rdLang === "en"}/>
													English
												</label>
												&nbsp;
												<label>
													<input type="radio" name="rdLang" value="vi" onChange={this.onHandleChange} checked={this.state.rdLang === "vi"}/>
													Vietnamese
												</label>
											</div>
										</div>
										<div className="form-group">
											<div className="checkbox">
												<label>
													<input type="checkbox" name="chkbStatus" value={true} onChange={this.onHandleChange} checked={this.state.chkbStatus === true}/>
													Status
												</label>
											</div>
										</div>
										<button type="submit" className="btn btn-primary">Save</button>
										<button type="reset" className="btn btn-default">Reset</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default App;

-- tạo ID ngẫu nhiên
var tasks = [
	{
		id: genarateID(),
		name: 'Hoc lap trinh',
		status: true
	},
	{
		id: genarateID(),
		name: 'Di boi',
		status: false
	}
];

s4() {
	return Math.floor((1+Math.random()) * 0x10000).toString(16).substring(1);
}

genarateID() {
	return this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4();
}

-- Lưu trữ bằng JSON
var tasks = [
	{
		id: '1',
		name: 'Hoc lap trinh',
		status: true
	},
	{
		id: '2',
		name: 'Di boi',
		status: false
	}
];
JSON.stringify(tasks)

-- lưu mảng vào localStorage ở máy local
constructor(props) {
	super(props);
	this.state = {
		tasks: [], // id: unique, name, status
		isDisplayForm: true
	};
}

componentWillMount() {
	if(localStorage && localStorage.getItem('tasks')) {
		var tasks_arr = JSON.parse( localStorage.getItem('tasks') );
		this.setState({
			tasks: tasks_arr
		});
	}
}

var tasksArr = [
	{
		id: 1,
		name: 'Hoc lap trinh',
		status: true
	},
	{
		id: 2,
		name: 'Di boi',
		status: false
	},
	{
		id: 3,
		name: 'Di ngu',
		status: true
	}
];

localStorage.setItem('tasks', JSON.stringify(tasksArr));

-- findIndex
findIndex = (id) => {
	var {tasks} = this.state;
	var result = -1;
	tasks.forEach((task, index) => {
		if(task.id === id) {
			result = index;
		}
	});
	return result;
}

var index = this.findIndex(id);

-- lodash
inport all
import _ from 'lodash';
var index = _.findIndex(tasks, (task) => {
	return task.id === id;
});

import not all
import {findIndex, filter} from 'lodash';
var index = findIndex(tasks, (task) => {
	return task.id === id;
});
filter(tasks, (task) => {
	return task.name.toLowerCase().indexOf(filterName) !== -1
});


-- filter - lodash
tasks = _.filter(tasks, (task) => {
	return task.name.toLowerCase().indexOf(filterName) !== -1
});

-- filter
// Lọc Dữ Liệu Trên Table
if(filterName) { // Lọc theo tên
	tasks = tasks.filter((task) => {
		return task.name.toLowerCase().indexOf(filterName) !== -1
	});
}

tasks = tasks.filter((task) => { // Lọc theo trạng thái
	if(filterStatus === -1) { // -1 tương ứng Tất cả
		return tasks;
	} else { // 0 hoặc 1 tương ứng với Ẩn hoặc Kích hoạt
		return task.status === (filterStatus === 1 ? true : false);
	}
});

// Tìm kiếm
if(keywork) {
	tasks = tasks.filter((task) => {
		return task.name.toLowerCase().indexOf(keywork) !== -1
	});
}

// Sắp xếp
if(sortBy === 'name') {
	tasks.sort((a, b) => {
		if(a.name > b.name) return sortValue;
		else if(a.name < b.name) return -sortValue;
		else return 0;
	});
} else {
	tasks.sort((a, b) => {
		if(a.status > b.status) return -sortValue;
		else if(a.status < b.status) return sortValue;
		else return 0;
	});
}

--
var active = "active";
<li className={active}></li>
<li className={"my-li " + active}></li>
<li className={`my-li ${active}`}></li>

-- react router lấy nhiều tham số
/*
/tin-tuc/vo-hlv-park-hang-seo-xem-tuyen-viet-nam-tap-luyen-3844810.html
path: '/tin-tuc/:slug.:id.html/edit'
*/

--Lấy tên file, xử lý file trong form
event.target.files[0].name;

--
//Toán tử 3 chấm:
var b1 = {
	num: [16, 17, 18],
	status: true
}

//=====> Ghi đè
var b2 = {...b1};
b2.status = false
console.log(b2); //{num: [16, 17, 18], status: false}

//cách viết tắt của ghi đè:
var b2 = {
	...b1,
	status: false
}
console.log(b2); //{num: [16, 17, 18], status: false}

//ghi đè nhiều phần tử
var b3 = {
	...b1,
	num: [26, 27, 28],
	status: false
}
console.log(b3); // {num: [26, 27, 28], status: false}

//=====> Thêm mới
var b4 = {...b1};
b4.num[3] = 100;
console.log(b4); // {num: [16, 17, 18, 100], status: true}

//cách viết tắt của thêm mới:
var b4 = {
	...b1,
	num: [...b1.num, 100]
};
console.log(b4); // {num: [16, 17, 18, 100], status: true}

//=====> Xóa
//xóa đi phần tử thứ 2 của mảng num
var b5 = {
	...b1,
	num: b1.num.filter((value, index) => index != 1 )
};
console.log(b5); // {num: [16,, 18], status: true}

--
// Hỗ trợ cho input = text
onFocus={e => e.target.setSelectionRange(0, e.target.value.length)}
